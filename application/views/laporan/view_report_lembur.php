<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">    
    	
		<div class="panel panel-default" style="margin-top:30px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Laporan Lembur</b></h3>	            
			</div>			
				<div class="panel-body" style="padding:10px;">					
			<div class="panel-body" style="padding:10px;">
				<form class="form-horizontal" method="post" role="form" id="formReport" name="formReport" action="<?php echo base_url('index.php/report/report_jo_area/'); ?>">
	        		<div class="form-group">
						<label for="area" class="control-label col-xs-8 col-md-3">Pilih Client</label>
							<div class="col-xs-2">
	                            <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode" data-provide="typeahead" disabled="disabled"/>
	                        </div>
			            	<div class="col-xs-4">
	                            <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="pilih client" data-provide="typeahead" />
	                        	<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>                                		            		
			            	</div>
			            	<div class="col-xs-2">
								<div class="input-group">
								  <span class="input-group-addon">
									  <input type="checkbox" id="" name=""/>
								  </span>
									  <input type="text" class="form-control" value="All" disabled="disabled" />
								</div>
							</div>
					</div>        		
	        		<div class="form-group">
						<label for="range_date_report_1" class="control-label col-xs-8 col-md-3">Tanggal </label>
						<div class="col-md-3 col-xs-6">
							<div class='input-group date' id='report_1_from_date' name='report_1_from_date'>
								<input type='text' class="form-control" name="from_date" id="from_date" placeholder="DARI" />
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<div class='input-group date' id='report_1_to_date' name='report_1_to_date'>
								<input type='text' class="form-control" name="to_date" id="to_date" placeholder="SAMPAI" />
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
			 		</div>
			 		<div class="form-group">
							<label for="atensi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="submit"><i class="fa fa-table"></i>Submit</button>
									
								</div>
							</div>
					</div>			 		
					</form>
					<hr  />					

					<div class="table-responsive">
						<div class="panel-heading clearfix">
							<div class="col-xs-2">
								<h3 class="panel-title pull-left"><b>List Karyawan</b></h3>	
							</div>
							<div class="col-xs-5">
								<div class="input-group">
	                            	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
	                            <input type="text" name="personal_name" id="search_personal" class="form-control" placeholder="Cari karyawan berdasarkan NRK dan Nama" />
	                          </div>
	                        </div>
							<div class="col-xs-2 pull-right">
								<div class="btn-group">
									<button class="btn btn-primary">
									<!-- <i class="fa fa-user-plus fa-fw"></i>  -->
									Export
									</button>
									
								</div>
							</div>
						</div>
						<br />

						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_Atensi">
								<thead>
								<tr class="info"><th>No</th><th>NRK</th><th>Nama Karyawan</th><th>Nama Perusahaan</th><th>Nama Client</th><th>Lokasi</th><th>Jabatan</th><th>Alamat</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>							
					</div>		
		</div>
</div>
</div>
</div>
<script>
$(function()
{
    $('#report_1_from_date').datetimepicker({
				format : "DD-MM-YYYY",
				useCurrent : true
	});
    $('#report_1_to_date').datetimepicker({
				format : "DD-MM-YYYY",
				useCurrent : true
	});
	
	$("#export").click(function(){
		if($("#formReport [id=from_date]").val()=="")
		{
			alert("Tanggal dari Harus Diisi");
			return false;
		}

		if($("#formReport [id=to_date]").val()=="")
		{
			alert("Tanggal sampai Harus Diisi");
			return false;
		}
		var status = $("#formReport [id=job_order_status_id]").val();
		var from = $("#formReport [id=from_date]").val();
		var to = $("#formReport [id=to_date]").val();
		window.location.href = "<?php echo base_url("index.php/report/report_jo_all"); ?>/"+status+"/"+from+"/"+to;
	});

	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/report_karyawan/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
		loadUnit();
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	

	
});
</script>
