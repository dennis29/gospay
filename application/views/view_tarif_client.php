<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Tarif Client
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Tarif Client</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formTarifClient" name="formTarifClient" autocomplete="off">
			    		<input type='hidden' class="form-control" name="m_p_tarif_client_id" id="m_p_tarif_client_id" placeholder="id" value="0"/>	
						<input type='hidden' class="form-control" name="m_p_tarif_transaction_id" id="m_p_tarif_transaction_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
                        <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
			    		<div class="form-group">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["m_p_tarif_id"]["required"]; ?>" >Pilih Tarif</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_tarif_id" name="" placeholder="kode tarif" data-provide="typeahead"  disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control " type="text" id="m_p_tarif_name" name="m_p_tarif_name" placeholder="cari nama tarif" data-provide="typeahead" />                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["m_client_id"]["required"]; ?>">Pilih Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="cari nama client" data-provide="typeahead" />            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["ref_category_id"]["required"]; ?>">Pilih Kategori</label>
								<div class="col-xs-2">
                                    <?php echo $category_id;?>                                    
                                </div> 
				            	<div class="col-xs-4" >
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="gp_personal_id" value="0"/>  
                                
				            		<div class="col-xs-14 <?php echo $vtarif_client["gp_personal_id"]["required"]; ?>" id="person" style="display: none;">
										<input class="form-control" type="text" id="gp_personal_name" name="gp_personal_name" placeholder="cari nama nrk / karyawan" data-provide="typeahead" />  
										<input class="form-control" type="hidden" id="nrk"  placeholder="cari nama nrk / karyawan" data-provide="typeahead" />
				            		</div>	
								</div>
						</div>
						<div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3 <?php echo $vtarif_client["ref_tax_type_client_id"]["required"]; ?>">Pilih Jenis Pajak</label>
                                <div class="col-xs-6">
                                    <?php echo $tax_type_client_id;?>                                    
                                </div>                               
                        </div>
                        <hr size='500px'>
			    		<div class="form-group">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["m_p_description_transaction_id"]["required"]; ?>">Pilih Transaksi</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_description_transaction_code" name="m_p_description_transaction_code" placeholder="kode transaksi" data-provide="typeahead" disabled="disabled"/>
									<input type='hidden' class="form-control" name="m_p_description_transaction_id" id="m_p_description_transaction_id" placeholder="id" value="0"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_p_description_transaction_name" name="m_p_description_transaction_name" placeholder="" data-provide="typeahead" />            		
				            	</div>								
						</div>
						<div class="form-group">
		                    <label for="tarif_client" class="control-label ccol-xs-8 col-md-3 <?php echo $vtarif_client["transaction_start_date"]["required"]; ?>">Tanggal TMT</label>
		                    <div class="col-xs-2">
		                        <div class='input-group date'>
		                            <input type='text' class="form-control" id="transaction_start_date" name="transaction_start_date"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		                <div class="form-group">                    
		                    <label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["transaction_end_date"]["required"]; ?>">Tanggal TAT</label>
		                    <div class="col-xs-2">
		                        <div class='input-group date'>
		                            <input type='text' class="form-control" id="transaction_end_date" name="transaction_end_date"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>
		                </div>	
		                <div class="form-group">
		                      <label for="tarif_client" class="control-label col-xs-8 col-md-3">Keterangan</label>
		                      <div class="col-xs-6">
		                          <input class="form-control" type="text" id="note" name="note" placeholder="" data-provide="typeahead"/>
		                                             
		                      </div>                
		                </div>
		                <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3 <?php echo $vtarif_client["ref_magnitude_id"]["required"]; ?>">Besaran</label>
                                <div class="col-xs-6">
                                    <?php echo $magnitude_id;?>                                    
                                </div>                               
                        </div>
                        <div class="form-group" id="div1">
		                      <label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["value"]["required"]; ?>">Nilai</label>
		                      <div class="col-xs-2">
		                          <input class="form-control" type="text" id="value" name="value" placeholder="" data-provide="typeahead"/>                                
		                      </div>
		                </div>
						<div class="form-group" id="div2" style="display: none;">
		                      <label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["faktor1"]["required"]; ?>">Kondisi Faktor</label>
		                      <div class="col-xs-1">
		                          <input class="form-control" type="text" id="faktor1" name="faktor1" placeholder="" data-provide="typeahead"/>
							  </div>
							  <div class="col-xs-1" style='position:relative;right:35px'>
							  <label for="tarif_client" class="control-label col-xs-1 col-md-1">/</label>
							  </div>
							  <div class="col-xs-1" style='position:relative;right:95px'>
								  <input class="form-control" type="text" id="faktor2" name="faktor2" placeholder="" data-provide="typeahead"/>
		                      </div>
		                </div>
		                <div class="form-group" >
                                <label for="tarif_client" class="control-label col-xs-3 <?php echo $vtarif_client["ref_multiplier_id"]["required"]; ?>">Pengali</label>
                                <div class="col-xs-6">
                                    <?php echo $multiplier_id;?>                                    
                                </div>                               
                        </div>	
                        <div class="form-group" id="pengali1">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["range_min"]["required"]; ?>">Range Nilai Pengali</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="range_min" name="range_min" placeholder="nilai minimum" data-provide="typeahead"/>
                                </div>
				            	<div class="col-xs-2">
                                    <input class="form-control" type="text" id="range_max" name="range_max" placeholder="nilai maksimum" data-provide="typeahead" />            		
				            	</div>								
						</div>	
						<div class="form-group" id="pengali2" style="display: none;">
		                      <label for="tarif_client" class="control-label col-xs-8 col-md-3 <?php echo $vtarif_client["others"]["required"]; ?>">Nilai Lain-lain</label>
		                      <div class="col-xs-2">
		                          <input class="form-control" type="text" id="others" name="others" placeholder="nilai lain" value ='0' data-provide="typeahead"/>
		                                             
		                      </div>                
		                </div>	
		                <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3 <?php echo $vtarif_client["ref_type_transaction_id"]["required"]; ?>">Jenis Transaksi</label>
                                <div class="col-xs-6">
                                    <?php echo $type_transaction_id;?>                                    
                                </div>                               
                        </div>
                        <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3">Komponen Gaji</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="salary_componen" id="salary_componen1" value="1" class=""  required /> Ya
									<input type="radio" name="salary_componen" id="salary_componen0" value="0" class=""  /> Tidak
                                </div>
                                <label for="tarif_client" class="control-label col-xs-2">Komponen BPJS</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="bpjs_componen" id="bpjs_componen1" value="1" class="" required/> Ya
									<input type="radio" name="bpjs_componen" id="bpjs_componen0" value="0" class="" /> Tidak
                                </div>
                        </div>
                         <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3">Komponen Ketenagakerjaan</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="astek_componen" id="astek_componen1" value="1" class="" required/> Ya
									<input type="radio" name="astek_componen" id="astek_componen0" value="0" class="" /> Tidak
                                </div>
                                <label for="tarif_client" class="control-label col-xs-2">Komponen THR</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="thr_componen" id="thr_componen1" value="1" class="" required/> Ya
									<input type="radio" name="thr_componen" id="thr_componen0" value="0" class="" /> Tidak
                                </div>
                        </div>
                         <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-3">Komponen Lembur</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen1" value="1" class="" required/> Ya
									<input type="radio" name="overtime_componen" id="overtime_componen0" value="0" class="" /> Tidak
                                </div>
                                <label for="tarif_client" class="control-label col-xs-2">Prorata</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="prorata_componen" id="prorata_componen1" value="1" class="" required/> Ya
									<input type="radio" name="prorata_componen" id="prorata_componen0" value="0" class="" /> Tidak
                                </div>
                        </div>

						<div class="form-group">
							<label for="tarif_client" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_tarif_name" id="search_tarif_client" class="form-control" placeholder="Cari Tarif" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<div style="width:5000px">
						<table class="table info table-bordered" style='font-size:13px' id="tbl_tarif_client" >
								<thead >
								<tr class="info" >
									<th>Nama Tarif</th><th>Nama Client</th><th>Kategori</th><th>Nama Karyawan</th><th>Jenis Pajak</th>
							        <th>Nama Transaksi</th><th>TMT</th><th>TAT</th><th>Besaran</th><th>Nilai</th><th>Nilai Factor</th>
									<th>Pengali</th><th>Minimum Pengali</th><th>Maksimum Pengali</th><th>Nilai Lain</th><th>Jenis Transaksi</th>
									<th>Komponen Gaji</th><th>Komponen Ketenagakerjaan</th><th>Komponen Lembur</th><th>Komponen BPJS</th><th>Komponen tdR</th><th>Prorata</th><th>Action</th>
								</tr>
								</thead>
								<tbody>
								   <!-- data tarif client -->
						        </tbody>
						</table>
						</div>
				   </div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadTarifClient(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var nama = '-'
			var par = new Array();
			par["page"] = page;

			var urlnya = "";

	//		var urlnya = "<?php echo base_url('index.php/tarif_client/getList/?m_client_id='); ?>" + $('#m_client_id').val();

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
		         		
					if (par[v.name] != "")
					{
						urlnya = "<?php echo base_url('index.php/tarif_client/getList1/?m_client_id='); ?>" + $('#m_client_id').val();
						
					}
					else
					{
						urlnya = "<?php echo base_url('index.php/tarif_client/getList/?m_client_id='); ?>" + $('#m_client_id').val();
					}

		
			});

			par = $.extend({}, par);

		

			$.ajax({
						   type: "GET",
						   url: urlnya,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_tarif_client > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_tarif_client > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></th></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){

										if(v.salary_componen == 't') v.salary_componen = 'Ya';
										  else v.salary_componen ='Tidak';
										if(v.bpjs_componen == 't') v.bpjs_componen = 'Ya';
										  else v.bpjs_componen ='Tidak';
										if(v.astek_componen == 't') v.astek_componen = 'Ya';
										  else v.astek_componen ='Tidak';
										if(v.thr_componen == 't') v.thr_componen = 'Ya';
										  else v.thr_componen ='Tidak';
										if(v.overtime_componen == 't') v.overtime_componen = 'Ya';
										  else v.overtime_componen ='Tidak';
										if(v.prorata_componen == 't') v.prorata_componen = 'Ya';
										  else v.prorata_componen ='Tidak';

										if (v.gp_personal_id == null || v.gp_personal_id == 0)
										{
											nama = '-';
										}
										else
										{
											nama = "( "+v.nrk+" )  "+v.personal_name;
										}

										var html = "<tr><th><input type=hidden value='"+v.m_p_tarif_client_id+"'>"+v.m_p_tarif_name+"</th>";
										
											html += "<th>"+v.m_client_name+"</th>";
											html += "<th>"+v.kategori+"</th>";
											html += "<th>"+nama+"</th>";
											html += "<th>"+v.jenis_pajak+"</th>";
											html += "<th>"+v.m_p_description_transaction_name+"</th>";
											html += "<th>"+view_date(v.transaction_start_date,2)+"</th>";
											html += "<th>"+view_date(v.transaction_end_date,2)+"</th>";
											html += "<th>"+v.besaran+"</th>";
											html += "<th>"+v.value+"</th>";
											html += "<th>"+v.factor_value+"</th>";
											html += "<th>"+v.pengali+"</th>";
											html += "<th>"+v.range_min+"</th>";
											html += "<th>"+v.range_max+"</th>";
											html += "<th>"+v.others+"</th>";
											html += "<th>"+v.jenis_transaksi+"</th>";
											html += "<th>"+v.salary_componen+"</th>";
											html += "<th>"+v.bpjs_componen+"</th>";
											html += "<th>"+v.astek_componen+"</th>";
											html += "<th>"+v.thr_componen+"</th>";
											html += "<th>"+v.overtime_componen+"</th>";
											html += "<th>"+v.prorata_componen+"</th>";

											html += "<th><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteTarifClient\" onclick=\"$('#act').val('delete'); $('#m_p_tarif_client_id').val('"+v.m_p_tarif_client_id+","+v.m_p_tarif_transaction_id+"').trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editTarifClient\" onclick=\"$('#act').val('edit'); $('#m_p_tarif_client_id').val('"+v.m_p_tarif_client_id+","+v.m_p_tarif_transaction_id+"').trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</th></tr>";
										$("#tbl_tarif_client").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadTarifClient();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadTarifClient(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadTarifClient(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadTarifClient(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadTarifClient(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_tarif_client_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val().split(',');
		
	
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tarif_client/get/?id='); ?>"+id[0]+"&id1="+id[1],
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);

											if(k=='salary_componen' && v == 't') $( "#salary_componen1" ).prop( "checked", true );
											if(k=='salary_componen' && v == 'f') $( "#salary_componen0" ).prop( "checked", true );	

											if(k=='bpjs_componen' && v == 't') $( "#bpjs_componen1" ).prop( "checked", true );
											if(k=='bpjs_componen' && v == 'f') $( "#bpjs_componen0" ).prop( "checked", true );
					
											if(k=='astek_componen' && v == 't') $( "#astek_componen1" ).prop( "checked", true );
											if(k=='astek_componen' && v == 'f') $( "#astek_componen0" ).prop( "checked", true );

											if(k=='thr_componen' && v == 't') $( "#thr_componen1" ).prop( "checked", true );
											if(k=='thr_componen' && v == 'f') $( "#thr_componen0" ).prop( "checked", true );

											if(k=='overtime_componen' && v == 't') $( "#overtime_componen1" ).prop( "checked", true );
											if(k=='overtime_componen' && v == 'f') $( "#overtime_componen0" ).prop( "checked", true );

											if(k=='prorata_componen' && v == 't') $( "#prorata_componen1" ).prop( "checked", true );
											if(k=='prorata_componen' && v == 'f') $( "#prorata_componen0" ).prop( "checked", true );;

											if(k == 'factor_value' && v != null)
											{
												j=v.split('/');
												$("#faktor1").val(j[0]);
												$("#faktor2").val(j[1]);

											}

											if(k == 'personal_name' && v != null)
											{
												$("#gp_personal_name").val(v);
												//alert(v.nrk);
											}

											if (k=='transaction_start_date') 
											{	
												date=v.split('-');
												$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
												
											}
											else if (k=='transaction_end_date') 
											{	
												date=v.split('-');
												$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
											}	
											else $("#"+k).val(v);
										
											

									});
									//var gn=$("#gp_personal_name").val();
									$("#gp_personal_name").val("( "+$("#nrk").val()+" )  "+$("#gp_personal_name").val());
									$('#ref_category_id').trigger('change');
									$('#ref_magnitude_id').trigger('change');
									$('#ref_multiplier_id').trigger('change');
									$("#m_p_tarif_transaction_id").val(id[1]);
									
									
							   }
						   }

						 });
			
			}
			else
			if(act=="delete")
			{
				bootbox.dialog({
                     message: "Anda yakin Tarif Client  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/tarif_client/delete/?id1=')?>"+id[1], id[0])).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadTarifClient();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 			}
	});

	$("#reset").click(function(){
		$("#formTarifClient").trigger("reset");
		$("#m_p_tarif_client_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formTarifClient").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/tarif_client/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadTarifClient();
								$("#formTarifClient").trigger("reset");
								$("#m_p_tarif_client").val(0);
					
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_tarif_client").keyup(function(){
			loadTarifClient(1, $("#formSearch").serializeArray());
	});




	$('#m_p_tarif_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getTarif/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_tarif_name').change(function() {
		current = $('#m_p_tarif_name').typeahead("getActive");
		$('#m_p_tarif_id').val(current.id);
		$('#m_p_tarif_code').val(current.code);
    });

	$('#m_p_tarif_name').blur(function() {
		if($('#m_p_tarif_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_tarif_id').val(0);
		}
    });	



	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
		loadTarifClient();
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	


	$('#gp_personal_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
//alert($('#m_client_id').val());
            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getPersonal/'); ?>/'+query+"/"+$('#m_client_id').val(), function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#gp_personal_name').change(function() {
		current = $('#gp_personal_name').typeahead("getActive");
		$('#gp_personal_id').val(current.id);
		//$('#m_client_code').val(current.code);
    });

	$('#gp_personal_name').blur(function() {
		if($('#gp_personal_id').val()==0)
		{
			// alert("Pilih ");
			$('#gp_personal_id').val(0);
		}
    });	



	$('#m_p_description_transaction_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getDescriptionTransaction/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_description_transaction_name').change(function() {
		current = $('#m_p_description_transaction_name').typeahead("getActive");
		$('#m_p_description_transaction_id').val(current.id);
		$('#m_p_description_transaction_code').val(current.code);
    });

	$('#m_p_description_transaction_name').blur(function() {
		if($('#m_p_description_transaction_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_description_transaction_id').val(0);
		}
    });	



    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getLokasi/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_location_name').change(function() {
		current = $('#m_location_name').typeahead("getActive");
		$('#m_location_id').val(current.id);		
    });

	$('#m_location_name').blur(function() {
		if($('#m_location_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_location_id').val(0);
		}
    });	


	 var date = new Date();
	 date.setMonth(date.getMonth() + 12, 1);

	$('#transaction_start_date').datetimepicker({
      format : "DD-MM-YYYY",
      defaultDate : "2015/01/01"

	});

	 $('#transaction_end_date').datetimepicker({
	    format : "DD-MM-YYYY"
		,defaultDate:  new Date(date)

	});


	$('#ref_category_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Individu')
        {
         // $('#gp_personal_name').show();
		 $("#person").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  //$('#gp_personal_name').hide();
		  $("#person").attr("style", "display:none");
		 
		}
			
    });

	
	$('#ref_magnitude_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Faktor')
        {
          $("#div1").attr("style", "display:none");
		  $("#div2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#div2").attr("style", "display:none");
		  $("#div1").attr("style", "display:block");
		}
			
    });	
	

	$('#ref_multiplier_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Lain-lain (BPJS)')
        {
          $("#pengali1").attr("style", "display:none");
		  $("#pengali2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#pengali2").attr("style", "display:none");
		  $("#pengali1").attr("style", "display:block");
		}
			
    });	


});


</script>