<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Parameter Overtime
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Parameter Overtime</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="overtime_parameter" name="overtime_parameter" autocomplete="off">
			    		<input type='hidden' class="form-control" name="m_p_overtime_parameter_id" id="m_p_overtime_parameter_id" placeholder="id" value="0"/>	
						<input type='hidden' class="form-control" name="m_p_tarif_id" id="m_p_tarif_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
                        <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
			    		<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Tarif</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_tarif_code" name="" placeholder="kode tarif" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_p_tarif_name" name="" placeholder="cari nama tarif" data-provide="typeahead" />                                		            		
				            	</div>
						</div>
						<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_client_name" name="" placeholder="cari nama client" data-provide="typeahead" />            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Kategori</label>
								<div class="col-xs-2">
                                 	<?php echo $ref_category_id;?>  
                                </div> 
				            	<div class="col-xs-4" >
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="" value="0"/>  
                                
				            		<div class="col-xs-14" id="person" style="display: none;">
										<input class="form-control" type="text" id="gp_personal_name" name="" placeholder="cari nama nrk / karyawan" data-provide="typeahead" />  
										<input class="form-control" type="hidden" id="nrk"  placeholder="cari nama nrk / karyawan" data-provide="typeahead" />
				            		</div>	
								</div>
						</div>
						<div class="form-group">
                                <label for="struktur" class="control-label col-xs-3">Pilih Jenis Pajak</label>
                                <div class="col-xs-2">
                                    <?php echo $ref_taxes_id;?>
                                </div>                               
                        </div>
                        <hr size='500px'>
			    		<div class="form-group">
		                    <label for="overtime" class="control-label ccol-xs-8 col-md-3">Tanggal TMT</label>
		                    <div class="col-xs-2">
		                        <div class='input-group date '>
		                            <input type='text' class="form-control" id="overtime_start" name="overtime_start"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>                    
		                                 
		                    <label for="overtime" class="control-label col-xs-8 col-md-2">Tanggal TAT</label>
		                    <div class="col-xs-2">
		                        <div class='input-group date '>
		                            <input type='text' class="form-control" id="overtime_end" name="overtime_end"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTN 1</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovtn1" name="ovtn1" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTN 2</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovtn2" name="ovtn2" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 1</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth1" name="ovth1" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 2</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth2" name="ovth2" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 3</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth3" name="ovth3" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Max Jam Lembur</label>
								<div class="col-xs-2">
                                    <?php echo $ref_max_ovt_hours_id;?>
                                </div> 
				            	<div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="max_ovt_hours" name="max_ovt_hours" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">Jam</span>
		                         </div>
		                    </div>
						</div>
						<div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Max Nilai Lembur</label>                  
		                   <div class="col-xs-4">
                                    <input class="form-control" type="text" id="max_value_ovts"  placeholder="" data-provide="typeahead" /> 
									<input class="form-control" type="hidden" id="max_value_ovt" name="max_value_ovt" /> 
				           </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Pengali Lembur</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="multiflier_overtime" name="multiflier_overtime" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Jenis Lembur Libur</label>
							<div class="col-xs-2">
                              <?php echo $ref_overtime_type_id;?>       
                            </div>
						</div>
						<hr size='500px'>
						<div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-4">Pembulatan</label>
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Minimum</label>                 
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="minimum_rounding" name="minimum_rounding" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding1_from" name="rounding1_from" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding1_to" name="rounding1_to" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="be_rounded1" name="be_rounded1" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding2_from" name="rounding2_from" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding2_to" name="rounding2_to" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="be_rounded2" name="be_rounded2" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding3_from" name="rounding3_from" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding3_to" name="rounding3_to" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="be_rounded3" name="be_rounded3" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding4_from" name="rounding4_from" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="rounding4_to" name="rounding4_to" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="be_rounded4" name="be_rounded4" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                

						<div class="form-group">
							<label for="tarif" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form" autocomplete="off">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_tarif_name" id="search_overtime_parameter" class="form-control" placeholder="Cari Tarif atau Client" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
					<div class="table-responsive">
						<div style="width:3000px">
							<table class="table table-bordered" style='font-size:13px' id="tbl_overtime_parameter">
									<thead>
									<tr class="info"><th>Tarif</th><th>Client</th><th>Kategori</th><th>Nama Karyawan</th><th>Jenis Pajak</th><th>TMT</th><th>TAT</th><th>Ovtn1</th><th>Ovtn2</th><th>Ovth1</th><th>Ovth2</th><th>Ovth3</th><th>Max Jam Lembur</th><th>Max Nilai Lembur</th><th>Pengali Lembur</th><th>Jenis Lembur Libur</th><th>Min Lembur</th><th>Pembulatan</th><th>Action</th></tr>
									</thead>
									<tbody>
									<!-- data tarif -->
								</tbody>
							</table>						
						</div>
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadOvertime(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var nama = '-'
			var par = new Array();
			par["page"] = page;

			var urlnya = "";

	//		var urlnya = "<?php echo base_url('index.php/overtime/getList/?m_client_id='); ?>" + $('#m_client_id').val();

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
		         		
					if (par[v.name] != "")
					{
						urlnya = "<?php echo base_url('index.php/overtime_parameter/getList1/?m_client_id='); ?>" + $('#m_client_id').val();
						
					}
					else
					{
						urlnya = "<?php echo base_url('index.php/overtime_parameter/getList/?m_client_id='); ?>" + $('#m_client_id').val();
					}

		
			});

			par = $.extend({}, par);

		

			$.ajax({
						   type: "GET",
						   url: urlnya,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_overtime_parameter > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_overtime_parameter > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></th></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){

										
										if (v.gp_personal_id == null || v.gp_personal_id == 0)
										{
											nama = '-';
										}
										else
										{
											nama = "( "+v.nrk+" )  "+v.personal_name;
										}

										var html = "<tr><th><input type=hidden value='"+v.m_p_overtime_parameter_id+"'>"+v.m_p_tarif_name+"</th>";
										
											html += "<th>"+v.m_client_name+"</th>";
											html += "<th>"+v.ref_category_id+"</th>";
											html += "<th>"+nama+"</th>";
											html += "<th>"+v.ref_taxes_id+"</th>";
											html += "<th>"+view_date(v.overtime_start,2)+"</th>";
											html += "<th>"+view_date(v.overtime_end,2)+"</th>";
											html += "<th>"+v.ovtn1+" %</th>";
											html += "<th>"+v.ovtn2+" %</th>";
											html += "<th>"+v.ovth1+" %</th>";
											html += "<th>"+v.ovth2+" %</th>";
											html += "<th>"+v.ovth3+" %</th>";
											html += "<th>( "+v.ref_max_ovt_hours_id+" ) "+v.max_ovt_hours+" Jam</th>";
											html += "<th style = 'text-align: right'>"+accounting.formatMoney(v.max_value_ovt ,"", 0)+"</th>";
											html += "<th>"+v.multiflier_overtime+" %</th>";
											html += "<th>"+v.ref_overtime_type_id+"</th>";
											html += "<th>"+v.minimum_rounding+" Detik</th>";
											html += "<th style = 'text-align: right'>"+v.rounding1_from+" -- "+v.rounding1_to+" => "+v.be_rounded1+" Detik";
											if (v.rounding2_from != null)
											   html += "<br>"+v.rounding2_from+" -- "+v.rounding2_to+" => "+v.be_rounded2+" Detik";
											if (v.rounding3_from != null)
											   html += "<br>"+v.rounding3_from+" -- "+v.rounding3_to+" => "+v.be_rounded3+" Detik";
											if (v.rounding4_from != null)
											   html += "<br>"+v.rounding4_from+" -- "+v.rounding4_to+" => "+v.be_rounded4+" Detik</th>";
											else
											   html += "</th>";
											html += "<th><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteTarifClient\" onclick=\"$('#act').val('delete'); $('#m_p_overtime_parameter_id').val('"+v.m_p_overtime_parameter_id+"').trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editTarifClient\" onclick=\"$('#act').val('edit'); $('#m_p_overtime_parameter_id').val('"+v.m_p_overtime_parameter_id+"').trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</th></tr>";
										$("#tbl_overtime_parameter").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadOvertime();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadOvertime(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadOvertime(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadOvertime(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadOvertime(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_overtime_parameter_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
	
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/overtime_parameter/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										$("#"+k.toLowerCase()).val(v);										
										if(k == 'personal_name' && v != null)
										{
												$("#gp_personal_name").val(v);
												//alert(v.nrk);
										}

										if(k == 'max_value_ovt' && v != null)
										{
												$("#max_value_ovts").val(accounting.formatMoney(v ,"Rp ", 0));
												//alert(v.nrk);
										}
										
										if (k=='overtime_start') 
										{	
											date=v.split('-');
											$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
										//	$('#overtime_start').data("DateTimePicker").defaultDate(new Date(v));
												
										}
										else if (k=='overtime_end') 
										{	
											date=v.split('-');
											$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
										//	$('#overtime_end').data("DateTimePicker").defaultDate(new Date(v));
										}	
										else $("#"+k).val(v);	


									});
									//var gn=$("#gp_personal_name").val();
									$("#gp_personal_name").val("( "+$("#nrk").val()+" )  "+$("#gp_personal_name").val());
									$('#ref_category_id').trigger('change');
									
									
							   }
						   }

						 });
			
			}
			else
			if(act=="delete")
			{
				bootbox.dialog({
                     message: "Anda yakin Parameter Overtime  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/overtime_parameter/delete/');?>",id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadOvertime();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
				// var result = window.confirm('data akan dihapus?');
    // 	        if (result == true) {
				// 	$.when(deleteRow("<?php echo base_url('index.php/cabang/delete/'); ?>", id)).done(function(rs){
				// 		if(rs)
				// 		{
				// 			//alert("Data Dihapus");
				// 			loadCabang();
				// 		}
				// 	});
	   //      	}
 			}
	});

	$("#reset").click(function(){
		$("#overtime_parameter").trigger("reset");
		$("#m_p_overtime_parameter_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#overtime_parameter").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/overtime_parameter/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadOvertime();
								$("#overtime_parameter").trigger("reset");
								$("#m_p_overtime").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_overtime_parameter").keyup(function(){
			loadOvertime(1, $("#formSearch").serializeArray());
	});




	$('#m_p_tarif_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/overtime_parameter/getTarif/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_tarif_name').change(function() {
		current = $('#m_p_tarif_name').typeahead("getActive");
		$('#m_p_tarif_id').val(current.id);
		$('#m_p_tarif_code').val(current.id);
    });

	$('#m_p_tarif_name').blur(function() {
		if($('#m_p_tarif_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_tarif_id').val(0);
		}
    });		



	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/overtime_parameter/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
		loadOvertime();
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	

	$('#gp_personal_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
//alert($('#m_client_id').val());
            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/overtime_parameter/getPersonal/'); ?>/'+query+"/"+$('#m_client_id').val(), function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#gp_personal_name').change(function() {
		current = $('#gp_personal_name').typeahead("getActive");
		$('#gp_personal_id').val(current.id);
		//$('#m_client_code').val(current.code);
    });

	$('#gp_personal_name').blur(function() {
		if($('#gp_personal_id').val()==0)
		{
			// alert("Pilih ");
			$('#gp_personal_id').val(0);
		}
    });
	


	$('#ref_category_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Individu')
        {
         // $('#gp_personal_name').show();
		 $("#person").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  //$('#gp_personal_name').hide();
		  $("#person").attr("style", "display:none");
		 
		}
			
    });

	
	$('#ref_magnitude_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Faktor')
        {
          $("#div1").attr("style", "display:none");
		  $("#div2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#div2").attr("style", "display:none");
		  $("#div1").attr("style", "display:block");
		}
			
    });	
	

	$('#ref_multiplier_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Lain-lain (BPJS)')
        {
          $("#pengali1").attr("style", "display:none");
		  $("#pengali2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#pengali2").attr("style", "display:none");
		  $("#pengali1").attr("style", "display:block");
		}
			
    });	

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    
	$('#overtime_start').datetimepicker({
                format : "DD-MM-YYYY"
				,useCurrent: false
    });

     $(".a-prevent").click(function(e) { e.preventDefault(); });
    
	$('#overtime_end').datetimepicker({
                format : "DD-MM-YYYY"
				,useCurrent: false
    });

	$('#max_value_ovts').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#max_value_ovts").keyup(function () {
       	$("#max_value_ovt").val($("#max_value_ovts").autoNumeric("get"));
    });	

	$("#max_value_ovts").keydown(function() {
       	if($("#max_value_ovts").val()=='0')
			$("#max_value_ovts").val('');
    });



});
</script>