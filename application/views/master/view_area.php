<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Wilayah dan Area
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Wilayah dan Area</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formArea" name="formArea" autocomplete="off">
			    		<input type='hidden' class="form-control" name="m_area_id" id="m_area_id" placeholder="id" value="0"/>
                        <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>	
			    		<div class="form-group">
							<label for="area" class="control-label col-xs-8 col-md-3">Pilih Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="pilih client" data-provide="typeahead" />
                                	<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>                                		            		
				            	</div>								
						</div>
			    		<div class="form-group">
							<label for="area" class="control-label col-xs-8 col-md-3">Pilih Lokasi</label>
							<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="pilih lokasi" data-provide="typeahead" />
                                	<input type='hidden' class="form-control" name="m_location_id" id="m_location_id" placeholder="id" value="0"/>				            		
				            </div>								
						</div>
						<div class="form-group">
							<label for="area" class="control-label col-xs-8 col-md-3">Wilayah</label>
							<div class="col-xs-8 col-md-6">
								<input type='text' class="form-control" name="wil_name" id="wil_name" placeholder="Wilayah" data-provide="typeahead" autocomplete="off"/>
							</div>							
						</div>
						<div class="form-group">
							<label for="area" class="control-label col-xs-8 col-md-3">Area</label>
							<div class="col-xs-8 col-md-6">
								<input type='text' class="form-control" name="area_name" id="area_name" placeholder="Area" data-provide="typeahead" autocomplete="off"/>							    
							</div>							
						</div>
						<div class="form-group">
							<label for="area" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="wil_name" id="search_area" class="form-control" placeholder="Cari Area dan Wilayah" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_area">
								<thead>
								<tr class="info"><th>Client</th><th>Lokasi</th><th>Wilayah</th><th>Area</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data area -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
					<div class="btn-group" role="group" aria-label="...">
						<div class="btn-group" role="group">
							<a class="btn btn-default" href="#" id="first">
								<i class="fa fa-fast-backward fa-fw"></i> 
								<span>First</span>
							</a>
							<a class="btn btn-default" href="#" id="prev" >
								<i class="fa fa-backward fa-fw"></i> 
								<span>Prev</span>
							</a>
							<a class="btn btn-default" href="#" id="next" >
								<i class="fa fa-forward fa-fw"></i> 
								<span>Next</span>
							</a>
							<a class="btn btn-default" href="#" id="last" >
								<i class="fa fa-fast-forward fa-fw"></i> 
								<span>Last</span>
							</a>
							<a class="btn btn-default" href="#" >
								<span id="textpage"></span>
							</a>
						</div>
					</div>
				</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadArea(page,id)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/area/getList/?m_client_id='); ?>"+$('#m_client_id').val(),
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_area > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_area > tbody").html("<tr><td colspan = '5' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_area_id+"'>("+v.m_client_id+") "+v.m_client_name+"</td>";
											html += "<td>"+v.m_location_name+"</td>";
											html += "<td>"+v.wil_name+"</td>";
											html += "<td>"+v.area_name+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteArea\" onclick=\"$('#act').val('delete'); $('#m_area_id').val("+v.m_area_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editArea\" onclick=\"$('#act').val('edit'); $('#m_area_id').val("+v.m_area_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_area").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadArea(1,'');	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadArea(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadArea(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadArea(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadArea(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_area_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/area/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				
				bootbox.dialog({
                     message: "Anda yakin Wilayah dan Area  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/area/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadArea();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 			}
	});

	$("#reset").click(function(){
		$("#formArea").trigger("reset");
		$("#m_area_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formArea").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/area/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadArea($("#m_area_id").val());
								$("#formArea").trigger("reset");
								$("#m_area_id").val(0);
                            
							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_area").keyup(function(){
			loadArea(1, $("#formSearch").serializeArray());
	});


	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/area/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
	//	$("#formArea").trigger("reset");
       loadArea(1,current.id);

    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	

    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/area/getLokasi/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_location_name').change(function() {
		current = $('#m_location_name').typeahead("getActive");
		$('#m_location_id').val(current.id);		
    });

	$('#m_location_name').blur(function() {
		if($('#m_location_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_location_id').val(0);
		}
    });	



});
</script>