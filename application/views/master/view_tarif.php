<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Tarif
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Tarif</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formTarif" name="formTarif" autocomplete="off">
			    		<div class="form-group">
							<label for="tarif" class="control-label col-xs-8 col-md-3">Nama Tarif</label>
							<div class="col-xs-8 col-md-5">
								<input type='text' class="form-control" name="m_p_tarif_name" id="m_p_tarif_name" placeholder="Nama Tarif" data-provide="typeahead" autocomplete="off" />
								<input type='hidden' class="form-control" name="m_p_tarif_id" id="m_p_tarif_id" placeholder="id" value="0"/>
			            		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>							
						</div>			    		
						<div class="form-group">
							<label for="tarif" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-5">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_tarif_name" id="search_tarif" class="form-control" placeholder="Cari Tarif" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_tarif">
								<thead>
								<tr class="info"><th>Kode Tarif</th><th>Nama Tarif</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadTarif(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tarif/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_tarif > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_tarif > tbody").html("<tr><td colspan = '5' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_p_tarif_id+"'>"+v.m_p_tarif_id+"</td>";
											html += "<td>"+v.m_p_tarif_name+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteTarif\" onclick=\"$('#act').val('delete'); $('#m_p_tarif_id').val("+v.m_p_tarif_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editTarif\" onclick=\"$('#act').val('edit'); $('#m_p_tarif_id').val("+v.m_p_tarif_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_tarif").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadTarif();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadTarif(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadTarif(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadTarif(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadTarif(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_tarif_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tarif/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				 
				bootbox.dialog({
                     message: "Anda yakin Nama Tarif  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/tarif/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadTarif();
										}
									});
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 			}
	});

	$("#reset").click(function(){
		$("#formTarif").trigger("reset");
		$("#m_p_tarif_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formTarif").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/tarif/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadTarif();
								$("#formTarif").trigger("reset");
								$("#m_p_tarif_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_tarif").keyup(function(){
			loadTarif(1, $("#formSearch").serializeArray());
	});

});
</script>