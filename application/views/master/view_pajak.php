<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2015, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Struktur Perhitungan Pajak
* 
********************************************************************/  -->
<div class="row">
  <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
      <div id="alert" class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Data berhasil disimpan.
      </div>
      <div class="panel panel-default" style="margin-top:0px;">
      
        <div class="panel-body" style="padding:0px;">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#pajak_reguler">Reguler</a></li>
                <li><a data-toggle="tab" href="#pajak_pesangon">Pesangon</a></li>              
            </ul> 
      
        <div id="form-tab" class="tab-content">       
        
          <div class="tab-pane active" id="pajak_reguler">
            <div class="panel-heading" style="background-color:#FFF">
              <span style="font-size:14px; font-weight:bold">Struktur Perhitungan Pajak (Reguler)</span>
            </div>
                <div class="panel-body" style="padding:10px;">         
              <form class="form-horizontal" role="form" id="formPajakReguler" name="formPajakReguler" autocomplete="off">                
                <div class="form-group">
                  <label for="pajak_reguler" class="control-label col-xs-8 col-md-3">Nomor</label>
                  <div class="col-xs-8 col-md-2">
                          <input type='text' class="form-control" name="m_p_tax_structure_regular_id" id="m_p_tax_structure_regular_id" placeholder="Seq" maxlength="3" readonly="readonly"/>
						  <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
                   </div>
                </div>
                <div class="form-group">
                  <label for="pajak_reguler" class="control-label col-xs-8 col-md-3">Batas Minimum</label>
                  <div class="col-xs-8 col-md-5">
                    <input type='hidden' class="form-control" name="minimum_income" id="minimum_income"  value ='0'/>
					<input class="form-control" type="text" id="minimum_incomes"  value ='0' placeholder="Batas Minimum" data-provide="typeahead" autocomplete="off"/>
                  </div>              
                </div>
                <div class="form-group">
                  <label for="pajak_reguler" class="control-label col-xs-8 col-md-3">Batas Maksimum</label>
                  <div class="col-xs-8 col-md-5">
				    <input type='hidden' class="form-control" name="maximum_income" id="maximum_income"  value ='0'/>
                    <input type='text' class="form-control"  id="maximum_incomes" placeholder="Batas Maksimum" data-provide="typeahead" autocomplete="off" value ='0'/>
                  </div>              
                </div>
                <div class="form-group">
                  <label for="pajak_reguler" class="control-label col-xs-8 col-md-3">Persentase Pajak</label>                  
                   <div class="col-xs-2">
                         <div class="input-group">
                            <input class="form-control" type="text" id="percentage" name="percentage" placeholder="">
                         	<span class="input-group-addon" id="basic-addon1">%</span>
                         </div>
                    </div>
                </div>
                <div class="form-group">                                            
                    <label for="pajak_reguler" class="control-label col-xs-3 ">Tanggal TMT</label>
                    <div class="col-xs-2">
                        <div class='input-group date ' id='tmt'>
                            <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">                                            
                    <label for="pajak_reguler" class="control-label col-xs-3 ">Tanggal TAT</label>
                    <div class="col-xs-2">
                        <div class='input-group date ' id='tat'>
                            <input type='text' class="form-control" id="tat_date" name="tat_date"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <label for="pajak_reguler" class="control-label col-xs-8 col-md-3"></label>
                  <div class="col-xs-8 col-md-5">
                    <div class="btn-group pull-right">
                      <button class="btn btn-primary" id="save">Save</button>
                      <!-- <a class="btn btn-danger" id="reset">Reset</a> -->
                    </div>
                  </div>
                </div>
              </form> 
              <hr  /> 
              <div class="table-responsive">
                <div class="panel-heading" style="background-color:#FFF">
                  <div class="search-form">
                    <div class="row">
                      <form id="formSearchpajak_reguler" role="form" autocomplete="off">
                        <div class="col-xs-6 pull-right">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                            <input type="text" name="m_p_tax_structure_regular_name" id="search_pajak_reguler" class="form-control" placeholder="Cari Batas" />
                          </div>
                        </div>
                      </form> 
                    </div>
                  </div>    
                </div>
				<form>
                <table class="table table-bordered" id="tbl_pajak_reguler">
                    <thead>
                    <tr class="info"><th>No</th><th>Batas Minimum</th><th>Batas Maksimum</th><th>Persentase Pajak</th><th>TMT</th><th>TAT</th><th>Action</th></tr>
                    </thead>
                    <tbody>
                    <!-- data pajak_reguler cabang -->
                    </tbody>
                </table> 
				 <div class="panel-footer center-block">
              <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <a class="btn btn-default" href="#" id="first">
                    <i class="fa fa-fast-backward fa-fw"></i> 
                    <span>First</span>
                  </a>
                  <a class="btn btn-default" href="#" id="prev" >
                    <i class="fa fa-backward fa-fw"></i> 
                    <span>Prev</span>
                  </a>
                  <a class="btn btn-default" href="#" id="next" >
                    <i class="fa fa-forward fa-fw"></i> 
                    <span>Next</span>
                  </a>
                  <a class="btn btn-default" href="#" id="last" >
                    <i class="fa fa-fast-forward fa-fw"></i> 
                    <span>Last</span>
                  </a>
                  <a class="btn btn-default" href="#" >
                    <span id="textpage"></span>
                  </a>
                </div>
              </div>
            </div> 
				</form>
                </div>
            </div>  
          </div>

          <div class="tab-pane" id="pajak_pesangon">
                <div class="panel-heading" style="background-color:#FFF">
                  <span style="font-size:14px; font-weight:bold">Struktur Perhitungan Pajak (Pesangon)</span>
                </div>
            <div class="panel-body" style="padding:10px;">
              <form class="form-horizontal" role="form" id="formPajakPesangon" name="formPajakPesangon">
                <div class="form-group">
                  <label for="pajak_pesangon" class="control-label col-xs-8 col-md-3">Nomor</label>
                  <div class="col-xs-8 col-md-2">
                          <input type='text' class="form-control" name="m_p_tax_structure_severance_id" id="m_p_tax_structure_severance_id" placeholder="Seq" maxlength="3" readonly="readonly"/>
						  <input type='hidden' class="form-control" name="act_pesangon" id="act_pesangon" placeholder="act"/>
                   </div>
                </div>
                <div class="form-group">
                  <label for="pajak_pesangon" class="control-label col-xs-8 col-md-3">Batas Minimum</label>
                  <div class="col-xs-8 col-md-5">
				    <input type='hidden' class="form-control" name="p_minimum_income" id="p_minimum_income"  value ='0'/>
                    <input type='text' class="form-control" id="p_minimum_incomes" placeholder="Batas Minimum" data-provide="typeahead" autocomplete="off"  value ='0'/>
                  </div>              
                </div>
                <div class="form-group">
                  <label for="pajak_pesangon" class="control-label col-xs-8 col-md-3">Batas Maksimum</label>
                  <div class="col-xs-8 col-md-5">
				  <input type='hidden' class="form-control" name="p_maximum_income" id="p_maximum_income"  value ='0'/>
                    <input type='text' class="form-control" id="p_maximum_incomes" placeholder="Batas Maksimum" data-provide="typeahead" autocomplete="off" value ='0'/>
                  </div>              
                </div>
                <div class="form-group">
                  <label for="pajak_pesangon" class="control-label col-xs-8 col-md-3">Persentase Pajak</label>                  
                   <div class="col-xs-2">
                         <div class="input-group">
                            <input class="form-control" type="text" id="p_percentage" name="p_percentage" placeholder="">
                         	<span class="input-group-addon" id="basic-addon1">%</span>
                         </div>
                    </div>
                </div>
                <div class="form-group">                                            
                    <label for="pajak_pesangon" class="control-label col-xs-3 ">Tanggal TMT</label>
                    <div class="col-xs-2">
                        <div class='input-group date ' id='tmt1'>
                            <input type='text' class="form-control" id="p_tmt_date" name="p_tmt_date"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">                                            
                    <label for="pajak_pesangon" class="control-label col-xs-3 ">Tanggal TAT</label>
                    <div class="col-xs-2">
                        <div class='input-group date ' id='tat1'>
                            <input type='text' class="form-control" id="p_tat_date" name="p_tat_date"/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <label for="pajak_pesangon" class="control-label col-xs-8 col-md-3"></label>
                  <div class="col-xs-8 col-md-5">
                    <div class="btn-group pull-right">
                      <button class="btn btn-primary" id="save1">Save</button>
                      <!-- <a class="btn btn-danger" id="reset">Reset</a> -->
                    </div>
                  </div>
                </div>
                
              </form> 
			  
              <hr  />
              <div class="table-responsive">
                <div class="panel-heading" style="background-color:#FFF">
                  <div class="search-form">
                    <div class="row">
                      <form id="formSearchpajak_pesangon" role="form">
                        <div class="col-xs-6 pull-right">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                            <input type="text" name="m_branch_name" id="search_pajak_pesangon" class="form-control" placeholder="Cari Batas" />
                          </div>
                        </div>
                      </form> 
                    </div>
                  </div>    
                </div>
				<form>
                <table class="table table-bordered" id="tbl_pajak_pesangon">
                    <thead>
                    <tr class="info"><th>No</th><th>Batas Minimum</th><th>Batas Maksimum</th><th>Persentase Pajak</th><th>TMT</th><th>TAT</th><th>Action</th></tr>
                    </thead>
                    <tbody>
                    <!-- data pajak_reguler cabang -->
                    </tbody>
                </table> 
				 <div class="panel-footer center-block">
              <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <a class="btn btn-default" href="#" id="first1">
                    <i class="fa fa-fast-backward fa-fw"></i> 
                    <span>First</span>
                  </a>
                  <a class="btn btn-default" href="#" id="prev1" >
                    <i class="fa fa-backward fa-fw"></i> 
                    <span>Prev</span>
                  </a>
                  <a class="btn btn-default" href="#" id="next1" >
                    <i class="fa fa-forward fa-fw"></i> 
                    <span>Next</span>
                  </a>
                  <a class="btn btn-default" href="#" id="last1" >
                    <i class="fa fa-fast-forward fa-fw"></i> 
                    <span>Last</span>
                  </a>
                  <a class="btn btn-default" href="#" >
                    <span id="textpage_pesangon"></span>
                  </a>
                </div>
              </div>
            </div> 
				</form>
                </div>
            </div>  
          </div>
        
        </div>      
      </div> 
          
    </div>
  </div>
</div>


<script>
$(function()
{
  var timeout;

  var curpage = 1;
  var totpage = 1;
  var curpage1 = 1;
  var totpage1 = 1;

    function loadPajakReguler(page)
    {
      if(typeof page === 'undefined')
      {
        page = 1;
        curpage = 1;
        totpage = 1;
      }     
      
      var par = new Array();
      par["page"] = page;

      q = $("#formSearchpajak_reguler").serializeArray();

      $.each(q, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/pajak_reguler/getList/'); ?>",
               data: par, // serializes the form's elements.
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                  $("#tbl_pajak_reguler > tbody").empty();
                  $('#textpage').html("Page "+page+" of "+data.totpage);
                    if(data.totpage == 0)
                    {
                      $("#tbl_pajak_reguler > tbody").html("<tr><td colspan = '6' style='color:red'><center> Data tidak ditemukan </center></td></tr>"); 
                    }
                  curpage = page;
                  totpage = data.totpage;
                  $.each(data.data, function(k,v){
                    var html = "<tr><td><input type=hidden value='"+v.m_p_tax_structure_regular_id+"'>"+v.m_p_tax_structure_regular_id+"</td>";
                      html += "<td style = 'text-align: right'>"+accounting.formatMoney(v.minimum_income,' ',0)+"</td>";
                      html += "<td style = 'text-align: right'>"+accounting.formatMoney(v.maximum_income,' ',0)+"</td>";
                      html += "<td style = 'text-align: right'>"+v.percentage+" % </td>";
					  html += "<td>"+view_date(v.tmt_date,2)+"</td>";
                      html += "<td>"+view_date(v.tat_date,2)+"</td>";
                      html += "<td><div class='btn-group'>";
                      html += "<button class='btn btn-small btn-danger' id=\"deletepajak_reguler\" onclick=\"$('#act').val('delete'); $('#m_p_tax_structure_regular_id').val("+v.m_p_tax_structure_regular_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                      html += "<button class='btn btn-small btn-info' id=\"editpajak_reguler\" onclick=\"$('#act').val('edit'); $('#m_p_tax_structure_regular_id').val("+v.m_p_tax_structure_regular_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                      html += "</div>";
                      html += "</td></tr>";
                    $("#tbl_pajak_reguler").append(html);                        
                  });
                 }
               }
             });                    
    }

	function loadPajakPesangon(page)
    {
      if(typeof page === 'undefined')
      {
        page = 1;
        curpage = 1;
        totpage = 1;
      }     
      
      var par = new Array();
      par["page"] = page;

      q = $("#formSearchpajak_pesangon").serializeArray();

      $.each(q, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/pajak_pesangon/getList/'); ?>",
               data: par, // serializes the form's elements.
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                  $("#tbl_pajak_pesangon > tbody").empty();
                  $('#textpage_pesangon').html("Page "+page+" of "+data.totpage);
                    if(data.totpage == 0)
                    {
                      $("#tbl_pajak_pesangon > tbody").html("<tr><td colspan = '6' style='color:red'><center> Data tidak ditemukan </center></td></tr>"); 
                    }
                  curpage = page;
                  totpage = data.totpage;
                  $.each(data.data, function(k,v){
                    var html = "<tr><td><input type=hidden value='"+v.m_p_tax_structure_severance_id+"'>"+v.m_p_tax_structure_severance_id+"</td>";
                      html += "<td style = 'text-align: right'>"+accounting.formatMoney(v.p_minimum_income,' ',0)+"</td>";
                      html += "<td style = 'text-align: right'>"+accounting.formatMoney(v.p_maximum_income,' ',0)+"</td>";
                      html += "<td style = 'text-align: right'>"+v.p_percentage+" % </td>";
					  html += "<td>"+view_date(v.p_tmt_date,2)+"</td>";
                      html += "<td>"+view_date(v.p_tat_date,2)+"</td>";
                      html += "<td><div class='btn-group'>";
                      html += "<button class='btn btn-small btn-danger' id=\"deletepajak_reguler\" onclick=\"$('#act_pesangon').val('delete'); $('#m_p_tax_structure_severance_id').val("+v.m_p_tax_structure_severance_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                      html += "<button class='btn btn-small btn-info' id=\"editpajak_reguler\" onclick=\"$('#act_pesangon').val('edit'); $('#m_p_tax_structure_severance_id').val("+v.m_p_tax_structure_severance_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                      html += "</div>";
                      html += "</td></tr>";
                    $("#tbl_pajak_pesangon").append(html);                        
                  });
                 }
               }
             });                    
    }

  loadPajakReguler();
  loadPajakPesangon();
  
    $("#m_p_tax_structure_regular_id").change(function(){
      var act = $("#act").val();
      var id = $(this).val();
     
      if(act=="edit")
      {
      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/pajak_reguler/get/'); ?>/"+id,
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {  
				
                  $.each(data.data, function(k,v){
					
					  if (k=='tmt_date') 
							{	
								date=v.split('-');
								$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
							}
					  else if (k=='tat_date') 
							{	
								date=v.split('-');
								$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
							}	
					  else $("#"+k).val(v);  
					  if(k == 'minimum_income') $('#minimum_incomes').val(accounting.formatMoney(v ,"Rp ", 0));
					  if(k == 'maximum_income') $('#maximum_incomes').val(accounting.formatMoney(v ,"Rp ", 0));
					  
					  //if (k=='m_company_name') $("#m_company_name").val(v);
					  
					  
					 // $("#m_client_start_pks_date2").val(v);  
                  });
				
                 }
               }
             });  
			
      }
      else

      if(act=="delete")
      {
        bootbox.dialog({
                     message: "Anda yakin Perhitungan Pajak  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					 $.when(deleteRow("<?php echo base_url('index.php/pajak_reguler/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadPajakReguler();
											$("#m_p_tax_structure_regular_id").val('');
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
      }
  });

     $("#m_p_tax_structure_severance_id").change(function(){
      var act = $("#act_pesangon").val();
      var id = $(this).val();
     
      if(act=="edit")
      {
      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/pajak_pesangon/get/'); ?>/"+id,
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {  
				
                  $.each(data.data, function(k,v){
					
					  if (k=='tmt_date') 
							{	
								date=v.split('-');
								$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
							}
					  else if (k=='tat_date') 
							{	
								date=v.split('-');
								$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
							}	
					  else $("#"+k).val(v);  
					  if(k == 'p_minimum_income') $('#p_minimum_incomes').val(accounting.formatMoney(v ,"Rp ", 0));
					  if(k == 'p_maximum_income') $('#p_maximum_incomes').val(accounting.formatMoney(v ,"Rp ", 0));
					  
					  //if (k=='m_company_name') $("#m_company_name").val(v);
					  
					  
					 // $("#m_client_start_pks_date2").val(v);  
                  });
				
                 }
               }
             });  
			
      }
      else

      if(act=="delete")
      {
        bootbox.dialog({
                     message: "Anda yakin Perhitungan Pajak  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					 $.when(deleteRow("<?php echo base_url('index.php/pajak_pesangon/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadPajakPesangon();
											$("#m_p_tax_structure_severance_id").val('');
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
      }
  });


  $("#next").click(function(){
    var next = curpage+1;
    if(next <= totpage) 
    {   
      loadPajakReguler(next);
    }
  });

  $("#prev").click(function(){
    var prev = curpage-1;
    if(prev >= 1)
    {
      loadPajakReguler(prev);
    }
  });
  
  $("#first").click(function(){
    var next = 1;
      loadPajakReguler(next);
  });

  $("#last").click(function(){
    var next = totpage;
      loadPajakReguler(next);
  });
  
  $("a").click(function(e){
      e.preventDefault();
  }); 

    

  $("#alert").hide();

  $("#save").click(function(){
      $("#formPajakReguler").ajaxForm({
          type: 'POST',
          url: "<?php echo base_url("index.php/pajak_reguler/save"); ?>",
          data: $(this).serialize(),
          success: function(data)
          {
             data = jQuery.parseJSON(data);
             if(data.success) 
             {
              $("#alert").show();
                loadPajakReguler();
                $("#formPajakReguler").trigger("reset");
                $("#m_p_tax_structure_regular_id").val(0);
             }
            else
            {
              alert(data.message);  
            }
          }
      });
  });

  $("#save1").click(function(){
      $("#formPajakPesangon").ajaxForm({
          type: 'POST',
          url: "<?php echo base_url("index.php/pajak_pesangon/save"); ?>",
          data: $(this).serialize(),
          success: function(data)
          {
             data = jQuery.parseJSON(data);
             if(data.success) 
             {
              $("#alert").show();
                loadPajakPesangon();
                $("#formPajakPesangon").trigger("reset");
                $("#m_p_tax_structure_severance_id").val(0);
             }
            else
            {
              alert(data.message);  
            }
          }
      });
  });


  $("#search_pajak_reguler").keyup(function(){
      loadPajakReguler(1, $("#formSearchpajak_reguler").serializeArray());
  });


  $('#m_p_tax_structure_regular_start_pks_date').datetimepicker({

      format : "DD-MM-YYYY"

  });


  $('#m_p_tax_structure_regular_end_pks_date').datetimepicker({
	  
      format : "DD-MM-YYYY"
	  //,defaultDate: 20
	
  });


  $("#next1").click(function(){
    var page = curpage1+1;
   
      pajak_regulerBrand($("#m_p_tax_structure_regular_id").val(),page,"next");
 
  });

  $("#prev1").click(function(){
    var page = curpage1-1;
  
      pajak_regulerBrand($("#m_p_tax_structure_regular_id").val(),page,"prev");
 
  });
  
  $("#first1").click(function(){
    var page = 1;
      pajak_regulerBrand($("#m_p_tax_structure_regular_id").val(),page,"first");
  });

  $("#last1").click(function(){
    var page = totpage1;
      pajak_regulerBrand($("#m_p_tax_structure_regular_id").val(),page,"last");
  });
  
  $("a").click(function(e){
      e.preventDefault();
  }); 


  $(".a-prevent").click(function(e) { e.preventDefault(); });
    
  $('#tmt').datetimepicker({
        format : "DD-MM-YYYY",
        defaultDate : new Date() 
  });  

  $(".a-prevent").click(function(e) { e.preventDefault(); });
    
  $('#tat').datetimepicker({
        format : "DD-MM-YYYY",
        defaultDate : new Date()
  });  

  $(".a-prevent").click(function(e) { e.preventDefault(); });
  
  $('#tmt1').datetimepicker({
        format : "DD-MM-YYYY",
        defaultDate : new Date()
  });  

  $(".a-prevent").click(function(e) { e.preventDefault(); });

  $('#tat1').datetimepicker({
        format : "DD-MM-YYYY",
        defaultDate : new Date()
  });  

    $('#minimum_incomes').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#minimum_incomes").keyup(function () {
       	$("#minimum_income").val($("#minimum_incomes").autoNumeric("get"));
    });	

	$("#minimum_incomes").keydown(function() {
       	if($("#minimum_incomes").val()=='0')
			$("#minimum_incomes").val('');
    });

	$('#maximum_incomes').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#maximum_incomes").keyup(function () {
       	$("#maximum_income").val($("#maximum_incomes").autoNumeric("get"));
    });	

	$("#maximum_incomes").keydown(function() {
       	if($("#maximum_incomes").val()=='0')
			$("#maximum_incomes").val('');
    });

	$('#p_minimum_incomes').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#p_minimum_incomes").keyup(function () {
       	$("#p_minimum_income").val($("#p_minimum_incomes").autoNumeric("get"));
    });	

	$("#p_minimum_incomes").keydown(function() {
       	if($("#p_minimum_incomes").val()=='0')
			$("#p_minimum_incomes").val('');
    });

	$('#p_maximum_incomes').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#p_maximum_incomes").keyup(function () {
       	$("#p_maximum_income").val($("#p_maximum_incomes").autoNumeric("get"));
    });	

	$("#p_maximum_incomes").keydown(function() {
       	if($("#p_maximum_incomes").val()=='0')
			$("#p_maximum_incomes").val('');
    });

}); 
</script>