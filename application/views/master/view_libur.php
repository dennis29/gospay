<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Hari Libur
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Hari Libur</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formLibur" name="formLibur"  autocomplete="off">
					    <div class="form-group">							
							<div class="col-xs-8 col-md-5">
			            		<input type='hidden' class="form-control" name="m_p_day_off_id" id="m_p_day_off_id" placeholder="id" value="0"/>
			            		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div>
			    		<div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">Hari Libur</label>
		                    <div class="col-xs-2">
		                        <div class='input-group date ' id='day_off_date'>
		                            <input type='text' class="form-control" id="day_off" name="day_off"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="client" class="control-label col-xs-8 col-md-3">Keterangan</label>
		                  <div class="col-xs-8 col-md-5">
		                    <input type='text' class="form-control" name="note" id="note" placeholder="Batas Minimum" data-provide="typeahead" autocomplete="off"/>
		                  </div>              
		                </div>
		                <div class="form-group">
                                <label for="libur_client" class="control-label col-xs-3">Jenis Libur</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="type_day_off" id="type_day_off1" value="HARI LIBUR" class=""/> Hari Libur         
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="type_day_off" id="type_day_off0" value="HARI BESAR" class=""/> Hari Besar
                                </div>                           
                        </div>
						<div class="form-group">
							<label for="libur" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-5">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form"  autocomplete="off">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_day_off_name" id="search_libur" class="form-control" placeholder="Cari libur" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_libur">
								<thead>
								<tr class="info"><th>Tahun</th><th>Tanggal Libur</th><th>Keterangan</th><th>Jenis Libur</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data libur -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadLibur(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/libur/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_libur > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_libur > tbody").html("<tr><td colspan = '5' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_p_day_off_id+"'>"+(v.day_off.split('-'))[0]+"</td>";
											html += "<td>"+view_date(v.day_off,2)+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td>"+v.type_day_off+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletelibur\" onclick=\"$('#act').val('delete'); $('#m_p_day_off_id').val("+v.m_p_day_off_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editlibur\" onclick=\"$('#act').val('edit'); $('#m_p_day_off_id').val("+v.m_p_day_off_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_libur").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadLibur();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadLibur(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadLibur(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadLibur(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadLibur(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_day_off_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/libur/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										   
											if(k=='type_day_off' && v == 'HARI LIBUR') $("#type_day_off1").prop( "checked", true );
											if(k=='type_day_off' && v == 'HARI BESAR') $("#type_day_off0").prop( "checked", true );	

											if (k=='day_off') 
											{	
												date=v.split('-');
												$("#"+k).val(date[2]+"-"+date[1]+"-"+date[0]); 
												
											}
											else
											  $("#"+k).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/libur/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadLibur();
							$("#formLibur").trigger("reset");
							$("#m_p_day_off_id").val(0);
						}
					});
	        	}
				}); 
				
 			}
	});

	$("#reset").click(function(){
		$("#formLibur").trigger("reset");
		$("#m_p_day_off_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formLibur").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/libur/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadLibur();
								$("#formLibur").trigger("reset");
								$("#m_p_day_off_id").val(0);
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_libur").keyup(function(){
			loadLibur(1, $("#formSearch").serializeArray());
	});

	
	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#day_off_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });

});
</script>