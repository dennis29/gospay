<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Periode Payrol Client
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Periode Payrol Client</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formPayrollPeriod" name="formPayrollPeriod" autocomplete="off">
					    
						<input type='hidden' class="form-control" name="m_p_payroll_period_id" id="m_p_payroll_period_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
			    		<div class="form-group">
							<label for="client" class="control-label col-xs-8 col-md-3">Pilih Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="cari nama client" data-provide="typeahead" />            		
				            	</div>								
						</div>
						<div class="form-group">
                                <label for="prorata" class="control-label col-xs-3">Pembagian Prorata</label>
                                <div class="col-xs-3">
                                    <?php echo $ref_prorata_id; ?>
                                </div> 
						</div>
						<div class="form-group" id="hari">
							<label class="control-label col-xs-3">&nbsp</label>
						</div>
						<div class="form-group" id="hari_tetap" style="display: none;">
								<label for="fix day" class="control-label col-xs-3">Hari Tetap</label>
								<div class="col-xs-2">
									   <?php echo $ref_fixed_day_id; ?>
								</div>                               
						</div>
									
						<div class="form-group" id="hari_kerja" style="display: none;">
								<label for="jumlah hari kerja" class="control-label col-xs-3">Jumlah Hari Kerja</label>
								<div class="col-xs-2">
										<?php echo $ref_month_salary_id; ?>
								</div>                               
						</div>
                        
                        
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Periode Gaji</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                                <label for="jenis periode" class="control-label col-xs-3">Jenis Periode</label>
                                <div class="col-xs-3">
                                   <?php echo $ref_pay_period_id; ?>
                                </div>                               
                        </div>
                        <div class="form-group" id="gaji1">
                                <label for="Bulan" class="control-label col-xs-3">Bulan</label>
                                <div class="col-xs-3">
                                    <?php echo $ref_pay_type_id; ?>
                                </div>                               
                        </div>
						<div class="form-group" id="gj2">
							<label class="control-label col-xs-8 col-md-3">&nbsp</label>
						</div>
						<div class="form-group" id="gj3">
							<label class="control-label col-xs-8 col-md-3">&nbsp</label>
						</div>
                        <div class="form-group" id="gaji2" style="display: none;">
							<label for="tanggal awal" class="control-label col-xs-8 col-md-3">Tanggal Awal</label>
								<div class="col-xs-1">
                                    <input class="form-control" type="text" id="day_pay_start" name="day_pay_start" placeholder="" data-provide="typeahead" maxlength=2 value='0'/>
                                </div>				            	
						</div>
						<div class="form-group" id="gaji3" style="display: none;">
							<label for="tanggal akhir" class="control-label col-xs-8 col-md-3">Tanggal Akhir</label>
								<div class="col-xs-1">
                                    <input class="form-control" type="text" id="day_pay_end" name="day_pay_end" placeholder="" data-provide="typeahead" maxlength=2 value='0'/>
                                </div>				            	
						</div>
						<span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Periode Lembur</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                                <label for="jenis periode" class="control-label col-xs-3">Jenis Periode</label>
                                <div class="col-xs-3">
                                   <?php echo $ref_ovt_period_id; ?>
                                </div>                               
                        </div>
                        <div class="form-group" id="lembur1">
                                <label for="Bulan" class="control-label col-xs-3">Bulan</label>
                                <div class="col-xs-3">
                                    <?php echo $ref_ovt_type_id; ?>
                                </div>                               
                        </div>
						<div class="form-group" id="lbr2">
							<label class="control-label col-xs-8 col-md-3">&nbsp</label>
						</div>
						<div class="form-group" id="lbr3">
							<label class="control-label col-xs-8 col-md-3">&nbsp</label>
						</div>
                        <div class="form-group"  id="lembur2" style="display: none;">
							<label for="tanggal awal" class="control-label col-xs-8 col-md-3">Tanggal Awal</label>
								<div class="col-xs-1">
                                    <input class="form-control" type="text" id="day_ovt_start" name="day_ovt_start" placeholder="" data-provide="typeahead" maxlength=2 value='0'/>
                                </div>				            	
						</div>
						<div class="form-group" id="lembur3" style="display: none;">
							<label for="tanggal akhir" class="control-label col-xs-8 col-md-3">Tanggal Akhir</label>
								<div class="col-xs-1">
                                    <input class="form-control" type="text" id="day_ovt_end" name="day_ovt_end" placeholder="" data-provide="typeahead" maxlength=2 value='0'/>
                                </div>				            	
						</div>


						<div class="form-group">
							<label for="payroll_period" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form" autocomplete="off">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_payroll_period_name" id="search_payroll_period" class="form-control" placeholder="Cari Client" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
					<div class="table-responsive">
						<div style="width:1500px">
							<table class="table table-bordered" id="tbl_payroll_period" style='font-size:13px'>
									<thead>
									<tr class="info"><th>Client</th><th>Prorata</th><th>Jumlah Hari</th><th>Periode Gaji</th><th>Bulan</th><th>Tanggal Awal</th><th>Tanggal Akhir</th><th>Periode Lembur</th><th>Bulan</th><th>Tanggal Awal</th><th>Tanggal Akhir</th><th>Action</th></tr>
									</thead>
									<tbody>
									<!-- data payroll_period -->
								</tbody>
							</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadPayrollPeriod(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			var day_pay_start,day_pay_end,day_ovt_start,day_ovt_end;
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/payroll_period/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_payroll_period > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_payroll_period > tbody").html("<tr><td colspan = '5' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){

										if ( v.ref_prorata_id == 'Jumlah Hari Tetap')
											jml_hari = v.ref_fixed_day_id;
										if ( v.ref_prorata_id == 'Jumlah Hari Kerja') 
											jml_hari = v.ref_month_salary_id; 
										if ( v.ref_prorata_id == 'Jumlah Hari Kalender') 
											jml_hari = '-';

										if ( v.day_pay_start == 0)
										    day_pay_start = '-';
										else
											day_pay_start = v.day_pay_start;

										if ( v.day_pay_end == 0)
										    day_pay_end = '-';
										else
											day_pay_end = v.day_pay_end;

										if ( v.day_ovt_start == 0)
										    day_ovt_start = '-';
										else
											day_ovt_start = v.day_ovt_start;

										if ( v.day_ovt_end == 0)
										    day_ovt_end = '-';
										else
											day_ovt_end = v.day_ovt_end;

										var html = "<tr><td><input type=hidden value='"+v.m_p_payroll_period_id+"'>"+v.m_client_name+"</td>";						html += "<th>"+v.ref_prorata_id+"</th>";
										    html += "<th style = 'text-align: right'>"+jml_hari+"</th>";
											html += "<th>"+v.ref_pay_period_id+"</th>";
											html += "<th>"+v.ref_pay_type_id+"</th>";
											html += "<th style = 'text-align: center'>"+day_pay_start+"</th>";
											html += "<th style = 'text-align: center'>"+day_pay_end+"</th>";
											html += "<th>"+v.ref_ovt_period_id+"</th>";
											html += "<th>"+v.ref_ovt_type_id+"</th>";
											html += "<th style = 'text-align: center'>"+day_ovt_start+"</th>";
											html += "<th style = 'text-align: center'>"+day_ovt_end+"</th>";
											
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletepayroll_period\" onclick=\"$('#act').val('delete'); $('#m_p_payroll_period_id').val("+v.m_p_payroll_period_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editpayroll_period\" onclick=\"$('#act').val('edit'); $('#m_p_payroll_period_id').val("+v.m_p_payroll_period_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_payroll_period").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadPayrollPeriod();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadPayrollPeriod(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadPayrollPeriod(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadPayrollPeriod(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadPayrollPeriod(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_payroll_period_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/payroll_period/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	

									$.each(data.data, function(k,v){
			
											if( (k=='day_pay_start' && v > '0') || (k=='day_pay_end' && v > '0') ) 
											{
												
												$("#gj2").attr("style", "display:none");
												$("#gj3").attr("style", "display:none");
												$("#gaji2").attr("style", "display:block");
												$("#gaji3").attr("style", "display:block");
											}

											if( (k=='day_ovt_start' && v > '0') || (k=='day_ovt_end' && v > '0') ) 
											{
												
												$("#lbr2").attr("style", "display:none");
												$("#lbr3").attr("style", "display:none");
												$("#lembur2").attr("style", "display:block");
												$("#lembur3").attr("style", "display:block");
											}
									
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.dialog({
                     message: "Anda yakin Periode Payroll Clinet akan dihapus?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/payroll_period/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadPayrollPeriod();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
				
				
 			}
	});

	$("#reset").click(function(){
		$("#formPayrollPeriod").trigger("reset");
		$("#m_p_payroll_period_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formPayrollPeriod").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/payroll_period/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadPayrollPeriod();
								$("#formPayrollPeriod").trigger("reset");
								$("#m_p_payroll_period_id").val(0);
								$("#gaji2").attr("style", "display:none");
								$("#gaji3").attr("style", "display:none");
								$("#gj2").attr("style", "display:block");
								$("#gj3").attr("style", "display:block");
								$("#lembur2").attr("style", "display:none");
								$("#lembur3").attr("style", "display:none");
								$("#lbr2").attr("style", "display:block");
								$("#lbr3").attr("style", "display:block");
									
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_payroll_period").keyup(function(){
			loadPayrollPeriod(1, $("#formSearch").serializeArray());
	});


	$('#ref_prorata_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Jumlah Hari Tetap')
        {
		 $("#hari_tetap").attr("style", "display:block");
		 $("#hari_kerja").attr("style", "display:none");
		 $("#hari").attr("style", "display:none");
        }
		if ( $(this).find("option:selected").text() == 'Jumlah Hari Kerja')
		{
		  $("#hari_tetap").attr("style", "display:none");
		  $("#hari_kerja").attr("style", "display:block"); 
		  $("#hari").attr("style", "display:none");
		}
		if ( $(this).find("option:selected").text() == 'Jumlah Hari Kalender')
		{
		  $("#hari_tetap").attr("style", "display:none");
		  $("#hari_kerja").attr("style", "display:none"); 
		  $("#hari").attr("style", "display:block");
		}
    });


	$('#ref_pay_period_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Periode')
        {
          $("#gaji2").attr("style", "display:block");
		  $("#gaji3").attr("style", "display:block");
		  $("#gj2").attr("style", "display:none");
		  $("#gj3").attr("style", "display:none");
        }
		  else
		{
			  //hide
		  $("#gj2").attr("style", "display:block");
		  $("#gj3").attr("style", "display:block");
		  $("#gaji2").attr("style", "display:none");
		  $("#gaji3").attr("style", "display:none");
		}
			
    });	

	$('#ref_ovt_period_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Periode')
        {
          $("#lembur2").attr("style", "display:block");
		  $("#lembur3").attr("style", "display:block");
		  $("#lbr2").attr("style", "display:none");
		  $("#lbr3").attr("style", "display:none");
        }
		  else
		{
			  //hide
		  $("#lbr2").attr("style", "display:block");
		  $("#lbr3").attr("style", "display:block");
		  $("#lembur2").attr("style", "display:none");
		  $("#lembur3").attr("style", "display:none");
		}
			
    });	

    $('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/payroll_period/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
	
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	


});
</script>