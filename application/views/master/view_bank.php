<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Bank
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Bank</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formBank" name="formBank" autocomplete="off">
			    		<!-- <div class="form-group">
							<label for="bank" class="control-label col-xs-8 col-md-3">Kode</label>
							<div class="col-xs-8 col-md-5">
			            		<input type='text' class="form-control" name="m_bank_code" id="m_bank_code" placeholder="Kode Bank" maxlength="3"/>
			            		<input type='hidden' class="form-control" name="m_bank_id" id="m_bank_id" placeholder="id" value="0"/>
			            		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div> -->
			    		<div class="form-group">
							<label for="bank" class="control-label col-xs-8 col-md-3">Nama Bank</label>
							<div class="col-xs-8 col-md-5">
								<input type='text' class="form-control" name="m_bank_name" id="m_bank_name" placeholder="Nama Bank" data-provide="typeahead" autocomplete="off"/>
								<input type='hidden' class="form-control" name="m_bank_id" id="m_bank_id" placeholder="id" value="0"/>
			            		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>							
						</div>
						<div class="form-group">
							<label for="Bank" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-5">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_bank_name" id="search_bank" class="form-control" placeholder="Cari Bank" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_bank">
								<thead>
								<tr class="info"><th>Kode</th><th>Nama Bank</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Bank -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadBank(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/bank/getList/'); ?>"+'?m_bank_name='+$("#m_bank_name").val(),
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_bank > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_bank > tbody").html("<tr><td colspan = '3' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td>"+v.m_bank_id+"</td>";
											html += "<td>"+v.m_bank_name+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletebank\" onclick=\"$('#act').val('delete'); $('#m_bank_id').val("+v.m_bank_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editbank\" onclick=\"$('#act').val('edit'); $('#m_bank_id').val("+v.m_bank_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_bank").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadBank();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadBank(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadBank(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadBank(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadBank(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_bank_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/bank/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.dialog({
                     message: "Anda yakin Bank  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/bank/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadBank();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 			}
	});

	$("#reset").click(function(){
		$("#formBank").trigger("reset");
		$("#m_bank_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formBank").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/bank/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadBank();
								$("#formBank").trigger("reset");
								$("#m_bank_id").val(0);

							// $("#m_bank_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadBank();
							// $("#formBank").trigger("reset");
							// $("#m_bank_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_bank").keyup(function(){
			loadBank(1, $("#formSearch").serializeArray());
	});
	$("#m_bank_name").change(function(){
	//	id = $("#m_bank_name").val();
		loadBank();
	});


});
</script>