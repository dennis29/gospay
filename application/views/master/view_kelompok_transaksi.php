<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Kelompok Transaksi
* 
********************************************************************/  -->
<div class="row">
  <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
      <div id="alert" class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Sukses!</strong> Data berhasil disimpan.
      </div>
      <div class="panel panel-default" style="margin-top:0px;">
      
        <div class="panel-body" style="padding:0px;">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#master">Master Kolom Transaksi</a></li>
                <li><a data-toggle="tab" href="#kolom">Kolom Transaksi</a></li>              
            </ul> 
      
        <div id="form-tab" class="tab-content">       
        
          <div class="tab-pane active" id="master">
            <div class="panel-heading" style="background-color:#FFF">
              <span style="font-size:14px; font-weight:bold">Master Kolom Transaksi</span>
            </div>
                <div class="panel-body" style="padding:10px;">         
              <form class="form-horizontal" role="form" id="formMasterKolom" name="formMasterKolom" autocomplete="off">                
                 <input type='hidden' class="form-control" name="m_p_thp_column_id" id="m_p_thp_column_id" placeholder="id" value="0"/>
                 <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
                <div class="form-group">
                  <label for="master_kolom" class="control-label col-xs-8 col-md-3">Nama Kolom</label>
                  <div class="col-xs-8 col-md-5">
                    <input type='text' class="form-control" name="column_name" id="column_name" placeholder="Nama Kolom Transaksi" data-provide="typeahead" autocomplete="off"/>
                  </div>              
                </div>
                <div class="form-group">
                                <label for="libur_client" class="control-label col-xs-3">Jenis Label</label>
                                <div class="col-xs-2">
                                    <input type="radio" name="ref_label_type_id" id="ref_label_type_id1" value="1" class=""/> Pendapatan                                   
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="ref_label_type_id" id="ref_label_type_id2" value="0" class=""/> Pemotongan
                                </div>                           
                </div>                
                <div class="form-group">
                  <label for="master_kolom" class="control-label col-xs-8 col-md-3"></label>
                  <div class="col-xs-8 col-md-5">
                    <div class="btn-group pull-right">
                      <button class="btn btn-primary" id="save">Save</button>
                      <!-- <a class="btn btn-danger" id="reset">Reset</a> -->
                    </div>
                  </div>
                </div>
              </form> 
              <hr  /> 
              <div class="table-responsive">
                <div class="panel-heading" style="background-color:#FFF">
                  <div class="search-form">
                    <div class="row">
                      <form id="formSearchMasterKolom" role="form" autocomplete="off">
                        <div class="col-xs-6 pull-right">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                            <input type="text" name="column_name" id="search_master_kolom" class="form-control" placeholder="Cari Batas" />
                          </div>
                        </div>
                      </form> 
                    </div>
                  </div>    
                </div>
				<form>
                <table class="table table-bordered" id="tbl_master_kolom">
                    <thead>
                    <tr class="info"><th>Kode Kolom</th><th>Nama Kolom</th><th>Jenis Label</th><th>Action</th></tr>
                    </thead>
                    <tbody>
                    <!-- data master -->
                    </tbody>
                </table> 
				 <div class="panel-footer center-block">
              <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <a class="btn btn-default" href="#" id="first">
                    <i class="fa fa-fast-backward fa-fw"></i> 
                    <span>First</span>
                  </a>
                  <a class="btn btn-default" href="#" id="prev" >
                    <i class="fa fa-backward fa-fw"></i> 
                    <span>Prev</span>
                  </a>
                  <a class="btn btn-default" href="#" id="next" >
                    <i class="fa fa-forward fa-fw"></i> 
                    <span>Next</span>
                  </a>
                  <a class="btn btn-default" href="#" id="last" >
                    <i class="fa fa-fast-forward fa-fw"></i> 
                    <span>Last</span>
                  </a>
                  <a class="btn btn-default" href="#" >
                    <span id="textpage"></span>
                  </a>
                </div>
              </div>
            </div> 
				</form>
                </div>
            </div>  
          </div>

          <div class="tab-pane" id="kolom">
                <div class="panel-heading" style="background-color:#FFF">
                  <span style="font-size:14px; font-weight:bold">Kolom Transaksi</span>
                </div>
            <div class="panel-body" style="padding:10px;">
              <form class="form-horizontal" role="form" id="formKolomTransaksi" name="formKolomTransaksi" autocomplete="off">
						<input type='hidden' class="form-control" name="m_p_thp_group_id" id="m_p_thp_group_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_p_thp_column_id1" id="m_p_thp_column_id1" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
                        <input type='hidden' class="form-control" name="act1" id="act1" placeholder="act"/>
                <div class="form-group">
					        <label for="tarif_client" class="control-label col-xs-8 col-md-3">Pilih Client</label>
						            <div class="col-xs-2">
                            <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                        </div>
        	            	<div class="col-xs-4">
                            <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Cari nama client" data-provide="typeahead" />            		
        	            	</div>								
				        </div>
        				<div class="form-group">
        					<label for="tarif_client" class="control-label col-xs-8 col-md-3">Nama Kolom</label>
        						<div class="col-xs-6">
                            <input class="form-control" type="text" id="m_p_thp_column_name" name="m_p_thp_column_name" placeholder="Cari Master Kolom Transaksi - Jenis Label" data-provide="typeahead" />            		
        		        </div>								
    				    </div>
        				<div class="form-group">
        					<label for="tarif_client" class="control-label col-xs-8 col-md-3">Pilih Transaksi</label>
        						<div class="col-xs-2">
                              <input class="form-control" type="text" id="m_p_description_transaction_code" name="m_p_description_transaction_code" placeholder="kode transaksi" data-provide="typeahead" disabled="disabled"/>
        							<input type='hidden' class="form-control" name="m_p_description_transaction_id" id="m_p_description_transaction_id" placeholder="id" value="0"/>
                    </div>
                  	<div class="col-xs-4">
                              <input class="form-control" type="text" id="m_p_description_transaction_name" name="m_p_description_transaction_name" placeholder="Cari Transaksi" data-provide="typeahead" />            		
                  	</div>								
        				</div>
                <div class="form-group">
                  <label for="kolom_transaksi" class="control-label col-xs-8 col-md-3"></label>
                  <div class="col-xs-8 col-md-6">
                    <div class="btn-group pull-right">
                      <button class="btn btn-primary" id="save1">Save</button>
                      <!-- <a class="btn btn-danger" id="reset">Reset</a> -->
                    </div>
                  </div>
                </div>
                
              </form> 
			  
              <hr  />
              <div class="table-responsive">
                <div class="panel-heading" style="background-color:#FFF">
                  <div class="search-form">
                    <div class="row">
                      <form id="formSearchKolomTransaksi" role="form" autocomplete="off">
                        <div class="col-xs-6 pull-right">
                          <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                            <input type="text" name="column_name" id="search_kolom_transaksi" class="form-control" placeholder="Cari Batas" />
                          </div>
                        </div>
                      </form> 
                    </div>
                  </div>    
                </div>
				<form>
                <table class="table table-bordered" id="tbl_kolom_transaksi">
                    <thead>
                    <tr class="info"><th>Kode</th><th>Nama Client</th><th>Nama Kolom</th><th>Jenis Label</th><th>(Kode) Nama Transaksi</th><th>Action</th></tr>
                    </thead>
                    <tbody>
                    <!-- data master_kolom cabang -->
                    </tbody>
                </table> 
				 <div class="panel-footer center-block">
              <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                  <a class="btn btn-default" href="#" id="first1">
                    <i class="fa fa-fast-backward fa-fw"></i> 
                    <span>First</span>
                  </a>
                  <a class="btn btn-default" href="#" id="prev1" >
                    <i class="fa fa-backward fa-fw"></i> 
                    <span>Prev</span>
                  </a>
                  <a class="btn btn-default" href="#" id="next1" >
                    <i class="fa fa-forward fa-fw"></i> 
                    <span>Next</span>
                  </a>
                  <a class="btn btn-default" href="#" id="last1" >
                    <i class="fa fa-fast-forward fa-fw"></i> 
                    <span>Last</span>
                  </a>
                  <a class="btn btn-default" href="#" >
                    <span id="textpage1"></span>
                  </a>
                </div>
              </div>
            </div> 
				</form>
                </div>
            </div>  
          </div>
        
        </div>      
      </div> 
          
    </div>
  </div>
</div>


<script>
$(function()
{
  var timeout;

  var curpage = 1;
  var totpage = 1;
  var curpage1 = 1;
  var totpage1 = 1;

    function loadThpColumn(page)
    {
      if(typeof page === 'undefined')
      {
        page = 1;
        curpage = 1;
        totpage = 1;
      }     
      
      var par = new Array();
	  var ref_label_type_id = '';
      par["page"] = page;

      q = $("#formSearchMasterKolom").serializeArray();

      $.each(q, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/kelompok_transaksi/getList/'); ?>",
               data: par, // serializes the form's elements.
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                  $("#tbl_master_kolom > tbody").empty();
                  $('#textpage').html("Page "+page+" of "+data.totpage);
                    if(data.totpage == 0)
                    {
                      $("#tbl_master_kolom > tbody").html("<tr><td colspan = '6' style='color:red'><center> Data tidak ditemukan </center></td></tr>"); 
                    }
                  curpage = page;
                  totpage = data.totpage;
                  $.each(data.data, function(k,v){

					  if(v.ref_label_type_id == 1) ref_label_type_id = 'Pendapatan';
					  if(v.ref_label_type_id == 0) ref_label_type_id = 'Pemotongan';

                    var html = "<tr><td><input type=hidden value='"+v.m_p_thp_column_id+"'>"+v.m_p_thp_column_id+"</td>";
                      html += "<td>"+v.column_name+"</td>";
                      html += "<td>"+ref_label_type_id+"</td>";                      
                      html += "<td><div class='btn-group'>";
                      html += "<button class='btn btn-small btn-danger' id=\"deletemaster_kolom\" onclick=\"$('#act').val('delete'); $('#m_p_thp_column_id').val("+v.m_p_thp_column_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                      html += "<button class='btn btn-small btn-info' id=\"editmaster_kolom\" onclick=\"$('#act').val('edit'); $('#m_p_thp_column_id').val("+v.m_p_thp_column_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                      html += "</div>";
                      html += "</td></tr>";
                    $("#tbl_master_kolom").append(html);                        
                  });
                 }
               }
             });                    
    }


	function loadThpGroup(page1)
    {
      if(typeof page1 === 'undefined')
      {
        page1 = 1;
        curpage1 = 1;
        totpage1 = 1;
      }     
      
      var par = new Array();
      par["page1"] = page1;

      q = $("#formSearchKolomTransaksi").serializeArray();

      $.each(q, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
       //        url: "<?php echo base_url('index.php/kelompok_transaksi/getListGroup/'); ?>",

	  url: "<?php echo base_url('index.php/kelompok_transaksi/getListGroup/?m_client_id='); ?>"+ $('#m_client_id').val(),
               data: par, // serializes the form's elements.
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
                  $("#tbl_kolom_transaksi > tbody").empty();
                  $('#textpage1').html("Page "+page1+" of "+data.totpage1);
                    if(data.totpage1 == 0)
                    {
                      $("#tbl_kolom_transaksi > tbody").html("<tr><td colspan = '6' style='color:red'><center> Data tidak ditemukan </center></td></tr>"); 
                    }
                  curpage1 = page1;
                  totpage1 = data.totpage1;
                  $.each(data.data, function(k,v){

					  if(v.ref_label_type_id == 1) ref_label_type_id = 'Pendapatan';
					  if(v.ref_label_type_id == 0) ref_label_type_id = 'Pemotongan';

                    var html = "<tr><td><input type=hidden value='"+v.m_p_thp_group_id+"'>"+v.m_p_thp_group_id+"</td>";
                      html += "<td>"+v.m_client_name+"</td>";
					  html += "<td>"+v.column_name+"</td>";
                      html += "<td>"+ref_label_type_id+"</td>"; 
					  html += "<td>"+v.m_p_description_transaction_name+"</td>"; 
                      html += "<td><div class='btn-group'>";
                      html += "<button class='btn btn-small btn-danger' id=\"deletemaster_kolom\" onclick=\"$('#act1').val('delete'); $('#m_p_thp_group_id').val("+v.m_p_thp_group_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                      html += "<button class='btn btn-small btn-info' id=\"editmaster_kolom\" onclick=\"$('#act1').val('edit'); $('#m_p_thp_group_id').val("+v.m_p_thp_group_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                      html += "</div>";
                      html += "</td></tr>";
                    $("#tbl_kolom_transaksi").append(html);                        
                  });
                 }
               }
             });                    
    }

  loadThpColumn(); 
  loadThpGroup();

  $("#next").click(function(){
    var next = curpage+1;
    if(next <= totpage) 
    {   
      loadThpColumn(next);
    }
  });

  $("#prev").click(function(){
    var prev = curpage-1;
    if(prev >= 1)
    {
      loadThpColumn(prev);
    }
  });
  
  $("#first").click(function(){
    var next = 1;
      loadThpColumn(next);
  });

  $("#last").click(function(){
    var next = totpage;
      loadThpColumn(next);
  });
  
  $("a").click(function(e){
      e.preventDefault();
  }); 

    
  
  $("#m_p_thp_column_id").change(function(){
      var act = $("#act").val();
      var id = $(this).val();
      
      if(act=="edit")
      {
      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/kelompok_transaksi/getthpcolumn/'); ?>/"+id,
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {  
				
                  $.each(data.data, function(k,v){
					
					   $("#"+k).val(v);  
					   if(k=='ref_label_type_id' && v == 1) $( "#ref_label_type_id1" ).prop( "checked", true );
					   if(k=='ref_label_type_id' && v == 0) $( "#ref_label_type_id2" ).prop( "checked", true );
					 
					  //if (k=='m_company_name') $("#m_company_name").val(v);
					  
					  
					 // $("#m_p_group_column_start_pks_date2").val(v);  
                  });
				
                 }
               }
             });  
      }
      else

      if(act=="delete")
      {

        bootbox.dialog({
                     message: "Anda yakin Master Kolom Transaksi  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/kelompok_transaksi/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadThpColumn();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
        
      }
  });

  $("#m_p_thp_group_id").change(function(){
	
      var act = $("#act1").val();
      var id = $(this).val();    
  
	  if(act=="edit")
      {
      $.ajax({
               type: "GET",
               url: "<?php echo base_url('index.php/kelompok_transaksi/getthpgroup/'); ?>/"+id,
               success: function(data)
               {
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {  
				
                  $.each(data.data, function(k,v){
					
					 if ( k == "column_name") k = "m_p_thp_column_name";
					 if ( k == "m_p_thp_column_id") k = "m_p_thp_column_id1";
					 
					 $("#"+k).val(v);  
					  
                  });
				
                 }
               }
             });  
		 

      }
      else
      if(act=="delete")
      {

			bootbox.dialog({
                     message: "Anda yakin Kolom Transaksi  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/kelompok_transaksi/delete1/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadThpGroup();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 
      }
  });

  $("#reset").click(function(){
    $("#formMasterKolom").trigger("reset");
    $("#m_p_group_column_id").val(0);
  });

  $("#alert").hide();

  $("#save").click(function(){
      $("#formMasterKolom").ajaxForm({
          type: 'POST',
          url: "<?php echo base_url("index.php/kelompok_transaksi/save"); ?>",
          data: $(this).serialize(),
          success: function(data)
          {
             data = jQuery.parseJSON(data);
             if(data.success) 
             {
              $("#alert").show();
                loadThpColumn();
				loadThpGroup();
                $("#formMasterKolom").trigger("reset");
                $("#m_p_thp_column_id").val(0);
         
             }
            else
            {
              alert(data.message);  
            }
          }
      });
  });


  $("#search_master_kolom").keyup(function(){
      loadThpColumn(1, $("#formSearchMasterKolom").serializeArray());
  });

	$("#search_kolom_transaksi").keyup(function(){
      loadThpGroup(1, $("#formSearchKolomTransaksi").serializeArray());
  });

  $("#next1").click(function(){
    var next = curpage1+1;
    if(next <= totpage1) 
    {   
      loadThpGroup(next);
    }
  });

  $("#prev1").click(function(){
    var prev = curpage1-1;
    if(prev >= 1)
    {
      loadThpGroup(prev);
    }
  });
  
  $("#first1").click(function(){
    var next = 1;
      loadThpGroup(next);
  });

  $("#last1").click(function(){
    var next = totpage1;
      loadThpGroup(next);
  });
  
  $("a").click(function(e){
      e.preventDefault();
  }); 


  $("#reset").click(function(){
    $("#formKolomTransaksi").trigger("reset");
    $("#m_p_thp_group_id").val(0);
  });

  $("#alert").hide();

  $("#save1").click(function(){
      $("#formKolomTransaksi").ajaxForm({
          type: 'POST',
          url: "<?php echo base_url("index.php/kelompok_transaksi/save1"); ?>",
          data: $(this).serialize(),
          success: function(data)
          {
             data = jQuery.parseJSON(data);
             if(data.success) 
             {
              $("#alert").show();
                loadThpGroup();
                $("#formKolomTransaksi").trigger("reset");
                $("#m_p_thp_group_id").val(0);           
             }
            else
            {
              alert(data.message);  
            }
          }
      });
	  
  });


 

	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
		loadThpGroup();
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	 


	$('#m_p_thp_column_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/kelompok_transaksi/getColumn/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_thp_column_name').change(function() {
		current = $('#m_p_thp_column_name').typeahead("getActive");
		$('#m_p_thp_column_id1').val(current.id);
		
    });

	$('#m_p_thp_column_name').blur(function() {
		if($('#m_p_thp_column_id1').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_thp_column_id1').val(0);
		}
    });	 

	
	$('#m_p_description_transaction_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tarif_client/getDescriptionTransaction/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_description_transaction_name').change(function() {
		current = $('#m_p_description_transaction_name').typeahead("getActive");
		$('#m_p_description_transaction_id').val(current.id);
		$('#m_p_description_transaction_code').val(current.code);
    });

	$('#m_p_description_transaction_name').blur(function() {
		if($('#m_p_description_transaction_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_description_transaction_id').val(0);
		}
    });	

});

</script>