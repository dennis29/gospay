<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Parameter Overtime
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Parameter Overtime</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formTarif" name="formTarif">
			    		<input type='hidden' class="form-control" name="m_p_overtime_id" id="m_p_overtime_id" placeholder="id" value="0"/>	
						<input type='hidden' class="form-control" name="m_p_tarif_transaction_id" id="m_p_tarif_transaction_id" placeholder="id" value="0"/>
						<input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
                        <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
			    		<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Tarif</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_tarif_id" name="m_p_tarif_id" placeholder="kode tarif" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_p_tarif_name" name="m_p_tarif_name" placeholder="cari nama tarif" data-provide="typeahead" />                                		            		
				            	</div>
						</div>
						<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="cari nama client" data-provide="typeahead" />            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Pilih Kategori</label>
								<div class="col-xs-2">
                                 	<?php echo $ref_type_tarif_id;?>  
                                </div> 
				            	<div class="col-xs-4" >
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="" value="0"/>  
                                
				            		<div class="col-xs-14" id="person" style="display: none;">
										<input class="form-control" type="text" id="gp_personal_name" name="gp_personal_name" placeholder="cari nama nrk / karyawan" data-provide="typeahead" />  
										<input class="form-control" type="hidden" id="nrk"  placeholder="cari nama nrk / karyawan" data-provide="typeahead" />
				            		</div>	
								</div>
						</div>
						<div class="form-group">
                                <label for="struktur" class="control-label col-xs-3">Pilih Jenis Pajak</label>
                                <div class="col-xs-6">
                                    <?php echo $tax_type_id;?>
                                </div>                               
                        </div>
                        <hr size='500px'>
			    		<div class="form-group">
		                    <label for="overtime" class="control-label ccol-xs-8 col-md-3">Tanggal TMT</label>
		                    <div class="col-xs-4">
		                        <div class='input-group date ' id='tmt_date'>
		                            <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>                    
		                </div>
		                <div class="form-group">                    
		                    <label for="overtime" class="control-label col-xs-8 col-md-3">Tanggal TAT</label>
		                    <div class="col-xs-4">
		                        <div class='input-group date ' id='tat_date'>
		                            <input type='text' class="form-control" id="tat_date" name="tat_date"/>
		                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTN 1</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovtn1" name="ovtn1" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTN 2</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovtn2" name="ovtn2" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 1</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth1" name="ovth1" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 2</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth2" name="ovth2" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">OVTH 3</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="ovth3" name="ovth3" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Max Jam Lembur</label>
								<div class="col-xs-2">
                                    <?php echo $ref_entitled_overtime_id;?>
                                </div> 
				            	<div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="" name="" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">Jam</span>
		                         </div>
		                    </div>
						</div>
						<div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Max Nilai Lembur</label>                  
		                   <div class="col-xs-4">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Pengali Lembur</label>                  
		                   <div class="col-xs-2">
		                         <div class="input-group">
		                            <input class="form-control" type="text" id="" name="" placeholder="">
		                         	<span class="input-group-addon" id="basic-addon1">%</span>
		                         </div>
		                    </div>
		                </div>
		                <div class="form-group">
							<label for="overtime" class="control-label col-xs-8 col-md-3">Jenis Lembur Libur</label>
							<div class="col-xs-6">
                              <?php echo $ref_overtime_type_id;?>       
                            </div>
						</div>
						<hr size='500px'>
						<div class="form-group">
		                  <label for="overtime" class="control-label col-xs-8 col-md-3">Pembulatan</label>
		                  <label for="overtime" class="control-label col-xs-8 col-md-4">Minimum</label>                 
		                   <div class="col-xs-2">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-2">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-2">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-2">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                <div class="form-group">
		                	 <label for="overtime" class="control-label col-xs-8 col-md-2"></label>
		                   <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-1">s/d</label>
				           <div class="col-xs-1">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
				           <label for="overtime" class="control-label col-xs-7 col-md-2">Dibulatkan</label>
				           <div class="col-xs-2">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                                		            		
				           </div>
		                </div>
		                

						<div class="form-group">
							<label for="tarif" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_tarif_name" id="search_tarif" class="form-control" placeholder="Cari Tarif atau Client" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_tarif">
								<thead>
								<tr class="info"><th>Tarif</th><th>Client</th><th>Kategori</th><th>Nama Karyawan</th><th>Jenis Pajak</th><th>TMT</th><th>TAT</th><th>Mendapat Lembur</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadOvertime(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var nama = '-'
			var par = new Array();
			par["page"] = page;

			var urlnya = "";

	//		var urlnya = "<?php echo base_url('index.php/overtime/getList/?m_client_id='); ?>" + $('#m_client_id').val();

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
		         		
					if (par[v.name] != "")
					{
						urlnya = "<?php echo base_url('index.php/overtime/getList1/?m_client_id='); ?>" + $('#m_client_id').val();
						
					}
					else
					{
						urlnya = "<?php echo base_url('index.php/overtime/getList/?m_client_id='); ?>" + $('#m_client_id').val();
					}

		
			});

			par = $.extend({}, par);

		

			$.ajax({
						   type: "GET",
						   url: urlnya,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_overtime > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_overtime > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></th></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){

										if(v.salary_componen == 't') v.salary_componen = 'Ya';
										  else v.salary_componen ='Tidak';
										if(v.bpjs_componen == 't') v.bpjs_componen = 'Ya';
										  else v.bpjs_componen ='Tidak';
										if(v.astek_componen == 't') v.astek_componen = 'Ya';
										  else v.astek_componen ='Tidak';
										if(v.thr_componen == 't') v.thr_componen = 'Ya';
										  else v.thr_componen ='Tidak';
										if(v.overtime_componen == 't') v.overtime_componen = 'Ya';
										  else v.overtime_componen ='Tidak';
										if(v.prorata_componen == 't') v.prorata_componen = 'Ya';
										  else v.prorata_componen ='Tidak';

										if (v.gp_personal_id == null || v.gp_personal_id == 0)
										{
											nama = '-';
										}
										else
										{
											nama = "( "+v.nrk+" )  "+v.personal_name;
										}

										var html = "<tr><th><input type=hidden value='"+v.m_p_overtime_id+"'>"+v.m_p_tarif_name+"</th>";
										
											html += "<th>"+v.m_client_name+"</th>";
											html += "<th>"+v.kategori+"</th>";
											html += "<th>"+nama+"</th>";
											html += "<th>"+v.jenis_pajak+"</th>";
											html += "<th>"+v.m_p_description_transaction_name+"</th>";
											html += "<th>"+v.transaction_start_date+"</th>";
											html += "<th>"+v.transaction_end_date+"</th>";
											html += "<th>"+v.besaran+"</th>";
											html += "<th>"+v.value+"</th>";
											html += "<th>"+v.factor_value+"</th>";
											html += "<th>"+v.pengali+"</th>";
											html += "<th>"+v.range_min+"</th>";
											html += "<th>"+v.range_max+"</th>";
											html += "<th>"+v.others+"</th>";
											html += "<th>"+v.jenis_transaksi+"</th>";
											html += "<th>"+v.salary_componen+"</th>";
											html += "<th>"+v.bpjs_componen+"</th>";
											html += "<th>"+v.astek_componen+"</th>";
											html += "<th>"+v.thr_componen+"</th>";
											html += "<th>"+v.overtime_componen+"</th>";
											html += "<th>"+v.prorata_componen+"</th>";

											html += "<th><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteTarifClient\" onclick=\"$('#act').val('delete'); $('#m_p_overtime_id').val('"+v.m_p_overtime_id+","+v.m_p_tarif_transaction_id+"').trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editTarifClient\" onclick=\"$('#act').val('edit'); $('#m_p_overtime_id').val('"+v.m_p_overtime_id+","+v.m_p_tarif_transaction_id+"').trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</th></tr>";
										$("#tbl_overtime").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadOvertime();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadOvertime(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadOvertime(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadOvertime(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadOvertime(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_overtime_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val().split(',');
		
	
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/overtime/get/?id='); ?>"+id[0]+"&id1="+id[1],
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);

											if(k=='salary_componen' && v == 't') $( "#salary_componen1" ).prop( "checked", true );
											if(k=='salary_componen' && v == 'f') $( "#salary_componen0" ).prop( "checked", true );	

											if(k=='bpjs_componen' && v == 't') $( "#bpjs_componen1" ).prop( "checked", true );
											if(k=='bpjs_componen' && v == 'f') $( "#bpjs_componen0" ).prop( "checked", true );
					
											if(k=='astek_componen' && v == 't') $( "#astek_componen1" ).prop( "checked", true );
											if(k=='astek_componen' && v == 'f') $( "#astek_componen0" ).prop( "checked", true );

											if(k=='thr_componen' && v == 't') $( "#thr_componen1" ).prop( "checked", true );
											if(k=='thr_componen' && v == 'f') $( "#thr_componen0" ).prop( "checked", true );

											if(k=='overtime_componen' && v == 't') $( "#overtime_componen1" ).prop( "checked", true );
											if(k=='overtime_componen' && v == 'f') $( "#overtime_componen0" ).prop( "checked", true );

											if(k=='prorata_componen' && v == 't') $( "#prorata_componen1" ).prop( "checked", true );
											if(k=='prorata_componen' && v == 'f') $( "#prorata_componen0" ).prop( "checked", true );;

											if(k == 'factor_value' && v != null)
											{
												j=v.split('/');
												$("#faktor1").val(j[0]);
												$("#faktor2").val(j[1]);

											}
										if(k == 'personal_name' && v != null)
										{
												$("#gp_personal_name").val(v);
												//alert(v.nrk);
										}
										
											

									});
									//var gn=$("#gp_personal_name").val();
									$("#gp_personal_name").val("( "+$("#nrk").val()+" )  "+$("#gp_personal_name").val());
									$('#ref_category_id').trigger('change');
									$('#ref_magnitude_id').trigger('change');
									$('#ref_multiplier_id').trigger('change');
									$("#m_p_tarif_transaction_id").val(id[1]);
									
									
							   }
						   }

						 });
			
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/overtime/delete/?id1=')?>"+id[1], id[0])).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadOvertime();
						}
					});
	        	}
				}); 
				// var result = window.confirm('data akan dihapus?');
    // 	        if (result == true) {
				// 	$.when(deleteRow("<?php echo base_url('index.php/cabang/delete/'); ?>", id)).done(function(rs){
				// 		if(rs)
				// 		{
				// 			//alert("Data Dihapus");
				// 			loadCabang();
				// 		}
				// 	});
	   //      	}
 			}
	});

	$("#reset").click(function(){
		$("#formTarifClient").trigger("reset");
		$("#m_p_overtime_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formTarifClient").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/overtime/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadOvertime();
								$("#formTarifClient").trigger("reset");
								$("#m_p_overtime").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_overtime").keyup(function(){
			loadOvertime(1, $("#formSearch").serializeArray());
	});




	$('#m_p_tarif_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/overtime/getTarif/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_tarif_name').change(function() {
		current = $('#m_p_tarif_name').typeahead("getActive");
		$('#m_p_tarif_id').val(current.id);
		$('#m_p_tarif_code').val(current.code);
    });

	$('#m_p_tarif_name').blur(function() {
		if($('#m_p_tarif_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_tarif_id').val(0);
		}
    });	



	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/overtime/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
		loadOvertime();
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	


	


	$('#ref_category_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Individu')
        {
         // $('#gp_personal_name').show();
		 $("#person").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  //$('#gp_personal_name').hide();
		  $("#person").attr("style", "display:none");
		 
		}
			
    });

	
	$('#ref_magnitude_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Faktor')
        {
          $("#div1").attr("style", "display:none");
		  $("#div2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#div2").attr("style", "display:none");
		  $("#div1").attr("style", "display:block");
		}
			
    });	
	

	$('#ref_multiplier_id').change(function() {

        if ( $(this).find("option:selected").text() == 'Lain-lain (BPJS)')
        {
          $("#pengali1").attr("style", "display:none");
		  $("#pengali2").attr("style", "display:block");
        }
		  else
		{
			  //hide
		  $("#pengali2").attr("style", "display:none");
		  $("#pengali1").attr("style", "display:block");
		}
			
    });	

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
                format : "DD-MM-YYYY"
    });

     $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tat_date').datetimepicker({
                format : "DD-MM-YYYY"
    });


});
</script>