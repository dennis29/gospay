<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Master Deskripsi Transaksi
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h2 class="panel-title pull-left"><b>Deskripsi Transaksi</b></h2>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formTransaksi" name="formTransaksi" autocomplete="off">
			    		<div class="form-group">
		                  <label for="transaksi" class="control-label col-xs-8 col-md-3">Kode Transaksi</label>
		                  <div class="col-xs-8 col-md-6"> 
		                          <input type='text' class="form-control" name="m_p_description_transaction_code" id="m_p_description_transaction_code" placeholder="Kode Transaksi" maxlength="3"/>
		                          <input type='hidden' class="form-control" name="m_p_description_transaction_id" id="m_p_description_transaction_id" placeholder="id" value="0"/>
								  <input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
		                  </div>
		                </div>
			    		<div class="form-group">
							<label for="transaksi" class="control-label col-xs-8 col-md-3">Nama Transaksi</label>
							<div class="col-xs-6">
                                    <input class="form-control" type="hidden" id="m_p_description_transaction_name" name="m_p_description_transaction_name" placeholder="Nama Transaksi" data-provide="typeahead" />  
									<input class="form-control" type="hidden" id="jenis_pajak" name="jenis_pajak"  /> 
									<input class="form-control" type="hidden" id="tipe_transaksi" name="tipe_transaksi"  /> 
									<input class="form-control" type="text" id="m_p_descriotion_name" name="m_p_descriotion_name" placeholder="Nama Transaksi" data-provide="typeahead" /> 
				            </div>								
						</div>
						<div class="form-group">
                                <label for="struktur" class="control-label col-xs-3">Pilih Jenis Pajak</label>
                                <div class="col-xs-6">
                                    <?php echo $tax_type_id;?>                                    
                                </div>                               
                        </div>
                        <div class="form-group">
                                <label for="struktur" class="control-label col-xs-3">Pilih Pendapatan</label>
                                <div class="col-xs-6">
                                    <?php echo $income_type_id;?>                                    
                                </div>                               
                        </div>
                        <div class="form-group">
                                <label for="struktur" class="control-label col-xs-3">Pilih Tipe Transaksi</label>
                                <div class="col-xs-6">
                                    <?php echo $transaction_type_id;?>                                    
                                </div>                               
                        </div>						
						<div class="form-group">
							<label for="transaksi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-6">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-6 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="m_p_description_transaction_name" id="search_transaksi" class="form-control" placeholder="Cari Transaksi" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
						<table class="table table-bordered" id="tbl_transaksi">
								<thead>
								<tr class="info"><th>Kode</th><th>Nama Transaksi</th><th>Jenis Pajak</th><th>Jenis Pendapatan</th><th>Tipe Transaksi</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data transaksi -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
						</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadTransaksi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/deskripsi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_transaksi > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_transaksi > tbody").html("<tr><td colspan = '6' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_p_description_transaction_id+"'>"+v.m_p_description_transaction_code+"</td>";
											html += "<td>"+v.m_p_description_transaction_name+"</td>";
											html += "<td>"+v.ref_tax_type_id+"</td>";
											html += "<td>"+v.ref_income_type_id+"</td>";
											html += "<td>"+v.ref_transaction_type_id+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteTransaksi\" onclick=\"$('#act').val('delete'); $('#m_p_description_transaction_id').val("+v.m_p_description_transaction_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editTransaksi\" onclick=\"$('#act').val('edit'); $('#m_p_description_transaction_id').val("+v.m_p_description_transaction_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_transaksi").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadTransaksi();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadTransaksi(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadTransaksi(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadTransaksi(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadTransaksi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#m_p_description_transaction_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/deskripsi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);
											if (k=='ref_tax_type_id')
											{  
												$('#ref_tax_type_id').trigger('change');
												
												
											}
											if (k=='ref_transaction_type_id')
											{  
												$('#ref_transaction_type_id').trigger('change');
												
											}
											
									});
							   }
						   }
						 });	
						 //$("#formTransaksi").trigger("reset");


			}
			else
			if(act=="delete")
			{
				bootbox.dialog({
                     message: "Anda yakin Deskripsi Penggajian  akan dihapus?",

                                               //   title: "Apakah Anda Yakin ?",

                     buttons: {
                          success: {
                               label: "Ya",
                               className: "btn-default",
                               callback: function()  {
                  					$.when(deleteRow("<?php echo base_url('index.php/deskripsi/delete/'); ?>", id)).done(function(rs){
										if(rs)
										{
											//alert("Data Dihapus");
											loadTransaksi();
										}
								    });
     						    }
					     },
						main: {
							label: "Tidak",
							className: "btn-primary",
							callback: function() {}
						}
							
		            }
				});
 			}
	});

	$("#reset").click(function(){
		$("#formTransaksi").trigger("reset");
		$("#m_p_description_transaction_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formTransaksi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/deskripsi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadTransaksi();
								$("#formTransaksi").trigger("reset");
								$("#m_p_description_transaction_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_transaksi").keyup(function(){
			loadTransaksi(1, $("#formSearch").serializeArray());
	});


	$('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/deskripsi/getClient/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
		current = $('#m_client_name').typeahead("getActive");
		$('#m_client_id').val(current.id);
		$('#m_client_code').val(current.code);
    });

	$('#m_client_name').blur(function() {
		if($('#m_client_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_client_id').val(0);
		}
    });	

    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/deskripsi/getLokasi/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_location_name').change(function() {
		current = $('#m_location_name').typeahead("getActive");
		$('#m_location_id').val(current.id);		
    });

	$('#m_location_name').blur(function() {
		if($('#m_location_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_location_id').val(0);
		}
    });	

    $('#m_p_description_transaction_name').change(function() {
	
		$('#m_p_descriotion_name').val($(this).val());		
    });



	$('#ref_tax_type_id').change(function() {

		$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/deskripsi/type_name/?id='); ?>"+$(this).val(),
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if (k=='name')
										{
										    $("#jenis_pajak").val(v);
										}	
									});
							   }
						   }
						 });
			
    });

	$('#ref_transaction_type_id').change(function() {

		$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/deskripsi/type_name/?id='); ?>"+$(this).val(),
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if (k=='name')
										{
										    $("#tipe_transaksi").val(v);
										}	
									});
							   }
						   }
						 });
			
    });


});
</script>