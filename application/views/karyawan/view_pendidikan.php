<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Pendidikan Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Pendidikan Formal</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formPendidikan" name="formPendidikan">			    		
			    		<div class="form-group">
							<label for="pendidikan" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>				
						<div class="form-group">
							<label for="pendidikan" class="control-label col-xs-8 col-md-3">Tingkat Pendidikan</label>
								<div class="col-xs-8">
                                    <?php echo $ref_institution_type;?>                                    
                                </div>				            	
						</div>
						<div class="form-group">
		                      <label for="pendidikan" class="control-label col-xs-8 col-md-3">Nama Sekolah/Universitas</label>
		                      <div class="col-xs-8">
		                          <input class="form-control" type="text" id="institution_name" name="institution_name" placeholder="" data-provide="typeahead"/>		                         
		                      </div>                
		                </div>
		                <div class="form-group">
		                      <label for="pendidikan" class="control-label col-xs-8 col-md-3">Jurusan</label>
		                      <div class="col-xs-8">
		                          <input class="form-control" type="text" id="major" name="major" placeholder="" data-provide="typeahead"/>		                         
		                      </div>
		                </div>
	                    <div class="form-group">
                            <label for="pendidikan" class="control-label col-xs-3">Kota</label>                            
		                    <div class="col-xs-3">
                                <?php //echo $m_city_id; ?>
                                <input class="form-control" type="text" id="m_city_name" name="m_city_name" placeholder="" data-provide="typeahead" />
                                <input class="form-control" type="hidden" id="m_city_name_temp" placeholder="" />
                                <input class="form-control" type="hidden" id="m_city_id" name="m_city_id" placeholder="" value=0 />
                            </div>
                            <label for="pendidikan" class="control-label col-xs-2">Negara</label>
                            <div class="col-xs-3">
                                <?php //echo $m_country_id; ?>
                                <input class="form-control" type="text" id="m_country_name" name="m_country_name" placeholder="" data-provide="typeahead" />
                                <input class="form-control" type="hidden" id="m_country_name_temp" placeholder="" />
                                <input class="form-control" type="hidden" id="m_country_id" name="m_country_id" placeholder="" value=0 />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pendidikan" class="control-label col-xs-3">IPK</label>
                            <div class="col-xs-3">
		                          <input class="form-control" type="text" id="gpa" name="gpa" placeholder="" data-provide="typeahead"/>		                         
		                    </div>
                            <label for="pendidikan" class="control-label col-xs-2">Tahun Lulus</label>
                            <div class="col-xs-3">                            
	                        	 <input class="form-control" type="text" id="graduated" name="graduated" placeholder="" data-provide="typeahead"/>		                         
	                        </div>
                        </div>
                        <div class="form-group">
                            <label for="certificate_no" class="control-label col-xs-3 ">No. Ijazah</label>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" id="certificate_no" name="certificate_no" placeholder="" data-provide="typeahead"/>
                            </div>
                            <label for="certificate_date" class="control-label col-xs-2 ">Tanggal</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='certificate_date'>
                                    <input type='text' class="form-control" id="certificate_date" name="certificate_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
							<label for="pendidikan" class="control-label col-xs-8 col-md-3">Biaya</label>
								<div class="col-xs-8">
                                    <?php echo $ref_cost_type_id;?>                                    
                                </div>				            	
						</div>
						<div class="form-group">
							<label for="pendidikan" class="control-label col-xs-8 col-md-3">Diakui Perusahaan</label>
								<div class="col-xs-8">
                                    <?php echo $ref_recognized_companies;?>                                    
                                </div>				            	
						</div>
						<div class="form-group">
							<label for="pendidikan" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />	
					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Pendidikan Formal</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_pendidikan">
								<thead>
								<tr class="info"><th>Tingkat Pendidikan</th><th>Nama Lembaga</th><th>Jurusan</th><th>Kota</th><th>Tahun lulus</th><th>IPK</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar Pendidikan Formal -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadPendidikan(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/pendidikan/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_pendidikan > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_pendidikan > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_education_id+"'>"+v.ref_institution_type+"</td>";
											html += "<td>"+v.institution_name+"</td>";
											html += "<td>"+v.major+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.graduated+"</td>";
											html += "<td>"+v.gpa+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletePendidikan\" onclick=\"$('#act').val('delete'); $('#gp_education_id').val("+v.gp_education_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editPendidikan\" onclick=\"$('#act').val('edit'); $('#gp_education_id').val("+v.gp_education_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_pendidikan").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadPendidikan();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadPendidikan(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadPendidikan(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadPendidikan(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadPendidikan(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    

    $('#m_city_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/pendidikan/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_city_name').change(function() {
		var current = $('#m_city_name').typeahead("getActive");
		try{
			$('#m_city_id').val(current.id);
		}catch(e){
			alert("Pilih kota dari List yang Ada");
		}
    });
	
	
	$('#m_city_name').blur(function(){
			$('#m_city_name').trigger("change");
	});	

	
	$('#m_country_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/pendidikan/getCountry/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_country_name').change(function() {
		var current = $('#m_country_name').typeahead("getActive");
		try{
			$('#m_country_id').val(current.id);
		}catch(e){
			alert("Pilih negara dari List yang Ada");
		}
    });
	
	
	$('#m_country_name').blur(function(){
			$('#m_country_name').trigger("change");
	});	

	

	$("#gp_education_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/pendidikan/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/pendidikan/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadPendidikan();
						}
					});
	        	}
				}); 
				// var result = window.confirm('data akan dihapus?');
    // 	        if (result == true) {
				// 	$.when(deleteRow("<?php echo base_url('index.php/cabang/delete/'); ?>", id)).done(function(rs){
				// 		if(rs)
				// 		{
				// 			//alert("Data Dihapus");
				// 			loadCabang();
				// 		}
				// 	});
	   //      	}
 			}
	});

	$("#reset").click(function(){
		$("#formPendidikan").trigger("reset");
		$("#gp_education_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formPendidikan").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/pendidikan/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadPendidikan();
								$("#formPendidikan").trigger("reset");
								$("#gp_education_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#certificate_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});    
    


});
</script>