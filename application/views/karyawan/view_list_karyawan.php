<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: 
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Daftar Karyawan
* 
********************************************************************/  -->

<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-12 bootcards-list" id="list" data-title="List Kandidat">
    <div class="panel panel-default" style="margin-top:30px;">
				<div id="demo_grid1"></div>
				<div class="panel-heading clearfix">
						<h3 class="panel-title pull-left"><b>List Karyawan</b></h3>	
						<div class="col-xs-4">
							<div class="input-group">
                            	<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                            <input type="text" name="personal_name" id="search_personal" class="form-control" placeholder="Cari karyawan berdasarkan NRK dan Nama" />
                          </div>
                        </div>
						<div class="col-xs-3 pull-right">
							<div class="btn-group">
								<button class="btn btn-primary" id="add-karyawan" onclick="$('#content').load('<?php echo base_url('index.php/karyawan/getPanel	/0'); ?>');" >
								<i class="fa fa-user-plus fa-fw"></i> 
								Add
								</button>
								<!-- <button class="btn btn-danger" data-toggle="modal" data-target="#searchKaryawan">
								<i class="fa fa-search"></i> 
								Search
								</button> -->
							</div>
						</div>
				</div>
                <!-- <div class="list-group" id="karyawanList"> -->
                <!-- list of sample data -->
                <!-- </div> -->

                <div class="table-responsive">
						<!-- <h4 class="panel-title pull-left" ><b>Daftar Karyawan</b></h4>
					</br> -->
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_karyawan">
								<thead>
								<tr class="info"><th>NRK</th><th>Nama</th><th>Kantor</th><th>Client</th><th>Lokasi Kerja</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
            
                <div class="panel-footer center-block">
                    <div class="btn-group" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                          <a class="btn btn-default" href="#" id="first" >
                            <i class="fa fa-fast-backward fa-fw"></i> 
                            <span>First</span>
                          </a>
                          <a class="btn btn-default" href="#" id="prev" >
                            <i class="fa fa-backward fa-fw"></i> 
                            <span>Prev</span>
                          </a>
                          <a class="btn btn-default" id="next" href="#" >
                            <i class="fa fa-forward fa-fw"></i> 
                            <span>Next</span>
                          </a>
                          <a class="btn btn-default" href="#" id="last" >
                            <i class="fa fa-fast-forward fa-fw"></i> 
                            <span>Last</span>
                          </a>
                          <a class="btn btn-default" href="#" >
                            <span id="textpage"></span>
                          </a>
                          <!--a class="btn btn-default" href="#" >
	                          <span id="totalKaryawan"></span>
                          </a-->
                      </div>
                    </div>
            </div>
        </div>
    </div>    
</div>
</div>

<div class="modal fade" role="dialog" aria-hidden="true" id="editKaryawan">
  <div class="modal-dialog">
	<div class="modal-content center-block">
    <div class="modal-header">
      <h3 class="modal-title">Preview</h3>
    </div>

    <div class="modal-body">
    </div>
    <div class="modal-footer">
      <div class="btn-group pull-right">
        <button class="btn btn-danger" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>  

    </form>  
	</div>

  </div>
</div>
</div>


<div class="modal fade" role="dialog" aria-hidden="true" id="searchKaryawan">
  <div class="modal-dialog">
	<div class="modal-content">
    <div class="modal-header">

      <div class="btn-group pull-right">
        <button class="btn btn-danger" id="btnCancel" data-dismiss="modal">
          Cancel
        </button>
        <button class="btn btn-success save" id="btnSearch" data-dismiss="modal">
          Search
        </button>
      </div>
      <h3 class="modal-title pull-left">Search Karyawan</h3>

    </div>
    <div class="modal-body">
	  <form class="form-horizontal" id="formSearch" role="form">
      <div class="form-group">
        <label class="col-xs-3 control-label">NRK</label>
        <div class="col-xs-9">
          <input type="text" name="nrk" id="nrk" class="form-control" placeholder="NRK">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Nama Karyawan</label>
        <div class="col-xs-9">
          <input type="text" name="name" id="name" class="form-control" placeholder="Nama Karyawan">
        </div>
      </div>      
      </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<script>

/*
<?php
foreach($akses as $k=>$v)
{
?>
	if($("#<?=$v?>").is(":visible"))
	{
		$("#<?=$v?>").hide();
		if($("#<?=$v?>").is(":visible"))
			$("#<?=$v?>").prop("visibility","hidden");
	}					
<?php
}
?>
*/

$(function()
{
/*	function cekAkses()
	{
		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/main/cekAkses/'); ?>",
			   success: function(data)
			   {
					var dt = jQuery.parseJSON(data);
					$.each(dt.role, function(k,v){
							if($("#"+v).is(":visible"))
							{
								$("#"+v).hide();
								if($("#"+v).is(":visible"))
									$("#"+v).prop("visibility","hidden");
							}
					$("#check_akses").val();
												
					});
			   }
		});
	}
	cekAkses();
*/
	var curpage = 1;
	var totpage = 1;

	$("#btnSearch").click(function(){
			loadKaryawan(1, $("#formSearch").serializeArray());
	});

	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadKaryawan(1);
	});

	function loadKaryawan(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});


			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/personal/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_karyawan > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_karyawan > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_salary_id+"'>"+v.nrk+"</td>";
											html += "<td>"+v.personal_name+"</td>";
											html += "<td>"+v.m_branch_name+"</td>";
											html += "<td>"+v.m_client_name+"</td>";
											html += "<td>"+v.m_location_name+"</td>";
											html += "</a>";
								            $("#tbl_karyawan").append($(html).dblclick(function() { $("#content").load("<?php echo base_url('index.php/karyawan/getPanel/'); ?>/"+'1'+"/"+v.gp_personal_id); }));
//									$("#tbl_karyawan").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadKaryawan();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) loadKaryawan(next);			
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1) loadKaryawan(prev);			
	});
	
	$("#first").click(function(){
		var next = 1;
		loadKaryawan(next);			
	});

	$("#last").click(function(){
		var next = totpage;
		loadKaryawan(next);			
	});
	$("a").click(function(e){
		e.preventDefault();	
	});


/*
	function cekAkses()
	{
		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/main/cekAkses/'); ?>",
			   success: function(data)
			   {
					var dt = jQuery.parseJSON(data);
					$.each(dt.role, function(k,v){
							if($("#"+v).is(":visible"))
							{
								$("#"+v).hide();
								if($("#"+v).is(":visible"))
									$("#"+v).prop("visibility","hidden");
							}					
					});
			   }
		});
	}
	cekAkses();
*/
	
});
</script>

