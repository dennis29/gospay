<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Dokumen Karyawan
* 
********************************************************************/  -->

<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Kelengkapan Dokumen</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formDokumen" name="formDokumen">			    		
			    		<div class="form-group">
							<label for="dokumen" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="name" name="name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>
						</div>
						<div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">Nama Dokumen</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_doc_type_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">Tanggal Kelengkapan</label>
		                    <div class="col-xs-3">
	                            <div class='input-group date ' id='doc_date'>
	                                <input type='text' class="form-control" id="doc_date" name="doc_date"/>
	                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                                </span>
	                            </div>
	                        </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">Status</label>
		                    <div class="col-xs-3">
                                <?php echo $ref_doc_status_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">Remarks</label>
		                	<div class="col-xs-8">
	                            <input class="form-control" type="text" id="doc_note" name="doc_note" placeholder="Remarks" data-provide="typeahead" />                                		            		
			            	</div>
			            </div>
			            <div class="form-group">                                            
		                    <label for="dokumen" class="control-label col-xs-3 ">File</label>
							<div class="col-xs-8">
	                            <input type="file" class="filestyle" data-placeholder="No file">
			            	</div>
			            </div>
			            <div class="form-group">
							<label for="dokumen" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />					


					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Kelengkapan Dokumen</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_Dokumen">
								<thead>
								<tr class="info"><th>Nama Dokumen</th><th>Tanggal</th><th>Status</th><th>Remarks</th><th>Nama URL</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadDokumen(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/dokumen/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_Dokumen > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_Dokumen > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_document_id+"'>"+v.ref_doc_type_id +"</td>";
											html += "<td>"+v.doc_date+"</td>";
											html += "<td>"+v.ref_doc_status_id+"</td>";
											html += "<td>"+v.doc_note+"</td>";
											html += "<td>"+v.doc_url+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteDokumen\" onclick=\"$('#act').val('delete'); $('#gp_document_id').val("+v.gp_document_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editDokumen\" onclick=\"$('#act').val('edit'); $('#gp_document_id').val("+v.gp_document_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_Dokumen").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadDokumen();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadDokumen(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadDokumen(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadDokumen(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadDokumen(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_document_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/dokumen/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/dokumen/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadDokumen();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formDokumen").trigger("reset");
		$("#gp_document_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formDokumen").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/dokumen/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadDokumen();
								$("#formDokumen").trigger("reset");
								$("#gp_document_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#doc_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});

	$(":file").filestyle({placeholder: "No file"});


});
</script>