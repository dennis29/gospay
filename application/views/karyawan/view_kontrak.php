<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Kontrak Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Kontrak Kerja</b></h3>
	            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formKontrak" name="formKontrak">			    		
			    		<div class="form-group">
							<label for="nama karyawan" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="client" class="control-label col-xs-8 col-md-3">Nama Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_id" name="m_client_id" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
                            <label for="Mulai Kontrak" class="control-label col-xs-3 ">Mulai Kontrak</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='start_contract_date'>
                                    <input type='text' class="form-control" id="start_contract_date" name="start_contract_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="Akhir Kontrak" class="control-label col-xs-2 ">Akhir Kontrak</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='stop_contract_date'>
                                    <input type='text' class="form-control" id="stop_contract_date" name="stop_contract_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 <?php echo $vkontrak ["ref_status_pkwt_id"]["required"]; ?>">Status Kontrak</label>
		                   <div class="col-xs-8">
                                <?php echo $ref_status_pkwt_id; ?>
                            </div>
		                </div>
                        <div class="form-group">                                            
		                    <label for="No. PKWT" class="control-label col-xs-3 ">No. PKWT</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="pkwt_no" name="pkwt_no" placeholder="No. PKWT" />
		                    </div>
		                    <label for="Tanggal PKWT" class="control-label col-xs-2 ">Tanggal PKWT</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='pkwt_date'>
                                    <input type='text' class="form-control" id="pkwt_date" name="pkwt_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="No. Penugasan" class="control-label col-xs-3 ">No. Penugasan</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="job_no" name="job_no" placeholder="No. Penugasan" />
		                    </div>
		                    <label for="Tanggal Penugasan" class="control-label col-xs-2 ">Tanggal Penugasan</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='job_date'>
                                    <input type='text' class="form-control" id="job_date" name="job_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">		                    
		                    <label for="Tanggal Surat Penugasan" class="control-label col-xs-3 ">Tanggal Surat Penugasan</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='letter_job_date'>
                                    <input type='text' class="form-control" id="letter_job_date" name="letter_job_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="PKS" class="control-label col-xs-3 ">No. PKS</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="pks_no" name="pks_no" placeholder="No. PKS" />
		                    </div>
		                    <label for="Tanggal PKWT" class="control-label col-xs-2 ">Tanggal PKS</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='stop_pks_date'>
                                    <input type='text' class="form-control" id="stop_pks_date" name="stop_pks_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>		                
						<div class="form-group">
							<label for="kontrak" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Kontrak Kerja</b></h4>
					</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_kontrak">
								<thead>
								<tr class="info"><th>Mulai</th><th>Sampai</th><th>Status Kontrak</th><th>No PKWT</th><th>Catatan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadKontrak(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/kontrak/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_kontrak > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_kontrak > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_pkwt_id+"'>"+v.start_contract_date+"</td>";
											html += "<td>"+v.stop_contract_date+"</td>";
											html += "<td>"+v.ref_status_pkwt_id+"</td>";
											html += "<td>"+v.pkwt_no+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletekontrak\" onclick=\"$('#act').val('delete'); $('#gp_pkwt_id').val("+v.gp_pkwt_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editkontrak\" onclick=\"$('#act').val('edit'); $('#gp_pkwt_id').val("+v.gp_pkwt_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_kontrak").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadKontrak();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadKontrak(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadKontrak(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadKontrak(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadKontrak(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_pkwt_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/kontrak/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/kontrak/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKontrak();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formKontrak").trigger("reset");
		$("#gp_pkwt_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formKontrak").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/kontrak/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadKontrak();
								$("#formKontrak").trigger("reset");
								$("#gp_pkwt_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#start_contract_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()	
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#stop_contract_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#pkwt_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});
    
    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#job_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#letter_job_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#stop_pks_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()	
	});


});
</script>