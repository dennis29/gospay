<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Mutasi Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Mutasi Jabatan</b></h3>
	            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formMutasi" name="formMutasi">			    		
			    		<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Nama Perusahaan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_company_name" name="m_company_name" placeholder="Nama Perusahaan" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 <?php echo $vmutasi["ref_mutation_type_id"]["required"]; ?>">Jenis Mutasi</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_mutation_type_id; ?>
                            </div>
		                </div>
		                <div class="form-group">
                            <label for="m_branch_id" class="control-label col-xs-3 <?php echo $vmutasi["m_branch_id"]["required"]; ?>">Kantor Cabang</label>
                            <div class="col-xs-8">
                                <input class="form-control" type="text" id="m_branch_name" name="m_branch_name" placeholder="cari kantor cabang">
                                <input type='hidden' class="form-control" name="m_branch_id" id="m_branch_id" placeholder="id" value="0"/>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="m_location_id" class="control-label col-xs-3 <?php echo $vmutasi["m_location_id"]["required"]; ?> ">Lokasi Kerja</label>
                            <div class="col-xs-8">                                                
                                <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="cari lokasi kerja (kode - nama lokasi)">
                                <input type='hidden' class="form-control" name="m_location_id" id="m_location_id" placeholder="id" value="0"/>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="unit kerja" class="control-label col-xs-3 <?php echo $vmutasi["m_unit_id"]["required"]; ?>">Unit Kerja</label>
                            <div class="col-xs-8">
                               <input class="form-control" type="text" id="m_unit_name" name="m_unit_name" placeholder="cari unit kerja (kode - nama unit)">
                               <input type='hidden' class="form-control" name="m_unit_id" id="m_unit_id" placeholder="id" value="0"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jabatan" class="control-label col-xs-3 <?php echo $vmutasi["m_structure_position_id"]["required"]; ?>">Jabatan</label>
                            <div class="col-xs-8">
                                <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="cari jabatan">
                               <input type='hidden' class="form-control" name="m_structure_position_id" id="m_structure_position_id" placeholder="id" value="0"/>
                            </div>
                        </div>
		                <div class="form-group">                            
		                    <label for="TMT Transaksi" class="control-label col-xs-3 <?php echo $vmutasi["effective_date"]["required"]; ?>">Tanggal Efektif Mutasi</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='effective_date'>
                                    <input type='text' class="form-control" id="effective_date" name="effective_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">                            
		                    <label for="TMT Transaksi" class="control-label col-xs-3 ">No. SK</label>
                            <div class="col-xs-3">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Mutasi Jabatan</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_Mutasi">
								<thead>
								<tr class="info"><th>Tanggal Mutasi</th><th>Lokasi</th><th>Unit Kerja</th><th>Nama Jabatan</th><th>Nama Cabang</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadMutasi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_Mutasi > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_Mutasi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_mutation_id+"'>"+v.effective_date+"</td>";
											html += "<td>"+v.m_location_id+"</td>";
											html += "<td>"+v.m_unit_id+"</td>";
											html += "<td>"+v.m_structure_position_id+"</td>";
											html += "<td>"+v.m_branch_id+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteMutasi\" onclick=\"$('#act').val('delete'); $('#gp_mutation_id').val("+v.gp_mutation_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editMutasi\" onclick=\"$('#act').val('edit'); $('#gp_mutation_id').val("+v.gp_mutation_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_Mutasi").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadMutasi();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadMutasi(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadMutasi(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadMutasi(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadMutasi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_mutation_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/mutasi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadMutasi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formMutasi").trigger("reset");
		$("#gp_mutation_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formMutasi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/mutasi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadMutasi();
								$("#formMutasi").trigger("reset");
								$("#gp_mutation_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#effective_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});


	$('#m_branch_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getCabang/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_branch_name').change(function() {
        current = $('#m_branch_name').typeahead("getActive");
        $('#m_branch_id').val(current.id);
        $('#m_branch_code').val(current.code);
    });

    $('#m_branch_name').blur(function() {
        if($('#m_branch_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_branch_id').val(0);
        }
    });


    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getLokasi/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_location_name').change(function() {
        current = $('#m_location_name').typeahead("getActive");
        $('#m_location_id').val(current.id);
        $('#m_location_code').val(current.code);
    });

    $('#m_location_name').blur(function() {
        if($('#m_location_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_location_id').val(0);
        }
    });


    $('#m_unit_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getUnit/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_unit_name').change(function() {
        current = $('#m_unit_name').typeahead("getActive");
        $('#m_unit_id').val(current.id);
        $('#m_unit_code').val(current.code);
    });

    $('#m_unit_name').blur(function() {
        if($('#m_unit_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_unit_id').val(0);
        }
    }); 

    $('#m_structure_position_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getJabatan/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_structure_position_name').change(function() {
        current = $('#m_structure_position_name').typeahead("getActive");
        $('#m_structure_position_id').val(current.id);
        $('#m_structure_position_code').val(current.code);
    });

    $('#m_structure_position_name').blur(function() {
        if($('#m_structure_position_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_structure_position_id').val(0);
        }
    });
 

});
</script>