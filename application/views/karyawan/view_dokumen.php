<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Dokumen Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Kelengkapan Dokumen</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formDokumen" name="formDokumen">			    		
			    		<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="gp_personal_id" name="gp_personal_id" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="name" name="name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>
						</div>
						<hr  />
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-2">Nama Dokumen</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Tanggal Kelengkapan</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-1">Status</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-2">Remarks</label>			
						</div>
						<div class="form-group">
						<div class="col-lg-3">
							    <div class="input-group">
							      <span class="input-group-addon">
							        <input type="checkbox" aria-label="...">
							      </span>
							      <input type="text" class="form-control" aria-label="..." disabled="disabled"/>
							    </div>
							</div>
							<div class="col-xs-2">
	                            <div class='input-group date ' id='doc_date'>
	                                <input type='text' class="form-control" id="doc_date" name="doc_date"/>
	                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="col-xs-2">
	                                <?php echo $ref_doc_status_id; ?>
	                        </div>
	                        <div class="col-xs-3">
	                            <input class="form-control" type="text" id="doc_note" name="doc_note" placeholder="Remarks" data-provide="typeahead" />                                		            		
			            	</div>
			            	<span class="btn btn-default btn-file">
							    Browse <input type="file">
							</span>
							<hr  />
						</div>

					</form>	
									
					
				</div>	
				
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadMutasi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_Mutasi > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_Mutasi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_fixed_transaction_id+"'>"+v.ref_institution_type+"</td>";
											html += "<td>"+v.institution_name+"</td>";
											html += "<td>"+v.major+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.graduated+"</td>";
											html += "<td>"+v.gpa+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteMutasi\" onclick=\"$('#act').val('delete'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editMutasi\" onclick=\"$('#act').val('edit'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_Mutasi").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadMutasi();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadMutasi(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadMutasi(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadMutasi(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadMutasi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_fixed_transaction_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/mutasi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadMutasi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formDokumen").trigger("reset");
		$("#gp_fixed_transaction_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formDokumen").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/mutasi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadMutasi();
								$("#formDokumen").trigger("reset");
								$("#gp_fixed_transaction_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#doc_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});


});
</script>