<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Sanksi Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Sanksi dan Kedisiplinan</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formSanksi" name="formSanksi">			    		
			    		<div class="form-group">
							<label for="sanksi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>						
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jenis Sanksi</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_punishment_type_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal Mulai</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='punishment_start_date'>
                                    <input type='text' class="form-control" id="punishment_start_date" name="punishment_start_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal Berakhir</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='punishment_end_date'>
                                    <input type='text' class="form-control" id="punishment_end_date" name="punishment_end_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">No. Keputusan</label>
                            <div class="col-xs-3">                                
                                    <input class="form-control" type="text" id="value" name="value" placeholder="">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='decision_date'>
                                    <input type='text' class="form-control" id="decision_date" name="decision_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Rujukan</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_reference_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Pasal</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="value" name="value" placeholder="">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-1 ">Ayat</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="value" name="value" placeholder="">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-1 ">Huruf</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="value" name="value" placeholder="">                                
                            </div>
                        </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Sebab</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Golongan Hukuman</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_group_punishment; ?>
                            </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Pejabat TTD</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="sanksi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Hukuman Disiplin</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_Sanksi">
								<thead>
								<tr class="info"><th>Nama Hukuman</th><th>Mulai</th><th>Akhir</th><th>Tanggal Keputusan</th><th>No Keputusan</th><th>Sebab</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadSanksi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sanksi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_Sanksi > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_Sanksi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_punishment_id+"'>"+v.ref_punishment_type_id+"</td>";
											html += "<td>"+v.punishment_start_date+"</td>";
											html += "<td>"+v.punishment_end_date+"</td>";
											html += "<td>"+v.decision_date+"</td>";
											html += "<td>"+v.decision_no+"</td>";
											html += "<td>"+v.reason +"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteSanksi\" onclick=\"$('#act').val('delete'); $('#gp_punishment_id').val("+v.gp_punishment_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editSanksi\" onclick=\"$('#act').val('edit'); $('#gp_punishment_id').val("+v.gp_punishment_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_Sanksi").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadSanksi();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadSanksi(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadSanksi(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadSanksi(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadSanksi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_punishment_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sanksi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/sanksi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadSanksi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formSanksi").trigger("reset");
		$("#gp_punishment_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formSanksi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/sanksi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadSanksi();
								$("#formSanksi").trigger("reset");
								$("#gp_punishment_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#punishment_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});


		$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#punishment_end_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#decision_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});
 

});
</script>