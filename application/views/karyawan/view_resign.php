<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Resign Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Alasan Keluar</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formResign" name="formResign">			    		
			    		<div class="form-group">
							<label for="resign" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Alasan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="reason" name="reason" placeholder="Judul Atensi">
		                    </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="keterangan" class="control-label col-xs-2 ">No. SK</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="sk_no" name="sk_no" placeholder="">
		                    </div>
                        </div>                        
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Job Berikutnya</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_nextjob_id; ?>
                            </div>
		                </div>
						<div class="form-group">
							<label for="resign" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>						
					</form>					
				</div>
				
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		
	
	
	$("#gp_resign_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/resign/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/resign/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadresign();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formResign").trigger("reset");
		$("#gp_resign_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formResign").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/resign/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadresign();
								$("#formResign").trigger("reset");
								$("#gp_resign_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});


});
</script>