<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-12 bootcards-list" id="list" data-title="List Kandidat">
    <div class="panel panel-default" style="margin-top:30px;">
				<div id="demo_grid1"></div>
				<div class="panel-heading clearfix">
						List Karyawan 
						<div class="col-xs-3 pull-right">
							<div class="btn-group">
								<button class="btn btn-primary" id="add-karyawan" onclick="$('#content').load('<?php echo base_url('index.php/karyawan/getDetail/0'); ?>');">
								<i class="fa fa-user-plus fa-fw"></i> 
								Add
								</button>
								<button class="btn btn-danger" data-toggle="modal" data-target="#searchApplicant">
								<i class="fa fa-search"></i> 
								Search
								</button>
							</div>
						</div>
				</div>
                <div class="list-group" id="karyawanList">
                <!-- list of sample data -->
                </div>
            
                <div class="panel-footer center-block">
                    <div class="btn-group" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                          <a class="btn btn-default" href="#" id="first" >
                            <i class="fa fa-fast-backward fa-fw"></i> 
                            <span>First</span>
                          </a>
                          <a class="btn btn-default" href="#" id="prev" >
                            <i class="fa fa-backward fa-fw"></i> 
                            <span>Prev</span>
                          </a>
                          <a class="btn btn-default" id="next" href="#" >
                            <i class="fa fa-forward fa-fw"></i> 
                            <span>Next</span>
                          </a>
                          <a class="btn btn-default" href="#" id="last" >
                            <i class="fa fa-fast-forward fa-fw"></i> 
                            <span>Last</span>
                          </a>
                          <a class="btn btn-default" href="#" >
                            <span id="textpage"></span>
                          </a>
                          <a class="btn btn-default" href="#" >
	                          <span id="totalapplicant"></span>
                          </a>
                      </div>
                    </div>
            </div>
        </div>
    </div>    
</div>
</div>

<div class="modal fade" role="dialog" aria-hidden="true" id="editApplicant">
  <div class="modal-dialog">
	<div class="modal-content center-block">
    <div class="modal-header">
      <h3 class="modal-title">Preview</h3>
    </div>

    <div class="modal-body">
    </div>
    <div class="modal-footer">
      <div class="btn-group pull-right">
        <button class="btn btn-danger" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>  

    </form>  
	</div>

  </div>
</div>
</div>


<div class="modal fade" role="dialog" aria-hidden="true" id="searchApplicant">
  <div class="modal-dialog">
	<div class="modal-content">
    <div class="modal-header">

      <div class="btn-group pull-right">
        <button class="btn btn-danger" id="btnCancel" data-dismiss="modal">
          Cancel
        </button>
        <button class="btn btn-success save" id="btnSearch" data-dismiss="modal">
          Search
        </button>
      </div>
      <h3 class="modal-title pull-left">Search Kandidat</h3>

    </div>
    <div class="modal-body">
	  <form class="form-horizontal" id="formSearch" role="form">
      <div class="form-group">
        <label class="col-xs-3 control-label">No. KTP</label>
        <div class="col-xs-9">
          <input type="text" name="id_card_no" id="id_card_no" class="form-control" placeholder="No. KTP">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Nama</label>
        <div class="col-xs-9">
          <input type="text" name="name" id="name" class="form-control" placeholder="Nama">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Handphone</label>
        <div class="col-xs-9">
          <input type="text" name="handphone" id="handphone" class="form-control" placeholder="Handphone">
        </div>
      </div>
      </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<script>
<?php
foreach($akses as $k=>$v)
{
?>
	if($("#<?=$v?>").is(":visible"))
	{
		$("#<?=$v?>").hide();
		if($("#<?=$v?>").is(":visible"))
			$("#<?=$v?>").prop("visibility","hidden");
	}					
<?php
}
?>

$(function()
{
/*	function cekAkses()
	{
		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/main/cekAkses/'); ?>",
			   success: function(data)
			   {
					var dt = jQuery.parseJSON(data);
					$.each(dt.role, function(k,v){
							if($("#"+v).is(":visible"))
							{
								$("#"+v).hide();
								if($("#"+v).is(":visible"))
									$("#"+v).prop("visibility","hidden");
							}
					$("#check_akses").val();
												
					});
			   }
		});
	}
	cekAkses();
*/
	var curpage = 1;
	var totpage = 1;

	$("#btnSearch").click(function(){
			loadApplicant(1, $("#formSearch").serializeArray());
	});

	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadApplicant(1);
	});

		function loadKaryawan(page)
		{
			if(typeof page === 'undefined')
				page = 1;
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);
			
			$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/karyawan/get/'); ?>",
				   data: par, // serializes the form's elements.
 				   beforeSend: function () { $("#loading").show(); },
        			// hides the loader after completion of request, whether successfull or failor.             
        		   complete: function () { $("#loading").hide(); },      
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
						    $("#karyawanList").empty();
							$('#textpage').html("Page "+page+" of "+data.totpage);
							$('#totalkaryawan').html("Total : "+data.total+" Karyawan(s)");
							curpage = page;
							totpage = data.totpage;
							$.each(data.data, function(k,v){
								//get Education
								var pendidikan = "";
								var sma = v.HIGHSCHOOL_INSTITUTION;
								var diploma = v.DIPLOMA_INSTITUTION;
								var sarjana = v.S1_INSTITUTION;
								var gpa = v.S1_GPA
								var gpa_diploma = v.DIPLOMA_GPA
								
								if(sarjana != "" || sarjana != 0)
								{
									if(gpa !=0)
									{
										pendidikan = "S1 ("+gpa+")";
									}
									else
									{
										pendidikan = "S1";
									}
								}
								else
								if((sarjana == "" || sarjana == 0) && (diploma != "" || diploma != 0) )
								{
									if(gpa_diploma !=0)
									{
										pendidikan = "DIPLOMA ("+gpa_diploma+")";
									}
									else
									{
										pendidikan = "DIPLOMA";
									}
								}
								else
								if( (sarjana == "" || sarjana == 0) && (diploma == "" || diploma == 0) && (sma != "" || sma != 0) )
								{
									pendidikan = "SMA";
								}
								else								
								{
									pendidikan = "-";
								}	
								
								//jika image tidak ada, default image = nophoto.png
								var photo = "";
								var file = v.FILE_PHOTO;
								if(file!="" && file!=null)
								{
									photo = v.FILE_PHOTO;
								}
								else
								{
									photo = "nophoto.png";
								}
																
								//get age
								dob = new Date(v.BIRTH_DATE);
								var today = new Date();
								var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
								
								var html = "<a class='list-group-item' href='#' onclick='return false;'>";
									html += "<div class=\"row listrow\" id=\""+v.FJ_PERSONAL_ID+"\">";

								    html += "<div class='col-sm-4'>";
									html += "<img src='<?php echo base_url('asset/uploads/'); ?>/"+photo+"' class='img-rounded pull-left'>";
									html += "<p class='list-group-item-heading' style='weight:bold;'>"+v.NAME+"</h4>";
									html += "<p class='list-group-item-text'>"+v.M_SUB_DISTRICT_ID+", "+v.M_CITY_ID+"</p>";
								  	html += "</div>";

								    html += "<div class='col-sm-3'>";
									html += "<p class='list-group-item-text'>"+v.M_STATE_ID+"</p>";
									html += "<p class='list-group-item-text'>"+v.GENDER_ID+"</p>";
								  	html += "</div>";

								    html += "<div class='col-sm-2'>";
									html += "<p class='list-group-item-text'>"+age+" tahun</p>";
									html += "<p class='list-group-item-text'>"+pendidikan+"</p>";
								  	html += "</div>";									
								    html += "<div class='col-sm-3'>";
									html += "<div class='btn-group'>";
									<?php
										if(!in_array("pp-admin",$akses)) 
										{	
									?>
									html += "<button class='btn btn-success btn-small preview' onclick=\"window.open('<?php echo base_url('index.php/applicant2/getData/'); ?>/"+v.FJ_PERSONAL_ID+"','_blank') ;\" data-toggle=\"modal\" >Admin</button>";
									<?php
										}
									?>
									<?php 
										if(!in_array("pp-appserv",$akses)) 
										{	
									?>
									html += "<button class='btn btn-warning btn-small preview' id='pp-appserv' onclick=\"window.open('<?php echo base_url('index.php/applicant/getPdf/'); ?>/"+v.FJ_PERSONAL_ID+"','_blank') ;\" data-toggle=\"modal\" >App Serv</button>";
									<?php 
										}
									?>
									html += "</div>";
									html += "</div>";
									
									
									html += "</div>";
									html += "</a>";
								$("#applicantList").append($(html).dblclick(function() { $("#content").load("<?php echo base_url('index.php/applicant/getDetail/'); ?>/"+v.FJ_PERSONAL_ID); }));
							});
					   }
				   }
				 });			
		}

	loadKaryawan();

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) loadKaryawan(next);			
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1) loadKaryawan(prev);			
	});
	
	$("#first").click(function(){
		var next = 1;
		loadKaryawan(next);			
	});

	$("#last").click(function(){
		var next = totpage;
		loadKaryawan(next);			
	});
	$("a").click(function(e){
		e.preventDefault();	
	});

	function cekAkses()
	{
		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/main/cekAkses/'); ?>",
			   success: function(data)
			   {
					var dt = jQuery.parseJSON(data);
					$.each(dt.role, function(k,v){
							if($("#"+v).is(":visible"))
							{
								$("#"+v).hide();
								if($("#"+v).is(":visible"))
									$("#"+v).prop("visibility","hidden");
							}					
					});
			   }
		});
	}
	cekAkses();
	
});
</script>

