<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Personal Detail Karyawan
* 
********************************************************************/  -->

<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-responsive.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-11 col-md-offset" id="form-detail" data-title="Contacts">
    <div class="panel panel-default" style="margin-top:0px;">
        <div class="panel-heading clearfix">
            <h2 class="panel-title pull-left"><b>Detail Karyawan</b></h2>
            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
			<!-- <button type="button" class="btn btn-primary pull-right" id="btnlistkandidat" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
			  List Kandidat
			</button>      -->   
			<div class="btn-group pull-right" id="btnkaryawan">
				<?php 
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>     
                <button type="button" class="btn btn-danger save" id="reset1" onClick="$('#content').load('<?php echo base_url('index.php/personal/getDetail/0'); ?>');">
                  Delete
                </button> 
                <button type="button" class="btn btn-success save" id="save1-kandidat">
                  Save
                </button>     
            </div>                  
        </div>
        <div class="panel-body" style="padding:0px;">
        	<div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="dropdown">                
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Personal <b class="caret"></b></a>
                            <ul class="dropdown-menu">                
                                <li class="active"><a data-toggle="tab" href="#detail">Data Diri</a></li>
                                <li><a data-toggle="tab" href="#keluarga">Lanjutan</a></li>
                            </ul>
                        </li>
						
                    </ul>
                </div>
                <div id="form-tab" class="tab-content">
                <div class="tab-pane active" id="detail" data-id="0">
                            <div class="panel-heading" style="background-color:#FFF">
                                <span style="font-size:14px; font-weight:bold">DATA DIRI</span>
                            </div>
                            <div class="panel-body" style="padding:10px;">                                        
                                    <form class="form-horizontal" role="form" id="formKaryawan1" autocomplete="off" name="formKaryawan1">   
                                        <div class="form-group">
                                            <label for="nrk" class="control-label col-xs-3 ">NRK</label>
                                            <div class="col-xs-6">
                                                <input class="form-control" type="text" id="nrk" name="nrk" placeholder="xxxxxxxxx" autocomplete="off" disabled="disabled">                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_card_no" class="control-label col-xs-3 <?php echo $vkaryawan["id_card_no"]["required"]; ?>">No. KTP</label>
                                            <div class="col-xs-6">
                                                <input class="form-control" type="text" id="id_card_no" name="id_card_no" placeholder="No. KTP" autocomplete="off">
                                                <input class="form-control" type="hidden" id="gp_personal_id" name="gp_personal_id" value=<?php echo $gp_personal_id; ?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["name"]["required"]; ?>">Nama</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama" autocomplete="off">
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="birth_place" class="control-label col-xs-3 <?php echo $vkaryawan["birth_place"]["required"]; ?>">Tempat Lahir</label>
                                            <div class="col-xs-3">
                                                <?php //echo $birth_place; ?>
                                                <input class="form-control" type="text" id="birth_place_name" name="birth_place_name" placeholder="Tempat Lahir" data-provide="typeahead" />
                                                <input class="form-control" type="hidden" id="m_birth_place_id" name="m_birth_place_id" placeholder="Tempat Lahir" />
                                            </div>
                                            <label for="tanggal lahir" class="control-label col-xs-2 <?php echo $vkaryawan["birth_date"]["required"]; ?>">Tanggal Lahir</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="birth_date" name="birth_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender_id" class="control-label col-xs-3 <?php echo $vkaryawan["gender_id"]["required"]; ?>">Jenis Kelamin</label>
                                            <div class="col-xs-3">
                                                <?php echo $gender_id; ?>
                                            </div>
                                            <label for="marital_status_id" class="control-label col-xs-2 <?php echo $vkaryawan["marital_status_id"]["required"]; ?>">Status Perkawinan</label>
                                            <div class="col-xs-3">
                                                <?php echo $marital_status_id; ?>
                                            </div>                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-xs-3 <?php echo $vkaryawan["religion_id"]["required"]; ?>">Agama</label>
                                            <div class="col-xs-3">
                                                <?php echo $religion_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="country" class="control-label col-xs-3">Kewarganegaraan</label>
                                            <div class="col-xs-3">
                                                <?php echo $country_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="blood" class="control-label col-xs-3 <?php echo $vkaryawan["blood_type_id"]["required"]; ?>">Golongan Darah</label>
                                            <div class="col-xs-3">
                                                <?php echo $blood_type_id; ?>
                                            </div>                                            
                                        </div>                                        
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Alamat KTP</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="address" class="control-label col-xs-3 <?php echo $vkaryawan["address"]["required"]; ?>">Alamat</label>
                                            <div class="col-xs-8">
                                                <textarea class="form-control" id="address" name="address" placeholder="Alamat"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="RT" class="control-label col-xs-3">RT</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rt" name="rt" maxlength='3' placeholder="RT">
                                            </div>
                                            <label for="RW" class="control-label col-xs-1">RW</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rw" name="rw" maxlength='3' placeholder="RW">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="sub_district" class="control-label col-xs-3 <?php echo $vkaryawan["m_sub_district_id"]["required"]; ?>">Kelurahan</label>
                                            <div class="col-xs-3">
                                                <?php //echo $sub_district_id; ?>
                                                <input type="text" class="form-control" id="m_sub_district_name" name="m_sub_district_name" placeholder="Kelurahan" data-provide="typeahead" />
                                                <input type="hidden" id="m_sub_district_id" name="m_sub_district_id" value="0" />
                                            </div>
                                            <label for="district" class="control-label col-xs-2 ">Kecamatan</label>
											<div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_district_name" name="m_district_name" placeholder="Kecamatan" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_district_id" name="m_district_id" value="0" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="control-label col-xs-3 ">Kota</label>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_city_name" name="m_city_name" placeholder="Kota" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_city_id" name="m_city_id" value="0" />
                                            </div>
                                            <label for="province" class="control-label col-xs-2 ">Provinsi</label>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_state_name" name="m_state_name" placeholder="Provinsi" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_state_id" name="m_state_id" value="0" />
                                            </div>
                                        </div>                                        
                                        <div class="form-group">                                            
                                            <label for="zipe code" class="control-label col-xs-3 ">Kode Pos</label>
                                            <div class="col-xs-3">
                                                <input class="form-control" type="text" maxlength="6" id="zipe_code" name="zipe_code" placeholder="Kode Pos" />
                                            </div>                                        
										</div>
                                        <div class="form-group" id="hph">
                                            <label for="handphone" class="control-label col-xs-3 <?php echo $vkaryawan["handphone"]["required"]; ?>">HP</label>                                            
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="handphone" name="handphone" placeholder="Handphone (pastikan nomor dapat dihubungi)">
                                            </div>
                                        </div>                                      
                                        <div class="form-group" id="hph">
                                            <label for="home_phone" class="control-label col-xs-3 <?php echo $vkaryawan["home_phone"]["required"]; ?>">No. Telepon</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="home_phone" name="home_phone" placeholder="No. Telp Rumah">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="control-label col-xs-3">Email</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="email_address" name="email_address" placeholder="Email">
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="faskes" class="control-label col-xs-3 ">Fasilitas Kesehatan</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_faskes_name" name="m_faskes_name" placeholder="cari fasilitas kesehatan">
                                                <input type='hidden' class="form-control" name="m_faskes_id" id="m_faskes_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="dokgi" class="control-label col-xs-3 ">Dokter Gigi</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="dokgi" name="dokgi" placeholder="">
                                            </div>
                                        </div>
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Perusahaan Karyawan</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="company" class="control-label col-xs-3 <?php echo $vkaryawan["m_company_id"]["required"]; ?>">Nama Perusahaan</label>
                                            <div class="col-xs-7">
                                                <?php echo $company_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="join_date" class="control-label col-xs-3 <?php echo $vkaryawan["join_date"]["required"]; ?>">Tanggal Diterima</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='join_date'>
                                                    <input type='text' class="form-control" id="join_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_join" class="control-label col-xs-1 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_join" name="sk_join" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="start_permanen_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_permanen_date"]["required"]; ?>">Tanggal Permanen</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="start_permanen_date" name="start_permanen_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_permanen" class="control-label col-xs-1 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_permanen" name="sk_permanen" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="status karyawan" class="control-label col-xs-3 <?php echo $vkaryawan["ref_status_kerja_id"]["required"]; ?>">Status Karyawan</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_status_kerja_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="stop_date" class="control-label col-xs-3 ">Tanggal Berhenti</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='stop_date'>
                                                    <input type='text' class="form-control" id="stop_date" name="stop_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_branch_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_branch_id"]["required"]; ?>">Kantor Cabang</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_branch_name" name="m_branch_name" placeholder="cari kantor cabang">
                                                <input type='hidden' class="form-control" name="m_branch_id" id="m_branch_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_client_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_client_id"]["required"]; ?>">Client</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="cari client">
                                                <input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
												<input type='hidden' class="form-control" id="m_client_code" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_location_id" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                            <div class="col-xs-7">                                                
                                                <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="pilih lokasi kerja">
                                                <input type='hidden' class="form-control" name="m_location_id" id="m_location_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
										<input class="form-control"  type="hidden" id="m_area_id" name="m_area_id" placeholder="Wilayah">
                                            <label for="wilayah" class="control-label col-xs-3 ">Wilayah / Area</label>
                                            <div class="col-xs-3">
                                                <div class="form-control" >
                                                    <input class="form-control" type="text" id="wil_name" name="wil_name" placeholder="Wilayah" data-provide="typeahead">
													
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-3">
												<div class="form-control" >
                                                     <input class="form-control" type="text" id="area_name" name="area_name" placeholder="Area"  disabled>
													 
                                                </div>
                                                   
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="unit kerja" class="control-label col-xs-3 <?php echo $vkaryawan["m_unit_id"]["required"]; ?>">Unit Kerja</label>
                                            <div class="col-xs-7">
                                               <input class="form-control" type="text" id="m_unit_name" name="m_unit_name" placeholder="pilih unit kerja">
                                               <input type='hidden' class="form-control" name="m_unit_id" id="m_unit_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jabatan" class="control-label col-xs-3 <?php echo $vkaryawan["m_structure_position_id"]["required"]; ?>">Jabatan</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="pilih jabatan">
                                               <input type='hidden' class="form-control" name="m_structure_position_id" id="m_structure_position_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ref_team_work_id" class="control-label col-xs-3 ">Kelompok Kerja</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_team_work_id; ?>
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="team_work_date" class="control-label col-xs-3 ">Tanggal Join</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='team_work_date'>
                                                    <input type='text' class="form-control" id="team_work_date" name="team_work_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="pkwt_no" class="control-label col-xs-3 ">No PKWT</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="pkwt_no" name="pkwt_no" placeholder="No PKWT">
                                            </div> 
                                            <label for="pkwt_date" class="control-label col-xs-2">Tanggal PKWT</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='pkwt_date'>
                                                    <input type='text' class="form-control" id="pkwt_date" name="pkwt_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="start_contract_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_contract_date"]["required"]; ?>">Awal Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='start_contract_date'>
                                                    <input type='text' class="form-control" id="start_contract_date" name="start_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>   
                                            <label for="stop_contract_date" class="control-label col-xs-2 <?php echo $vkaryawan["stop_contract_date"]["required"]; ?>">Akhir Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="stop_contract_date" name="stop_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="Status Kontrak" class="control-label col-xs-3">Status Kontrak</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_status_pkwt_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="job_no" class="control-label col-xs-3 ">No Penugasan</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="job_no" name="job_no" placeholder="No Penugasan">
                                            </div> 
                                            <label for="job_date" class="control-label col-xs-2 ">Tgl Penugasan</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='job_date'>
                                                    <input type='text' class="form-control" id="job_date" name="job_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>                                        
                                    </form>
                              </div>
                </div>

                <div class="tab-pane" id="keluarga" data-id="1">                    
                    <div class="panel-body" style="padding:10px;">
                        <form class="form-horizontal" role="form" id="formKaryawan2" name="formKaryawan2" autocomplete="off">                                                        
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="user" class="control-label col-xs-3 ">User</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name" name="m_criteria_name" placeholder="cari user"  >
                                <input type='hidden' class="form-control" name="m_criteria_id" id="m_criteria_id" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code" id="m_criteria_code" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="HRBP" class="control-label col-xs-3 ">HRBP</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name2" name="m_criteria_name2" placeholder="cari HRBP">
                                <input type='hidden' class="form-control" name="m_criteria_id2" id="m_criteria_id2" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code2" id="m_criteria_code2" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Cost Center" class="control-label col-xs-3 ">Cost Center</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name3" name="m_criteria_name3" placeholder="cari cost center">
                                <input type='hidden' class="form-control" name="m_criteria_id3" id="m_criteria_id3" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code3" id="m_criteria_code3" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Asuransi" class="control-label col-xs-3 ">Asuransi</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name4" name="m_criteria_name4" placeholder="cari asuransi">
                                <input type='hidden' class="form-control" name="m_criteria_id4" id="m_criteria_id4" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code4" id="m_criteria_code4" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Bucket" class="control-label col-xs-3 ">Bucket</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name5" name="m_criteria_name5" placeholder="cari bucket">
                                <input type='hidden' class="form-control" name="m_criteria_id5" id="m_criteria_id5" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code5" id="m_criteria_code5" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="sipo" class="control-label col-xs-3 ">SIPO-ID</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="sipo_id" name="sipo_id" placeholder="SIPO ID" autocomplete="off">
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">                                            
                            <label for="mother_name" class="control-label col-xs-3 ">Nama Ibu Kandung</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="mother_name" name="mother_name" placeholder="Nama Ibu Kandung" />
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="family_card_no" class="control-label col-xs-3 ">No. Kartu Keluarga (KK)</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="family_card_no" name="family_card_no" placeholder="No. Kartu Keluarga (KK)" />
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="lembur" class="control-label col-xs-3 ">Berhak Lembur</label>
                            <div class="col-xs-3">
                                <?php echo $ref_overtime_status_id; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="bpjs" class="control-label col-xs-3 ">Berhak BPJS Kesehatan</label>
                            <div class="col-xs-3">
                                <?php echo $ref_bpjskes_status_id; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="bpjs" class="control-label col-xs-3 ">No. BPJS Kesehatan</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="bpjskes_no" name="bpjskes_no" placeholder="Nomor BPJS jika ada">
                            </div>                            
                        </div>
                        <div class="form-group">                            
                            <label for="jamsostek" class="control-label col-xs-3 ">No. Jamsostek</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="jamsostek_no" name="jamsostek_no" placeholder="Nomor Jamsostek jika ada">
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Pendidikan Terakhir</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="Pendidikan" class="control-label col-xs-3 <?php echo $vkaryawan["ref_institution_type"]["required"]; ?>">Pendidikan</label>
                            <div class="col-xs-3">
                                <?php echo $ref_institution_type; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="institusi" class="control-label col-xs-3 ">Nama Sekolah / Universitas</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="institution_name" name="institution_name" placeholder="Nama Sekolah / Universitas">
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="major" class="control-label col-xs-3 ">Jurusan</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="major" name="major" placeholder="jurusan">
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="IPK" class="control-label col-xs-3 ">IPK</label>
                            <div class="col-xs-2">
                                <input class="form-control" type="text" id="gpa" maxlength="10" name="gpa" placeholder="IPK">
                            </div>
                            <label for="tahun lulus" class="control-label col-xs-3 <?php echo $vkaryawan["graduated"]["required"]; ?>">Tahun Lulus</label>
                            <div class="col-xs-2">
                                <input class="form-control" type="text" maxlength="4" id="graduated" name="graduated" placeholder="Tahun lulus">
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Pajak</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="PTKP" class="control-label col-xs-3 <?php echo $vkaryawan["ref_stat_ptkp"]["required"]; ?>">Status PTKP</label>
                            <div class="col-xs-3">
                                <?php echo $ref_stat_ptkp; ?>
                            </div>      
                            <label for="tanggungan" class="control-label col-xs-2 <?php echo $vkaryawan["number_dependents"]["required"]; ?>">Jumlah Tanggungan</label>
                            <div class="col-xs-1">
                                <input class="form-control" type="text" id="number_dependents" name="number_dependents" placeholder="">
                            </div>
                            <label class="control-label">Orang</label>
                        </div>
                        <div class="form-group">                                            
                            <label for="npwp_no" class="control-label col-xs-3 ">No. NPWP</label>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" maxlength="20" id="npwp_no" name="npwp_no" placeholder="No. NPWP" />
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Rekening Pembayaran Gaji</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="bank" class="control-label col-xs-3">Nama Bank</label>
                            <div class="col-xs-3">
                                <?php echo $bank_id; ?>
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="acc id" class="control-label col-xs-3 ">No. Rekening</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="acc_id" name="acc_id" placeholder="No. Rekening" />
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="acc name" class="control-label col-xs-3 ">Pemilik Rekening</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="acc_name" name="acc_name" placeholder="Pemilik Rekening" />
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="personal_note" class="control-label col-xs-3 ">Keterangan</label>
                            <div class="col-xs-6">
                                <input class="form-control" type="text" id="personal_note" name="personal_note" placeholder="catatan">
                            </div>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#addNote">                                
                                Add
                            </button>                             
                        </div>
                       </form>
                    </div>
                </div>
                
               
             </div>
        </div>

        <div class="panel-footer" id="footerkaryawan">
			<div class="btn-group pull-left _tabs_navigation" data-toggle="buttons-radio" id="btnNavigation">
				<a class="btn btn btn-info" href="#" id="btnPrev">
					<i class="fa fa-backward fa-fw"></i> 
					<span>Prev</span>
				</a>
				<a class="btn btn btn-info" href="#" id="btnNext">
					<i class="fa fa-forward fa-fw"></i> 
					<span>Next</span>
				</a>
			</div>
            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory1">(tanda * wajib diisi)</h3>
            <div class="btn-group pull-right" id="btnkaryawan1">
				<?php
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" onClick="$('#content').load('<?php echo base_url('index.php/karyawan/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>        
                
                <button type="button" class="btn btn-danger" id="reset" onClick="$('#content').load('<?php echo base_url('index.php/karyawan/getDetail/0'); ?>');">
                  Delete
                </button>
                <button type="button" class="btn btn-success" id="save-kandidat">
                  Save
                </button>    
             </div>                    
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" role="dialog" aria-hidden="true" id="addNote">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">      
      <h3 class="modal-title pull-left">Atensi Manajemen</h3>
    </div>
    <div class="modal-body">
      <form class="form-horizontal" id="formNote" role="form">
        <div class="form-group">
            <label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                </div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                                            
                </div>
        </div>                      
        <div class="form-group">                            
            <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal Catatan</label>
            <div class="col-xs-5">
                <div class='input-group date'>
                    <input type='text' class="form-control" id="add_date" name="add_date" disabled="disabled"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>                        
        <div class="form-group">
            <label for="keterangan" class="control-label col-xs-3 ">Judul</label>
            <div class="col-xs-8">
                <input class="form-control" type="text" id="title" name="title" placeholder="Judul Atensi">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label col-xs-3">Uraian</label>
            <div class="col-xs-8">
                <textarea class="form-control" id="note" name="note" placeholder=""></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="modal" class="control-label col-xs-8 col-md-3"></label>
            <div class="btn-group">
                <button class="btn btn-danger" id="btnCancel" data-dismiss="modal">
                  Cancel
                </button>
                <button class="btn btn-success save" id="save" data-dismiss="modal">
                  Save
                </button>
             </div>            
        </div>
      </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<script>



$(function()
{
	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	  loadTab();
	});	

	$('.nav-tabs > .dropdown > .dropdown-menu ').click(function(){
		$('.nav li').removeClass('active');
	});	

	var finish = 0;
	var timeout;
	
	
	
    $('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/personal/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id').val(resp.m_district_id);
			   $('#m_district_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	

    $('#birth_place_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/personal/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#birth_place_name').change(function() {
		var current = $('#birth_place_name').typeahead("getActive");
		try{
			$('#m_birth_place_id').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
    });
	

	
	
	$('#birth_place_name').blur(function(){
			$('#birth_place_name').trigger("change");
	});	


    $('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/personal/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id_id').val(resp.m_district_id);
			   $('#m_district_id_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	$("#weight").ForceNumericOnly();
	$("#height").ForceNumericOnly();


	function loadEducation()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/education/getEducation/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									$("#"+k).val(v);
																		
								});
						   }
					   }
				});				
			}
	}

	function loadFamily()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/family/getFamily/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
										if(k.search("_DOB")>-1) 
										{
											v = view_date(v, 1);	// 1 = ubah ke format dd-mm-yyyy
											$("#"+k+"[type=text]").val(v);
										}
										else
										{
											$("#"+k).val(v);
										}
								});
						   }
					   }
				});				
			}
	}




$('#btnNext').click(function(){
//		setUpper();
//		$("#formKaryawan1").submit();
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').next('li').find('a').trigger('click');
});

$('#btnPrev').click(function(){
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').prev('li').find('a').trigger('click');
});

	function setUpper()
	{
		$("input[type=text]").val(function(){
			return this.value.toUpperCase();	
		});
		$("textarea").val(function(){
			return this.value.toUpperCase();	
		});

	}


	$("#reset").click(function(){
		
	
	});

	$("#reset1").click(function(){
		
	});
	$("#save-kandidat").click(function(){
	$("#save1-kandidat").click();
	});
	$("#save1-kandidat").click(function(){
	//alert('d');
	//$('#m_client_name').change();
	  cari_nrk();
	  var par = new Array();
      q = $("#formKaryawan1").serializeArray();
	  r = $("#formKaryawan2").serializeArray();	
      
	  $.each(q, function(k,v){
        par[v.name] = v.value;
      });
	  $.each(r, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
               url: "<?php echo base_url("index.php/personal/save"); ?>"
			   +"?nrk="+$('#nrk').val()+"&m_criteria_name="+$('#m_criteria_name').val()+"&m_criteria_name2="+$('#m_criteria_name2').val()
			   +"&m_criteria_name3="+$('#m_criteria_name3').val()+"?m_criteria_name4="+$('#m_criteria_name4').val()+"&m_criteria_name5="
			   +$('#m_criteria_name5').val()+"&m_criteria_id="+$('#m_criteria_id').val()+"&m_criteria_id2="+$('#m_criteria_id2').val()
			   +"&m_criteria_id3="+$('#m_criteria_id3').val()+"&m_criteria_id3="+$('#m_criteria_id3').val()
			   +"&m_criteria_id4="+$('#m_criteria_id4').val()+"&m_criteria_id5="+$('#m_criteria_id5').val()
			   +"&m_criteria_code="+$('#m_criteria_code').val()+"&m_criteria_code2="+$('#m_criteria_code2').val()
			   +"&m_criteria_code3="+$('#m_criteria_code3').val()+"&m_criteria_code4="+$('#m_criteria_code4').val()
			   +"&m_criteria_code5="+$('#m_criteria_code5').val()+"&m_client_id="+$('#m_client_id').val()
			   +"&start_permanen_date="+$('#start_permanen_date').val()			   ,
			   
               data: par, // serializes the form's elements.
               success: function(data)
               {
			   
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
				 if(data.msg) 
                 {
					
					alert(data.msg);
                 }
				 else
				 {
				 $("#alert").show();
				 $('#m_criteria_name').attr( 'disabled','disabled');
				 $('#m_criteria_name2').attr( 'disabled','disabled');
				 $('#m_criteria_name3').attr( 'disabled','disabled');
				 $('#m_criteria_name4').attr( 'disabled','disabled');
				 $('#m_criteria_name5').attr( 'disabled','disabled');
				 
				 }
				 }
				 
               }
			  
             });    	
	});

	

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#birth_date').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : "1990/01/01"	
	});    
    $('#join_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : "2012/01/01"	
	});
    $('#team_work_date').datetimepicker({
                format : "DD-MM-YYYY",
           //     defaultDate : "2012/01/01"  
    });
    $('#start_permanen_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : "2012/01/01"	
	});    
    $('#stop_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : new Date()
	});
    $('#pkwt_date').datetimepicker({
                format : "DD-MM-YYYY",  
                defaultDate : new Date()
    });
    $('#job_date').datetimepicker({
                format : "DD-MM-YYYY",  
                defaultDate : new Date()
    });
    $('#start_contract_date').datetimepicker({
                format : "DD-MM-YYYY",  
                defaultDate : new Date()
    });
    $('#stop_contract_date').datetimepicker({
                format : "DD-MM-YYYY",  
				defaultDate : new Date()
				//maxDate: new Date()
				
  
    });
    $('#add_date').datetimepicker({
                format : "DD-MM-YYYY",  
                defaultDate : new Date()
    });


 
	
	$('#home_phone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#handphone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	

    $('#m_faskes_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getFaskes/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_faskes_name').change(function() {
		var current = $('#m_faskes_name').typeahead("getActive");
		try{
		$('#m_faskes_id').val(current.id);
        $('#m_faskes_code').val(current.code);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
        
    });
	$('#m_company_id').change(function() {
        
    });

    $('#m_faskes_name').blur(function() {
        if($('#m_faskes_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_faskes_id').val(0);
        }
    }); 


    $('#m_branch_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getCabang/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_branch_name').change(function() {
        current = $('#m_branch_name').typeahead("getActive");
        $('#m_branch_id').val(current.id);
        $('#m_branch_code').val(current.code);
    });

    $('#m_branch_name').blur(function() {
        if($('#m_branch_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_branch_id').val(0);
        }
    });


    $('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getClient/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
        current = $('#m_client_name').typeahead("getActive");	
        $('#m_client_id').val(current.id);
        $('#m_client_code').val(current.code);
		var stop_contract_date = current.m_client_end_pks_date;
		var start_contract_date = current.m_client_start_pks_date;
		//alert(current.m_client_end_pks_date);
		$('#start_contract_date').data("DateTimePicker").minDate(new Date(start_contract_date));
		$('#stop_contract_date').data("DateTimePicker").maxDate(new Date(stop_contract_date));
		//alert($(this).val());
		cari_nrk();
	
    });

    $('#m_client_name').blur(function() {
        if($('#m_client_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_client_id').val(0);
        }
    });


    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getLokasi/'); ?>/'+query+'/'+$('#m_client_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#wil_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getWilayah/'); ?>/'+query+'/'+$('#m_client_id').val()+'/'+$('#m_location_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_location_name').change(function() {
        current = $('#m_location_name').typeahead("getActive");
		try{
		$('#m_location_id').val(current.id);

		}catch(e){
			alert("Pilih Lokasi Kerja dari List yang Ada");
		}
           });
		   
	$('#wil_name').change(function() {
        current = $('#wil_name').typeahead("getActive");
		try{
		$('#m_area_id').val(current.id);
		$('#area_name').val(current.area_name);

		}catch(e){
			alert("Pilih Lokasi Kerja dari List yang Ada");
		}
           });
	   

    $('#m_location_name').blur(function() {
        if($('#m_location_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_location_id').val(0);
        }
    });


    $('#m_unit_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getUnit/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_unit_name').change(function() {
        current = $('#m_unit_name').typeahead("getActive");
        $('#m_unit_id').val(current.id);
        $('#m_unit_code').val(current.code);
    });

    $('#m_unit_name').blur(function() {
        if($('#m_unit_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_unit_id').val(0);
        }
    }); 

    $('#m_structure_position_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getJabatan/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_structure_position_name').change(function() {
        current = $('#m_structure_position_name').typeahead("getActive");
        $('#m_structure_position_id').val(current.id);
        $('#m_structure_position_code').val(current.code);
    });

    $('#m_structure_position_name').blur(function() {
        if($('#m_structure_position_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_structure_position_id').val(0);
        }
    });

    $('#m_criteria_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaUser/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	$('#m_criteria_name2').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaHRBP/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name3').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaCostCenter/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name4').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaAsuransi/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name5').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaBucket/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_criteria_name').change(function() {
	//alert('dddd');
        current = $('#m_criteria_name').typeahead("getActive");
		if (current)
		{
        $('#m_criteria_id').val(current.id);
        $('#m_criteria_code').val(current.code);
		}
		else
		{
		$('#m_criteria_id').val(0);
        $('#m_criteria_code').val(0);
		}
		
    });
	
	$('#m_criteria_name').keypress(function() {
		$('#m_criteria_id').val(0);
		$('#m_criteria_code').val(0);
    });
	
	$('#m_criteria_name2').keypress(function() {
		$('#m_criteria_id2').val(0);
		$('#m_criteria_code2').val(0);
    });
	
	$('#m_criteria_name3').keypress(function() {
		$('#m_criteria_id3').val(0);
		$('#m_criteria_code3').val(0);
    });
	
	$('#m_criteria_name4').keypress(function() {
		$('#m_criteria_id4').val(0);
		$('#m_criteria_code4').val(0);
    });
	
	$('#m_criteria_name5').keypress(function() {
		$('#m_criteria_id5').val(0);
		$('#m_criteria_code5').val(0);
    });
	
	$('#m_criteria_name2').change(function() {
        current = $('#m_criteria_name2').typeahead("getActive");
        $('#m_criteria_id2').val(current.id);
        $('#m_criteria_code2').val(current.code);
    });
	
	$('#m_criteria_name3').change(function() {
        current = $('#m_criteria_name3').typeahead("getActive");
        $('#m_criteria_id3').val(current.id);
        $('#m_criteria_code3').val(current.code);
    });
	
	$('#m_criteria_name4').change(function() {
        current = $('#m_criteria_name4').typeahead("getActive");
        $('#m_criteria_id4').val(current.id);
        $('#m_criteria_code4').val(current.code);
    });
	
	$('#m_criteria_name5').change(function() {
        current = $('#m_criteria_name5').typeahead("getActive");
		try{
			  $('#m_criteria_id5').val(current.id);
			  $('#m_criteria_code5').val(current.code);
		}catch(e){
	//		alert("Pilih Tempat Lahir dari List yang Ada");
		}
      
    });

    $('#m_criteria_name').blur(function() {
        if($('#m_criteria_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_criteria_id').val(0);
        }
    }); 

function cari_nrk()
	{
		var id = $('#m_client_code').val();
		var idLama = '';
		var d = new Date();
		var thn = d.getFullYear();
		thn = thn.toString().substr(2,2);
		$.ajax({
               type: "GET",
               url: "<?php echo base_url("index.php/personal/cari_nrk"); ?>"+'/'+$('#m_client_id').val(),
               success: function(data)
               {
                 //alert('iiii');			 
                 data = jQuery.parseJSON(data);
				 //alert(data.s);
                 if(data.s != null) 
                 {
				 //alert(data.s.toString().substr(3,2));
				 //alert(data.s.toString().substr(5,4));
				 //alert(thn);
				 if (data.s.toString().substr(3,2) == thn) 
				 {
				 //alert('uuu');
				 idBaru=parseInt(data.s.toString().substr(5,4))+1;
				 //alert(idBaru);
				 if (idBaru < 10) idBaru = '000'+idBaru;
				 else if (idBaru < 100 ) idBaru = '00'+idBaru;
				 else if (idBaru < 1000 ) idBaru = '0'+idBaru;
				 $('#nrk').val(id+thn+idBaru);
				 }
				 else
				 {
				  $('#nrk').val(id+thn+'0001');
				 }
				 }
				 else
				 {
				 //nilaiPerusahaan=$(this).val();
				 //alert('iiii');
				 $('#nrk').val(id+thn+'0001');
				 //alert($('#nrk').val())
				 
				 }
				 //alert($('#nrk').val());
               }
             });
	}

//$('#m_criteria_name').attr('disabled',false);
});
// $('#stop_contract_date').datetimepicker('setStartDate', new Date('2016-08-08'));
// $("#stop_contract_date").data('DateTimePicker').setMaxDate(new Date());
 // $("#stop_contract_date").datepicker('option', 'maxDate', "2014/31/01" );
 //$('#stop_contract_date').datetimepicker('destroy');

//$('#stop_contract_date').datetimepicker('option', 'minDate', new Date());
function pesan()
{
alert('hh');
}
</script>

