<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan User Client Karyawan (User, HRBP, dll)
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>User, HRBP dll</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formUserClient" name="formUserClient">			    		
			    		<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jabatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_structure_position_id" name="m_structure_position_id" placeholder="Kode - Nama Jabatan" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3">Nama Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_id" name="m_client_id" placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                            
		                    <label for="keterangan" class="control-label col-xs-8 col-md-3">Kriteria Kelompok</label>
		            		<div class="col-xs-3">
                                <?php echo $ref_criteria_id; ?>
                            </div>
                            <label for="Akhir user_client" class="control-label col-xs-2 ">TMT</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='tmt_date'>
                                    <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_criteria_name" name="m_criteria_name" placeholder="cari nama kriteria">
		                    	<input type='hidden' class="form-control" name="m_criteria_id" id="m_criteria_id" placeholder="id" value="0"/>
		                    </div>
		                </div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar User, HRBP dll </b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">
						</div>
						<table class="table table-bordered" id="tbl_user_client">
								<thead>
								<tr class="info"><th>Kriteria</th><th>Keterangan</th><th>TMT</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar User, HRBP dll -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadUserClient(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/user_client/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_user_client > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_user_client > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_user_client_id+"'>"+v.ref_criteria_id+"</td>";
											html += "<td>"+v.m_criteria_name+"</td>";
											html += "<td>"+v.tmt_date+"</td>";											
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteUserClient\" onclick=\"$('#act').val('delete'); $('#gp_user_client_id').val("+v.gp_user_client_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editUserClient\" onclick=\"$('#act').val('edit'); $('#gp_user_client_id').val("+v.gp_user_client_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_user_client").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadUserClient();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadUserClient(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadUserClient(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadUserClient(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadUserClient(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_user_client_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/user_client/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/user_client/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadUserClient();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formUserClient").trigger("reset");
		$("#gp_user_client_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formUserClient").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/user_client/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadUserClient();
								$("#formUserClient").trigger("reset");
								$("#gp_user_client_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});

    $('#m_criteria_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/user_client/getCriteria/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_criteria_name').change(function() {
		current = $('#m_criteria_name').typeahead("getActive");
		$('#m_criteria_id').val(current.id);		
    });

	$('#m_criteria_name').blur(function() {
		if($('#m_criteria_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_criteria_id').val(0);
		}
    });	

});
</script>