<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Cuti Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Cuti</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formCuti" name="formCuti">			    		
			    		<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3">Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code" name="m_client_code" placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jenis Cuti</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_leave_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="Tanggal Kontrak" class="control-label col-xs-3 ">Tanggal Kontrak</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" id="start_contract_date" name="start_contract_date" placeholder="Tanggal Kontrak" data-provide="typeahead" disabled="disabled"/>
                                </div>                            
                            <label for="Sampai" class="control-label col-xs-2 ">Sampai</label>
                                 <div class="col-xs-3">
                                    <input class="form-control" type="text" id="stop_contract_date" name="stop_contract_date" placeholder="Sampai" data-provide="typeahead" disabled="disabled"/>
                                </div> 
                        </div>
                        <div class="form-group">
		                    <label for="mulai" class="control-label col-xs-3 ">Dari Tanggal</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='leave_start_date'>
                                    <input type='text' class="form-control" id="leave_start_date" name="leave_start_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="akhir" class="control-label col-xs-2 ">Sampai</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='leave_end_date'>
                                    <input type='text' class="form-control" id="leave_end_date" name="leave_end_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="leave_sum" class="control-label col-xs-3 ">Jumlah Hari</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_sum" name="leave_sum" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="sk_no" class="control-label col-xs-3 ">No. SK</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="sk_no" name="sk_no" placeholder="">
		                    </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
		                <div class="form-group">
		                    <label for="signer" class="control-label col-xs-3 ">Penandatangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="signer" name="signer" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="alasan" class="control-label col-xs-3 ">Alasan Cuti</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="reason_leave" name="reason_leave" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="pengganti" class="control-label col-xs-3 ">Pengganti</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="replacement" name="replacement" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="address" class="control-label col-xs-3 ">Alamat</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="address" name="address" placeholder="">
		                    </div>
		                </div>
                        <div class="form-group">
		                    <label for="handphone" class="control-label col-xs-3 ">Telepon</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="handphone" name="handphone" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
						<br />
						<span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Ketersediaan Cuti</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
		                    <label for="hak cuti" class="control-label col-xs-3 ">Hak Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_entitlements" name="leave_entitlements" placeholder="" disabled="disabled">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Cuti Yang Diambil</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_taken" name="leave_taken" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Hutang Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="debt_leave" name="debt_leave" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Saldo Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="saldo" name="saldo" placeholder="">
		                    </div>
		                </div>
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Cuti</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_cuti">
								<thead>
								<tr class="info"><th>Jenis Cuti</th><th>Mulai</th><th>Sampai</th><th>Hari</th><th>Saldo Cuti</th><th>Alamat Waktu Cuti</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadCuti(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/cuti/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_cuti > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_cuti > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_leave_id+"'>"+v.ref_leave_id+"</td>";
											html += "<td>"+v.leave_start_date+"</td>";
											html += "<td>"+v.leave_end_date+"</td>";
											html += "<td>"+v.leave_sum+"</td>";
											html += "<td>"+v.saldo+"</td>";
											html += "<td>"+v.address+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteCuti\" onclick=\"$('#act').val('delete'); $('#gp_leave_id').val("+v.gp_leave_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editCuti\" onclick=\"$('#act').val('edit'); $('#gp_leave_id').val("+v.gp_leave_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_cuti").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadCuti();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadCuti(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadCuti(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadCuti(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadCuti(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_leave_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/cuti/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/cuti/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadCuti();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formCuti").trigger("reset");
		$("#gp_leave_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formCuti").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/cuti/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadCuti();
								$("#formCuti").trigger("reset");
								$("#gp_leave_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#leave_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()	
	});


		$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#leave_end_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()
	});
 

});
</script>