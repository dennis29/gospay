 <!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Gaji Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Perubahan Gaji Pokok</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formGaji" name="formGaji">			    		
			    		<div class="form-group">
							<label for="gaji" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Unit</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_unit_id" name="m_unit_id" placeholder="" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="keterangan" class="control-label col-xs-8 col-md-3">Jabatan</label>
		            		<div class="col-xs-3">
	                             <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="Nama Jabatan" disabled="disabled">
	                        </div>                            
	                        <label for="Akhir gaji" class="control-label col-xs-2 ">Tanggal Dijabat</label>
	                        <div class="col-xs-3">
	                              <div class='input-group date ' id='join_date'>
                                    <input type='text' class="form-control" id="join_date" name="join_date" disabled="disabled"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
	                        </div>
                        </div>
						<div class="form-group">                            
		                    <label for="Akhir user_client" class="control-label col-xs-3 ">TMT Gaji</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='salary_start_date'>
                                    <input type='text' class="form-control" id="salary_start_date" name="salary_start_date" disabled="disabled" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Gaji Pokok</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">                            
		                    <label for="status" class="control-label col-xs-8 col-md-3">Status Aktif</label>
		            		<div class="col-xs-3">
                                <?php echo $status; ?>
                            </div>		                	
                            
                        </div>
                        <div class="form-group">
		                    <label for="Akhir user_client" class="control-label col-xs-3 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="Akhir gaji" class="control-label col-xs-2 ">No. SK</label>
	                        <div class="col-xs-3">
	                             <input class="form-control" type="text" id="sk_no" name="sk_no" placeholder="" >
	                        </div>                            
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="gaji" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Perubahan Gaji Pokok </b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_gaji">
								<thead>
								<tr class="info"><th>TMT Gaji</th><th>Gaji Pokok</th><th>Status (N/Y)</th><th>Keterangan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadGaji(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/gaji/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_gaji > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_gaji > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_salary_id+"'>"+v.salary_start_date+"</td>";
											html += "<td>"+v.value+"</td>";
											html += "<td>"+v.status+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteGaji\" onclick=\"$('#act').val('delete'); $('#gp_salary_id').val("+v.gp_salary_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editGaji\" onclick=\"$('#act').val('edit'); $('#gp_salary_id').val("+v.gp_salary_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_gaji").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadGaji();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadGaji(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadGaji(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadGaji(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadGaji(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_salary_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/gaji/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/gaji/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadGaji();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formGaji").trigger("reset");
		$("#gp_salary_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formGaji").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/gaji/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadGaji();
								$("#formGaji").trigger("reset");
								$("#gp_salary_id").val(0);
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#salary_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()	
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});



});
</script>