<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Alamat Karyawan
* 
********************************************************************/  -->
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>

<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Alamat Rumah</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formAlamat" name="formAlamat">			    		
			    		<div class="form-group">
							<label for="alamat" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
                        <div class="form-group">
                            <label for="name" class="control-label col-xs-3 ">Jalan</label>
                            <div class="col-xs-8">
                                <textarea class="form-control" id="address" name="address" placeholder="Jalan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
		                	<label for="bpjs" class="control-label col-xs-3">RT</label>
		                    <div class="col-xs-2">
		                        <input class="form-control" type="text" id="rt" name="rt" placeholder="RT">
		                    </div>
		                    <label for="npwp" class="control-label col-xs-1">RW</label>
		                    <div class="col-xs-2">
		                        <input class="form-control" type="text" id="rw" name="rw" placeholder="RW">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="control-label col-xs-3 ">Kelurahan</label>
		                    <div class="col-xs-3">
		                        <?php //echo $sub_district_id; ?>
		                        <input type="text" class="form-control" id="m_sub_district_name" name="m_sub_district_name" placeholder="Kelurahan" data-provide="typeahead" />
		                        <input type="hidden" id="m_sub_district_id" name="m_sub_district_id" value="0" />
		                    </div>
		                    <label for="name" class="control-label col-xs-2 ">Kecamatan</label>
							<div class="col-xs-3">

		                        <input type="text" class="form-control" id="m_district_name" name="m_district_name" placeholder="Kecamatan" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_district_id" name="m_district_id" value="0" />
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="city_id" class="control-label col-xs-3 ">Kota</label>
		                    <div class="col-xs-3">
		                        <input type="text" class="form-control" id="m_city_name" name="m_city_name" placeholder="Kota" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_city_id" name="m_city_id" value="0" />
		                    </div>
		                    <label for="province_id" class="control-label col-xs-2 ">Provinsi</label>
		                    <div class="col-xs-3">
		                        <input type="text" class="form-control" id="m_state_name" name="m_state_name" placeholder="Provinsi" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_state_id" name="m_state_id" value="0" />
		                    </div>
		                </div>                                        
		                <div class="form-group">                                            
		                    <label for="name" class="control-label col-xs-3 ">Kode Pos</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="zipe_code" name="zipe_code" placeholder="Kode Pos" />
		                    </div>                                        
						</div>
		                <div class="form-group" id="hph">     
                            <label for="handphone" class="control-label col-xs-3 ">HP</label>                                            
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="handphone" name="handphone" placeholder="Handphone (pastikan nomor dapat dihubungi)">
                            </div>
                        </div>                                      
                        <div class="form-group" id="hph">
                            <label for="home_phone" class="control-label col-xs-3 ">No. Telepon</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="home_phone" name="home_phone" placeholder="No. Telp Rumah">
                            </div>
                        </div>
		                <br />
		                <hr style="margin-top:10px" />
		                <div class="form-group">
		                    <label for="faskes" class="control-label col-xs-3 ">Fasilitas Kesehatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_faskes_name" name="m_faskes_name" placeholder="cari fasilitas kesehatan">
		                    	<input type='hidden' class="form-control" name="m_faskes_id" id="m_faskes_id" placeholder="id" value="0"/>
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="dokgi" class="control-label col-xs-3 ">Dokter Gigi</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="dokgi" name="dokgi" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="alamat" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Alamat Rumah</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_alamat">
								<thead>
								<tr class="info"><th>Jalan</th><th>RT</th><th>RW</th><th>Kode Pos</th><th>Kelurahan</th><th>Kecamatan</th><th>Kota</th><th>No. Telp</th><th>No. HP</th><th>Keterangan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar Alamat Rumah -->
						    </tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadAlamat(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/alamat/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_alamat > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_alamat > tbody").html("<tr><td colspan = '11' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_address_id+"'>"+v.address+"</td>";
											html += "<td>"+v.rt+"</td>";
											html += "<td>"+v.rw+"</td>";
											html += "<td>"+v.m_sub_district_id+"</td>";
											html += "<td>"+v.m_district_id+"</td>";
											html += "<td>"+v.m_state_id+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.home_phone+"</td>";
											html += "<td>"+v.handphone+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletealamat\" onclick=\"$('#act').val('delete'); $('#gp_address_id').val("+v.gp_address_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editalamat\" onclick=\"$('#act').val('edit'); $('#gp_address_id').val("+v.gp_address_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_alamat").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadAlamat();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadAlamat(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadAlamat(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadAlamat(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadAlamat(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_address_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/alamat/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/alamat/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadAlamat();
						}
					});
	        	}
				}); 
				
 			}
	});

	
	$('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/alamat/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 1,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id').val(resp.m_district_id);
			   $('#m_district_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });


	$("#reset").click(function(){
		$("#formAlamat").trigger("reset");
		$("#gp_address_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formAlamat").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/alamat/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadAlamat();
								$("#formAlamat").trigger("reset");
								$("#gp_address_id").val(0);					
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$('#home_phone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#handphone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});

	$('#m_faskes_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/alamat/getFaskes/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_faskes_name').change(function() {
		current = $('#m_faskes_name').typeahead("getActive");
		$('#m_faskes_id').val(current.id);
		$('#m_faskes_code').val(current.code);
    });

	$('#m_faskes_name').blur(function() {
		if($('#m_faskes_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_faskes_id').val(0);
		}
    });	

});
</script>