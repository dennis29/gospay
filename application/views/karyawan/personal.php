<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-responsive.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-10" id="form-detail" data-title="Contacts">
    <div class="panel panel-default" style="margin-top:0px;">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left">Detail Karyawan </h3>
            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
			<!-- <button type="button" class="btn btn-primary pull-right" id="btnlistkandidat" onClick="$('#content').load('<?php echo base_url('index.php/applicant/getPanel/'); ?>');">
			  List Kandidat
			</button>      -->   
			<div class="btn-group pull-right" id="btnkandidat">
				<?php 
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" id="listkandidatbutton" onClick="$('#content').load('<?php echo base_url('index.php/applicant/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>     
                <button type="button" class="btn btn-danger save" id="reset1" onClick="$('#content').load('<?php echo base_url('index.php/applicant/getDetail/0'); ?>');">
                  Delete
                </button> 
                <button type="button" class="btn btn-success save" id="save1-kandidat">
                  Save
                </button>     
            </div>                  
        </div>
        <div class="panel-body" style="padding:0px;">
        	<!-- <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="dropdown">                
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Personal <b class="caret"></b></a>
                            <ul class="dropdown-menu">                
                                <li class="active"><a data-toggle="tab" href="#detail">Data Diri</a></li>
                                <li><a data-toggle="tab" href="#keluarga">Data Keluarga</a></li>
                                <li><a data-toggle="tab" href="#pendidikan">Pendidikan Formal</a></li>
                                <li><a data-toggle="tab" href="#kursus">Pelatihan/Kursus & Pengalaman Organisasi</a></li>
                                <li><a data-toggle="tab" href="#bahasa">Bahasa Asing & Keahlian Lain</a></li>
                                <li><a data-toggle="tab" href="#infolain">Informasi Lainnya & Alamat Dalam Keadaan Darurat</a></li>
                                <li><a data-toggle="tab" href="#pengalaman">Pengalaman Kerja</a></li>
                            </ul>
                        </li>
						<?php 
							if($form=="")
							{
						?>
                        <li class="dropdown" id="hasil-interview">                
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Interview <b class="caret"></b></a>
                            <ul class="dropdown-menu">                
                                <li><a data-toggle="tab" href="#interview">Hasil Interview</a></li>
                            </ul>
                        </li>
						<?php 
							if($tabs!="")
							{
						?>
                        <li class="tab" id="riwayat-applicant">                
                            <a data-toggle="tab"  href="#riwayat">Riwayat</a>
                        </li>
                        <li class="tab" id="kirim-jo">                
                            <a data-toggle="tab"  href="#kirim">Pengiriman</a>
                        </li>
						<?php
							}
							}
						?>
                    </ul>
                </div> -->
                <div id="form-tab" class="tab-content">
                <div class="tab-pane active" id="detail" data-id="0">
                            <div class="panel-heading" style="background-color:#FFF">
                                <span style="font-size:14px; font-weight:bold">DATA DIRI</span>
                            </div>
                            <div class="panel-body" style="padding:10px;">                                        
                                    <form class="form-horizontal" role="form" id="formKaryawan" name="formKaryawan">
                                        
                                        <div class="form-group">
                                            <label for="nrk" class="control-label col-xs-3 ">NRK</label>
                                            <div class="col-xs-6">
                                                <input class="form-control" type="text" id="nrk" name="nrk" placeholder="xxxxxxxxx" autocomplete="off" disabled="disabled">                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_card_no" class="control-label col-xs-3 <?php echo $vkaryawan["id_card_no"]["required"]; ?>">No. KTP</label>
                                            <div class="col-xs-6">
                                                <input class="form-control" type="text" id="id_card_no" name="id_card_no" placeholder="No. KTP" autocomplete="off">
                                                <input class="form-control" type="hidden" id="gp_personal_id" name="gp_personal_id" value=<?php echo $gp_personal_id; ?> />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["name"]["required"]; ?>">Nama</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="name" name="name" placeholder="Nama" autocomplete="off">
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="birth_place" class="control-label col-xs-3 <?php echo $vkaryawan["birth_place"]["required"]; ?>">Tempat Lahir</label>
                                            <div class="col-xs-3">
                                                <?php //echo $birth_place; ?>
                                                <input class="form-control" type="text" id="birth_place_name" name="birth_place_name" placeholder="Tempat Lahir" data-provide="typeahead" />
                                                <input class="form-control" type="hidden" id="birth_place_temp" placeholder="Tempat Lahir" />
                                                <input class="form-control" type="hidden" id="birth_place" name="birth_place" placeholder="Tempat Lahir" value=0 />
                                            </div>
                                            <label for="birth_date" class="control-label col-xs-2 <?php echo $vkaryawan["birth_date"]["required"]; ?>">Tanggal Lahir</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='birth_date'>
                                                    <input type='text' class="form-control" id="birth_date" name="birth_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender_id" class="control-label col-xs-3 <?php echo $vkaryawan["gender_id"]["required"]; ?>">Jenis Kelamin</label>
                                            <div class="col-xs-3">
                                                <?php echo $gender_id; ?>
                                            </div>
                                            <label for="marital_status_id" class="control-label col-xs-2 <?php echo $vkaryawan["marital_status_id"]["required"]; ?>">Status Perkawinan</label>
                                            <div class="col-xs-3">
                                                <?php echo $marital_status_id; ?>
                                            </div>                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-xs-3 <?php echo $vkaryawan["religion_id"]["required"]; ?>">Agama</label>
                                            <div class="col-xs-3">
                                                <?php echo $religion_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="marital_status_id" class="control-label col-xs-3">Kewarganegaraan</label>
                                            <div class="col-xs-3">
                                                <?php echo $country_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="height" class="control-label col-xs-3 <?php echo $vkaryawan["blood_type_id"]["required"]; ?>">Golongan Darah</label>
                                            <div class="col-xs-3">
                                                <?php echo $blood_type_id; ?>
                                            </div>                                            
                                        </div>                                        
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Alamat KTP</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["idcard_address"]["required"]; ?>">Alamat</label>
                                            <div class="col-xs-9">
                                                <textarea class="form-control" id="idcard_address" name="idcard_address" placeholder="Alamat"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="bpjs" class="control-label col-xs-3">RT</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rt" name="rt" placeholder="RT">
                                            </div>
                                            <label for="npwp" class="control-label col-xs-1">RW</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rw" name="rw" placeholder="RW">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["idcard_sub_district_id"]["required"]; ?>">Kelurahan</label>
                                            <div class="col-xs-4">
                                                <?php //echo $sub_district_id; ?>
                                                <input type="text" class="form-control" id="idcard_sub_district_name" name="idcard_sub_district_name" placeholder="Kelurahan" data-provide="typeahead" />
                                                <input type="hidden" id="idcard_sub_district_id" name="idcard_sub_district_id" value="0" />
                                            </div>
                                            <label for="name" class="control-label col-xs-2 <?php echo $vkaryawan["idcard_district_id"]["required"]; ?>">Kecamatan</label>
											<div class="col-xs-3">

                                                <input type="text" class="form-control" id="idcard_district_name" name="idcard_district_name" placeholder="Kecamatan" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="idcard_district_id" name="idcard_district_id" value="0" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city_id" class="control-label col-xs-3 <?php echo $vkaryawan["idcard_city_id"]["required"]; ?>">Kota</label>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" id="idcard_city_name" name="idcard_city_name" placeholder="Kota" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="idcard_city_id" name="idcard_city_id" value="0" />
                                            </div>
                                            <label for="province_id" class="control-label col-xs-2 <?php echo $vkaryawan["idcard_state_id"]["required"]; ?>">Provinsi</label>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" id="idcard_state_name" name="idcard_state_name" placeholder="Provinsi" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="idcard_state_id" name="idcard_state_id" value="0" />
                                            </div>
                                        </div>                                        
                                        <div class="form-group">                                            
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["idcard_zipcode"]["required"]; ?>">Kode Pos</label>
                                            <div class="col-xs-4">
                                                <input class="form-control" type="text" id="zipe_code" name="zipe_code" placeholder="Kode Pos" />
                                            </div>                                        
										</div>
                                        <div class="form-group" id="hph">
                                            <label for="home_phone" class="control-label col-xs-3 <?php echo $vkaryawan["home_phone"]["required"]; ?>">HP</label>                                            
                                            <div class="col-xs-9">
                                                <input class="form-control" type="text" id="handphone" name="handphone" placeholder="Handphone (pastikan nomor dapat dihubungi)">
                                            </div>
                                        </div>                                      
                                        <div class="form-group" id="hph">
                                            <label for="home_phone" class="control-label col-xs-3 <?php echo $vkaryawan["home_phone"]["required"]; ?>">No. Telepon</label>
                                            <div class="col-xs-9">
                                                <input class="form-control" type="text" id="home_phone" name="home_phone" placeholder="No. Telp Rumah">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="control-label col-xs-3">Email</label>
                                            <div class="col-xs-9">
                                                <input class="form-control" type="text" id="email_address" name="email_address" placeholder="Email">
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="bpjs" class="control-label col-xs-3 ">Fasilitas Kesehatan</label>
                                            <div class="col-xs-9">
                                                <input class="form-control" type="text" id="m_faskes_id" name="m_faskes_id" placeholder="cari fasilitas kesehatan">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="npwp" class="control-label col-xs-3 ">Dokter Gigi</label>
                                            <div class="col-xs-9">
                                                <input class="form-control" type="text" id="dokgi" name="dokgi" placeholder="">
                                            </div>
                                        </div>
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Perusahaan Karyawan</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="name company" class="control-label col-xs-3 <?php echo $vkaryawan["m_company_id"]["required"]; ?>">Nama Perusahaan</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_company_id" name="m_company_id" placeholder="Nama Perusahaan" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="join_date" class="control-label col-xs-3 <?php echo $vkaryawan["join_date"]["required"]; ?>">Tanggal Diterima</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='join_date'>
                                                    <input type='text' class="form-control" id="join_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_join" class="control-label col-xs-2 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_join" name="sk_join" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="start_permanen_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_permanen_date"]["required"]; ?>">Tanggal Permanen</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='start_permanen_date'>
                                                    <input type='text' class="form-control" id="start_permanen_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_join" class="control-label col-xs-2 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_join" name="sk_join" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="status karyawan" class="control-label col-xs-3 <?php echo $vkaryawan["ref_status_kerja_id"]["required"]; ?>">Status Karyawan</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="ref_status_kerja_id" name="ref_status_kerja_id" placeholder="Status Karyawan" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="stop_date" class="control-label col-xs-3 ">Tanggal Berhenti</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='stop_date'>
                                                    <input type='text' class="form-control" id="stop_date" name="stop_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_branch_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_branch_id"]["required"]; ?>">Kantor Cabang</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_branch_id" name="m_branch_id" placeholder="cari kantor cabang" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_client_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_client_id"]["required"]; ?>">Client</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_client_id" name="m_client_id" placeholder="cari client" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_location_id" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_location_id" name="m_location_id" placeholder="pilih lokasi kerja" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="wilayah" class="control-label col-xs-3 ">Wilayah</label>
                                            <div class="col-xs-3">
                                                <div class="form-control" id='wil_name'>
                                                    <input class="form-control" type="text" id="wil_name" name="wil_name" placeholder="">
                                                </div>
                                            </div>
                                            <label for="area" class="control-label col-xs-2 ">Area</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="area_name" name="area_name" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="unit kerja" class="control-label col-xs-3 <?php echo $vkaryawan["m_unit_id"]["required"]; ?>">Unit Kerja</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_unit_id" name="m_unit_id" placeholder="pilih unit kerja" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jabatan" class="control-label col-xs-3 <?php echo $vkaryawan["m_structure_position_id"]["required"]; ?>">Jabatan</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="m_structure_position_id" name="m_structure_position_id" placeholder="pilih jabatan" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ref_team_work_id" class="control-label col-xs-3 ">Kelompok Kerja</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="ref_team_work_id" name="ref_team_work_id" placeholder="pilih kelompok kerja" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="join_date" class="control-label col-xs-3 ">Tanggal Join</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='join_date'>
                                                    <input type='text' class="form-control" id="join_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="sk_join" class="control-label col-xs-3 ">No PKWT</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_join" name="sk_join" placeholder="No PKWT">
                                            </div> 
                                            <label for="join_date" class="control-label col-xs-2 <?php echo $vkaryawan["join_date"]["required"]; ?>">Tanggal PKWT</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='join_date'>
                                                    <input type='text' class="form-control" id="join_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="start_contract_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_contract_date"]["required"]; ?>">Awal Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='start_contract_date'>
                                                    <input type='text' class="form-control" id="start_contract_date" name="start_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>   
                                            <label for="stop_contract_date" class="control-label col-xs-2 <?php echo $vkaryawan["stop_contract_date"]["required"]; ?>">Akhir Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='stop_contract_date'>
                                                    <input type='text' class="form-control" id="stop_contract_date" name="stop_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["idcard_zipcode"]["required"]; ?>">Status Kontrak</label>
                                            <div class="col-xs-3">
                                                <input class="form-control" type="text" id="zipe_code" name="zipe_code" placeholder="Kode Pos" />
                                            </div>                                        
                                        </div>
                                        <div class="form-group">
                                            <label for="job_no" class="control-label col-xs-3 ">No Penugasan</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="job_no" name="job_no" placeholder="No Penugasan">
                                            </div> 
                                            <label for="job_date" class="control-label col-xs-2 ">Tgl Penugasan</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date ' id='job_date'>
                                                    <input type='text' class="form-control" id="job_date" name="job_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <!-- <div class="form-group">
                                            <label for="bpjs" class="control-label col-xs-3 ">No. BPJS</label>
                                            <div class="col-xs-3">
                                                <input class="form-control" type="text" id="bpjs" name="bpjs" placeholder="Nomor BPJS jika ada">
                                            </div>
                                            <label for="npwp" class="control-label col-xs-2 ">No. NPWP</label>
	                                        <div class="col-xs-3">
												<input class="form-control" type="text" id="npwp" name="npwp" placeholder="Nomor NPWP jika ada">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="height" class="control-label col-xs-3 <?php echo $vkaryawan["blood_type_id"]["required"]; ?>">Gol. Darah</label>
                                            <div class="col-xs-3">
                                                <?php echo $blood_type_id; ?>
                                            </div>                                            
                                        </div>  -->
                                    </form>
                              </div>
                </div>

                <div class="tab-pane" id="keluarga" data-id="1">
                    <div class="panel-heading" style="background-color:#CCC">
                            <span style="font-size:14px; font-weight:bold">DATA KELUARGA</span>
                    </div>
                    <div class="panel-body" style="padding:10px;">
                        <form class="form-horizontal" role="form" id="formFamily" name="formFamily">                                                       
                        <div class="table">
                          <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Data Orang Tua dan Diri Sendiri</span>
                            <hr style="margin-top:10px" />
                            <input class="form-control" type="hidden" id="fj_family_id" name="fj_family_id" value="0">
                            <table class="table-condensed">
                                <thead>
                                    <tr class="active" align="center">
                                        <td><strong>Nama</strong></td>
                                        <td class="col-md-2"><strong>Tempat Lahir</strong></td>
                                        <td class="col-md-2"><strong>Tanggal Lahir</strong></td>
                                        <td class="col-md-2"><strong>Pendidikan</strong></td>
                                        <td class="col-md-2"><strong>Pekerjaan</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 <?php echo $vfamily["father_name"]["required"]; ?>">Bapak</label>
                                                <div class="col-md-9">
                                                    <input type="text" class="form-control" id="father_name" name="father_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="father_place_birth_name" name="father_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="father_place_birth" name="father_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='father_dob' name='father_dob'>
                                                        <input type='text' class="form-control" id="father_dob" name="father_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $father_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="father_occupation" name="father_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-3 <?php echo $vfamily["mother_name"]["required"]; ?>">Ibu</label>
                                                <div class="col-xs-9">
                                                    <input type="text" class="form-control" id="mother_name" name="mother_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="mother_place_birth_name" name="mother_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="mother_place_birth" name="mother_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='mother_dob' name='mother_dob'>
                                                        <input type='text' class="form-control" id="mother_dob" name="mother_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $mother_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="mother_occupation" name="mother_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <label class="control-label col-md-2 <?php echo $vfamily["family_order"]["required"]; ?>">Anak ke</label>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control" id="family_order" name="family_order">
                                            </div>
                                            <div class="col-md-9">
                                                <label class="control-label col-md-1">Dari</label>
                                                <div class="col-md-2"><input type="text" class="form-control" id="number_of_siblings" name="number_of_siblings" value="1" /></div>
                                                <label class="control-label col-md-1">Bersaudara</label>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br  />
                        <hr style="margin-top:10px" />
                        <div class="table">
                          <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Data Keluarga (bila sudah menikah)</span>
                            <hr style="margin-top:10px" />
                            <table class="table-condensed">
                                <thead>
                                    <tr class="active" align="center">
                                        <td><strong>Nama</strong></td>
                                        <td><strong>Tempat Lahir</strong></td>
                                        <td class="col-md-2"><strong>Tanggal Lahir</strong></td>
                                        <td><strong>Pendidikan</strong></td>
                                        <td><strong>Pekerjaan</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-4">Pasangan</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="spouse_name" name="spouse_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="spouse_place_birth_name" name="spouse_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="spouse_place_birth" name="spouse_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='spouse_dob' name='spouse_dob'>
                                                        <input type='text' class="form-control" id="spouse_dob" name="spouse_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $spouse_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="spouse_occupation" name="spouse_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-4">Anak 1</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="child_1_name" name="child_1_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_1_place_birth_name" name="child_1_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="child_1_place_birth" name="child_1_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='child_1_dob' name='child_1_dob'>
                                                        <input type='text' class="form-control" id="child_1_dob" name="child_1_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $child_1_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_1_occupation" name="child_1_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-4">Anak 2</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="child_2_name" name="child_2_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_2_place_birth_name" name="child_2_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="child_2_place_birth" name="child_2_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='child_2_dob' name='child_2_dob'>
                                                        <input type='text' class="form-control" id="child_2_dob" name="child_2_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $child_2_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_2_occupation" name="child_2_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label col-xs-4">Anak 3</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="child_3_name" name="child_3_name">
                                                </div>
                                             </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_3_place_birth_name" name="child_3_place_birth_name" data-provide="typeahead"/>
                                                    <input type="hidden" class="form-control" id="child_3_place_birth" name="child_3_place_birth" />
                                                </div>
                                            </div>                                      
                                        </td>
                                        <td>                                            
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class='input-group date' id='child_3_dob' name='child_3_dob'>
                                                        <input type='text' class="form-control" id="child_3_dob" name="child_3_dob" />
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>                                                    
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <?php echo $child_3_education; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td>                                                
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" id="child_3_occupation" name="child_3_occupation" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>                                          
                       </form>
                    </div>
                </div>
                
               
             </div>
        </div>

        <div class="panel-footer" id="footerApplicant">
			<div class="btn-group pull-left _tabs_navigation" data-toggle="buttons-radio" id="btnNavigation">
				<a class="btn btn btn-info" href="#" id="btnPrev">
					<i class="fa fa-backward fa-fw"></i> 
					<span>Prev</span>
				</a>
				<a class="btn btn btn-info" href="#" id="btnNext">
					<i class="fa fa-forward fa-fw"></i> 
					<span>Next</span>
				</a>
			</div>
            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory1">(tanda * wajib diisi)</h3>
            <div class="btn-group pull-right" id="btnkandidat1">
				<?php
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" onClick="$('#content').load('<?php echo base_url('index.php/applicant/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>        
                
                <button type="button" class="btn btn-danger" id="reset" onClick="$('#content').load('<?php echo base_url('index.php/applicant/getDetail/0'); ?>');">
                  Delete
                </button>
                <button type="button" class="btn btn-success" id="save-kandidat">
                  Save
                </button>    
             </div>                    
        </div>
    </div>
</div>
</div>
</div>

<script>
<?php
foreach($akses as $k=>$v)
{
?>
	if($("#<?=$v?>").is(":visible"))
	{
		$("#<?=$v?>").hide();
		if($("#<?=$v?>").is(":visible"))
			$("#<?=$v?>").prop("visibility","hidden");
	}					
<?php
}
?>

$(function()
{
	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	  loadTab();
	});	

	$('.nav-tabs > .dropdown > .dropdown-menu ').click(function(){
		$('.nav li').removeClass('active');
	});	

	var finish = 0;
	var timeout;
	

    $('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/applicant/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/applicant/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id').val(resp.m_district_id);
			   $('#m_district_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	
/*	$('#m_sub_district_name').change(function() {
		var current = $('#m_sub_district_name').typeahead("getActive");
		$('#m_sub_district_id').val(current.id);
		$.get('<?php echo base_url('index.php/applicant/getAddress/'); ?>/'+current.m_district_id, function (resp) {
		   resp = jQuery.parseJSON(resp);
		   $('#m_district_id').val(resp.m_district_id);
		   $('#m_district_name').val(resp.m_district_name);
		   $('#m_city_name').val(resp.m_city_name);
		   $('#m_city_id').val(resp.m_city_id);
		   $('#m_province_name').val(resp.m_province_name);
		   $('#m_state_id').val(resp.m_state_id);
		   $('#m_state_name').val(resp.m_state_name);
		});
    });
*/
    $('#birth_place_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3
    });

	$('#birth_place_name').change(function() {
		var current = $('#birth_place_name').typeahead("getActive");
		try{
			$('#birth_place').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
    });
	
	
	$('#birth_place_name').blur(function(){
			$('#birth_place_name').trigger("change");
	});
	
	 $('#father_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#father_place_birth_name').change(function() {
		var current = $('#father_place_birth_name').typeahead("getActive");
		try{
			$('#father_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#father_place_birth_name').val("");
			$('#father_place_birth').val(0);
		}
    });

	$('#father_place_birth_name').focusout(function() {
		if($('#father_place_birth_name').val()==""){
			$('#father_place_birth').val(0);
		}
    });	

	 $('#mother_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#mother_place_birth_name').change(function() {
		var current = $('#mother_place_birth_name').typeahead("getActive");
		try{
			$('#mother_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#mother_place_birth_name').val("");
			$('#mother_place_birth').val(0);
		}
    });

	$('#mother_place_birth_name').focusout(function() {
		if($('#mother_place_birth_name').val()==""){
			$('#mother_place_birth').val(0);
		}
    });	
	
	$('#spouse_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#spouse_place_birth_name').change(function() {
		var current = $('#spouse_place_birth_name').typeahead("getActive");
		try{
			$('#spouse_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#spouse_place_birth_name').val("");
			$('#spouse_place_birth').val(0);
		}
    });

	$('#spouse_place_birth_name').focusout(function() {
		if($('#spouse_place_birth_name').val()==""){
			$('#spouse_place_birth').val(0);
		}
    });	
	
	$('#child_1_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#child_1_place_birth_name').change(function() {
		var current = $('#child_1_place_birth_name').typeahead("getActive");
		try{
			$('#child_1_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#child_1_place_birth_name').val("");
			$('#child_1_place_birth').val(0);
		}
    });

	$('#child_1_place_birth_name').focusout(function() {
		if($('#child_1_place_birth_name').val()==""){
			$('#child_1_place_birth').val(0);
		}
    });	
	
	$('#child_2_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#child_2_place_birth_name').change(function() {
		var current = $('#child_2_place_birth_name').typeahead("getActive");
		try{
			$('#child_2_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#child_2_place_birth_name').val("");
			$('#child_2_place_birth').val(0);
		}
    });

	$('#child_2_place_birth_name').focusout(function() {
		if($('#child_2_place_birth_name').val()==""){
			$('#child_2_place_birth').val(0);
		}
    });	
	
	$('#child_3_place_birth_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/applicant/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });

	$('#child_3_place_birth_name').change(function() {
		var current = $('#child_3_place_birth_name').typeahead("getActive");
		try{
			$('#child_3_place_birth').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
			$('#child_3_place_birth_name').val("");
			$('#child_3_place_birth').val(0);
		}
    });

	$('#child_3_place_birth_name').focusout(function() {
		if($('#child_3_place_birth_name').val()==""){
			$('#child_3_place_birth').val(0);
		}
    });	


    $('#idcard_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/applicant/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#idcard_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/applicant/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#idcard_district_id').val(resp.m_district_id);
			   $('#idcard_district_name').val(resp.m_district_name);
			   $('#idcard_city_name').val(resp.m_city_name);
			   $('#idcard_city_id').val(resp.m_city_id);
			   $('#idcard_province_name').val(resp.m_province_name);
			   $('#idcard_state_id').val(resp.m_state_id);
			   $('#idcard_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	$("#weight").ForceNumericOnly();
	$("#height").ForceNumericOnly();

	$('#s1_gpa').mask('0.00',{
		onChange : function(cep){
			if($('s1_gpa').val()>4)
				$('#s1_gpa').val('4.00');			
		}
	});

	$('#diploma_gpa').mask('0.00',{
		onChange : function(cep){
			if($('diploma_gpa').val()>4)
				$('#diploma_gpa').val('4.00');			
		}
	});


/*	$('#idcard_sub_district_name').change(function() {
		var current = $('#idcard_sub_district_name').typeahead("getActive");
		$('#idcard_sub_district_id').val(current.id);
		$.get('<?php echo base_url('index.php/applicant/getAddress/'); ?>/'+current.m_district_id, function (resp) {
		   resp = jQuery.parseJSON(resp);
		   $('#idcard_district_id').val(resp.m_district_id);
		   $('#idcard_district_name').val(resp.m_district_name);
		   $('#idcard_city_name').val(resp.m_city_name);
		   $('#idcard_city_id').val(resp.m_city_id);
		   $('#idcard_province_name').val(resp.m_province_name);
		   $('#idcard_state_id').val(resp.m_state_id);
		   $('#idcard_state_name').val(resp.m_state_name);
		});
    });
*/		
	$('#expected_salary_name').autoNumeric('init',{aSep: '.', aDec: ',', aSign: 'Rp ',aPad: false, lZero : 'deny', wEmpty : 'sign'});	
	$("#expected_salary_name").keyup(function () {
       	$("#expected_salary").val($("#expected_salary_name").autoNumeric("get"));
    });	

	$("#expected_salary_name").keydown(function() {
       	if($("#expected_salary_name").val()=='0')
			$("#expected_salary_name").val('');
    });			
	
	$("input[type=text]").css('text-transform','uppercase');	
	
	$("textarea").css('text-transform','uppercase');
	$('#oversea_city').tokenfield({
	  autocomplete: {
		source: "<?php echo base_url('index.php/applicant/getOversea/auto'); ?>",
		delay: 500,
		minLength : 3
	  },
	  createTokensOnBlur : true
	});


	$("input").prop("autocomplete","off");
	
	function loadApplicant()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/applicant/getApplicant/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									if(k!="FILE_PHOTO" && v!=null)
									{
										if(k.search("_DATE")>-1) 
										{
											v = view_date(v, 1);	// 1 = ubah ke format dd-mm-yyyy
											$("#"+k.toLowerCase()+"[type=text]").val(v);
										}
										else
										if(k.search("PHONE")>-1 || k.search("OVERSEA_CITY")>-1) 
										{
											$("#"+k.toLowerCase()).tokenfield('setTokens', v);
										}
										else
										if(k=="JO_NO") 
										{
											$("#applicant_jo_no").val(v);
											$("#history_jo").html("Nomor JO: "+v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}
									}
									else
									if(k=="FILE_PHOTO")
									{
										if(v==null || v=="")
										{
											$("#file_photo_preview").css("display","none");										
										}
										else
										{
											$("#file_photo_preview").attr("src","<?php echo base_url('asset/uploads/'); ?>/"+v);
										}
									}
																										
								});
						   }
					   }
				});				
			}
			else
			{
				$("#file_photo_preview").css("display","none");	
			}
	}
	
	function loadEducation()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/education/getEducation/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									$("#"+k.toLowerCase()).val(v);
																		
								});
						   }
					   }
				});				
			}
	}

	function loadFamily()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/family/getFamily/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
										if(k.search("_DOB")>-1) 
										{
											v = view_date(v, 1);	// 1 = ubah ke format dd-mm-yyyy
											$("#"+k.toLowerCase()+"[type=text]").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}
								});
						   }
					   }
				});				
			}
	}

	function loadOthers()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/others/getOthers/'); ?>/"+id,
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									if(v)
									{
										if($("#"+k.toLowerCase()).is(":checkbox"))
										{
											if(v=="Y")
											{
												$("#"+k.toLowerCase()).prop("checked",true);										
											}
										}
										else
										if(k.search("_DATE")>-1) 
										{
											v = view_date(v, 1);	// 1 = ubah ke format dd-mm-yyyy
											$("#"+k.toLowerCase()+"[type=text]").val(v);
										}
										else
										if(k=="EXPECTED_SALARY") 
										{
											$("#expected_salary").val(v);
											$("#expected_salary_name").autoNumeric('set', v);
										}
										else
										if(k=="AVAILABLE_ID") 
										{
											$("#available_id").val(v);
											if(v==26)
											{
												$("#available_date").show();
											}
											else
											{
												$("#available_date").hide();
											}
										}
										else
										{
											if(k=="OVERSEA_CITY")
											{
												$("#"+k.toLowerCase()).tokenfield('setTokens', v);
												//console.log(v);											
											}
											else
											{
												$("#"+k.toLowerCase()).val(v);
											}
										}
									}
								});
						   }
					   }
				});				
			}
	}

	function loadInterview()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/interview/getInterviewResult/'); ?>/"+id,
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									if($("#"+k.toLowerCase()).is(":radio"))
									{
										if(v!=0 || v!="")
										$('.'+k+'_'+v).prop('checked', true);
									}
									else
									if(k=="PASSION" || k=="RECOMMENDATION")
									{
										if(v!="" && v!=null)
										{
											v = v.split(",");
											$("#"+k.toLowerCase()).select2("val",v);
										}
										//console.log(v);											
									}
									else
									{
										$("#"+k.toLowerCase()).val(v);
									}
								});

						   }
						   
					   }
				});				
			}
	}

	function loadSkill()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/skill/getSkill/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									if($("#"+k.toLowerCase()).is(":radio"))
									{
										if(v!=0 || v!="")
										$('.'+k+'_'+v).prop('checked', true);
									}
									else
									{
										$("#"+k.toLowerCase()).val(v);
									}
								});
						   }
					   }
				});				
			}
	}


	function loadEmergency()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/emergency/getEmergency/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									if(k.search("_NUMBER")>-1) 
									{
										$("#"+k.toLowerCase()).tokenfield('setTokens', v);
									}
									else
									{
										$("#"+k.toLowerCase()).val(v);
									}
								});
						   }
					   }
				});				
			}
	}

	loadApplicant();
	loadEducation();
	loadFamily();
	loadOthers();
	loadSkill();
	loadEmergency();
	


/*    var tabIndex;
    var tabs = $('a[data-toggle="tab"]');
    tabs.on('shown', function(e) {
        tabIndex = $(e.target).closest('li').index();
		console.log(tabIndex);
    }).eq(0).trigger('shown');

    $('._tabs_navigation').on('click', 'a', function() {
		
        var index = tabIndex + ($(this).index() ? 1 : -1);
        if (index >= 0 && index < tabs.length) {
            tabs.eq(index).tab('show');
			console.log(index);
        }
        return false;
    });
*/

$('#btnNext').click(function(){
//		setUpper();
//		$("#formKaryawan").submit();
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').next('li').find('a').trigger('click');
});

$('#btnPrev').click(function(){
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').prev('li').find('a').trigger('click');
});

	function setUpper()
	{
		$("input[type=text]").val(function(){
			return this.value.toUpperCase();	
		});
		$("textarea").val(function(){
			return this.value.toUpperCase();	
		});

	}

	function saveFamily(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/family/save"); ?>",
			data: $("#formFamily").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
					$("#fj_family_id").val(data.id);
			//		console.log($("#fj_family_id").val());
			   }
			   else
			   {
					alert(data.message);
					$('a[href="#keluarga"]').trigger('click');   
			   }
			}
		});
	}

	function saveEducation(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/education/save"); ?>",
			data: $("#formEducation").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
					$("#fj_education_id").val(data.id);
			//		console.log($("#fj_education_id").val());
			   }
			}
		});
	}

	function saveEmergency(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/emergency/save"); ?>",
			data: $("#formEmergency").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {

					$("#fj_emergency_id").val(data.id);
				//	console.log($("#fj_emergency_id").val());
			   }
			}
		});
	}
	
	function saveInterview(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/interview/save"); ?>",
			data: $("#formInterview").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
					$("#fj_interview_result_id").val(data.id);
				//	console.log($("#fj_interview_result_id").val());
			   }
			}
		});
	}

	function saveOthers(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/others/save"); ?>",
			data: $("#formOthers").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
					$("#fj_others_id").val(data.id);
			//		console.log($("#fj_others_id").val());
			   }
			}
		});
	}

	function saveSkill(id)
	{
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url("index.php/skill/save"); ?>",
			data: $("#formSkill").serialize() + "&gp_personal_id="+id,
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
					$("#fj_skill_id").val(data.id);
			//		console.log($("#fj_others_id").val());
			   }
			}
		});
	}

	$("#formKaryawan").ajaxForm({
			type: 'POST',
			url: "<?php echo base_url("index.php/applicant/save"); ?>",
			data: $(this).serialize(),
			success: function(data)
			{
			   data = jQuery.parseJSON(data);
			   if(data.success) 
			   {
//				  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').next('li').find('a').trigger('click');
					$("#gp_personal_id").val(data.id);
					//alert("Data Applicant Berhasil Disimpan");
//					console.log($("#gp_personal_id").val());
				//	return data.id;
					saveFamily(data.id);
					saveEducation(data.id);
					saveEmergency(data.id);
					saveOthers(data.id);
					saveInterview(data.id);		
					saveSkill(data.id);		
					AddCourses(data.id);
					AddOrganization(data.id);
					AddLanguage(data.id);
					AddExperience(data.id);
					if(finish==1)
					{
						if($("#listkandidatbutton").length)
						{
							$('#content').load('<?php echo base_url('index.php/applicant/getDetail/0'); ?>');
							finish = 0;
						}
						else
						{
						    $("#formKaryawan").trigger("reset");
						    $("#formFamily").trigger("reset");
 							$("#formEducation").trigger("reset");
						    $("#formCourse").trigger("reset");
						    $("#formOrganization").trigger("reset");
						    $("#formLanguage").trigger("reset");
						    $("#formSkill").trigger("reset");
						    $("#formOthers").trigger("reset");
						    $("#formEmergency").trigger("reset");
						    $("#formExperience").trigger("reset");
						    $("#formInterview").trigger("reset");
							$("input[type=hidden]").val(0);
							$('a[href="#detail"]').trigger('click'); 
//							$('#home_phone').tokenfield('setTokens', '', false, false);
/*							$('#home_phone').remove(); 
							$('#handphone').tokenfield('destroy'); 
							$('#emergency_number').tokenfield('destroy'); 
							$('#emergency_number2').tokenfield('destroy'); 
*/							$("#tbl_kursus > tbody").empty();							
							$("#tbl_organisasi > tbody").empty();							
							$("#tbl_bahasa > tbody").empty();							
							$("#tbl_pengalaman > tbody").empty();							
							finish = 0;
//							window.location = location.href + '?upd=' + 123456;
//							window.location.reload(true);
//							console.log("ok");
//							$('#content').load('<?php echo base_url('index.php/applicant/getPanel'); ?>');
						}						
					}
			   }
/*				else
				{
					alert(data.message);	
				}
*/			}
	});


	$("#save-kandidat").click(function(){
/*			$.when($("#formKaryawan").submit()).then(function(rs){
			console.log(rs.find('input[id="gp_personal_id"]').val());
			//saveFamily($("#gp_personal_id").val());
		saveEducation(rs.id);
			saveEmergency(rs.id);
			saveOthers(rs.id);
			saveInterview(rs.id);		
			saveSkill(rs.id);		
		});	 */				
			setUpper();

			$("#formKaryawan").submit();
	});

	$("#save1-kandidat").click(function(){
			setUpper();

			$("#formKaryawan").submit();
	});


	$("#finish").click(function(e){
		var konfirmasi = "Mohon pastikan anda sudah mengisi data dengan lengkap terutama : <br />1. Pengalaman Kerja <br />2. Bahasa dan Keahlian <br />3. Pengalaman Organisasi <br />4. Pelatihan dan Kursus"
		bootbox.confirm(konfirmasi, function(result) {
			if (result == true) {
				finish = 1;
				$("#save1-kandidat").trigger("click");
	//				$("#reset").trigger("click");
						//alert("Data Applicant Berhasil Disimpan");
				//});				
			}
		});	
	});	
	
	$("#reset").click(function(){
		
		
		/*$("#formKaryawan").get(0).reset();
		$("#formFamily").get(0).reset();
		$("#formEducation").get(0).reset();
		$("#formCourse").get(0).reset();
		$("#formOrganization").get(0).reset();
		$("#formLanguage").get(0).reset();
		$("#formSkill").get(0).reset();
		$("#formOthers").get(0).reset();
		$("#formEmergency").get(0).reset();
		$("#formInterview").get(0).reset();*/
	});

	$("#reset1").click(function(){
		/*$("#formKaryawan").get(0).reset();
		$("#formFamily").get(0).reset();
		$("#formEducation").get(0).reset();
		$("#formCourse").get(0).reset();
		$("#formOrganization").get(0).reset();
		$("#formLanguage").get(0).reset();
		$("#formSkill").get(0).reset();
		$("#formOthers").get(0).reset();
		$("#formEmergency").get(0).reset();
		$("#formInterview").get(0).reset();*/
	});
	
	//kursus dan pelatihan
	function loadCourses()
	{
			var id = $("#gp_personal_id").val();
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/course/getList/'); ?>/"+id,
							beforeSend: function () { $("#loading").show(); },
							// hides the loader after completion of request, whether successfull or failor.             
							complete: function () { $("#loading").hide(); },      
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_kursus > tbody").empty();
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.FJ_COURSES_ID+"'>"+v.INSTITUTION+"</td>";
											html += "<td>"+v.COURSE_FROM_MONTH+" "+v.COURSE_FROM_YEAR+"</td>";
											html += "<td>"+v.COURSE_TO_MONTH+" "+v.COURSE_TO_YEAR+"</td>";
											html += "<td>"+v.COURSE+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteCourse\" onclick=\"$('#act').val('delete'); $('#fj_courses_id').val("+v.FJ_COURSES_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editCourse\" onclick=\"$('#act').val('edit'); $('#fj_courses_id').val("+v.FJ_COURSES_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_kursus").append(html);				   							
									});
							   }
						   }
						 });										
		
	}

	$("#fj_courses_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/course/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/course/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadCourses();
					    	$(":input","#formCourse").val("")	 		
						}
					});
	        	}
 			}
	});

	loadCourses();

	$("#add_course").click(function(){
	//	AddCourses($("#gp_personal_id").val());	
		setUpper();
		if ($("#gp_personal_id").val()==0)
		{
			$.when($("#save-kandidat").trigger("click")).done(function(rs){
				//AddCourses($("#gp_personal_id").val());
			});					
		}
		else
		{
			AddCourses($("#gp_personal_id").val());
			$("#formCourse").trigger("reset");	 		
		}
	});

	$("#cancel_course").click(function(){
		   $(":input","#formCourse").val("");	 		
			$("#formCourse").trigger("reset");	 		
	});


	function AddCourses(id)
	{
		if($("#institution").val()!="")
		{
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url("index.php/course/save"); ?>",
				data: $("#formCourse").serialize() + "&gp_personal_id="+id,
				success: function(data)
				{
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						$("#formCourse").trigger("reset");
						loadCourses();
				   }
				}
			});
		}
	}


	//pengalaman organisasi
	function loadOrganization()
	{
			var id = $("#gp_personal_id").val();
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/organization/getList/'); ?>/"+id,
							beforeSend: function () { $("#loading").show(); },
							// hides the loader after completion of request, whether successfull or failor.             
							complete: function () { $("#loading").hide(); },      
						   success: function(data)
						   {							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_organisasi > tbody").empty();
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.FJ_ORGANIZATION_ID+"'>"+v.ORGANIZATION+"</td>";
											html += "<td>"+v.ORGANIZATION_FROM_MONTH+" "+v.ORGANIZATION_FROM_YEAR+"</td>";
											html += "<td>"+v.ORGANIZATION_TO_MONTH+" "+v.ORGANIZATION_TO_YEAR+"</td>";
											html += "<td>"+v.POSITION+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteOrganization\" onclick=\"$('#act').val('delete'); $('#fj_organization_id').val("+v.FJ_ORGANIZATION_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editOrganization\" onclick=\"$('#act').val('edit'); $('#fj_organization_id').val("+v.FJ_ORGANIZATION_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_organisasi").append(html);				   							
									});
							   }
						   }
						 });										
		
	}

	$("#fj_organization_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/organization/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										$("#"+k.toLowerCase()).val(v);																			
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/organization/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadOrganization();
					    	$("#formOrganization").trigger("reset");	 		
						}
					});
	        	}
 			}
	});

	loadOrganization();

	$("#add_org").click(function(){
		setUpper();
		if ($("#gp_personal_id").val()==0)
		{
			$.when($("#save-kandidat").trigger("click")).done(function(rs){
//			AddOrganization($("#gp_personal_id").val());			
			});					
		}
		else
		{
			AddOrganization($("#gp_personal_id").val());
			$(":input","#formOrganization").val("");	 		
	    	$("#formOrganization").trigger("reset");	 		
		}
	});

	$("#cancel_org").click(function(){
		   $(":input","#formOrganization").val("");	 		
    	   $("#formOrganization").trigger("reset");	 		
	});


	function AddOrganization(id)
	{
		if($("#organization").val()!="")
		{
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url("index.php/organization/save"); ?>",
				data: $("#formOrganization").serialize() + "&gp_personal_id="+id,
				success: function(data)
				{
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						loadOrganization();
						$("#formOrganization").trigger("reset");				   }
				}
			});
		}
	}

	//bahasa asing
	function loadLanguage()
	{
		var id = $("#gp_personal_id").val();
		$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/language/getList/'); ?>/"+id,
					beforeSend: function () { $("#loading").show(); },
					// hides the loader after completion of request, whether successfull or failor.             
					complete: function () { $("#loading").hide(); },      
				   success: function(data)
				   {							   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#tbl_bahasa > tbody").empty();
							$.each(data.data, function(k,v){
								var html = "<tr><td><input type=hidden value='"+v.FJ_LANGUAGE_ID+"'>"+v.LANGUAGE_ID+"</td>";
									html += "<td>"+v.LANGUAGE_LEVEL_ID+"</td>";
									html += "<td><div class='btn-group'>";
									html += "<button class='btn btn-small btn-danger' id=\"deleteLanguage\" onclick=\"$('#act_language').val('delete'); $('#fj_language_id').val("+v.FJ_LANGUAGE_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
									html += "<button class='btn btn-small btn-info' id=\"editLanguage\" onclick=\"$('#act_language').val('edit'); $('#fj_language_id').val("+v.FJ_LANGUAGE_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
									html += "</div>";
									html += "</td></tr>";
								$("#tbl_bahasa").append(html);				   							
							});
					   }
				   }
				 });			
	}

	$("#fj_language_id").change(function(){
			var act = $("#act_language").val();
			var id = $(this).val();	
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/language/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										$("#"+k.toLowerCase()).val(v);
																			
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/language/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadLanguage();
					    	$("#formLanguage").trigger("reset");	 		
						}
					});
	        	}
 			}
	});

	loadLanguage();

	$("#add_language").click(function(){
		setUpper();
		if ($("#gp_personal_id").val()==0)
		{
			$.when($("#save-kandidat").trigger("click")).done(function(rs){
			//AddLanguage($("#gp_personal_id").val());			
			});					
		}
		else
		{
			AddLanguage($("#gp_personal_id").val());
			$(":input","#formLanguage").val("")	 		
		}
	});

	$("#cancel_language").click(function(){
		   $(":input","#formLanguage").val("")	 		
	});


	function AddLanguage(id)
	{
		if($("#language_id").val()!=0)
		{
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url("index.php/language/save"); ?>",
				data: $("#formLanguage").serialize() + "&gp_personal_id="+id,
				success: function(data)
				{
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						loadLanguage();
						$(":input","#formLanguage").val("")	 		
				   }
				}
			});
		}
	}

	//pengalaman kerja
	function loadExperience()
	{
		var id = $("#gp_personal_id").val();
		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/experience/getList/'); ?>/"+id,
				beforeSend: function () { $("#loading").show(); },
				// hides the loader after completion of request, whether successfull or failor.             
				complete: function () { $("#loading").hide(); },      
			   success: function(data)
			   {							   
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						$("#tbl_pengalaman > tbody").empty();
						$.each(data.data, function(k,v){
							if(v.EXPERIENCE_FROM_DATE!=0) var exp_from = view_date(v.EXPERIENCE_FROM_DATE,1);
							else var exp_from = "-";

							if(v.EXPERIENCE_TO_DATE!=0) var exp_to = view_date(v.EXPERIENCE_TO_DATE,1);
							else var exp_to = "-";
							
							var html = "<tr><td><input type=hidden value='"+v.FJ_JOB_EXPERIENCE_ID+"'>"+v.COMPANY_NAME+"</td>";
								html += "<td>"+v.EXPERIENCE_POSITION+"</td>";
								html += "<td>"+exp_from+"</td>";
								html += "<td>"+exp_to+"</td>";
								html += "<td>"+v.JOB_FUNCTION_ID+"</td>";
								html += "<td>"+v.BOSS_NAME+"</td>";
								html += "<td>"+v.BOSS_POSITION+"</td>";
								html += "<td>"+v.BOSS_PHONE_NUMBER+"</td>";
								html += "<td><div class='btn-group'>";
								html += "<button class='btn btn-small btn-danger' id=\"deleteExperience\" onclick=\"$('#act_exp').val('delete'); $('#fj_job_experience_id').val("+v.FJ_JOB_EXPERIENCE_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
								html += "<button class='btn btn-small btn-info' id=\"editExperience\" onclick=\"$('#act_exp').val('edit'); $('#fj_job_experience_id').val("+v.FJ_JOB_EXPERIENCE_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
								html += "</div>";
								html += "</td></tr>";
							$("#tbl_pengalaman").append(html);				   							
						});
				   }
			   }
			 });			
	}

	$("#fj_job_experience_id").change(function(){
			var act = $("#act_exp").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/experience/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k.search("DATE")>-1) 
										{
											v = view_date(v, 1);	// 1 = ubah ke format dd-mm-yyyy
											$("#"+k.toLowerCase()+"[type=text]").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);							
										}
//										$("#"+k.toLowerCase()).val(v);
																			
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/experience/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadExperience();
					    	$("#formExperience").trigger("reset");	 		
						}
					});
	        	}
 			}
	});


	loadExperience();

	$("#add_experience").click(function(){
		setUpper();
		if ($("#gp_personal_id").val()==0)
		{
			$.when($("#save-kandidat").trigger("click")).done(function(rs){
//			AddExperience($("#gp_personal_id").val());			
			});					
		}
		else
		{
			AddExperience($("#gp_personal_id").val());
 		    $("#formExperience").trigger("reset");
			$("#fj_job_experience_id").val(0);
			$('#act_exp').val('');	 		
		}
	});

	$("#cancel_experience").click(function(){
 		    $("#formExperience").trigger("reset");
			$("#fj_job_experience_id").val(0);
			$('#act_exp').val('');	 		
	});


	function AddExperience(id)
	{
		if($("#company_name").val()!="")
		{
			$.ajax({
				type: 'POST',
				url: "<?php echo base_url("index.php/experience/save"); ?>",
				data: $("#formExperience").serialize() + "&gp_personal_id="+id,
				success: function(data)
				{
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						loadExperience();
			 		    $("#formExperience").trigger("reset");
						$("#fj_job_experience_id").val(0);
						$('#act_exp').val('');	 		
				   }
				}
			});
		}
	}


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#birth_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/02/20"	
	});
    $('#father_dob').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1900/01/01"	
	});
    $('#mother_dob').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "1900/01/01"	
	});
    $('#spouse_dob').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "1900/01/01"	
	});
    $('#child_1_dob').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "1900/01/01"	
	});
    $('#child_2_dob').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "1900/01/01"	
	});
    $('#child_3_dob').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "1900/01/01"	
	});
	
    $('#applicant_date').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : new Date()
	});

    $('#interview_date').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : new Date()
	});

    $('#experience_from_date').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "2015/01/01"	
	});

    $('#experience_to_date').datetimepicker({
				format : "DD-MM-YYYY",	
				defaultDate : "2015/01/01"	
	});

    $('#course_from').datetimepicker({
				format : "MMM-YYYY"	
	});

    $('#course_to').datetimepicker({
				format : "MMM-YYYY"	
	});

    $('#available_date').hide();
	
	$("#available_id").on("change", function(me){
				if($(this).val()=='26') $("#available_date").show();
				else $("#available_date").hide();
	});

    $('#available_date').datetimepicker({
				format : "DD-MM-YYYY"
	});
				
	$("#sameaddress").on("click", function()
	{
		if($("#sameaddress").is(":checked")) 
		{
			$("#address").val($("#idcard_address").val());
			$("#m_district_name").val($("#idcard_district_name").val());
			$("#m_sub_district_name").val($("#idcard_sub_district_name").val());
			$("#m_city_name").val($("#idcard_city_name").val());
			$("#zipcode").val($("#idcard_zipcode").val());
			$("#m_state_name").val($("#idcard_state_name").val());
			$("#m_district_id").val($("#idcard_district_id").val());
			$("#m_sub_district_id").val($("#idcard_sub_district_id").val());
			$("#m_city_id").val($("#idcard_city_id").val());
			$("#m_state_id").val($("#idcard_state_id").val());
			
			$("#address").attr("readonly",true);
			$("#district_name").attr("disabled",true);
			$("#sub_district_name").attr("disabled",true);
			$("#city_name").attr("disabled",true);
			$("#zipcode").attr("readonly",true);
			$("#state_name").attr("disabled",true);

		}
		else
		{
			$("#address").val('');
			$("#m_district_name").val('');
			$("#m_sub_district_name").val('');
			$("#m_city_name").val('');
			$("#zipcode").val('');
			$("#m_state_name").val('');
			$("#m_district_id").val('');
			$("#m_sub_district_id").val('');
			$("#m_city_id").val('');
			$("#m_state_id").val('');

			$("#address").attr("readonly",false);
			$("#district_name").attr("disabled",false);
			$("#sub_district_name").attr("disabled",false);
			$("#city_id").attr("disabled",false);
			$("#zipcode").attr("readonly",false);
			$("#state_name").attr("disabled",false);

		}
	});

	
	$('#home_phone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#handphone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#emergency_number').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#emergency_number_2').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});


/*	$('#hph').focusout(function(){
		var hmp = $('#home_phone').tokenfield('getTokens');
		var hmpcur = $('#home_phone').val();
		console.log(hmp+' '+hmpcur);
		$('#home_phone').tokenfield('createToken', hmpcur);		
		if(hmp!==hmpcur || hmp=="")
		{
			$('#home_phone').tokenfield('createToken', hmpcur);		
		}
		
		$('#home_phone').on("keypress",function(e,pr){
			console.log("ok");
		});
		$('#home_phone').trigger("keypress", [73]);
	});
*/
//    $("#oversea_id").tokenInput("<?php //echo base_url("index.php/applicant/getOversea"); ?>");

//	$("#oversea_city").select2();
    $("#passion").select2();
    $("#recommendation").select2();
//    $("#passion").tokenInput("<?php echo base_url("index.php/applicant/getJobFunction"); ?>");
//    $("#recommendation").tokenInput("<?php echo base_url("index.php/applicant/getJobFunction"); ?>");

	$("#shift").on("click", function()
	{
		if($("#shift").is(":checked",true)) 
		{
			$("#shift").val('Y');			
		}
	});
	
	
	$("#overtime").on("click", function()
	{
		if($("#overtime").is(":checked")) 
		{
			$("#overtime").val('Y');			
		}
	});
	$("#oversea").on("click", function()
	{
		if($("#oversea").is(":checked")) 
		{
			$("#oversea").val('Y');			
		}
	});
	$("#motorcycle").on("click", function()
	{
		if($("#motorcycle").is(":checked")) 
		{
			$("#motorcycle").val('Y');			
		}
	});
	$("#car").on("click", function()
	{
		if($("#car").is(":checked")) 
		{
			$("#car").val('Y');			
		}
	});
	$("#sim_a").on("click", function()
	{
		if($("#sim_a").is(":checked")) 
		{
			$("#sim_a").val('Y');			
		}
	});
	$("#sim_b").on("click", function()
	{
		if($("#sim_b").is(":checked")) 
		{
			$("#sim_b").val('Y');			
		}
	});
	$("#sim_c").on("click", function()
	{
		if($("#sim_c").is(":checked")) 
		{
			$("#sim_c").val('Y');			
		}
	});
	$("#certificate").on("click", function()
	{
		if($("#certificate").is(":checked")) 
		{
			$("#certificate").val('Y');			
		}
	});		

/*	$("#id_card_no").on('keyup',function() {
		var inp = $(this);
		if(inp.val().length>5)
		{
			delay(function(){
				$.ajax({
					   type: "GET",
					   url: "<?php //echo base_url('index.php/applicant/getPersonal/'); ?>/"+inp.val(),
					   success: function(data)
					   {
						   data = jQuery.parseJSON(data);
						   if(data.success)
						   {
						   		alert("data sudah ada");
								$("#gp_personal_id").val(data.gp_personal_id).trigger("change");
								loadApplicant();
								loadEducation();
								loadFamily();
								loadOthers();
								loadInterview();
								loadSkill();
								loadEmergency();
							
						   }
					   }
				});			
			}, 1000);
		}
	});		*/

	$('#m_client_name').typeahead({
		source: function (query, process) {
			return $.get('<?php echo base_url('index.php/tracking/getClient/'); ?>/'+query, function (data) {
			   data = jQuery.parseJSON(data);
			   return process(data.s);
			});
		},
		autoSelect : true
	});


	$('#m_client_name').change(function() {
		var client = $('#m_client_name').val();
		var current = $('#m_client_name').typeahead("getActive");
		if (client!='')
		{
			$('#m_client_id').val(current.id);
		}
		else
		{
			$('#m_client_id').val(0);
		}

    });	
	
	var curpage = 1;
	var totpage = 1;

	$("#filterResult").hide();
	$("#btnFilter").click(function(){
			$("#filterResult").show();
			loadJO(1, $("#formKirim").serializeArray());
			$("#footer_paging").show();
			
	});

	$("#btnReset").click(function(){
		$("#filterResult").hide();
		$("#formKirim").trigger("reset");
		$("#tbl_jo > tbody").empty();
		$("#footer_paging").hide();
	});

	function loadJO(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
			curpage = 1;
			totpage = 1;
		}
			
		var par = new Array();
		par["page"] = page;
		
		q = $("#formKirim").serializeArray();

		$.each(q, function(k,v){
			par[v.name] = v.value;
		});

		par = $.extend({}, par);

		$.ajax({
			   type: "GET",
			   url: "<?php echo base_url('index.php/tracking/getFilter/'); ?>",
			   data: par, // serializes the form's elements.
			   success: function(data)
			   {
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						$("#tbl_jo > tbody").empty();
						$('#textpage').html("Page "+page+" of "+data.totpage);
						curpage = page;
						totpage = data.totpage;
						$.each(data.data, function(k,v){
							var html = "<tr><td>"+v.JO_NO+"</td>";
								html += "<td>"+(v.BRANCH).toUpperCase()+"</td>";
								html += "<td>"+(v.CLIENT).toUpperCase()+"</td>";
								html += "<td>"+v.POSISI+"</td>";
								html += "<td>"+v.WORK_PLACE+"</td>";
								html += "<td><div class='btn-group'>";
								html += "<button class='btn btn-small btn-success' id=\"sendJO\" onclick=\"$('#act_jo').val('send'); $('#fj_job_order_id').val("+v.FJ_JOB_ORDER_ID+").trigger('change');\"><i class='fa fa-arrow-right'></i>Kirim</button>";
								html += "</div>";
								html += "</td></tr>";
							$("#tbl_jo").append(html);	
							$("#footer_pagging").show();	
										   							
						});
											
				   }
			   }
			 });			
	}
	//loadJO();

	$("#fj_job_order_id").change(function(){
			var act = $("#act_jo").val();
			var gp_personal_id = $("#gp_personal_id").val();
			var id = $(this).val();	
			var nomor_jo = $("#applicant_jo_no").val();
			var status = $("#status_tracking_id").val();
			if(nomor_jo=="0" || nomor_jo==0)
			{
				if(act=="send")
				{
/*					bootbox.dialog({
					  message: "I am a custom dialog",
					  title: "Custom title",
					  buttons: {
						success: {
						  label: "Success!",
						  className: "btn-success",
						  callback: function() {
							Example.show("great success");
						  }
						},
						danger: {
						  label: "Danger!",
						  className: "btn-danger",
						  callback: function() {
							Example.show("uh oh, look out!");
						  }
						},
						main: {
						  label: "Click ME!",
						  className: "btn-primary",
						  callback: function() {
							Example.show("Primary button");
						  }
						}
					  }
					});
*/					bootbox.confirm("Kirim Kandidat ke JO ini ?", function(result) {
						if (result == true) {
							$.ajax({
								   type: "GET",
								   url: "<?php echo base_url('index.php/tracking/sendJO/'); ?>",
								   data: "gp_personal_id="+gp_personal_id + "&fj_job_order_id="+id,
								   success: function(data)
								   {
									   data = jQuery.parseJSON(data);
									   if(data.success) 
									   {	
											SaveTracking();
//											alert("Kirim JO berhasil");
											bootbox.alert("Kirim Kandidat berhasil", function() {
												$("#btnReset").trigger("click");
												$("#act_jo").val("");
												$("#fj_job_order_id").val("");		
											});
									   }
								   }
								 });	
						}				 		
					});
				}
			}
			else
			{
				if(status==72598) bootbox.alert("Kandidat sudah Hired di JO nomor "+nomor_jo); 
				else bootbox.alert("Kandidat ini sedang diproses di JO nomor "+nomor_jo);
			}
	});

	function SaveTracking()
	{
			var gp_personal_id = $("#gp_personal_id").val();
			var fj_job_order_id = $("#fj_job_order_id").val();
			$.ajax({
				type: 'GET',
			   	url: "<?php echo base_url('index.php/tracking/save_tracking/'); ?>",
			   	data: "gp_personal_id="+gp_personal_id + "&fj_job_order_id="+fj_job_order_id,
				success: function(data)
				{
				   data = jQuery.parseJSON(data);
				}
			});
	}
		
	$('#footer_paging').hide();

		function loadHistory(page)
		{
			var id = $("#gp_personal_id").val();
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}
			
			var par = new Array();
			par["page"] = page;

/*			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

*/			par = $.extend({}, par);
			$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/tracking/getApplicantHistory/'); ?>/"+id,
				   data: par, // serializes the form's elements.
				   success: function(data)
				   {
				   
						$.ajax({
							   type: "GET",
							   url: "<?php echo base_url('index.php/tracking/getFromHistory/'); ?>/"+id,
							   data: par, // serializes the form's elements.
							   success: function(data)
							   {
								   
								   data = jQuery.parseJSON(data);
								   if(data.success) 
								   {
										//$("#historyList > tbody").empty();
										$('#textpage').html("Page "+page+" of "+data.totpage);
										curpage = page;
										totpage = data.totpage;
											$.each(data.data, function(k,v){
												if(v.INTERVIEW_1_DATE!=null) 
												{
													var INTERVIEW_1_DATE = view_date(v.INTERVIEW_1_DATE,2);
												}
												else 
												{
													var INTERVIEW_1_DATE="-";	
												}				
												if(v.INTERVIEW_2_DATE!=null) 
												{
													var INTERVIEW_2_DATE = view_date(v.INTERVIEW_2_DATE,2);
												}
												else 
												{
													var INTERVIEW_2_DATE="-";	
												}				
												if(v.MCU_DATE!=null) 
												{
													var MCU_DATE = view_date(v.MCU_DATE,2);
												}
												else 
												{
													var MCU_DATE="-";	
												}				
												if(v.TRAINING_DATE!=null) 
												{
													var TRAINING_DATE = view_date(v.TRAINING_DATE,2);
												}
												else 
												{
													var TRAINING_DATE="-";	
												}				
												if(v.HIRED_DATE!=null) 
												{
													var HIRED_DATE = view_date(v.HIRED_DATE,2);
												}
												else 
												{
													var HIRED_DATE="-";	
												}				
												var reasons = "";
												if(v.STATUS=='Hired')
												{
													reasons="-";
												}	
												else
												if(v.STATUS=='Reject')
												{	
													if(v.REJECT_REASONS!=null) reasons=v.REJECT_REASONS;
													else reasons="-";
												}	
												else
												if(v.STATUS=='Refuse')
												{
													if(v.REFUSE_REASONS!=null) reasons=v.REFUSE_REASONS;
													else reasons="-";
												}	
												else
												{
													if(v.NOTE!=null) reasons=v.NOTE;
													else reasons="-";
												}
												var html = "<tr>";
													html += "<td>"+v.JO_NO+"</td>";
													html += "<td>"+v.CLIENT+"</td>";
													html += "<td>"+v.POSISI+"</td>";
													html += "<td>"+v.USER+"</td>";
													html += "<td>"+v.BRANCH+"</td>";
													html += "<td>"+view_date(v.CREATED_DATE,2)+"</td>";
													html += "<td>"+INTERVIEW_1_DATE+"</td>";
													html += "<td>"+INTERVIEW_2_DATE+"</td>";
													html += "<td>"+MCU_DATE+"</td>";
													html += "<td>"+TRAINING_DATE+"</td>";
													html += "<td>"+v.STATUS+"</td>";
													html += "<td>"+HIRED_DATE+"</td>";
													html += "<td>"+reasons+"</td>";
													html += "</tr>";
												$("#footer_pagging").show();	
												$("#historyList").append($(html).dblclick(function() { $("#content").load("<?php echo base_url('index.php/jo/getDetail/'); ?>/"+v.FJ_JOB_ORDER_ID); }));
										});
								   }
							   }
							 });			
				   
					// mengambil data dari tracking   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
						    $("#historyList > tbody").empty();
							$('#textpage').html("Page "+page+" of "+data.totpage);
							curpage = page;
							totpage = data.totpage;
								$.each(data.data, function(k,v){
									if(v.INTERVIEW_1_DATE!=null) 
									{
										var INTERVIEW_1_DATE = view_date(v.INTERVIEW_1_DATE,2);
									}
									else 
									{
										var INTERVIEW_1_DATE="-";	
									}				
									if(v.INTERVIEW_2_DATE!=null) 
									{
										var INTERVIEW_2_DATE = view_date(v.INTERVIEW_2_DATE,2);
									}
									else 
									{
										var INTERVIEW_2_DATE="-";	
									}				
									if(v.MCU_DATE!=null) 
									{
										var MCU_DATE = view_date(v.MCU_DATE,2);
									}
									else 
									{
										var MCU_DATE="-";	
									}				
									if(v.TRAINING_DATE!=null) 
									{
										var TRAINING_DATE = view_date(v.TRAINING_DATE,2);
									}
									else 
									{
										var TRAINING_DATE="-";	
									}				
									if(v.HIRED_DATE!=null) 
									{
										var HIRED_DATE = view_date(v.HIRED_DATE,2);
									}
									else 
									{
										var HIRED_DATE="-";	
									}				
									var html = "<tr>";
										html += "<td>"+v.JO_NO+"</td>";
										html += "<td>"+v.CLIENT+"</td>";
										html += "<td>"+v.POSISI+"</td>";
										html += "<td>"+v.USER+"</td>";
										html += "<td>"+v.BRANCH+"</td>";
										html += "<td>"+view_date(v.CREATED_DATE,2)+"</td>";
										html += "<td>"+INTERVIEW_1_DATE+"</td>";
										html += "<td>"+INTERVIEW_2_DATE+"</td>";
										html += "<td>"+MCU_DATE+"</td>";
										html += "<td>"+TRAINING_DATE+"</td>";
										html += "<td>"+v.STATUS+"</td>";
										html += "<td>"+HIRED_DATE+"</td>";
										html += "<td>-</td>";
										html += "</tr>";
									$("#footer_pagging").show();	
									$("#historyList").append($(html).dblclick(function() { $("#content").load("<?php echo base_url('index.php/jo/getDetail/'); ?>/"+v.FJ_JOB_ORDER_ID); }));
							});
					   }
				   }
				 });			
		}


	loadTab();
	
	function loadTab(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
		}
		if(activeTab=="#riwayat")
		{
			loadHistory(page);	
//			$("#historyList").empty();
			$('#footer_paging').hide();
			$('#footerApplicant').hide();
			$('#title_mandatory').hide();
			$('#btnkandidat').hide();
			$('#btnlistkandidat').show();
		}
		else
		if(activeTab=="#kirim")
		{
			$('#footer_paging').hide();
			$('#footerApplicant').hide();
			$('#title_mandatory').hide();
			$('#btnkandidat').hide();
			$('#btnlistkandidat').show();
		}
		else
		if(activeTab=="#pengalaman")
		{
			$('#btnNavigation').show();
			$('#btnkandidat').show();
			$('#btnkandidat1').show();
			$('#btnNext').attr("disabled",true);
			$('#footer_paging').hide();
			$('#btnlistkandidat').hide();
		}
		else
		if(activeTab=="#interview")
		{
			$('#btnNavigation').hide();
			$('#title_mandatory').hide();
			$('#title_mandatory1').hide();
			$('#btnkandidat').show();
			$('#btnkandidat1').show();
			$('#footerApplicant').show();
			$('#btnlistkandidat').hide();
			$('#footer_paging').hide();
			loadInterview();
		}
		else
		{
//			loadApplicant(page);				
			$('#btnNavigation').show();
			$('#title_mandatory').show();
//			$('#btnkandidat').hide();
//			$('#btnkandidat1').hide();
			$('#btnlistkandidat').hide();
			$('#footerApplicant').show();
			$('#footer_paging').hide();
			$('#btnNext').attr("disabled",false);
		}
	}
	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{
			if(activeTab=="#riwayat") loadHistory(next);			
			else loadJO(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			if(activeTab=="#riwayat") loadHistory(prev);			
			else loadJO(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
		if(activeTab=="#riwayat") loadHistory(next);			
		else loadJO(next);
	});

	$("#last").click(function(){
		var next = totpage;
		if(activeTab=="#riwayat") loadHistory(next);			
		else loadJO(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

	$("#id_card_no").on('keyup',function() {
		var inp = $(this);
		if(inp.val().length>5)
		{
			delay(function(){
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/applicant/getPersonal/'); ?>/"+inp.val(),
					   success: function(data)
					   {
						   data = jQuery.parseJSON(data);
						   if(data.success)
						   {
						   		//alert("Data sudah ada");
								bootbox.dialog({
								  message: "<h4 align=center>Data sudah ada!</h4>",
								  closeButton:false,
								  buttons: {
									success: {
									  label: "OK",
									  className: "btn-success",
									  callback: function() {
											$("#gp_personal_id").val(data.gp_personal_id).trigger("change");
											loadApplicant();
											loadEducation();
											loadFamily();
											loadOthers();
											loadInterview();
											loadSkill();
											loadEmergency();
											loadCourses();
											loadOrganization();
											loadLanguage();
											loadExperience();
									  		}
										}
								  	}
								});
						   }
					   }
				});			
			}, 1000);
		}
	});	
	
	


});
</script>

