<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Keluarga Karyawan
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Data Keluarga</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formKeluarga" name="formKeluarga">			    		
			    		<div class="form-group">
							<label for="keluarga" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
                        <div class="form-group">                                            
		                    <label for="name" class="control-label col-xs-3 ">No. Kartu Keluarga</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="family_card_no" name="family_card_no" placeholder="No. Kartu Keluarga" />
		                    </div>
						</div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Nama Anggota</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="family_name" name="family_name" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Hubungan</label>
		                    <div class="col-xs-3">
                                <?php echo $ref_relation_id; ?>
                            </div>
		                </div>
		                <div class="form-group">
                            <label for="m_birth_place" class="control-label col-xs-3 ">Tempat Lahir</label>
                            <div class="col-xs-3">
                                <?php //echo $m_m_birth_place_id; ?>
                                <input class="form-control" type="text" id="m_birth_place_name" name="m_birth_place_name" placeholder="Tempat Lahir" data-provide="typeahead" />
                                <input class="form-control" type="hidden" id="m_birth_place_temp" placeholder="Tempat Lahir" />
                                <input class="form-control" type="hidden" id="m_birth_place" name="m_birth_place" placeholder="Tempat Lahir" value=0 />
                            </div>
                            <label for="tanggal lahir" class="control-label col-xs-2 ">Tanggal Lahir</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='birth_date'>
                                    <input type='text' class="form-control" id="birth_date" name="birth_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="gender_id" class="control-label col-xs-3 ">Jenis Kelamin</label>
                            <div class="col-xs-3">
                                <?php echo $gender_id; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="religion" class="control-label col-xs-3 ">Agama</label>
                            <div class="col-xs-3">
                                <?php echo $religion_id; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="BPJS" class="control-label col-xs-3">BPJS</label>
                            <div class="col-xs-3">
                                <?php echo $ref_bpjskes_status_id; ?>
                            </div>
                            <label for="No. BPJS" class="control-label col-xs-2 ">No. BPJS</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="bpjskes_no" name="bpjskes_no" placeholder="No. BPJS" />
		                    </div>                                        
                        </div>		                
						<div class="form-group">
							<label for="keluarga" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Data Keluarga</b></h4>
					</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_keluarga">
								<thead>
								<tr class="info"><th>Nama</th><th>Hubungan</th><th>Tempat Tanggal Lahir</th><th>Agama</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

	function loadKeluarga(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/keluarga/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_keluarga > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_keluarga > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_family_id+"'>"+v.family_name+"</td>";
											html += "<td>"+v.ref_relation_id+"</td>";
											html += "<td>"+v.m_birth_place_id+","+v.birth_date+"</td>";
											html += "<td>"+v.ref_religion_id+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteKeluarga\" onclick=\"$('#act').val('delete'); $('#gp_family_id').val("+v.gp_family_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editKeluarga\" onclick=\"$('#act').val('edit'); $('#gp_family_id').val("+v.gp_family_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_keluarga").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadKeluarga();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadKeluarga(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadKeluarga(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadKeluarga(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadKeluarga(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    $('#m_birth_place_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/keluarga/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_birth_place_name').change(function() {
		var current = $('#m_birth_place_name').typeahead("getActive");
		try{
			$('#m_birth_place').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
    });
	
	
	$('#m_birth_place_name').blur(function(){
			$('#m_birth_place_name').trigger("change");
	});	
	
	$("#gp_family_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/keluarga/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/keluarga/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKeluarga();
						}
					});
	        	}
				}); 
				
 			}
	});

	$("#reset").click(function(){
		$("#formKeluarga").trigger("reset");
		$("#gp_family_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formKeluarga").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/keluarga/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadKeluarga();
								$("#formKeluarga").trigger("reset");
								$("#gp_family_id").val(0);							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#birth_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});    
    


});
</script>