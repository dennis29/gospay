<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 24, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Sidebar
* 
********************************************************************/  -->
    
    <style type="text/css">

       


                /*
        A Bootstrap 3.1 affix sidebar template
        from http://bootply.com

        This CSS code should follow the 'bootstrap.css'
        in your HTML file.

        license: MIT
        author: bootply.com
        */

        
    </style>

    <!--main-->
    <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
              <li><a href="" id='tab-perusahaan' onclick="$('#content-master').load('<?php echo base_url('index.php/perusahaan/getPanel');?> ')">Perusahaan</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/cabang/getPanel');?> ')">Cabang</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/client/getPanel');?> ')">Client</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/unit_kerja/getPanel');?> ')">Unit Kerja</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/posisi/getPanel');?> ')">Tipe Jabatan</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/struktur_jabatan/getPanel');?> ')">Struktur Jabatan</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/lokasi/getPanel');?> ')">Lokasi Kerja</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/area/getPanel');?> ')">Wilayah dan Area</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/kriteria/getPanel');?> ')">Kriteria Kelompok</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/bank/getPanel');?> ')">Bank</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/cuti_bersama/getPanel');?> ')">Cuti Bersama</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/parameter_cuti/getPanel');?> ')">Parameter Cuti</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/deskripsi/getPanel');?> ')">Deskripsi Penggajian</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/tarif/getPanel');?> ')">Tarif</a></li>
              <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/tarif_client/getPanel');?> ')">Tarif Client</a></li>
            
            </ul>
      </div>
    </div>
    <div class="col-md-12">
      <div class="mainPage" id="content-master" style="padding-top:30px">
      
      </div>
     </div>
    <script>

    $("a").click(function(e){
        e.preventDefault();
    });

    $("#tab-perusahaan").trigger('click');

    /* activate sidebar */
    $('#sidebar').affix({
      offset: {
        top: 235
      }
    });

    /* activate scrollspy menu */
    var $body   = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;

    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });

    /* smooth scrolling sections */
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 50
            }, 1000);
            return false;
          }
        }
    });


    </script>