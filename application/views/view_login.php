<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 24, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Form Login
* 
********************************************************************/  -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>GOS Payment</title>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-theme.min.css'); ?>"/>
    <!-- Fav icon -->
    <link rel="shortcut icon" href="http://localhost/gospay/asset/img/favicon.png">
    <!-- JS -->
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.js'); ?>"></script>
</head>
<body>

<div class="container">
<div class="row" style="padding:20px">
	<div class="col-md-6 col-md-offset-3 text-center">
		<img src="<?php echo base_url('asset/img/gospay.png'); ?>">
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
        <div class="panel panel-info">
            <div class="panel-heading">LOGIN</div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" style="padding:10px" id="formLogin" name="formLogin">
                    <div class="row">
                        <div class="form-group">
                            <label for="username" class="control-label col-md-2">Username</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" id="username" name="username" placeholder="Username">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="username" class="control-label col-md-2">Password</label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-8">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
             </div>
        </div>

	</div>
</div>
</div>

</body>
</html>

<script>
$(function(){
    $("#formLogin").submit(function() {
    
        $.ajax({
               type: "POST",
               url: "<?php echo base_url('index.php/login/cekLogin'); ?>",
               data: $("#formLogin").serialize(), // serializes the form's elements.
               success: function(data)
               {
                   data = jQuery.parseJSON(data);
                   if(data.success == true) 
                   {

                        window.location="<?php echo base_url(); ?>"; 
                   }
                   else
                   {
                        alert("Login Gagal");
                   }
               }
             });
    
        return false; // avoid to execute the actual submit of the form.
    });     
});
</script>