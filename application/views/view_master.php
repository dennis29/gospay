<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-responsive.css'); ?>"/>
<div id="main" class="container">
<!-- left list column -->
<div class="navbar-default sidebar" role="navigation">
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li>
                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> UI Elements<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="panels-wells.html">Panels and Wells</a>
                                </li>
                                <li>
                                    <a href="buttons.html">Buttons</a>
                                </li>
                                <li>
                                    <a href="notifications.html">Notifications</a>
                                </li>
                                <li>
                                    <a href="typography.html">Typography</a>
                                </li>
                                <li>
                                    <a href="icons.html"> Icons</a>
                                </li>
                                <li>
                                    <a href="grid.html">Grid</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Second Level Item</a>
                                </li>
                                <li>
                                    <a href="#">Third Level <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                        <li>
                                            <a href="#">Third Level Item</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="blank.html">Blank Page</a>
                                </li>
                                <li>
                                    <a href="login.html">Login Page</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
</nav>         

</div>	

<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	  loadTab();
	});	

	$("#search").keyup(function(){
			loadSubDistrict(1, $("#formSearch").serializeArray());
	});

	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});

		function loadSubDistrict(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sub_district/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_sub_district > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.M_SUB_DISTRICT_ID+"'>"+v.NAME+"</td>";
											html += "<td>"+v.M_DISTRICT_ID+"</td>";
											html += "<td>"+v.CITY+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteSubdistrict\" onclick=\"$('#act').val('delete'); $('#m_sub_district_id').val("+v.M_SUB_DISTRICT_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->	
											html += "<button class='btn btn-small btn-info' id=\"editSubdistrict\" onclick=\"$('#act').val('edit'); $('#m_sub_district_id').val("+v.M_SUB_DISTRICT_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_sub_district").append(html);				   							
									});
							   }
						   }
						 });										
		}

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			if(activeTab=="#sub_district") loadSubDistrict(prev);			
			else if(activeTab=="#district") loadDistrict(prev);			
			else if(activeTab=="#city") loadCity(prev);			
			else loadState(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
	});

	$("#last").click(function(){
		var next = totpage;
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    $('#m_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/district/getDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		}
    });
	

/* 	$('#m_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php //echo base_url('index.php/district/getDistrict/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
*/	
	$('#m_district_name').change(function() {
		var current = $('#m_district_name').typeahead("getActive");
		$('#m_district_id').val(current.id);
    });


	$("#m_sub_district_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sub_district/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/sub_district/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadSubDistrict();
						}
					});
	        	}
 			}
	});

	$("#reset_sub").click(function(){
		$(":input","#formSubdistrict").val("");
	});


	$("#add_sub").click(function(){
			$("#formSubdistrict").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/sub_district/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_sub_district_id").val(data.id);
							alert("Data Berhasil Disimpan");
							loadSubDistrict();
							document.getElementById("formSubdistrict").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$("#search_district").keyup(function(){
			loadDistrict(1, $("#formSearchDistrict").serializeArray());
	});

/*	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadDistrict(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchDistrict").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/district/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_district > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.M_DISTRICT_ID+"'>"+v.NAME+"</td>";
											html += "<td>"+v.M_CITY_ID+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteDistrict\" onclick=\"$('#act_district').val('delete'); $('#m_district_id_1').val("+v.M_DISTRICT_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->	
										html += "<button class='btn btn-small btn-info' id=\"editDistrict\" onclick=\"$('#act_district').val('edit'); $('#m_district_id_1').val("+v.M_DISTRICT_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_district").append(html);				   							
									});
							   }
						   }
						 });										
		}

 	$('#m_city_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/city/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
	
	$('#m_city_name').change(function() {
		var current = $('#m_city_name').typeahead("getActive");
		$('#m_city_id').val(current.id);
    });


	$("#m_district_id_1").change(function(){
			var act = $("#act_district").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/district/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="NAME")
										{
											$("#m_district_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/district/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadDistrict();
						}
					});
	        	}
 			}
	});

	$("#reset_district").click(function(){
		document.getElementById("formDistrict").reset();
	});


	$("#add_district").click(function(){
			$("#formDistrict").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/district/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_district_id_1").val(data.id);
							alert("Data Berhasil Disimpan");
							loadDistrict();
							document.getElementById("formDistrict").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});
		
	$("#search_city").keyup(function(){
			loadCity(1, $("#formSearchCity").serializeArray());
	});

	/*$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadCity(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchCity").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/city/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_city > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.M_CITY_ID+"'>"+v.NAME+"</td>";
											html += "<td>"+v.M_STATE_ID+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteCity\" onclick=\"$('#act_city').val('delete'); $('#m_city_id_1').val("+v.M_CITY_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->
											html += "<button class='btn btn-small btn-info' id=\"editCity\" onclick=\"$('#act_city').val('edit'); $('#m_city_id_1').val("+v.M_CITY_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_city").append(html);				   							
									});
							   }
						   }
						 });										
		}
 	$('#m_state_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/state/getState/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
	
	$('#m_state_name').change(function() {
		var current = $('#m_state_name').typeahead("getActive");
		$('#m_state_id').val(current.id);
    });


	$("#m_city_id_1").change(function(){
			var act = $("#act_city").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/city/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="NAME")
										{
											$("#m_city_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/city/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadCity();
						}
					});
	        	}
 			}
	});

	$("#reset_city").click(function(){
		document.getElementById("formCity").reset();
	});

	$("#add_city").click(function(){
			$("#formCity").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/city/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_city_id_1").val(data.id);
							alert("Data Berhasil Disimpan");
							loadCity();
							document.getElementById("formCity").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#search_state").keyup(function(){
			loadState(1, $("#formSearchState").serializeArray());
	});

	/*$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadState(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchState").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/state/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_state > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.M_STATE_ID+"'>"+v.NAME+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteState\" onclick=\"$('#act_state').val('delete'); $('#m_state_id_1').val("+v.M_STATE_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->
											html += "<button class='btn btn-small btn-info' id=\"editState\" onclick=\"$('#act_state').val('edit'); $('#m_state_id_1').val("+v.M_STATE_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_state").append(html);				   							
									});
							   }
						   }
						 });										
		}

	//loadState();	

	$("#m_state_id_1").change(function(){
			var act = $("#act_state").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/state/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="NAME")
										{
											$("#m_state_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/state/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadState();
						}
					});
	        	}
 			}
	});

	$("#reset_state").click(function(){
		$(":input","#formState").val("");
	});


	$("#add_state").click(function(){
			$("#formState").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/state/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_state_id_1").val(data.id);
							alert("Data Berhasil Disimpan");
							loadState();
							document.getElementById("formState").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});
	
	loadTab();
	
	function loadTab(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
		}
		if(activeTab=="#sub_district")
		{
			loadSubDistrict(page);	
		}
		else
		if(activeTab=="#district")
		{
			loadDistrict(page);	
		}
		else
		if(activeTab=="#city")
		{
			loadCity(page);	
		}
		else
		if(activeTab=="#state")
		{
			loadState(page);				
		}
	}

	$("a").click(function(e){
			e.preventDefault();
	});	


});
</script>