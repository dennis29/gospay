<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 27, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Menu
* 
********************************************************************/  -->
<body>
<?php
	function buildMenu($m,$parent=0)
	{
		$html = "";
		foreach($m as $v)
		{
			if($v->parent_id==$parent)
			{
				$child = buildMenu($m,$v->m_menu_id);		
				if($child)
				{
					if($v->parent_id==0) $html .= "<li class=\"dropdown\">";
					else $html .= "<li>";
					$html .= "<a href=\"#\" class=\"dropdown-toggle navbar-link\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">".$v->name_menu."<span class=\"caret\"></span></a>
							 <ul class=\"dropdown-menu\" role=\"menu\">";
					$html .= $child;
					$html .= "</ul>";
				}
				else
				{
            		$html .= "<li><a href=\"#\" onclick=\"$('#content').load('".base_url('index.php'.$v->url)."')\">".$v->name_menu."</a></li>";
				}
					
			}
		}

		return $html;
	}
?>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#" style="padding:10px;">
        <img alt="Brand" src="<?php echo base_url("asset/img/gospay.png"); ?>" style="width:80px; height:30px">
      </a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
<!--   		<li><a href="#" onClick="$('#content').load('<?php //echo base_url('index.php/dashboard/getPanel'); ?>');" id="dashboard">Dashboard</a></li>
-->		<?php echo buildMenu($menu); ?>
   		<li><a href="#" onClick="window.location = '<?php echo base_url('index.php/login/out'); ?>';">Logout</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <!-- <li><p class="navbar-text"><?php echo $full_name; ?></p></li> -->
      </ul>
    </div>
  </div>
</div>
