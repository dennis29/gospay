<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 24, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Sidebar Master
* 
********************************************************************/  -->
    
    <!-- CSS untuk side bar -->

    <style type="text/css">
       
        #leftCol {
        position: fixed;
        width: 250px;        
        overflow-y: scroll;
        top: 30;
        bottom:10;
        }

                /*
        A Bootstrap 3.1 affix sidebar template
        from http://bootply.com

        This CSS code should follow the 'bootstrap.css'
        in your HTML file.

        license: MIT
        author: bootply.com
        */

        
    </style>
    <!-- Bootstrap Core CSS -->


    <div id="leftCol" class="navbar-default sidebar" role="navigation" style="position:fixed">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li><a data-toggle="tab" href="#detail" id ="tab-personal" name ="tab-personal">Personal</a></li>
						<li><a data-toggle="tab" href="#alamat" id ="tab-alamat" name ="tab-alamat"  >Alamat Rumah</a></li>
                        <li><a data-toggle="tab" href="#keluarga" id ="tab-keluarga" name ="tab-keluarga"  >Data Keluarga</a></li>
                        <li><a data-toggle="tab" href="#kontrak_kerja" id ="tab-kontrak" name ="tab-kontrak">Kontrak Kerja</a></li>
                        <li><a data-toggle="tab" href="#user" id ="tab-user" name ="tab-user">User, HRBP dll</a></li>
                        <li><a data-toggle="tab" href="#gaji" id ="tab-gaji" name ="tab-gaji">Perubahan Gaji Pokok</a></li>
                        <li><a data-toggle="tab" href="#tunjangan" id ="tab-tunjangan" name ="tab-tunjangan">Tunjangan-tunjangan</a></li>
                        <li><a data-toggle="tab" href="#mutasi" id ="tab-mutasi" name ="tab-mutasi">Mutasi Jabatan</a></li>
                        <li><a data-toggle="tab" href="#dokumen" id ="tab-dokumen" name ="tab-dokumen">Dokumen Dan Kelengkapan</a></li>
                        <li><a data-toggle="tab" href="#sanksi"  id ="tab-sanksi" name ="tab-sanksi">Sanksi dan Kedisiplinan</a></li>
                        <li><a data-toggle="tab" href="#cuti" id ="tab-cuti" name ="tab-cuti">Cuti</a></li>
                        <li><a data-toggle="tab" href="#atensi" id ="tab-atensi" name ="tab-atensi">Atensi Manajemen</a></li>
                        <li><a data-toggle="tab" href="#alasan"  id ="tab-alasan" name ="tab-alasan">Alasan Keluar</a></li>                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
     </div>
    <div class="col-md-12">
    <div class="mainPage" id="content-personal" style="padding-top:30px">
    <!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Personal Detail Karyawan
* 
********************************************************************/  -->

<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-responsive.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-11 col-md-offset" id="form-detail" data-title="Contacts">
    <div class="panel panel-default" style="margin-top:0px;">
        <div class="panel-heading clearfix" id="headerkaryawan">
			<div>
				<h2 class="panel-title pull-left"><b>Detail Karyawan</b></h2>
				<h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
				<!-- <button type="button" class="btn btn-primary pull-right" id="btnlistkandidat" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				  List Kandidat
				</button>      -->
			</div>			
			<div class="btn-group pull-right" id="btnkaryawan">
				<?php 
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>     
                <button type="button" class="btn btn-danger save" id="reset1" onClick="$('#content').load('<?php echo base_url('index.php/personal/getDetail/0'); ?>');">
                  Delete
                </button> 
                <button type="button" class="btn btn-success save" id="save1-kandidat">
                  Save
                </button>     
            </div>                  
        </div>
		 <input class="form-control" type="hidden" id="gp_personal_id_ref" name="gp_personal_id_ref"<?php 
		 if (!empty($gp_personal_id)) echo 'value="'.$gp_personal_id.'"'; ?> />
		 
        <div class="panel-body" style="padding:0px;">
        	<div class="tabbable hide">
                    <ul class="nav nav-tabs">
                        <li class="dropdown">                
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Personal <b class="caret"></b></a>
                            <ul class="dropdown-menu">                
                                <li class="active"><a data-toggle="tab" href="#detail">Data Diri</a></li>
                                <li><a data-toggle="tab" href="#detailpage2">Lanjutan</a></li>
                            </ul>
                        </li>
						
                    </ul>
                </div>
                <div id="form-tab" class="tab-content">
				<!--- Detail (Personal) -->
                <div class="tab-pane active" id="detail" data-id="0">
							
							<div class="panel-heading" style="background-color:#FFF">
				                <span style="font-size:14px; font-weight:bold">DATA DIRI</span>
                            </div>
                            <div class="panel-body" style="padding:10px;">        
                                
                                    <form class="form-horizontal" role="form" id="formKaryawan1" autocomplete="off" name="formKaryawan1">   
                                        <div class="form-group">
                                            <label for="nrk" class="control-label col-xs-3 ">NRK</label>
                                            <div class="col-xs-6"> 
                                                <input class="form-control" type="text" style="text-align:justify" id="nrk" name="nrk" placeholder="xxxxxxxxx" autocomplete="off" disabled="disabled">                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="id_card_no" class="control-label col-xs-3 <?php echo $vkaryawan["id_card_no"]["required"]; ?>">No. KTP</label>
                                            <div class="col-xs-6" style="text-align:right">
                                                <input class="form-control" type="text" id="id_card_no" name="id_card_no" placeholder="No. KTP" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="name" class="control-label col-xs-3 <?php echo $vkaryawan["name"]["required"]; ?>">Nama</label>
                                            <div class="col-xs-8">
                                                <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama" autocomplete="off">
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="birth_place" class="control-label col-xs-3 <?php echo $vkaryawan["birth_place"]["required"]; ?>">Tempat Lahir</label>
                                            <div class="col-xs-3">
                                                <?php //echo $birth_place; ?>
                                                <input class="form-control" type="text" id="birth_place_name" name="birth_place_name" placeholder="Tempat Lahir" data-provide="typeahead" />
                                                <input class="form-control" type="hidden" id="m_birth_place_id" name="m_birth_place_id" placeholder="Tempat Lahir" />
                                            </div>
                                            <label for="tanggal lahir" class="control-label col-xs-2 <?php echo $vkaryawan["birth_date"]["required"]; ?>">Tanggal Lahir</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="birth_date" name="birth_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="gender_id" class="control-label col-xs-3 <?php echo $vkaryawan["gender_id"]["required"]; ?>">Jenis Kelamin</label>
                                            <div class="col-xs-3">
                                                <?php echo $gender_id; ?>
                                            </div>
                                            <label for="marital_status_id" class="control-label col-xs-2 <?php echo $vkaryawan["marital_status_id"]["required"]; ?>">Status Perkawinan</label>
                                            <div class="col-xs-3">
                                                <?php echo $marital_status_id; ?>
                                            </div>                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="religion" class="control-label col-xs-3 <?php echo $vkaryawan["religion_id"]["required"]; ?>">Agama</label>
                                            <div class="col-xs-3">
                                                <?php echo $religion_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="country" class="control-label col-xs-3">Kewarganegaraan</label>
                                            <div class="col-xs-3">
                                                <?php echo $country_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="blood" class="control-label col-xs-3 <?php echo $vkaryawan["blood_type_id"]["required"]; ?>">Golongan Darah</label>
                                            <div class="col-xs-3">
                                                <?php echo $blood_type_id; ?>
                                            </div>                                            
                                        </div>                                        
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Alamat KTP</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="address" class="control-label col-xs-3 <?php echo $vkaryawan["address"]["required"]; ?>">Alamat</label>
                                            <div class="col-xs-8">
                                                <textarea class="form-control" id="address" name="address" placeholder="Alamat"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="RT" class="control-label col-xs-3">RT</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rt" name="rt" maxlength='3' placeholder="RT">
                                            </div>
                                            <label for="RW" class="control-label col-xs-1">RW</label>
                                            <div class="col-xs-2">
                                                <input class="form-control" type="text" id="rw" name="rw" maxlength='3' placeholder="RW">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="sub_district" class="control-label col-xs-3 <?php echo $vkaryawan["m_sub_district_id"]["required"]; ?>">Kelurahan</label>
                                            <div class="col-xs-3">
                                                <?php //echo $sub_district_id; ?>
                                                <input type="text" class="form-control" id="m_sub_district_name" name="m_sub_district_name" placeholder="Kelurahan" data-provide="typeahead" />
                                                <input type="hidden" id="m_sub_district_id" name="m_sub_district_id" value="0" />
                                            </div>
                                            <label for="district" class="control-label col-xs-2 ">Kecamatan</label>
											<div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_district_name" name="m_district_name" placeholder="Kecamatan" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_district_id" name="m_district_id" value="0" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="city" class="control-label col-xs-3 ">Kota</label>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_city_name" name="m_city_name" placeholder="Kota" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_city_id" name="m_city_id" value="0" />
                                            </div>
                                            <label for="province" class="control-label col-xs-2 ">Provinsi</label>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" id="m_state_name" name="m_state_name" placeholder="Provinsi" data-provide="typeahead" readonly="readonly" />
                                                <input type="hidden" id="m_state_id" name="m_state_id" value="0" />
                                            </div>
                                        </div>                                        
                                        <div class="form-group">                                            
                                            <label for="zipe code" class="control-label col-xs-3 ">Kode Pos</label>
                                            <div class="col-xs-3">
                                                <input class="form-control" type="text" maxlength="6" id="zipe_code" name="zipe_code" placeholder="Kode Pos" />
                                            </div>                                        
										</div>
                                        <div class="form-group" id="hph">
                                            <label for="handphone" class="control-label col-xs-3 <?php echo $vkaryawan["handphone"]["required"]; ?>">HP</label>                                            
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="handphone" name="handphone" placeholder="Handphone (pastikan nomor dapat dihubungi)">
                                            </div>
                                        </div>                                      
                                        <div class="form-group" id="hph">
                                            <label for="home_phone" class="control-label col-xs-3 <?php echo $vkaryawan["home_phone"]["required"]; ?>">No. Telepon</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="home_phone" name="home_phone" placeholder="No. Telp Rumah">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="control-label col-xs-3">Email</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="email_address" name="email_address" placeholder="Email">
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="faskes" class="control-label col-xs-3 ">Fasilitas Kesehatan</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_faskes_name" name="m_faskes_name" placeholder="cari fasilitas kesehatan" data-provide="typeahead">
                                                <input type='hidden' class="form-control" name="m_faskes_id" id="m_faskes_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="dokgi" class="control-label col-xs-3 ">Dokter Gigi</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="dokgi" name="dokgi" placeholder="">
                                            </div>
                                        </div>
                                        <br />
                                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Perusahaan Karyawan</span>
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="company" class="control-label col-xs-3 <?php echo $vkaryawan["m_company_id"]["required"]; ?>">Nama Perusahaan</label>
                                            <div class="col-xs-7">
                                                <?php echo $company_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="join_date" class="control-label col-xs-3 <?php echo $vkaryawan["join_date"]["required"]; ?>">Tanggal Diterima</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="join_date" name="join_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_join" class="control-label col-xs-1 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_join" name="sk_join" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="start_permanen_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_permanen_date"]["required"]; ?>">Tanggal Permanen</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="start_permanen_date" name="start_permanen_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                            <label for="sk_permanen" class="control-label col-xs-1 ">No SK</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="sk_permanen" name="sk_permanen" placeholder="">
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="status karyawan" class="control-label col-xs-3 <?php echo $vkaryawan["ref_status_kerja_id"]["required"]; ?>">Status Karyawan</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_status_kerja_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="stop_date" class="control-label col-xs-3 ">Tanggal Berhenti</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="stop_date" name="stop_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_branch_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_branch_id"]["required"]; ?>">Kantor Cabang</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_branch_name" name="m_branch_name" placeholder="cari kantor cabang">
                                                <input type='hidden' class="form-control" name="m_branch_id" id="m_branch_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_client_id" class="control-label col-xs-3 <?php echo $vkaryawan["m_client_id"]["required"]; ?>">Client</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="cari client">
                                                <input type='hidden' class="form-control" name="m_client_id" id="m_client_id" placeholder="id" value="0"/>
												<input type='hidden' class="form-control" id="m_client_code" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="m_location_id" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                            <div class="col-xs-7">                                                
                                                <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="pilih lokasi kerja">
                                                <input type='hidden' class="form-control" name="m_location_id" id="m_location_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
										<input class="form-control"  type="hidden" id="m_area_id" name="m_area_id" placeholder="Wilayah">
                                            <label for="wilayah" class="control-label col-xs-3 ">Wilayah / Area</label>
                                            <div class="col-xs-3">
                                                <div class="form-control" >
                                                    <input class="form-control" type="text" id="wil_name" name="wil_name" placeholder="Wilayah" data-provide="typeahead">
													
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-3">
												<div class="form-control" >
                                                     <input class="form-control" type="text" id="area_name" name="area_name" placeholder="Area"  disabled>
													 
                                                </div>
                                                   
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="unit kerja" class="control-label col-xs-3 <?php echo $vkaryawan["m_unit_id"]["required"]; ?>">Unit Kerja</label>
                                            <div class="col-xs-7">
                                               <input class="form-control" type="text" id="m_unit_name" name="m_unit_name" placeholder="pilih unit kerja">
                                               <input type='hidden' class="form-control" name="m_unit_id" id="m_unit_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="jabatan" class="control-label col-xs-3 <?php echo $vkaryawan["m_structure_position_id"]["required"]; ?>">Jabatan</label>
                                            <div class="col-xs-7">
                                                <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="pilih jabatan">
                                               <input type='hidden' class="form-control" name="m_structure_position_id" id="m_structure_position_id" placeholder="id" value="0"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ref_team_work_id" class="control-label col-xs-3 ">Kelompok Kerja</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_team_work_id; ?>
                                            </div>                                            
                                        </div>
                                        <div class="form-group">
                                            <label for="team_work_date" class="control-label col-xs-3 ">Tanggal Join</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date '>
                                                    <input type='text' class="form-control" id="team_work_date" name="team_work_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <hr style="margin-top:10px" />
                                        <div class="form-group">
                                            <label for="pkwt_no" class="control-label col-xs-3 ">No PKWT</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="pkwt_no" name="pkwt_no" placeholder="No PKWT">
                                            </div> 
                                            <label for="pkwt_date" class="control-label col-xs-2">Tanggal PKWT</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date '>
                                                    <input type='text' class="form-control" id="pkwt_date" name="pkwt_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">
                                            <label for="start_contract_date" class="control-label col-xs-3 <?php echo $vkaryawan["start_contract_date"]["required"]; ?>">Awal Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="start_contract_date" name="start_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>   
                                            <label for="stop_contract_date" class="control-label col-xs-2 <?php echo $vkaryawan["stop_contract_date"]["required"]; ?>">Akhir Kontrak</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="stop_contract_date" name="stop_contract_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                        <div class="form-group">                                            
                                            <label for="Status Kontrak" class="control-label col-xs-3">Status Kontrak</label>
                                            <div class="col-xs-3">
                                                <?php echo $ref_status_pkwt_id; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="job_no" class="control-label col-xs-3 ">No Penugasan</label>
                                            <div class="col-xs-3">
                                                    <input class="form-control" type="text" id="job_no" name="job_no" placeholder="No Penugasan">
                                            </div> 
                                            <label for="job_date" class="control-label col-xs-2 ">Tgl Penugasan</label>
                                            <div class="col-xs-3">
                                                <div class='input-group date'>
                                                    <input type='text' class="form-control" id="job_date" name="job_date"/>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>                                                                                       
                                        </div>                                        
                                    </form>
                              </div>
                </div>
				<!--- Personal (detailpage2) -->
                <div class="tab-pane" id="detailpage2" data-id="1">                    
                    <div class="panel-body" style="padding:10px;">
                        <form class="form-horizontal" role="form" id="formKaryawan2" name="formKaryawan2" autocomplete="off">                                                        
                        <div class="form-group">
                            <label for="user" class="control-label col-xs-3 ">User</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name" name="m_criteria_name" placeholder="cari user"  >
                                <input type='hidden' class="form-control" name="m_criteria_id" id="m_criteria_id" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code" id="m_criteria_code" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="HRBP" class="control-label col-xs-3 ">HRBP</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name2" name="m_criteria_name2" placeholder="cari HRBP">
                                <input type='hidden' class="form-control" name="m_criteria_id2" id="m_criteria_id2" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code2" id="m_criteria_code2" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Cost Center" class="control-label col-xs-3 ">Cost Center</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name3" name="m_criteria_name3" placeholder="cari cost center">
                                <input type='hidden' class="form-control" name="m_criteria_id3" id="m_criteria_id3" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code3" id="m_criteria_code3" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Asuransi" class="control-label col-xs-3 ">Asuransi</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name4" name="m_criteria_name4" placeholder="cari asuransi">
                                <input type='hidden' class="form-control" name="m_criteria_id4" id="m_criteria_id4" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code4" id="m_criteria_code4" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="Bucket" class="control-label col-xs-3 ">Bucket</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="m_criteria_name5" name="m_criteria_name5" placeholder="cari bucket">
                                <input type='hidden' class="form-control" name="m_criteria_id5" id="m_criteria_id5" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code5" id="m_criteria_code5" placeholder="id" value="0"/>
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="sipo" class="control-label col-xs-3 ">SIPO-ID</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="sipo_id" name="sipo_id" placeholder="SIPO ID" autocomplete="off">
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">                                            
                            <label for="mother_name" class="control-label col-xs-3 ">Nama Ibu Kandung</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="mother_name" name="mother_name" placeholder="Nama Ibu Kandung" />
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="family_card_no" class="control-label col-xs-3 ">No. Kartu Keluarga (KK)</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="family_card_no" name="family_card_no" placeholder="No. Kartu Keluarga (KK)" />
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="lembur" class="control-label col-xs-3 ">Berhak Lembur</label>
                            <div class="col-xs-3">
                                <?php echo $ref_overtime_status_id; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="bpjs" class="control-label col-xs-3 ">Berhak BPJS Kesehatan</label>
                            <div class="col-xs-3">
                                <?php echo $ref_bpjskes_status_id; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="bpjs" class="control-label col-xs-3 ">No. BPJS Kesehatan</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="bpjskes_no" name="bpjskes_no" placeholder="Nomor BPJS jika ada">
                            </div>                            
                        </div>
                        <div class="form-group">                            
                            <label for="jamsostek" class="control-label col-xs-3 ">No. Jamsostek</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="jamsostek_no" name="jamsostek_no" placeholder="Nomor Jamsostek jika ada">
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Pendidikan Terakhir</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="Pendidikan" class="control-label col-xs-3 <?php echo $vkaryawan["ref_institution_type"]["required"]; ?>">Pendidikan</label>
                            <div class="col-xs-3">
                                <?php echo $ref_institution_type; ?>
                            </div>                                            
                        </div> 
                        <div class="form-group">
                            <label for="institusi" class="control-label col-xs-3 ">Nama Sekolah / Universitas</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="institution_name" name="institution_name" placeholder="Nama Sekolah / Universitas">
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="major" class="control-label col-xs-3 ">Jurusan</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="major" name="major" placeholder="jurusan">
                            </div>                                            
                        </div>
                        <div class="form-group">
                            <label for="IPK" class="control-label col-xs-3 ">IPK</label>
                            <div class="col-xs-2">
                                <input class="form-control" type="text" id="gpa" maxlength="10" name="gpa" placeholder="IPK">
                            </div>
                            <label for="tahun lulus" class="control-label col-xs-3 <?php echo $vkaryawan["graduated"]["required"]; ?>">Tahun Lulus</label>
                            <div class="col-xs-2">
                                <input class="form-control" type="text" maxlength="4" id="graduated" name="graduated" placeholder="Tahun lulus">
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Pajak</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="PTKP" class="control-label col-xs-3 <?php echo $vkaryawan["ref_stat_ptkp"]["required"]; ?>">Status PTKP</label>
                            <div class="col-xs-3">
                                <?php echo $ref_stat_ptkp; ?>
                            </div>      
                            <label for="tanggungan" class="control-label col-xs-2 <?php echo $vkaryawan["number_dependents"]["required"]; ?>">Jumlah Tanggungan</label>
                            <div class="col-xs-1">
                                <input class="form-control" type="text" id="number_dependents" name="number_dependents" placeholder="">
                            </div>
                            <label class="control-label">Orang</label>
                        </div>
                        <div class="form-group">                                            
                            <label for="npwp_no" class="control-label col-xs-3 ">No. NPWP</label>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" maxlength="20" id="npwp_no" name="npwp_no" placeholder="No. NPWP" />
                            </div>
                        </div>
                        <br />
                        <span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Rekening Pembayaran Gaji</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="bank" class="control-label col-xs-3">Nama Bank</label>
                            <div class="col-xs-3">
                                <?php echo $bank_id; ?>
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="acc id" class="control-label col-xs-3 ">No. Rekening</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="acc_id" name="acc_id" placeholder="No. Rekening" />
                            </div>
                        </div>
                        <div class="form-group">                                            
                            <label for="acc name" class="control-label col-xs-3 ">Pemilik Rekening</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" maxlength="20" id="acc_name" name="acc_name" placeholder="Pemilik Rekening" />
                            </div>
                        </div>
                        <br />
                        <hr style="margin-top:10px" />
                        <div class="form-group">
                            <label for="personal_note" class="control-label col-xs-3 ">Keterangan</label>
                            <div class="col-xs-6">
                                <input class="form-control" type="text" id="personal_note" name="personal_note" placeholder="catatan">
                            </div>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#addNote">                                
                                Add
                            </button>                             
                        </div>
                       </form>
                    </div>
                </div>
		<!-----------------------------------------------------alamat------------------------------------------------------------------------------->
         <div class="tab-pane" id="alamat" style="background-color:#FFF" data-id="1">                    
           <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Alamat Rumah</b></h3>
				</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formAlamat" name="formAlamat" autocomplete='off'>
						 <input class="form-control" type="hidden" id="act-alamat"/>
						 <input class="form-control" type="hidden" id="gp_address_id" name="gp_address_id" value='0'/>
			    		<div class="form-group">
							<label for="alamat" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-alamat" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-alamat" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
                        <div class="form-group">
                            <label for="name" class="control-label col-xs-3 ">Jalan</label>
                            <div class="col-xs-8">
                                <textarea class="form-control" id="address-alamat" name="address" placeholder="Jalan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
		                	<label for="bpjs" class="control-label col-xs-3">RT</label>
		                    <div class="col-xs-2">
		                        <input class="form-control" type="text" id="rt-alamat" name="rt" placeholder="RT">
		                    </div>
		                    <label for="npwp" class="control-label col-xs-1">RW</label>
		                    <div class="col-xs-2">
		                        <input class="form-control" type="text" id="rw-alamat" name="rw" placeholder="RW">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="name" class="control-label col-xs-3 ">Kelurahan</label>
		                    <div class="col-xs-3">
		                        <?php //echo $sub_district_id; ?>
		                        <input type="text" class="form-control" id="m_sub_district_name-alamat" name="m_sub_district_name" placeholder="Kelurahan" data-provide="typeahead" />
		                        <input type="hidden" id="m_sub_district_id-alamat" name="m_sub_district_id" />
		                    </div>
		                    <label for="name" class="control-label col-xs-2 ">Kecamatan</label>
							<div class="col-xs-3">

		                        <input type="text" class="form-control" id="m_district_name-alamat" name="m_district_name" placeholder="Kecamatan" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_district_id-alamat" name="m_district_id"  />
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="city_id" class="control-label col-xs-3 ">Kota</label>
		                    <div class="col-xs-3">
		                        <input type="text" class="form-control" id="m_city_name-alamat" name="m_city_name" placeholder="Kota" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_city_id-alamat" name="m_city_id"/>
		                    </div>
		                    <label for="province_id" class="control-label col-xs-2 ">Provinsi</label>
		                    <div class="col-xs-3">
		                        <input type="text" class="form-control" id="m_state_name-alamat" name="m_state_name" placeholder="Provinsi" data-provide="typeahead" readonly="readonly" />
		                        <input type="hidden" id="m_state_id-alamat" name="m_state_id" value="0" />
		                    </div>
		                </div>                                        
		                <div class="form-group">                                            
		                    <label for="name" class="control-label col-xs-3 ">Kode Pos</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="zipe_code-alamat" maxlength='6' name="zipe_code" placeholder="Kode Pos" />
		                    </div>                                        
						</div>
		                <div class="form-group" id="hph">     
                            <label for="handphone" class="control-label col-xs-3 ">HP</label>                                            
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="handphone-alamat" name="handphone" placeholder="Handphone (pastikan nomor dapat dihubungi)">
                            </div>
                        </div>                                      
                        <div class="form-group" id="hph">
                            <label for="home_phone" class="control-label col-xs-3 ">No. Telepon</label>
                            <div class="col-xs-7">
                                <input class="form-control" type="text" id="home_phone-alamat" name="home_phone" placeholder="No. Telp Rumah">
                            </div>
                        </div>
		                <br />
		                <hr style="margin-top:10px" />
		                <div class="form-group">
		                    <label for="faskes" class="control-label col-xs-3 ">Fasilitas Kesehatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_faskes_name-alamat" name="m_faskes_name" placeholder="cari fasilitas kesehatan">
		                    	<input type='hidden' class="form-control" name="m_faskes_id" id="m_faskes_id-alamat" placeholder="id" value="0"/>
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="dokgi" class="control-label col-xs-3 ">Dokter Gigi</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="dokgi-alamat" name="dokgi" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="alamat" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-alamat">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Alamat Rumah</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<div style="width:2000px">
						<table class="table table-bordered" id="tbl-alamat">
								<thead>
								<tr class="info"><th>Jalan</th><th>RT</th><th>RW</th><th>Kode Pos</th><th>Kelurahan</th><th>Kecamatan</th><th>Kota</th><th>No. Telp</th><th>No. HP</th><th>Keterangan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar Alamat Rumah -->
						    </tbody>
						</table>
						</div>	
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-karyawan">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-karyawan" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-karyawan" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-karyawan" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-alamat"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan_karyawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton_karyawan" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
				</div>
                </div>
		<!------------------------------------------keluarga------------------------------------------------------------------------------------------>		
		<div class="tab-pane" id="keluarga" style="background-color:#FFF" data-id="1">                    
           <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Data Keluarga</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formKeluarga" name="formKeluarga" autocomplete='false'>			    		
			    		<div class="form-group">
						<input class="form-control" type="hidden" id="gp_family_id" name="gp_family_id" value='0' />
						<input class="form-control" type="hidden" id="act-keluarga"  />
							<label for="keluarga" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-keluarga" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-keluarga" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
                        <div class="form-group">                                            
		                    <label for="name" class="control-label col-xs-3 ">No. Kartu Keluarga</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="family_card_no-keluarga" name="family_card_no" readonly='readonly' placeholder="No. Kartu Keluarga" />
		                    </div>
						</div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Nama Anggota</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="family_name" name="family_name" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Hubungan</label>
		                    <div class="col-xs-3">
                                <?php echo $ref_relation_id; ?>
                            </div>
		                </div>
		                <div class="form-group">
                            <label for="m_birth_place" class="control-label col-xs-3 ">Tempat Lahir</label>
                            <div class="col-xs-3">
                                <?php //echo $m_m_birth_place_id; ?>
                                <input class="form-control" type="text" id="m_birth_place_name" placeholder="Tempat Lahir" data-provide="typeahead" />
                                <input class="form-control" type="hidden" id="m_birth_place_id-keluarga" name="m_birth_place_id" placeholder="Tempat Lahir" value=0 />
                            </div>
                            <label for="tanggal lahir" class="control-label col-xs-2 ">Tanggal Lahir</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="birth_date-keluarga" name="birth_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="gender_id" class="control-label col-xs-3 ">Jenis Kelamin</label>
                            <div class="col-xs-3">
                                <?php echo $gender_id_keluarga; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="religion" class="control-label col-xs-3 ">Agama</label>
                            <div class="col-xs-3">
                                <?php echo $religion_id_keluarga; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="BPJS" class="control-label col-xs-3">BPJS</label>
                            <div class="col-xs-3">
                                <?php echo $ref_bpjskes_status_id_keluarga; ?>
                            </div>
                            <label for="No. BPJS" class="control-label col-xs-2 ">No. BPJS</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="bpjskes_no-keluarga" name="bpjskes_no" placeholder="No. BPJS" />
		                    </div>                                        
                        </div>		                
						<div class="form-group">
							<label for="keluarga" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-keluarga">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Data Keluarga</b></h4>
					</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-keluarga">
								<thead>
								<tr class="info"><th>Nama</th><th>Hubungan</th><th>Tempat Tanggal Lahir</th><th>Agama</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-keluarga">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-keluarga" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-keluarga" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-keluarga" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-keluarga"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		
		<!-------kontrak kerja--------------------------------------------------------------------------------------------------->
		
	  <div class="tab-pane" id="kontrak_kerja" style="background-color:#FFF" data-id="1">         
         <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Kontrak Kerja</b></h3>
	            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formKontrak" name="formKontrak">			    		
			    		<div class="form-group">
						<input type='hidden' class='form-control'/>
							<label for="nama karyawan" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-kontrak" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-kontrak" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="client" class="control-label col-xs-8 col-md-3">Nama Client</label>
								<div class="col-xs-2">
					               <input class="form-control" type="text" id="m_client_code-kontrak" name="m_client_code" placeholder="kode client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name-kontrak" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
                            <label for="Mulai Kontrak" class="control-label col-xs-3 ">Mulai Kontrak</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="start_contract_date-kontrak" name="start_contract_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="Akhir Kontrak" class="control-label col-xs-2 ">Akhir Kontrak</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="stop_contract_date-kontrak" name="stop_contract_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 <?php echo $vkontrak ["ref_status_pkwt_id"]["required"]; ?>">Status Kontrak</label>
		                   <div class="col-xs-8">
                                <?php echo $ref_status_pkwt_id; ?>
                            </div>
		                </div>
                        <div class="form-group">                                            
		                    <label for="No. PKWT" class="control-label col-xs-3 ">No. PKWT</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="pkwt_no" name="pkwt_no" placeholder="No. PKWT" />
		                    </div>
		                    <label for="Tanggal PKWT" class="control-label col-xs-2 ">Tanggal PKWT</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="pkwt_date-kontrak" name="pkwt_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="No. Penugasan" class="control-label col-xs-3 ">No. Penugasan</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="job_no" name="job_no" placeholder="No. Penugasan" />
		                    </div>
		                    <label for="Tanggal Penugasan" class="control-label col-xs-2 ">Tanggal Penugasan</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="job_date-kontrak" name="job_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">		                    
		                    <label for="Tanggal Surat Penugasan" class="control-label col-xs-3 ">Tanggal Surat Penugasan</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='letter_job_date'>
                                    <input type='text' class="form-control" id="letter_job_date" name="letter_job_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="PKS" class="control-label col-xs-3 ">No. PKS</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="pks_no" name="pks_no" placeholder="No. PKS" />
		                    </div>
		                    <label for="Tanggal PKWT" class="control-label col-xs-2 ">Tanggal PKS</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='stop_pks_date'>
                                    <input type='text' class="form-control" id="stop_pks_date" name="stop_pks_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
						</div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>		                
						<div class="form-group">
							<label for="kontrak" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-kontrak">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Kontrak Kerja</b></h4>
					</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-kontrak">
								<thead>
								<tr class="info"><th>Mulai</th><th>Sampai</th><th>Status Kontrak</th><th>No PKWT</th><th>Catatan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-kontrak">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-kontrak" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-kontrak" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-kontrak" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-kontrak"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>   
	</div>  
	<!---------------------------------------------user, HRBP Dll------------------------------------------------------------------------->
	<div class="tab-pane" id="user" style="background-color:#FFF" data-id="1">
		<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>User, HRBP dll</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formUserClient" name="formUserClient">	
						<input class="form-control" type="hidden" id="gp_user_client_id" name="gp_user_client_id" value="0"/>
						<input class="form-control" type="hidden" id="act-user" name="act-user" />
			    		<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-user" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-user" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jabatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_structure_position_id-user" name="m_structure_position_id" placeholder="Kode - Nama Jabatan" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3">Nama Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code-user"  placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name-user"  placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                            
		                    <label for="keterangan" class="control-label col-xs-8 col-md-3">Kriteria Kelompok</label>
		            		<div class="col-xs-3">
                                <?php echo $ref_criteria_id; ?>
                            </div>
                            <label for="Akhir user_client" class="control-label col-xs-2 ">TMT</label>
                            <div class="col-xs-3">
                                <div class='input-group date '>
                                    <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_criteria_name-user" name="m_criteria_name" placeholder="cari nama kriteria">
		                    	<input type='hidden' class="form-control" name="m_criteria_id" id="m_criteria_id-user" placeholder="id" value="0"/>
								<input type='hidden' class="form-control" name="m_criteria_code" id="m_criteria_code-user" placeholder="id" value="0"/>
		                    </div>
		                </div>
						<div class="form-group">                                            
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note-user" name="note-user" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="user_client" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-user">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar User, HRBP dll </b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">
						</div>
						<table class="table table-bordered" id="tbl-user">
								<thead>
								<tr class="info"><th>Kriteria</th><th>Keterangan</th><th>TMT</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar User, HRBP dll -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-user">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-user" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-user" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-user" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-user"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
	<!-------------------------------------------------------gaji---------------------------------------------------------------------->
	<div class="tab-pane" id="gaji" style="background-color:#FFF" data-id="1"> 
		<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Perubahan Gaji Pokok</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formGaji" name="formGaji">			    		
			    		<div class="form-group">
						 <input class="form-control" type="hidden" id="gp_salary_id" name="gp_salary_id"/>
						  <input class="form-control" type="hidden" id="act-gaji" />
							<label for="gaji" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-gaji" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-gaji" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Unit</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_unit_name-gaji" name="m_unit_name" placeholder="" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="keterangan" class="control-label col-xs-8 col-md-3">Jabatan</label>
		            		<div class="col-xs-3">
	                             <input class="form-control" type="text" id="m_structure_position_name-gaji" name="m_structure_position_name" placeholder="Nama Jabatan" disabled="disabled">
	                        </div>                            
	                        <label for="Akhir gaji" class="control-label col-xs-2 ">Tanggal Dijabat</label>
	                        <div class="col-xs-3">
	                              <div class='input-group date'>
                                    <input type='text' class="form-control" id="join_date-gaji" name="join_date" disabled="disabled"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
	                        </div>
                        </div>
						<div class="form-group">                            
		                    <label for="Akhir user_client" class="control-label col-xs-3 ">TMT Gaji</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="salary_start_date" name="salary_start_date" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Gaji Pokok</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">                            
		                    <label for="status" class="control-label col-xs-8 col-md-3">Status Aktif</label>
		            		<div class="col-xs-3">
                                <?php echo $status; ?>
                            </div>		                	
                            
                        </div>
                        <div class="form-group">
		                    <label for="Akhir user_client" class="control-label col-xs-3 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="sk_date-gaji" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="Akhir gaji" class="control-label col-xs-2 ">No. SK</label>
	                        <div class="col-xs-3">
	                             <input class="form-control" type="text" id="sk_no-gaji" name="sk_no" placeholder="" >
	                        </div>                            
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note-gaji" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="gaji" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-gaji">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Perubahan Gaji Pokok </b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-gaji">
								<thead>
								<tr class="info"><th>TMT Gaji</th><th>Gaji Pokok</th><th>Status (N/Y)</th><th>Keterangan</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-gaji">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-gaji" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-gaji" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-gaji" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-gaji"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		<!---------------------------------------tunjangan------------------------------------------------->
		<div class="tab-pane" id="tunjangan" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Tunjangan-Tunjangan</b></h3>
	            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formTunjangan" name="formTunjangan">	
						<input class="form-control" type='hidden' id="gp_fixed_transaction_id" name='gp_fixed_transaction_id'/>
						<input class="form-control" type= 'hidden' id="act-tunjangan"/>
			    		<div class="form-group">
							<label for="Tunjangan" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-tunjangan" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-tunjangan" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
								<label for="Tunjangan" class="control-label col-xs-8 col-md-3 <?php echo $vtunjangan["m_p_description_transaction_name"]["required"]; ?>">Nama Transaksi</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_description_transaction_code" placeholder="Kode Transaksi" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">                                    
                                    <input class="form-control" type="text" id="m_p_description_transaction_name" placeholder="cari nama tunjangan - tipe transaksi - tarif client / indvidu" data-provide="typeahead" />
                                	<input type='hidden' class="form-control" name="m_p_description_transaction_id" id="m_p_description_transaction_id" placeholder="id" value="0"/>	
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 <?php echo $vtunjangan["value"]["required"]; ?>">Nilai</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value-tunjangan" name="value" placeholder="Masukkan nilai">
		                    </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TMT Transaksi" class="control-label col-xs-3 <?php echo $vtunjangan["transaction_start_date"]["required"]; ?>">TMT Transaksi</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="transaction_start_date-tunjangan" name="transaction_start_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">TAT Transaksi</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='transaction_end_date'>
                                    <input type='text' class="form-control" id="transaction_end_date-tunjangan" name="transaction_end_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>						
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note-tunjangan" name="note" placeholder="">
		                    </div>
		                </div>						
						<div class="form-group">
							<label for="Tunjangan" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-tunjangan">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Tunjangan - Tunjangan</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-tunjangan">
								<thead>
								<tr class="info"><th>Kode Transaksi</th><th>Nama Transaksi</th><th>Nilai</th><th>TMT</th><th>TAT</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-tunjangan">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-tunjangan" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-tunjangan" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-tunjangan" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-tunjangan"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>	
		<!--------------------------------------------------------------mutasi------------------------------------------------------------>
		<div class="tab-pane" id="mutasi" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Mutasi Jabatan</b></h3>
	            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory">(tanda * wajib diisi)</h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formMutasi" name="formMutasi">			    		
			    		<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-mutasi" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-mutasi" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code-mutasi" name="m_client_code" placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name-mutasi" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Nama Perusahaan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="m_company_name" name="m_company_name" placeholder="Nama Perusahaan" disabled="disabled">
		                    </div>
		                </div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 <?php echo $vmutasi["ref_mutation_type_id"]["required"]; ?>">Jenis Mutasi</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_mutation_type_id; ?>
                            </div>
		                </div>
		                <div class="form-group">
                            <label for="m_branch_id" class="control-label col-xs-3 <?php echo $vmutasi["m_branch_id"]["required"]; ?>">Kantor Cabang</label>
                            <div class="col-xs-8">
                                <input class="form-control" type="text" id="m_branch_name-mutasi" name="m_branch_name" placeholder="cari kantor cabang">
                                <input type='hidden' class="form-control" name="m_branch_id" id="m_branch_id-mutasi" placeholder="id" value="0"/>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="m_location_id" class="control-label col-xs-3 <?php echo $vmutasi["m_location_id"]["required"]; ?> ">Lokasi Kerja</label>
                            <div class="col-xs-8">                                                
                                <input class="form-control" type="text" id="m_location_name-mutasi" name="m_location_name" placeholder="cari lokasi kerja (kode - nama lokasi)">
                                <input type='hidden' class="form-control" name="m_location_id" id="m_location_id-mutasi" placeholder="id" value="0"/>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="unit kerja" class="control-label col-xs-3 <?php echo $vmutasi["m_unit_id"]["required"]; ?>">Unit Kerja</label>
                            <div class="col-xs-8">
                               <input class="form-control" type="text" id="m_unit_name-mutasi" name="m_unit_name" placeholder="cari unit kerja (kode - nama unit)">
                               <input type='hidden' class="form-control" name="m_unit_id" id="m_unit_id-mutasi" placeholder="id" value="0"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="jabatan" class="control-label col-xs-3 <?php echo $vmutasi["m_structure_position_id"]["required"]; ?>">Jabatan</label>
                            <div class="col-xs-8">
                                <input class="form-control" type="text" id="m_structure_position_name-mutasi" name="m_structure_position_name" placeholder="cari jabatan">
                               <input type='hidden' class="form-control" name="m_structure_position_id" id="m_structure_position_id-mutasi" placeholder="id" value="0"/>
                            </div>
                        </div>
		                <div class="form-group">                            
		                    <label for="TMT Transaksi" class="control-label col-xs-3 <?php echo $vmutasi["effective_date"]["required"]; ?>">Tanggal Efektif Mutasi</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='effective_date'>
                                    <input type='text' class="form-control" id="effective_date" name="effective_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">                            
		                    <label for="TMT Transaksi" class="control-label col-xs-3 ">No. SK</label>
                            <div class="col-xs-3">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="sk_date-mutasi" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="value" name="value" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-mutasi">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Mutasi Jabatan</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-mutasi">
								<thead>
								<tr class="info"><th>Tanggal Mutasi</th><th>Lokasi</th><th>Unit Kerja</th><th>Nama Jabatan</th><th>Nama Cabang</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-mutasi">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-mutasi" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-mutasi" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-mutasi" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-mutasi"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		
		<!-----------------------------------------dokumen------------------------------------------------------------->
		<div class="tab-pane" id ="dokumen" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Kelengkapan Dokumen</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formDokumen" name="formDokumen">			    		
			    		<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-dokumen" name="nrk-dokumen" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-dokumen" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>
						</div>
						<hr  />
						<div class="form-group">
							<label for="Mutasi" class="control-label col-xs-8 col-md-2">Nama Dokumen</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-3">Tanggal Kelengkapan</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-1">Status</label>
							<label for="Mutasi" class="control-label col-xs-8 col-md-2">Remarks</label>			
						</div>
						<div class="form-group">
						<div class="col-lg-3">
							    <div class="input-group">
							      <span class="input-group-addon">
							        <input type="checkbox" aria-label="...">
							      </span>
							      <input type="text" class="form-control" aria-label="..." disabled="disabled"/>
							    </div>
							</div>
							<div class="col-xs-2">
	                            <div class='input-group date ' id='doc_date'>
	                                <input type='text' class="form-control" id="doc_date" name="doc_date"/>
	                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
	                                </span>
	                            </div>
	                        </div>
	                        <div class="col-xs-2">
	                                <?php echo $ref_doc_status_id; ?>
	                        </div>
	                        <div class="col-xs-3">
	                            <input class="form-control" type="text" id="doc_note" name="doc_note" placeholder="Remarks" data-provide="typeahead" />                                		            		
			            	</div>
			            	<span class="btn btn-default btn-file">
							    Browse <input type="file">
							</span>
							<hr  />
						</div>

					</form>	
									
					
				</div>	
				
		</div>
		</div>
		<!-----------------------------------------sanksi Dan Disiplin------------------------------------->
		
		<div class="tab-pane" id="sanksi" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Sanksi dan Kedisiplinan</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formSanksi" name="formSanksi">
									
			    		<div class="form-group">
							<label for="sanksi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
									<input type='hidden' id="gp_punishment_id" style="form-control" name='gp_punishment_id' value='0'/>
									<input type='hidden' id="act-sanksi" style="form-control"/>
                                   <input class="form-control" type="text" id="nrk-sanksi" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-sanksi" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>							
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jenis Sanksi</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_punishment_type_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal Mulai</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="punishment_start_date" name="punishment_start_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal Berakhir</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="punishment_end_date" name="punishment_end_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">No. Keputusan</label>
                            <div class="col-xs-3">                                
                                    <input class="form-control" type="text" id="decision_no" name="decision_no">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal</label>
                            <div class="col-xs-3">
                                <div class='input-group date'>
                                    <input type='text' class="form-control" id="decision_date" name="decision_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Rujukan</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_reference_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Pasal</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="article" name="article" placeholder="">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-1 ">Ayat</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="verse" name="verse" placeholder="">                                
                            </div>
                            <label for="TAT Transaksi" class="control-label col-xs-1 ">Huruf</label>
                            <div class="col-xs-2">                                
                                    <input class="form-control" type="text" id="letter" name="letter" placeholder="">                                
                            </div>
                        </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Sebab</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="reason" name="reason" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Golongan Hukuman</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_group_punishment; ?>
                            </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Pejabat TTD</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="official" name="official" placeholder="">
		                    </div>
		                </div>
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Keterangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note-sanksi" name="note" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="sanksi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-sanksi">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>                       
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Hukuman Disiplin</b></h4>
						</br>
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-sanksi">
								<thead>
								<tr class="info"><th>Nama Hukuman</th><th>Mulai</th><th>Akhir</th><th>Tanggal Keputusan</th><th>No Keputusan</th><th>Sebab</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-sanksi">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-sanksi" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-sanksi" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-sanksi" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-sanksi"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		
		<!----------------------------Cuti-------------------------------------------------->
		<div class="tab-pane" id="cuti" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Cuti</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formCuti" name="formCuti">			    		
			    		<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-cuti" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-cuti" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3">Client</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_client_code-cuti" name="m_client_code" placeholder="Kode Client" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="m_client_name-cuti" name="m_client_name" placeholder="Nama Client" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Jenis Cuti</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_leave_id; ?>
                            </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="Tanggal Kontrak" class="control-label col-xs-3 ">Tanggal Kontrak</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" id="start_contract_date-cuti" name="start_contract_date" placeholder="Tanggal Kontrak" data-provide="typeahead" disabled="disabled"/>
                                </div>                            
                            <label for="Sampai" class="control-label col-xs-2 ">Sampai</label>
                                 <div class="col-xs-3">
                                    <input class="form-control" type="text" id="stop_contract_date-cuti" name="stop_contract_date" placeholder="Sampai" data-provide="typeahead" disabled="disabled"/>
                                </div> 
                        </div>
                        <div class="form-group">
		                    <label for="mulai" class="control-label col-xs-3 ">Dari Tanggal</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='leave_start_date'>
                                    <input type='text' class="form-control" id="leave_start_date" name="leave_start_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="akhir" class="control-label col-xs-2 ">Sampai</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='leave_end_date'>
                                    <input type='text' class="form-control" id="leave_end_date" name="leave_end_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
		                    <label for="leave_sum" class="control-label col-xs-3 ">Jumlah Hari</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_sum" name="leave_sum" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="sk_no" class="control-label col-xs-3 ">No. SK</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="sk_no" name="sk_no" placeholder="">
		                    </div>
                            <label for="TAT Transaksi" class="control-label col-xs-2 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
		                <div class="form-group">
		                    <label for="signer" class="control-label col-xs-3 ">Penandatangan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="signer" name="signer" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="alasan" class="control-label col-xs-3 ">Alasan Cuti</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="reason_leave" name="reason_leave" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="pengganti" class="control-label col-xs-3 ">Pengganti</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="replacement" name="replacement" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="address" class="control-label col-xs-3 ">Alamat</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="address" name="address" placeholder="">
		                    </div>
		                </div>
                        <div class="form-group">
		                    <label for="handphone" class="control-label col-xs-3 ">Telepon</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="handphone" name="handphone" placeholder="">
		                    </div>
		                </div>
						<div class="form-group">
							<label for="cuti" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-cuti">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
						<br />
						<span style="font-size:16px; font-weight:bold; margin-bottom:0px;">Informasi Ketersediaan Cuti</span>
                        <hr style="margin-top:10px" />
                        <div class="form-group">
		                    <label for="hak cuti" class="control-label col-xs-3 ">Hak Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_entitlements" name="leave_entitlements" placeholder="" disabled="disabled">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Cuti Yang Diambil</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="leave_taken" name="leave_taken" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Hutang Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="debt_leave" name="debt_leave" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Saldo Cuti</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="saldo" name="saldo" placeholder="">
		                    </div>
		                </div>
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Cuti</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-cuti">
								<thead>
								<tr class="info"><th>Jenis Cuti</th><th>Mulai</th><th>Sampai</th><th>Hari</th><th>Saldo Cuti</th><th>Alamat Waktu Cuti</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-cuti">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-cuti" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-cuti" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-cuti" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-cuti"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		
		<!--------------------atensi---------------------------------------------------->
		
		<div class="tab-pane" id="atensi" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Atensi Manajemen</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formAtensi" name="formAtensi">			    		
			    		<div class="form-group">
						<input class="form-control" type='hidden' id="gp_atensi_id"  name="gp_atensi_id" value='0'/>
						<input class="form-control" type='hidden' id="act-atensi" />
							<label for="atensi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="hidden" id="nrk-atensi"  placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="hidden" id="personal_name-atensi"  placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>						
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal Catatan</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='add_date'>
                                    <input type='text' class="form-control" id="add_date-atensi" name="add_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group">
		                    <label for="title" class="control-label col-xs-3 ">Judul</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="title-atensi" name="title" placeholder="Judul Atensi">
		                    </div>
		                </div>
		                <div class="form-group">
                            <label for="note" class="control-label col-xs-3">Uraian</label>
                            <div class="col-xs-8">
                                <textarea class="form-control" id="note-atensi" name="note" placeholder=""></textarea>
                            </div>
                        </div>
						<div class="form-group">
							<label for="atensi" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-atensi">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>						
					</form>	
					<hr  />					

					<div class="table-responsive">
						<h4 class="panel-title pull-left" ><b>Daftar Atensi Manajemen</b></h4>
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl-atensi">
								<thead>
								<tr class="info"><th>Judul</th><th>Tanggal Catatan</th><th>Uraian</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data tarif client -->
						    	</tbody>
						</table>						
					</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first-atensi">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev-atensi" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next-atensi" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last-atensi" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage-atensi"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
		</div>
		<!-------------------alasan----------------------------------------------------->
		<div class="tab-pane"  id ="alasan" style="background-color:#FFF" data-id="1">                    
			<div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Alasan Keluar</b></h3>
	            <div class="btn-group pull-right" id="btnkaryawan">				
	                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
	                  List Karyawan
	                </button>
            	</div>
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formResign" name="formResign">			    		
			    		<div class="form-group">
							<label for="resign" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
								<div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk-alasan" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
				            	<div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name-alasan" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                		            		
				            	</div>								
						</div>
						<div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Alasan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="reason" name="reason" placeholder="Judul">
		                    </div>
		                </div>
		                <div class="form-group">                            
		                    <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal SK</label>
                            <div class="col-xs-3">
                                <div class='input-group date ' id='sk_date'>
                                    <input type='text' class="form-control" id="sk_date-alasan" name="sk_date"/>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <label for="keterangan" class="control-label col-xs-2 ">No. SK</label>
		                    <div class="col-xs-3">
		                        <input class="form-control" type="text" id="sk_no-alasan" name="sk_no" placeholder="">
		                    </div>
                        </div>                        
                        <div class="form-group">
		                    <label for="keterangan" class="control-label col-xs-3 ">Catatan</label>
		                    <div class="col-xs-8">
		                        <input class="form-control" type="text" id="note" name="note" placeholder="">
		                    </div>
		                </div>
		                <div class="form-group">                                            
		                    <label for="jabatan" class="control-label col-xs-3 ">Job Berikutnya</label>
		                    <div class="col-xs-8">
                                <?php echo $ref_nextjob_id; ?>
                            </div>
		                </div>
						<div class="form-group">
							<label for="resign" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="save-alasan">Save</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>						
					</form>					
				</div>
				
		</div>
		
		</div>
	
		<!------------------------------------------------------------------------------->
             </div>
        </div>

        <div class="panel-footer" id="footerkaryawan">
			<div class="btn-group pull-left _tabs_navigation" data-toggle="buttons-radio" id="btnNavigation">
				<a class="btn btn btn-info" href="#" id="btnPrev">
					<i class="fa fa-backward fa-fw"></i> 
					<span>Prev</span>
				</a>
				<a class="btn btn btn-info" href="#" id="btnNext">
					<i class="fa fa-forward fa-fw"></i> 
					<span>Next</span>
				</a>
			</div>
            <h3 class="panel-title pull-left" style="padding-left:10px; font-style:italic; color:#F00" id="title_mandatory1">(tanda * wajib diisi)</h3>
            <div class="btn-group pull-right" id="btnkaryawan1">
				<?php
					if($form=="")
					{
				?>
                <button type="button" class="btn btn-primary" onClick="$('#content').load('<?php echo base_url('index.php/karyawan/getPanel/'); ?>');">
                  List Karyawan
                </button>        
				<?php
					}
				?>        
                
                <button type="button" class="btn btn-danger" id="reset" onClick="$('#content').load('<?php echo base_url('index.php/karyawan/getDetail/0'); ?>');">
                  Delete
                </button>
                <button type="button" class="btn btn-success" id="save-kandidat">
                  Save
                </button>    
             </div>                    
        </div>
    </div>
</div>
</div>
</div>

<div class="modal fade" role="dialog" aria-hidden="true" id="addNote">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">      
      <h3 class="modal-title pull-left">Atensi Manajemen</h3>
    </div>
    <div class="modal-body">
      <form class="form-horizontal" id="formNote" role="form">
        <div class="form-group">
            <label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
                <div class="col-xs-3">
                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                </div>
                <div class="col-xs-6">
                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Nama Karyawan" data-provide="typeahead" disabled="disabled"/>                                                            
                </div>
        </div>                      
        <div class="form-group">                            
            <label for="TAT Transaksi" class="control-label col-xs-3 ">Tanggal Catatan</label>
            <div class="col-xs-5">
                <div class='input-group date'>
                    <input type='text' class="form-control" id="add_date" name="add_date" disabled="disabled"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>                        
        <div class="form-group">
            <label for="keterangan" class="control-label col-xs-3 ">Judul</label>
            <div class="col-xs-8">
                <input class="form-control" type="text" id="title" name="title" placeholder="Judul Atensi">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="control-label col-xs-3">Uraian</label>
            <div class="col-xs-8">
                <textarea class="form-control" id="note" name="note" placeholder=""></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="modal" class="control-label col-xs-8 col-md-3"></label>
            <div class="btn-group">
                <button class="btn btn-danger" id="btnCancel" data-dismiss="modal">
                  Cancel
                </button>
                <button class="btn btn-success save" id="save" data-dismiss="modal">
                  Save
                </button>
             </div>            
        </div>
      </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<script>



$(function()
{

////////////personal/////////////////////////////////////////

	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	//  loadTab();
	});	

	$('.nav-tabs > .dropdown > .dropdown-menu ').click(function(){
		$('.nav li').removeClass('active');
	});	
	
	$('#tab-personal').click(function(){
	loadPersonal();
		tampil_header_footer();
		});

	var finish = 0;
	var timeout;
	
			
	
    $('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/personal/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id').val(resp.m_district_id);
			   $('#m_district_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	

    $('#birth_place_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/personal/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#birth_place_name').change(function() {
		var current = $('#birth_place_name').typeahead("getActive");
		try{
			$('#m_birth_place_id').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
    });
	
	$('#ref_criteria_id').change(function() {
		loadUser();
    });
	$('#ref_group_punishment').change(function() {
		alert($(this).val());
    });
	$('#ref_reference_id').change(function() {
		alert($(this).val());
    });
	
	$('#status').change(function() {
		alert($(this).val());
    });
	

	
	
	$('#birth_place_name').blur(function(){
			$('#birth_place_name').trigger("change");
	});	


    $('#m_sub_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/personal/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id_id').val(resp.m_district_id);
			   $('#m_district_id_name').val(resp.m_district_name);
			   $('#m_city_name').val(resp.m_city_name);
			   $('#m_city_id').val(resp.m_city_id);
			   $('#m_province_name').val(resp.m_province_name);
			   $('#m_state_id').val(resp.m_state_id);
			   $('#m_state_name').val(resp.m_state_name);
			});
			return items;
		}
    });
	$("#id_card_no").ForceNumericOnly();

	function loadEducation()
	{
			var id = $("#gp_personal_id").val();
			if(id != 0)
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/education/getEducation/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									$("#"+k).val(v);
																		
								});
						   }
					   }
				});				
			}
	}

	function loadKeluarga(page)
	{
		if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;
			par = $.extend({}, par);
			//alert('ddd');
			var id = $("#gp_personal_id_ref").val();
			
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/keluarga/getListById/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
						   //alert('dddd');
								 {
									$("#tbl-keluarga > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-keluarga').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-keluarga > tbody").html("<tr><td colspan = '3' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
										curpage = page;
										totpage = data.totpage;
										$.each(data.data, function(k,v){
											var html = "<tr><td><input type=hidden value='"+v.gp_family_id+"'>"+v.family_name+"</td>";
												html += "<td>"+v.family_relation+"</td>";
												html += "<td>"+v.m_birth_place+' / '+view_date(v.birth_date,2)+"</td>";
												html += "<td>"+v.ref_religion_name+"</td>";
												html += "<td><div class='btn-group'>";
												html += "<button class='btn btn-small btn-danger' id=\"deleteKeluarga\" onclick=\"$('#act-keluarga').val('delete');; $('#gp_family_id').val("+v.gp_family_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
												html += "<button class='btn btn-small btn-info' id=\"editKeluarga\" onclick=\"$('#act-keluarga').val('edit'); $('#gp_family_id').val("+v.gp_family_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
												html += "</div>";
												html += "</td></tr>";
											$("#tbl-keluarga").append(html);				   							
									});
							   }
						   }
					   }
				});				
	}
	
	function loadAlamat(page)
	{
		if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;
			par = $.extend({}, par);
			//alert('ddd');
			var id = $("#gp_personal_id_ref").val();
			
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/alamat/getListById/'); ?>/"+id,
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
						   //alert('dddd');
								 {
									$("#tbl-alamat > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-alamat').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-alamat > tbody").html("<tr><td colspan = '3' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
											curpage = page;
											totpage = data.totpage;
											$.each(data.data, function(k,v){
											if (v.gp_address_id)
											{
												var html = "<tr><td><input type=hidden value='"+v.gp_address_id+"'>"+v.address+"</td>";
												html += "<td>"+v.rt+"</td>";
												html += "<td>"+v.rw+"</td>";
												html += "<td>"+v.zipe_code+"</td>";
												html += "<td>"+v.sub_district_name+"</td>";
												html += "<td>"+v.district_name+"</td>";
												html += "<td>"+v.city_name+"</td>";
												html += "<td>"+v.handphone+"</td>";
												html += "<td>"+v.home_phone+"</td>";
												html += "<td>"+v.note+"</td>";
												html += "<td><div class='btn-group'>";
												html += "<button class='btn btn-small btn-danger' id=\"deleteALamat\" onclick=\"$('#act-alamat').val('delete');; $('#gp_address_id').val("+v.gp_address_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
												html += "<button class='btn btn-small btn-info' id=\"editALamat\" onclick=\"$('#act-alamat').val('edit'); $('#gp_address_id').val("+v.gp_address_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
												html += "</div>";
												html += "</td></tr>";
												$("#tbl-alamat").append(html);
											}		
									});
							   }
						   }
					   }
				});				
	}

$('#btnPrev').hide();

$('#btnNext').click(function(){
//		setUpper();

	if($("#gp_personal_id_ref").val() == '')
		$("#save1-kandidat").click();
	$('#btnPrev').show();
	$(this).hide();
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').next('li').find('a').trigger('click');
});
$('#btnPrev').click(function(){
	$('#btnNext').show();
	$(this).hide();
  	$('.nav-tabs > .dropdown > .dropdown-menu > .active').prev('li').find('a').trigger('click');
});

	function setUpper()
	{
		$("input[type=text]").val(function(){
			return this.value.toUpperCase();	
		});
		$("textarea").val(function(){
			return this.value.toUpperCase();	
		});

	}



	$("#save-kandidat").click(function(){
	$("#save1-kandidat").click();
	});
	$("#save1-kandidat").click(function(){
	//alert('d');
	//$('#m_client_name').change();
	  cari_nrk();
	  var par = new Array();
      q = $("#formKaryawan1").serializeArray();
	  r = $("#formKaryawan2").serializeArray();	
      
	  $.each(q, function(k,v){
        par[v.name] = v.value;
      });
	  $.each(r, function(k,v){
        par[v.name] = v.value;
      });

      par = $.extend({}, par);

      $.ajax({
               type: "GET",
               url: "<?php echo base_url("index.php/personal/save"); ?>"
			   +"?nrk="+$('#nrk').val()+"&m_criteria_name="+$('#m_criteria_name').val()+"&m_criteria_name2="+$('#m_criteria_name2').val()
			   +"&m_criteria_name3="+$('#m_criteria_name3').val()+"?m_criteria_name4="+$('#m_criteria_name4').val()+"&m_criteria_name5="
			   +$('#m_criteria_name5').val()+"&m_criteria_id="+$('#m_criteria_id').val()+"&m_criteria_id2="+$('#m_criteria_id2').val()
			   +"&m_criteria_id3="+$('#m_criteria_id3').val()+"&m_criteria_id3="+$('#m_criteria_id3').val()
			   +"&m_criteria_id4="+$('#m_criteria_id4').val()+"&m_criteria_id5="+$('#m_criteria_id5').val()
			   +"&m_criteria_code="+$('#m_criteria_code').val()+"&m_criteria_code2="+$('#m_criteria_code2').val()
			   +"&m_criteria_code3="+$('#m_criteria_code3').val()+"&m_criteria_code4="+$('#m_criteria_code4').val()
			   +"&m_criteria_code5="+$('#m_criteria_code5').val()+"&m_client_id="+$('#m_client_id').val()
			   +"&start_permanen_date="+$('#start_permanen_date').val()			   ,
			   
               data: par, // serializes the form's elements.
               success: function(data)
               {
			   
                 
                 data = jQuery.parseJSON(data);
                 if(data.success) 
                 {
				 if(data.msg) 
                 {
					
					alert(data.msg);
                 }
				 else
				 {
				 $("#alert").show();
				 $('#m_criteria_name').attr( 'disabled','disabled');
				 $('#m_criteria_name2').attr( 'disabled','disabled');
				 $('#m_criteria_name3').attr( 'disabled','disabled');
				 $('#m_criteria_name4').attr( 'disabled','disabled');
				 $('#m_criteria_name5').attr( 'disabled','disabled');
				 
				 }
				 }
				 
               }
			  
             });    	
	});

	
	$('#birth_date').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : "1990/01/01"	
	});
	
	
	$(".a-prevent").click(function(e) { e.preventDefault(); });
	
	$('#birth_date-keluarga').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : "1990/01/01"	
	}); 	
    $('#join_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : "2012/01/01"	
	});
    $('#team_work_date').datetimepicker({
                format : "DD-MM-YYYY",
           //     defaultDate : "2012/01/01"  
    });
    $('#start_permanen_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : "2012/01/01"	
	});    
    $('#stop_date').datetimepicker({
				format : "DD-MM-YYYY",	
			//	defaultDate : new Date()
	});
    $('#pkwt_date').datetimepicker({
                format : "DD-MM-YYYY",  
              //  defaultDate : new Date()
    });
    $('#job_date').datetimepicker({
                format : "DD-MM-YYYY",  
                //defaultDate : new Date()
    });
    $('#start_contract_date').datetimepicker({
                format : "DD-MM-YYYY",  
      //          defaultDate : new Date()
    });

    $('#stop_contract_date').datetimepicker({
                format : "DD-MM-YYYY",  
				//defaultDate : new Date()
				//maxDate: new Date()
				
  
    });
	$('#add_date').datetimepicker({
                format : "DD-MM-YYYY",  
                //defaultDate : new Date()
    });


 
	
	$('#home_phone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#handphone').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	

    $('#m_faskes_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getFaskes/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_faskes_name').change(function() {
		var current = $('#m_faskes_name').typeahead("getActive");
		try{
		$('#m_faskes_id').val(current.id);
        $('#m_faskes_code').val(current.code);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
        
    });
	$('#m_company_id').change(function() {
        
    });

    $('#m_faskes_name').blur(function() {
        if($('#m_faskes_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_faskes_id').val(0);
        }
    }); 


    $('#m_branch_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getCabang/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_branch_name').change(function() {
        current = $('#m_branch_name').typeahead("getActive");
        $('#m_branch_id').val(current.id);
        $('#m_branch_code').val(current.code);
    });

    $('#m_branch_name').blur(function() {
        if($('#m_branch_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_branch_id').val(0);
        }
    });


    $('#m_client_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getClient/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
	$('#m_client_name').change(function() {
        current = $('#m_client_name').typeahead("getActive");	
        $('#m_client_id').val(current.id);
        $('#m_client_code').val(current.code);
		var stop_contract_date = current.m_client_end_pks_date;
		var start_contract_date = current.m_client_start_pks_date;
		//alert(current.m_client_end_pks_date);
		$('#start_contract_date').data("DateTimePicker").minDate(new Date(start_contract_date));
		$('#stop_contract_date').data("DateTimePicker").maxDate(new Date(stop_contract_date));
		//alert($(this).val());
		cari_nrk();
	
    });

    $('#m_client_name').blur(function() {
        if($('#m_client_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_client_id').val(0);
        }
    });


    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getLokasi/'); ?>/'+query+'/'+$('#m_client_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#wil_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getWilayah/'); ?>/'+query+'/'+$('#m_client_id').val()+'/'+$('#m_location_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_location_name').change(function() {
        current = $('#m_location_name').typeahead("getActive");
		try{
		$('#m_location_id').val(current.id);

		}catch(e){
			alert("Pilih Lokasi Kerja dari List yang Ada");
		}
           });
		   
	$('#wil_name').change(function() {
        current = $('#wil_name').typeahead("getActive");
		try{
		$('#m_area_id').val(current.id);
		$('#area_name').val(current.area_name);

		}catch(e){
			alert("Pilih Lokasi Kerja dari List yang Ada");
		}
           });
	   

    $('#m_location_name').blur(function() {
        if($('#m_location_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_location_id').val(0);
        }
    });

				
    $('#m_unit_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getUnit/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_unit_name').change(function() {
        current = $('#m_unit_name').typeahead("getActive");
        $('#m_unit_id').val(current.id);
        $('#m_unit_code').val(current.code);
    });

    $('#m_unit_name').blur(function() {
        if($('#m_unit_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_unit_id').val(0);
        }
    }); 

    $('#m_structure_position_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getJabatan/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_structure_position_name').change(function() {
        current = $('#m_structure_position_name').typeahead("getActive");
        $('#m_structure_position_id').val(current.id);
        $('#m_structure_position_code').val(current.code);
    });

    $('#m_structure_position_name').blur(function() {
        if($('#m_structure_position_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_structure_position_id').val(0);
        }
    });

    $('#m_criteria_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaUser/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	$('#m_criteria_name2').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaHRBP/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name3').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaCostCenter/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name4').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaAsuransi/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
	$('#m_criteria_name5').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteriaBucket/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_criteria_name').change(function() {
	//alert('dddd');
        current = $('#m_criteria_name').typeahead("getActive");
		if (current)
		{
        $('#m_criteria_id').val(current.id);
        $('#m_criteria_code').val(current.code);
		}
		else
		{
		$('#m_criteria_id').val(0);
        $('#m_criteria_code').val(0);
		}
		
    });
	
	$('#m_criteria_name').keypress(function() {
		$('#m_criteria_id').val(0);
		$('#m_criteria_code').val(0);
    });
	
	$('#m_criteria_name2').keypress(function() {
		$('#m_criteria_id2').val(0);
		$('#m_criteria_code2').val(0);
    });
	
	$('#m_criteria_name3').keypress(function() {
		$('#m_criteria_id3').val(0);
		$('#m_criteria_code3').val(0);
    });
	
	$('#m_criteria_name4').keypress(function() {
		$('#m_criteria_id4').val(0);
		$('#m_criteria_code4').val(0);
    });
	
	$('#m_criteria_name5').keypress(function() {
		$('#m_criteria_id5').val(0);
		$('#m_criteria_code5').val(0);
    });
	
	$('#m_criteria_name2').change(function() {
        current = $('#m_criteria_name2').typeahead("getActive");
        $('#m_criteria_id2').val(current.id);
        $('#m_criteria_code2').val(current.code);
    });
	
	$('#m_criteria_name3').change(function() {
        current = $('#m_criteria_name3').typeahead("getActive");
        $('#m_criteria_id3').val(current.id);
        $('#m_criteria_code3').val(current.code);
    });
	
	$('#m_criteria_name4').change(function() {
        current = $('#m_criteria_name4').typeahead("getActive");
        $('#m_criteria_id4').val(current.id);
        $('#m_criteria_code4').val(current.code);
    });
	
	$('#m_criteria_name5').change(function() {
        current = $('#m_criteria_name5').typeahead("getActive");
		try{
			  $('#m_criteria_id5').val(current.id);
			  $('#m_criteria_code5').val(current.code);
		}catch(e){
	//		alert("Pilih Tempat Lahir dari List yang Ada");
		}
      
    });

    $('#m_criteria_name').blur(function() {
        if($('#m_criteria_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_criteria_id').val(0);
        }
    }); 

function cari_nrk()
	{
		var id = $('#m_client_code').val();
		var idLama = '';
		var d = new Date();
		var thn = d.getFullYear();
		thn = thn.toString().substr(2,2);
		$.ajax({
               type: "GET",
               url: "<?php echo base_url("index.php/personal/cari_nrk"); ?>"+'/'+$('#m_client_id').val(),
               success: function(data)
               {
                 //alert('iiii');			 
                 data = jQuery.parseJSON(data);
				 //alert(data.s);
                 if(data.s != null) 
                 {
				 //alert(data.s.toString().substr(3,2));
				 //alert(data.s.toString().substr(5,4));
				 //alert(thn);
				 if (data.s.toString().substr(3,2) == thn) 
				 {
				 //alert('uuu');
				 idBaru=parseInt(data.s.toString().substr(5,4))+1;
				 //alert(data.s.toString().substr(5,4));
				 if (idBaru < 10) idBaru = '000'+idBaru;
				 else if (idBaru < 100 ) idBaru = '00'+idBaru;
				 else if (idBaru < 1000 ) idBaru = '0'+idBaru;
				 $('#nrk').val(id+thn+idBaru);
				 }
				 else
				 {
				  $('#nrk').val(id+thn+'0001');
				 }
				 }
				 else
				 {
				 //nilaiPerusahaan=$(this).val();
				 //alert('iiii');
				 $('#nrk').val(id+thn+'0001');
				 //alert($('#nrk').val())
				 
				 }
				 //alert($('#nrk').val());
               }
             });
	}

//$('#m_criteria_name').attr('disabled',false);

//////////////////////////////////////////////////////////////////



//////////////////////////////alamat//////////////////////////////////////////////////
$('#tab-alamat').click(function(){
$('#btnPrev').click();
sembunyikan_header_footer();
loadAlamat();
});

	$("#next-alamat").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadAlamat(next);
		}
	});

	$("#prev-alamat").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadAlamat(prev);
		}
	});
	
	$("#first-alamat").click(function(){
		var next = 1;
			loadAlamat(next);
	});

	$("#last-alamat").click(function(){
		var next = totpage;
			loadAlamat(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_address_id").change(function(){
			var act = $("#act-alamat").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/alamat/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
									//	$("#address-alamat").val("");
									//	$("#rt-alamat").val("");
									//	$("#rw-alamat").val("");
									//	$("#zipe_code-alamat").val("");
									//	$("#dokgi-alamat").val("");
									//	$("#note-alamat").val("");
											if (k == 'handphone-alamat' || k == 'home_phone-alamat')
											{
												$('#'+k).tokenfield('setTokens', v);	
																								
											//$('#birth_date').val(v);
											}
											else if (k == 'rt-alamat') $("#rt-alamat").val(v);
											else $("#"+k).val(v);	

											
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/alamat/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadAlamat();
						}
					});
	        	}
				}); 
				
 			}
	});

	
	$('#m_sub_district_name-alamat').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/alamat/getSubDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 1,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		},
		updater : function(items){
			var current = items;
			$('#m_sub_district_id-alamat').val(current.id);
			$.get('<?php echo base_url('index.php/personal/getAddress/'); ?>/'+current.m_district_id, function (resp) {
			   resp = jQuery.parseJSON(resp);
			   $('#m_district_id-alamat').val(resp.m_district_id);
			   $('#m_district_name-alamat').val(resp.m_district_name);
			   $('#m_city_name-alamat').val(resp.m_city_name);
			   $('#m_city_id-alamat').val(resp.m_city_id);
			   $('#m_province_name-alamat').val(resp.m_province_name);
			   $('#m_state_id-alamat').val(resp.m_state_id);
			   $('#m_state_name-alamat').val(resp.m_state_name);
			});
			return items;
		}
    });


	$("#reset").click(function(){
		$("#formAlamat").trigger("reset");
		$("#gp_address_id").val(0);
	});

	$("#alert").hide();

	$("#save-alamat").click(function(){
			$("#formAlamat").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/alamat/save/"); ?>/"+$('#gp_personal_id_ref').val(),
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadAlamat();
								$("#address-alamat").val("");
								$("#rt-alamat").val("");
								$("#rw-alamat").val("");
								$("#zipe_code-alamat").val("");
								$("#dokgi-alamat").val("");
								$("#zipe_code-alamat").val("");
								$("#note-alamat").val("");
								$("#gp_address_id").val(0);					
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$('#home_phone-alamat').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});
	$('#handphone-alamat').tokenfield({
		createTokensOnBlur : true,
		inputType : "number"	
	});

	$('#m_faskes_name-alamat').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/alamat/getFaskes/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_faskes_name-alamat').change(function() {
		current = $('#m_faskes_name-alamat').typeahead("getActive");
		$('#m_faskes_id-alamat').val(current.id);
		$('#m_faskes_code-alamat').val(current.code);
    });

	$('#m_faskes_name-alamat').blur(function() {
		if($('#m_faskes_id-alamat').val()==0)
		{
			// alert("Pilih ");
			$('#m_faskes_id').val(0);
		}
    });		
	$("#next_alamat").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadAlamat(next);
		}
	});

	$("#prev_alamat").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadAlamat(prev);
		}
	});
	
	$("#first_alamat").click(function(){
		var next = 1;
			loadAlamat(next);
	});

	$("#last_alamat").click(function(){
		var next = totpage;
			loadAlamat(next);
	});
	


///////////////////////////////////////////////////////////////////////////////


/////////////////////////////keluarga/////////////////////////////////////////
$('#tab-keluarga').click(function(){
sembunyikan_header_footer();
$('#btnPrev').click();
loadKeluarga();
//alert('lll');
});



	$("#next-keluarga").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadKeluarga(next);
		}
	});

	$("#prev-keluarga").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadKeluarga(prev);
		}
	});
	
	$("#firstkeluarga").click(function(){
		var next = 1;
			loadKeluarga(next);
	});

	$("#last-keluarga").click(function(){
		var next = totpage;
			loadKeluarga(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    $('#m_birth_place_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/keluarga/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_birth_place_name').change(function() {
		var current = $('#m_birth_place_name').typeahead("getActive");
		try{
			$('#m_birth_place_id-keluarga').val(current.id);
		}catch(e){
			alert("Pilih Tempat Lahir dari List yang Ada");
		}
    });
	
	
	$('#m_birth_place_name').blur(function(){
			$('#m_birth_place_name').trigger("change");
	});	
	
	$("#gp_family_id").change(function(){
			var act = $("#act-keluarga").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/keluarga/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									
											$("#family_name").val("");
											$("#ref_relation_id-keluarga").val("");
											$("#m_birth_place_id-keluarga").val("");
											$("#m_birth_place_name").val("");
											$("#birth_date-keluarga").val("");
											$("#gender_id-keluarga").val("");
											$("#religion_id-keluarga").val("");
											$("#ref_bpjskes_status_id-keluarga").val("");
											$("#bpjskes_no-keluarga").val("");
											$.each(data.data, function(k,v){
											if (k=='birth_date-keluarga')
											{
											 var potong = v.split('-');
											 $("#"+k).val(potong[2]+'-'+potong[1]+'-'+potong[0]);
												
											}
											else $("#"+k).val(v);
											
											
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/keluarga/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKeluarga();
						}
					});
	        	}
				}); 
				
 			}
	});
	
	$("#gp_address_id").change(function(){
			var act = $("#act-keluarga").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/keluarga/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											if (k=='birth_date-keluarga')
											{
											 var potong = v.split('-');
											 $("#"+k).val(potong[2]+'-'+potong[1]+'-'+potong[0]);
												
											}
											else $("#"+k).val(v);
											
											
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/keluarga/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKeluarga();
						}
					});
	        	}
				}); 
				
 			}
	});
	
	$("#gp_punishment_id").change(function(){
			var act = $("#act-sanksi").val();
			var id = $(this).val();
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sanksi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										$("#"+k).val(v);
											
											
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/keluarga/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKeluarga();
						}
					});
	        	}
				}); 
				
 			}
	});
	
			

	$("#reset").click(function(){
		$("#formKeluarga").trigger("reset");
		$("#gp_family_id").val(0);
	});

	$("#alert").hide();

	$("#save-keluarga").click(function(){
	var id = $('#gp_personal_id_ref').val();
			$("#formKeluarga").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/keluarga/save"); ?>/"+id,
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadKeluarga();
								$("#family_name").val("");
								$("#ref_relation_id-keluarga").val("");
								$("#m_birth_place_id-keluarga").val("");
								$("#m_birth_place_name").val("");
								$("#birth_date-keluarga").val("");
								$("#gender_id-keluarga").val("");
								$("#religion_id-keluarga").val("");
								$("#ref_bpjskes_status_id-keluarga").val("");
								$("#bpjskes_no-keluarga").val("");
								$("#gp_family_id").val(0);							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    



///////////////////////////////////////////////////////////////////////////////

/////////////////////////kontrak//////////////////////////////////////////////

$('#tab-kontrak').click(function(){
$('#btnPrev').click();
sembunyikan_header_footer();
loadKontrak();
});

function loadKontrak(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;
			var id = $("#gp_personal_id_ref").val();

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/kontrak/getListById/'); ?>/"+id,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-kontrak > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-kontrak').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-kontrak > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_pkwt_id+"'>"+view_date(v.start_contract_date)+"</td>";
											html += "<td>"+view_date(v.stop_contract_date)+"</td>";
											html += "<td>"+v.ref_status_pkwt+"</td>";
											html += "<td>"+v.pkwt_no+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deletekontrak\" onclick=\"$('#act').val('delete'); $('#gp_pkwt_id').val("+v.gp_pkwt_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editkontrak\" onclick=\"$('#act').val('edit'); $('#gp_pkwt_id').val("+v.gp_pkwt_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-kontrak").append(html);				   							
									});
							   }
						   }
						 });										
		}

	$('#start_contract_date-kontrak').datetimepicker({
                format : "DD-MM-YYYY",  
    //            defaultDate : new Date()
    });
	$('#stop_contract_date-kontrak').datetimepicker({
                format : "DD-MM-YYYY",  
				//defaultDate : new Date()
				//maxDate: new Date()
				
  
    });
    
	
	$("#next-kontrak").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadKontrak(next);
		}
	});

	$("#prev-kontrak").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadKontrak(prev);
		}
	});
	
	$("#first-kontrak").click(function(){
		var next = 1;
			loadKontrak(next);
	});

	$("#last-kontrak").click(function(){
		var next = totpage;
			loadKontrak(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_pkwt_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/kontrak/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/kontrak/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadKontrak();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formKontrak").trigger("reset");
		$("#gp_pkwt_id").val(0);
	});

	$("#alert").hide();

	$("#save-kontrak").click(function(){
			$("#formKontrak").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/kontrak/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadKontrak();
								$("#formKontrak").trigger("reset");
								$("#gp_pkwt_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#start_contract_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
		//		defaultDate : new Date()	
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#stop_contract_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#pkwt_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});
    
    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#job_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#letter_job_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : new Date()
	});

	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#stop_pks_date-kontrak').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()	
	});





//////////////////////////////////////////User/////////////////////////////////////
$('#tab-user').click(function(){
$('#btnPrev').click();
sembunyikan_header_footer();
loadUser();
});

$('#m_criteria_name-user').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getKriteria/'); ?>/'+query+'?ref_criteria_id='+$('#ref_criteria_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
});

$('#m_criteria_name-user').change(function() {
	//alert('dddd');
        current = $('#m_criteria_name-user').typeahead("getActive");
		if (current)
		{
        $('#m_criteria_id-user').val(current.id);
        $('#m_criteria_code-user').val(current.code);
		}
		else
		{
		$('#m_criteria_id-user').val(0);
        $('#m_criteria_code-user').val(0);
		}
		
    });
	
	$('#m_criteria_name-user').keyup(function() {
		$('#m_criteria_id-user').val(0);
		$('#m_criteria_code-user').val(0);
    });
	
	$('#m_criteria_name-user').blur(function() {
		$('#m_criteria_id-user').val(0);
		$('#m_criteria_code-user').val(0);
    });
	
	function loadUser(page)
	{
				if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;
			par = $.extend({}, par);
			//alert('ddd');
			var id = $("#gp_personal_id_ref").val();
			
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/user_client/getListById/'); ?>/"+id+'?page='+page+'&ref_criteria_id='+$('#ref_criteria_id').val(),
						beforeSend: function () { $("#loading").show(); },
						// hides the loader after completion of request, whether successfull or failor.             
						complete: function () { $("#loading").hide(); },      
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
						   //alert('dddd');
								 {
									$("#tbl-user > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-user').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-user > tbody").html("<tr><td colspan = '3' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
										curpage = page;
										totpage = data.totpage;
										$.each(data.data, function(k,v){
											if (v.m_criteria_id)
											{
											var html = "<tr><td><input type=hidden value='"+v.gp_user_client_id+"'>"+v.m_criteria_id+"</td>";
												html += "<td>"+v.m_criteria_name+"</td>";
												html += "<td>"+view_date(v.tmt_date,2)+"</td>";
												html += "<td><div class='btn-group'>";
												html += "<button class='btn btn-small btn-danger' id=\"deleteUser\" onclick=\"$('#act-user').val('delete');; $('#gp_user_client_id').val("+v.gp_user_client_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
												html += "<button class='btn btn-small btn-info' id=\"editUser\" onclick=\"$('#act-user').val('edit'); $('#gp_user_client_id').val("+v.gp_user_client_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
												html += "</div>";
												html += "</td></tr>";
											$("#tbl-user").append(html);	
											}		
									});
							   }
						   }
					   }
				});										
	}


	$("#next-user").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadUser(next);
		}
	});

	$("#prev-user").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadUser(prev);
		}
	});
	
	$("#first-user").click(function(){
		var next = 1;
			loadUser(next);
	});

	$("#last-user").click(function(){
		var next = totpage;
			loadUser(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_user_client_id").change(function(){
			var act = $("#act-user").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/user_client/get/'); ?>/"+id+'?gp_personal_id='+$("#gp_personal_id_ref").val(),
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/user_client/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadUser();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formUserClient").trigger("reset");
		$("#gp_user_client_id").val(0);
	});

	$("#alert").hide();

	$("#save-user").click(function(){
			$("#formUserClient").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/user_client/save/"); ?>/"+$("#gp_personal_id_ref").val(),
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
								$("#alert").show();
								loadUser();
								$('#m_criteria_id-user').val('');
								$('#m_criteria_name-user').val('');
								$('#note-user').val('');
								$('#m_criteria_code-user').val('');
								$("#gp_user_client_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : "1990/01/01"	
	});

    $('#m_criteria_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/user_client/getCriteria/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_criteria_name').change(function() {
		current = $('#m_criteria_name').typeahead("getActive");
		$('#m_criteria_id').val(current.id);		
    });

	$('#m_criteria_name').blur(function() {
		if($('#m_criteria_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_criteria_id').val(0);
		}
    });	

////////////////////////////gaji//////////////////////////////////
$('#tab-gaji').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadGaji();
});
function loadGaji(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var id = $("#gp_personal_id_ref").val();
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();
			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/gaji/getListById/'); ?>/"+id,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-gaji > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-gaji').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-gaji > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_salary_id+"'>"+v.salary_start_date+"</td>";
											html += "<td>"+v.value+"</td>";
											html += "<td>"+v.status+"</td>";
											html += "<td>"+v.note+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteGaji\" onclick=\"$('#act-gaji').val('delete'); $('#gp_salary_id').val("+v.gp_salary_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editGaji\" onclick=\"$('#act-gaji').val('edit'); $('#gp_salary_id').val("+v.gp_salary_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-gaji").append(html);				   							
									});
							   }
						   }
						 });										
		}


	$("#next-gaji").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadGaji(next);
		}
	});

	$("#prev-gaji").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadGaji(prev);
		}
	});
	
	$("#first-gaji").click(function(){
		var next = 1;
			loadGaji(next);
	});

	$("#last-gaji").click(function(){
		var next = totpage;
			loadGaji(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	
	
    $('#sk_date-gaji').datetimepicker({
				format : "DD-MM-YYYY",
				defaultDate : new Date()	
	});
	
	$("#gp_salary_id").change(function(){
			var act = $("#act-gaji").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/gaji/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/gaji/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadGaji();
						}
					});
	        	}
				}); 				
 			}
	});
	$("#gp_fixed_transaction_id").change(function(){
			var act = $("#act-tunjangan").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tunjangan/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/gaji/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadGaji();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formGaji").trigger("reset");
		$("#gp_salary_id").val(0);
	});

	$("#alert").hide();

	$("#save-gaji").click(function(){
	
			$("#formGaji").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/gaji/save"); ?>/"+$('#gp_personal_id_ref').val(),
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadGaji();
								$("#gp_salary_id").val(0);
								$("#note-gaji").val('');
								$("#sk_no-gaji").val('');
								$("#value").val('');
								
								
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#salary_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()	
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});


/////////////////////////////////////////////////////

///////////////Tunjangan//////////////////////////
$('#tab-tunjangan').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadTunjangan();
});

$('#transaction_start_date-tunjangan').datetimepicker({
				format : "DD-MM-YYYY",
			//	defaultDate : "1990/01/01"	
});

$("#value-tunjangan").ForceNumericOnly();
function loadTunjangan(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var id = $("#gp_personal_id_ref").val();
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});
			var id = $("#gp_personal_id_ref").val();

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tunjangan/getListById/'); ?>/"+id,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									if (data.totpage==0) data.totpage=1;
									$("#tbl-tunjangan > tbody").empty();
									$('#textpage-tunjangan').html("Page "+page+" of "+data.totpage);
							
										if(data.data == '')
										{
											$("#tbl-tunjangan > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
										else
										{
										curpage = page;
										totpage = data.totpage;
										$.each(data.data, function(k,v){
											var html = "<tr><td><input type=hidden value='"+v.gp_fixed_transaction_id+"'>"+v.m_p_description_transaction_id+"</td>";
												html += "<td>"+v.m_p_description_transaction_name+"</td>";
												html += "<td>"+v.value+"</td>";
												html += "<td>"+v.transaction_start_date+"</td>";
												html += "<td>"+v.transaction_end_date+"</td>";
												html += "<td><div class='btn-group'>";
												html += "<button class='btn btn-small btn-danger' id=\"deleteTunjangan\" onclick=\"$('#act-tunjangan').val('delete'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
												html += "<button class='btn btn-small btn-info' id=\"editTunjangan\" onclick=\"$('#act-tunjangan').val('edit'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
												html += "</div>";
												html += "</td></tr>";
											$("#tbl-tunjangan").append(html);				   							
										});
										}
							   }
						   }
						 });										
		}


	$("#next-tunjangan").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadTunjangan(next);
		}
	});

	$("#prev-tunjangan").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadTunjangan(prev);
		}
	});
	
	$("#first-tunjangan").click(function(){
		var next = 1;
			loadTunjangan(next);
	});

	$("#last-tunjangan").click(function(){
		var next = totpage;
			loadTunjangan(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_fixed_transaction_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/tunjangan/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/tunjangan/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadTunjangan();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formTunjangan").trigger("reset");
		$("#gp_fixed_transaction_id").val(0);
	});

	$("#alert").hide();

	$("#save-tunjangan").click(function(){
			$("#formTunjangan").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/tunjangan/save/"); ?>/"+$('#gp_personal_id_ref').val(),
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadTunjangan();
								$('#m_p_description_transaction_code').val('');
								$('#m_p_description_transaction_name').val('');
								$('#m_p_description_transaction_id').val('');
								$('#value-tunjangan').val('');
								$('#note-tunjangan').val('');
								$("#gp_fixed_transaction_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#transaction_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#transaction_end_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});

		$('#m_p_description_transaction_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/tunjangan/getTransaksi/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : false,
		minLength : 1,
		delay : 3
    });
    
    var current = 0;
	$('#m_p_description_transaction_name').change(function() {
		current = $('#m_p_description_transaction_name').typeahead("getActive");
				try{
		
		$('#m_p_description_transaction_id').val(current.id);
		$('#m_p_description_transaction_code').val(current.code);


		}catch(e){
			alert("Pilih Tunjangan");
		}
    });

	$('#m_p_description_transaction_name').blur(function() {
		if($('#m_p_description_transaction_id').val()==0)
		{
			// alert("Pilih ");
			$('#m_p_description_transaction_id').val(0);
		}
    });	
 




//////////////////////////////////////////////////



//////////////////mutasi/////////////////
$('#tab-mutasi').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadMutasi();
});

		function loadMutasi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-mutasi > tbody").empty();
									if(data.totpage==0) data.totpage=1;
									$('#textpage-mutasi').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-mutasi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_mutation_id+"'>"+v.effective_date+"</td>";
											html += "<td>"+v.m_location_id+"</td>";
											html += "<td>"+v.m_unit_id+"</td>";
											html += "<td>"+v.m_structure_position_id+"</td>";
											html += "<td>"+v.m_branch_id+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteMutasi\" onclick=\"$('#act').val('delete'); $('#gp_mutation_id').val("+v.gp_mutation_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editMutasi\" onclick=\"$('#act').val('edit'); $('#gp_mutation_id').val("+v.gp_mutation_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-mutasi").append(html);				   							
									});
							   }
						   }
						 });										
		}
	$('#m_location_name-mutasi').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getLokasiTerpisah/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
	
    
    var current = 0;
    $('#m_location_name-mutasi').change(function() {
        current = $(this).typeahead("getActive");
		try{
		$('#m_location_id-mutasi').val(current.id);

		}catch(e){
			alert("Pilih Lokasi Kerja dari List yang Ada");
		}
           });
	
	$('#m_unit_name-mutasi').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getUnit/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_unit_name-mutasi').change(function() {
        current = $('#m_unit_name-mutasi').typeahead("getActive");
        $('#m_unit_id-mutasi').val(current.id);
        $('#m_unit_code-mutasi').val(current.code);
    });

    $('#m_unit_name-mutasi').blur(function() {
        if($('#m_unit_id-mutasi').val()==0)
        {
            // alert("Pilih ");
            $('#m_unit_id-mutasi').val(0);
        }
    });
	
	 $('#m_structure_position_name-mutasi').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getJabatan/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_structure_position_name-mutasi').change(function() {
        current = $('#m_structure_position_name-mutasi').typeahead("getActive");
        $('#m_structure_position_id-mutasi').val(current.id);
        $('#m_structure_position_code-mutasi').val(current.code);
    });

    $('#m_structure_position_name-mutasi').blur(function() {
        if($('#m_structure_position_id-mutasi').val()==0)
        {
            // alert("Pilih ");
            $('#m_structure_position_id-mutasi').val(0);
        }
    });
		
	
	$('#sk_date-mutasi').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});
	
	$('#m_branch_name-mutasi').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/personal/getCabang/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_branch_name-mutasi').change(function() {
        current = $('#m_branch_name-mutasi').typeahead("getActive");
        $('#m_branch_id-mutasi').val(current.id);
        $('#m_branch_code-mutasi').val(current.code);
    });

    $('#m_branch_name-mutasi').blur(function() {
        if($('#m_branch_id-mutasi').val()==0)
        {
            // alert("Pilih ");
            $('#m_branch_id-mutasi').val(0);
        }
    });
	
	
	
	$("#next-mutasi").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadMutasi(next);
		}
	});

	$("#prev-mutasi").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadMutasi(prev);
		}
	});
	
	$("#first-mutasi").click(function(){
		var next = 1;
			loadMutasi(next);
	});

	$("#last-mutasi").click(function(){
		var next = totpage;
			loadMutasi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_mutation_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/mutasi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadMutasi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formMutasi").trigger("reset");
		$("#gp_mutation_id").val(0);
	});

	$("#alert").hide();

	$("#save-mutasi").click(function(){
			$("#formMutasi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/mutasi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadMutasi();
								$("#formMutasi").trigger("reset");
								$("#gp_mutation_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#effective_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});


	$('#m_branch_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getCabang/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_branch_name').change(function() {
        current = $('#m_branch_name').typeahead("getActive");
        $('#m_branch_id').val(current.id);
        $('#m_branch_code').val(current.code);
    });

    $('#m_branch_name').blur(function() {
        if($('#m_branch_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_branch_id').val(0);
        }
    });


    $('#m_location_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getLokasi/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_location_name').change(function() {
        current = $('#m_location_name').typeahead("getActive");
        $('#m_location_id').val(current.id);
        $('#m_location_code').val(current.code);
    });

    $('#m_location_name').blur(function() {
        if($('#m_location_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_location_id').val(0);
        }
    });


    $('#m_unit_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getUnit/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_unit_name').change(function() {
        current = $('#m_unit_name').typeahead("getActive");
        $('#m_unit_id').val(current.id);
        $('#m_unit_code').val(current.code);
    });

    $('#m_unit_name').blur(function() {
        if($('#m_unit_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_unit_id').val(0);
        }
    }); 

    $('#m_structure_position_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/mutasi/getJabatan/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#m_structure_position_name').change(function() {
        current = $('#m_structure_position_name').typeahead("getActive");
        $('#m_structure_position_id').val(current.id);
        $('#m_structure_position_code').val(current.code);
    });

    $('#m_structure_position_name').blur(function() {
        if($('#m_structure_position_id').val()==0)
        {
            // alert("Pilih ");
            $('#m_structure_position_id').val(0);
        }
    });




//////////////////////////

//////////////////////////dokumen///////

$('#tab-dokumen').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
});

function loadMutasi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-mutasi > tbody").empty();
									if (data.totpage==0) data.totpage=1; 
									$('#textpage-mutasi').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-mutasi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_fixed_transaction_id+"'>"+v.ref_institution_type+"</td>";
											html += "<td>"+v.institution_name+"</td>";
											html += "<td>"+v.major+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.graduated+"</td>";
											html += "<td>"+v.gpa+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteMutasi\" onclick=\"$('#act').val('delete'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editMutasi\" onclick=\"$('#act').val('edit'); $('#gp_fixed_transaction_id').val("+v.gp_fixed_transaction_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-mutasi").append(html);				   							
									});
							   }
						   }
						 });										
		}


	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadMutasi(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadMutasi(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadMutasi(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadMutasi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_fixed_transaction_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/mutasi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/mutasi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadMutasi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formDokumen").trigger("reset");
		$("#gp_fixed_transaction_id").val(0);
	});

	$("#alert").hide();

	$("#save-dokumen").click(function(){
			$("#formDokumen").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/mutasi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadMutasi();
								$("#formDokumen").trigger("reset");
								$("#gp_fixed_transaction_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#doc_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});



//////////////////////////////

/////////////////////sanksi/////////////////////
$('#tab-sanksi').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadSanksi();
});
function loadSanksi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var id = $('#gp_personal_id_ref').val();
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sanksi/getListById/'); ?>/"+id,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-sanksi > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-sanksi').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-sanksi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_punishment_id+"'>"+v.name+"</td>";
											html += "<td>"+v.punishment_start_date+"</td>";
											html += "<td>"+v.punishment_end_date+"</td>";
											html += "<td>"+v.decision_date+"</td>";
											html += "<td>"+v.decision_no+"</td>";
											html += "<td>"+v.reason +"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteSanksi\" onclick=\"$('#act-sanksi').val('delete'); $('#gp_punishment_id').val("+v.gp_punishment_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editSanksi\" onclick=\"$('#act-sanksi').val('edit'); $('#gp_punishment_id').val("+v.gp_punishment_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-sanksi").append(html);				   							
									});
							   }
						   }
						 });										
		}

	$("#next-sanksi").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadSanksi(next);
		}
	});

	$("#prev-sanksi").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadSanksi(prev);
		}
	});
	
	$("#first-sanksi").click(function(){
		var next = 1;
			loadSanksi(next);
	});

	$("#last-sanksi").click(function(){
		var next = totpage;
			loadSanksi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_punishment_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sanksi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/sanksi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadSanksi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formSanksi").trigger("reset");
		$("#gp_punishment_id").val(0);
	});

	$("#alert").hide();

	$("#save-sanksi").click(function(){
			$("#formSanksi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/sanksi/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadSanksi();
								$("#formSanksi").trigger("reset");
								$("#gp_punishment_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#punishment_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});


		$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#punishment_end_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#decision_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : "1990/01/01"	
	});



//////////////////////////////////////////////




////////////////Cuti//////////////////

$('#tab-cuti').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadCuti();
});

function loadCuti(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/cuti/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-cuti > tbody").empty();
									if (data.totpage==0) data.totpage=1;
									$('#textpage-cuti').html("Page "+page+" of "+data.totpage);
										if(data.data == '')
										{
											$("#tbl-cuti > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_leave_id+"'>"+v.ref_leave_id+"</td>";
											html += "<td>"+v.leave_start_date+"</td>";
											html += "<td>"+v.leave_end_date+"</td>";
											html += "<td>"+v.leave_sum+"</td>";
											html += "<td>"+v.saldo+"</td>";
											html += "<td>"+v.address+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteCuti\" onclick=\"$('#act').val('delete'); $('#gp_leave_id').val("+v.gp_leave_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editCuti\" onclick=\"$('#act').val('edit'); $('#gp_leave_id').val("+v.gp_leave_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-cuti").append(html);				   							
									});
							   }
						   }
						 });										
		}


	$("#next-cuti").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadCuti(next);
		}
	});

	$("#prev-cuti").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadCuti(prev);
		}
	});
	
	$("#first-cuti").click(function(){
		var next = 1;
			loadCuti(next);
	});

	$("#last-cuti").click(function(){
		var next = totpage;
			loadCuti(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_leave_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/cuti/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/cuti/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadCuti();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formCuti").trigger("reset");
		$("#gp_leave_id").val(0);
	});

	$("#alert").hide();

	$("#save-cuti").click(function(){
			$("#formCuti").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/cuti/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadCuti();
								$("#formCuti").trigger("reset");
								$("#gp_leave_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#leave_start_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()	
	});


		$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#leave_end_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	});
 




////////////////////////


////////////////atensi////////////
$('#tab-atensi').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
loadAtensi();
}); 

function loadAtensi(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;
			var gp_personal_id_ref = $('#gp_personal_id_ref').val();

			q = $("#formAtensi").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/atensi/getListById/'); ?>/"+gp_personal_id_ref,
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl-atensi > tbody").empty();
									$('#textpage-atensi').html("Page "+page+" of "+data.totpage);
									if (data.totpage == 0) data.totpage=1;
										if(data.data == '')
										{
											$("#tbl-atensi > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_atensi_id+"'>"+v.title+"</td>";
											html += "<td>"+v.add_date+"</td>";
											html += "<td>"+v.note +"</td>";											
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteAtensi\" onclick=\"$('#act-atensi').val('delete'); $('#gp_atensi_id').val("+v.gp_atensi_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editAtensi\" onclick=\"$('#act-atensi').val('edit'); $('#gp_atensi_id').val("+v.gp_atensi_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl-atensi").append(html);				   							
									});
							   }
						   }
						 });										
		}
	

	$("#next-atensi").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadAtensi(next);
		}
	});

	$("#prev-atensi").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadAtensi(prev);
		}
	});
	
	$("#first-atensi").click(function(){
		var next = 1;
			loadAtensi(next);
	});

	$("#last-atensi").click(function(){
		var next = totpage;
			loadAtensi(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    
	
	$("#gp_atensi_id").change(function(){
			var act = $("#act-atensi").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/atensi/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/atensi/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadAtensi();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formAtensi").trigger("reset");
		$("#gp_atensi_id").val(0);
	});

	$("#alert").hide();

	$("#save-atensi").click(function(){
			$("#formAtensi").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/atensi/save/"); ?>/"+$('#gp_atensi_id').val(),
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadAtensi();
								$("#note-atensi").val("");
								$("#title-atensi").val("");
								$("#act-atensi").val('');	
								$("#gp_atensi_id").val(0);					
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#add_date').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()
	}); 



/////////////////////////////////////////


////////////alasan////////////////
$('#tab-alasan').click(function()
{
$('#btnPrev').click();
sembunyikan_header_footer();
});


	$("#gp_resign_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/resign/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/resign/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadresign();
						}
					});
	        	}
				}); 				
 			}
	});

	$("#reset").click(function(){
		$("#formResign").trigger("reset");
		$("#gp_resign_id").val(0);
	});

	$("#alert").hide();

	$("#save-alasan").click(function(){
	var id=$('#gp_personal_id_ref').val();
			$("#formResign").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/resign/save/"); ?>/"+id,
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							    $("#alert").show();
								$('#sk_date-alasan').val('');
								$('#reason').val('');
								$("#gp_resign_id").val(0);

							// $("#m_branch_id").val(data.id);
							// bootbox.alert("Data berhasil disimpan!", function() {
							//   loadCabang();
							// $("#formCabang").trigger("reset");
							// $("#m_branch_id").val(0);
							// });
							// alert("Data Berhasil Disimpan");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	$(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#sk_date-alasan').datetimepicker({
				format : "DD-MM-YYYY",
				//defaultDate : new Date()	
	});






/////////////////////////////////////////
function loadPersonal()
{
if($('#gp_personal_id_ref').val() != "" )
	{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/karyawan/getAll/'); ?>/"+$('#gp_personal_id_ref').val(),
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											if (k == 'birth_date' || k == 'team_work_date' ||  k == 'join_date' || k == 'start_permanen_date' || k == 'stop_date' || k == 'pkwt_date'
											 || k == 'start_contract_date' || k == 'stop_contract_date' || k == 'job_date' ||
												k == 'letter_job_date' || k == 'stop_pks_date'		
											 
											)
											{	
												
												
												var potong = v.split('-');
												$('#'+k).val(potong[2]+'-'+potong[1]+'-'+potong[0]);
												//$('#'+k).data("DateTimePicker").defaultDate(new Date(v));
																								
											//$('#birth_date').val(v);
											}
											else if (k == 'handphone' || k == 'home_phone')
											{
												$('#'+k).tokenfield('setTokens', v);
												$('#'+k).tokenfield('readonly','true');		
																								
											//$('#birth_date').val(v);
											}
											else 
											{
												$("#"+k).val(v);
											}
											if (k == 'address'|| k == 'rt' || k == 'rw' || k == 'rt' || k=='m_sub_district_name' ||
											k == 'm_district_name' || k == 'm_city_name' || k == 'm_state_name' || 
											k == 'zipe_code' || k == 'email_address' || k == 'm_faskes_name'  || k == 'dokgi' || k == 'm_branch_name'
											|| k == 'm_client_name'	|| k == 'm_location_name' || k == 'wil_name' || k == 'm_unit_name' || k == 'm_criteria_name'
											|| k == 'm_criteria_name2' || k == 'm_criteria_name3' || k == 'm_criteria_name4' || k == 'm_criteria_name5'	
											|| k == 'ref_institution_type'
											) 
											{
											$("#"+k).attr('disabled','disabled');
											}
											//$('#handphone').data("tokenfield").add('88888888');
											//$('#handphone').tokenfield('setTokens', v);	
											//$('#handphone').tokenfield('9999', '099999');		

											//alert(v+" : "+k);

									});
									//$('#m_sub_district_name').trigger('change');
									
							   }
						   }
						 });	
	}
//loadPersonal(); 
 }	
loadPersonal();  
});

// $('#stop_contract_date').datetimepicker('setStartDate', new Date('2016-08-08'));
// $("#stop_contract_date").data('DateTimePicker').setMaxDate(new Date());
 // $("#stop_contract_date").datepicker('option', 'maxDate', "2014/31/01" );
 //$('#stop_contract_date').datetimepicker('destroy');

//$('#stop_contract_date').datetimepicker('option', 'minDate', new Date());

function pesan()
{
alert('hh');
}

function tampil_header_footer()
{
 	$('#footerkaryawan').show();
	$('#headerkaryawan').show();
}
function sembunyikan_header_footer()
{
 	$('#footerkaryawan').hide();
	$('#headerkaryawan').hide();
}
</script>


    </div>
     </div>
