<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/token-input-facebook.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-tokenfield.min.css'); ?>"/>
<link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-responsive.css'); ?>"/>
<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
	</div>
    <div class="panel panel-default" style="margin-top:30px;">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left"><b>Master Alamat</b></h3>
        </div>
        <div class="panel-body" style="padding:0px;">
 			<ul class="nav nav-tabs">
            	<li class="active"><a data-toggle="tab" href="#sub_district">Kelurahan</a></li>
				<li><a data-toggle="tab" href="#district">Kecamatan</a></li>
				<li><a data-toggle="tab" href="#city">Kota</a></li>
				<li><a data-toggle="tab" href="#state">Provinsi</a></li>
			</ul>	
            <div id="form-tab" class="tab-content">				
			<div class="tab-pane active" id="sub_district">
				<div class="panel-heading" style="background-color:#FFF">
					<span style="font-size:14px; font-weight:bold">Kelurahan</span>
				</div>
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formSubdistrict" name="formSubdistrict">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Kelurahan</label>
							<div class="col-xs-6 col-md-3">
	                    		<input type='text' class="form-control" name="name" id="name" placeholder="Kelurahan"/>
	                    		<input type='hidden' class="form-control" name="m_sub_district_id" id="m_sub_district_id" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div>
                		<div class="form-group">
							<label for="district" class="control-label col-xs-6 col-md-2">Kecamatan</label>
							<div class="col-xs-6 col-md-3">
								<input type='text' class="form-control" name="m_district_name" id="m_district_name" placeholder="Kecamatan" data-provide="typeahead" autocomplete="off"/>
								<input type='hidden' class="form-control" name="m_district_id" id="m_district_id" placeholder="Kecamatan"/>
							</div>
							
						</div>
						<div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-2"></label>
							<div class="col-xs-6 col-md-3">
								<div class="btn-group">
									<button class="btn btn-primary" id="add_sub">Save</button>
									<button class="btn btn-danger" id="reset_sub">Reset</button>
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearch" role="form">
										<div class="col-xs-4 pull-right">
											<div class="input-group ">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="name" id="search" class="form-control" placeholder="Cari Kelurahan" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
    					<table class="table table-bordered" id="tbl_sub_district">
      						<thead>
        						<tr class="info"><th>Kelurahan</th><th>Kecamatan</th><th>Kota</th><th>Provinsi</th><th>Action</th></tr>
      						</thead>
      						<tbody>
								<!-- data kelurahan -->
						    </tbody>
    					</table>
   					</div>
				</div>  
			</div>

			<div class="tab-pane" id="district">
						<div class="panel-heading" style="background-color:#FFF">
							<span style="font-size:14px; font-weight:bold">Kecamatan</span>
						</div>
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formDistrict" name="formDistrict">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Kecamatan</label>
							<div class="col-xs-6 col-md-3">
	                    		<input type='text' class="form-control" name="m_district_name_1" id="m_district_name_1" placeholder="Kecamatan"/>
	                    		<input type='hidden' class="form-control" name="m_district_id_1" id="m_district_id_1" placeholder="ID"/>
	                    		<input type='hidden' class="form-control" name="act_district" id="act_district" placeholder="act"/>
							</div>
						</div>
                		<div class="form-group">
							<label for="district" class="control-label col-xs-6 col-md-2">Kota</label>
							<div class="col-xs-6 col-md-3">
								<input type='text' class="form-control" name="m_city_name" id="m_city_name" placeholder="Kota" data-provide="typeahead" />
								<input type='hidden' class="form-control" name="m_city_id" id="m_city_id" placeholder="Kota"/>
							</div>
							
						</div>
						<div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-2"></label>
							<div class="col-xs-6 col-md-3">
								<div class="btn-group">
									<button class="btn btn-primary" id="add_district">Save</button>
									<button class="btn btn-danger" id="reset_district">Reset</button>
								</div>
							</div>
						</div>
					</form>	
					<hr  />
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearchDistrict" role="form">
										<div class="col-xs-4 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="name" id="search_district" class="form-control" placeholder="Cari Kecamatan" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
    					<table class="table table-bordered" id="tbl_district">
      						<thead>
        						<tr class="info"><th>Kecamatan</th><th>Kota</th><th>Provinsi</th><th>Action</th></tr>
      						</thead>
      						<tbody>
								<!-- data kecamatan -->
						    </tbody>
    					</table>
   					</div>
				</div>  
			</div>

			<div class="tab-pane" id="city">
						<div class="panel-heading" style="background-color:#FFF">
							<span style="font-size:14px; font-weight:bold">Kota</span>
						</div>
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formCity" name="formCity">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Kota</label>
							<div class="col-xs-6 col-md-3">
	                    		<input type='text' class="form-control" name="m_city_name_1" id="m_city_name_1" placeholder="Kota"/>
	                    		<input type='hidden' class="form-control" name="m_city_id_1" id="m_city_id_1" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act_city" id="act_city" placeholder="act"/>
							</div>
						</div>
                		<div class="form-group">
							<label for="district" class="control-label col-xs-6 col-md-2">Provinsi</label>
							<div class="col-xs-6 col-md-3">
								<input type='text' class="form-control" name="m_state_name" id="m_state_name" placeholder="Provinsi" data-provide="typeahead"/>
								<input type='hidden' class="form-control" name="m_state_id" id="m_state_id" value="0"/>
							</div>
							
						</div>
						<div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-2"></label>
							<div class="col-xs-6 col-md-3">
								<div class="btn-group">
									<button class="btn btn-primary" id="add_city">Save</button>
									<button class="btn btn-danger" id="reset_city">Reset</button>
								</div>
							</div>
						</div>
					</form>	
					<hr  />
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearchCity" role="form">
										<div class="col-xs-4 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="name" id="search_city" class="form-control" placeholder="Cari Kota" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
    					<table class="table table-bordered" id="tbl_city">
      						<thead>
        						<tr class="info"><th>Kota</th><th>Provinsi</th><th>Action</th></tr>
      						</thead>
      						<tbody>
								<!-- data Kota -->
						    </tbody>
    					</table>
   					</div>
				</div>  
			</div>

			<div class="tab-pane" id="state">
						<div class="panel-heading" style="background-color:#FFF">
							<span style="font-size:14px; font-weight:bold">Provinsi</span>
						</div>
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formState" name="formState">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Provinsi</label>
							<div class="col-xs-8 col-md-4">
	                    		<input type='text' class="form-control" name="m_state_name_1" id="m_state_name_1" placeholder="Provinsi"/>
	                    		<input type='hidden' class="form-control" name="m_state_id_1" id="m_state_id_1" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act_state" id="act_state" placeholder="act"/>
							</div>				
							
						</div>
						<div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-2"></label>
							<div class="col-xs-6 col-md-3">
								<div class="btn-group">
									<button class="btn btn-primary" id="add_state">Save</button>
									<button class="btn btn-danger" id="reset_state">Reset</button>
								</div>
							</div>
						</div>
					</form>	
					<hr  />
					<div class="table-responsive">
						<div class="panel-heading" style="background-color:#FFF">
							<div class="search-form">
								<div class="row">
									<form id="formSearchState" role="form">
										<div class="col-xs-4 pull-right">
											<div class="input-group">
											  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
												<input type="text" name="name" id="search_state" class="form-control" placeholder="Cari Provinsi" />
											</div>
										</div>
									</form>	
								</div>
							</div>		
						</div>
    					<table class="table table-bordered" id="tbl_state">
      						<thead>
        						<tr class="info"><th>Provinsi</th><th>Action</th></tr>
      						</thead>
      						<tbody>
								<!-- data provinsi -->
						    </tbody>
    					</table>
   					</div>
				</div>  
			</div>
			</div>			
		</div>
		<div class="panel-footer center-block">
			<div class="btn-group" role="group" aria-label="...">
				<div class="btn-group" role="group">
					<a class="btn btn-default" href="#" id="first">
						<i class="fa fa-fast-backward fa-fw"></i> 
						<span>First</span>
					</a>
					<a class="btn btn-default" href="#" id="prev" >
						<i class="fa fa-backward fa-fw"></i> 
						<span>Prev</span>
					</a>
					<a class="btn btn-default" href="#" id="next" >
						<i class="fa fa-forward fa-fw"></i> 
						<span>Next</span>
					</a>
					<a class="btn btn-default" href="#" id="last" >
						<i class="fa fa-fast-forward fa-fw"></i> 
						<span>Last</span>
					</a>
					<a class="btn btn-default" href="#" >
						<span id="textpage"></span>
					</a>
				</div>
			</div>
		</div>		                                    		
	</div>
</div>
</div>		

<script>
$(function()
{
	$("#alert").hide();

	var timeout;

	var curpage = 1;
	var totpage = 1;

	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	  loadTab();
	});	

	$("#search").keyup(function(){
			loadSubDistrict(1, $("#formSearch").serializeArray());
	});

	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});

		function loadSubDistrict(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sub_district/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_sub_district > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_sub_district_id+"'>"+v.name+"</td>";
											html += "<td>"+v.district+"</td>";
											html += "<td>"+v.city+"</td>";
											html += "<td>"+v.state+"</td>";
											html += "<td><div class='btn-group'>";
<!--										html += "<button class='btn btn-small btn-danger' id=\"deleteSubdistrict\" onclick=\"$('#act').val('delete'); $('#m_sub_district_id').val("+v.m_sub_district_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->	
											html += "<button class='btn btn-small btn-info' id=\"editSubdistrict\" onclick=\"$('#act').val('edit'); $('#m_sub_district_id').val("+v.m_sub_district_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_sub_district").append(html);				   							
									});
							   }
						   }
						 });										
		}

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			if(activeTab=="#sub_district") loadSubDistrict(prev);			
			else if(activeTab=="#district") loadDistrict(prev);			
			else if(activeTab=="#city") loadCity(prev);			
			else loadState(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
	});

	$("#last").click(function(){
		var next = totpage;
			if(activeTab=="#sub_district") loadSubDistrict(next);			
			else if(activeTab=="#district") loadDistrict(next);			
			else if(activeTab=="#city") loadCity(next);			
			else loadState(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    $('#m_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function() {
				 return $.get('<?php echo base_url('index.php/district/getDistrict/'); ?>/'+query, function (data) {
					   data = jQuery.parseJSON(data);
					   return process(data.s);
            	 });
            }, 500); 
        },
		minLength : 3,
		delay : 3,
		autoSelect : true,
		items : 50,
		matcher : function(items){
				qu = this.query;
				console.log("run");
				it = items.name;
				qu = qu.split(" ");
				if(qu.length>1)
				{
					var i = 0;
					$.each(qu, function(k,v){
							var re = new RegExp(v, "i");
							if(it.search(re)>-1)
							{
								i = 1;				
							}
							else
							{
								i = 0;	
							}
					});
					if(i==1)
						return true;
					else
						return false;
				}
				else
				{
					var re = new RegExp(qu[0], "i");
					if(it.search(re)>-1)
					{
						return true;
					}	
					else
					{
						return false;	
					}
				}
		}
    });
	

/* 	$('#m_district_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php //echo base_url('index.php/district/getDistrict/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
*/	
	$('#m_district_name').change(function() {
		var current = $('#m_district_name').typeahead("getActive");
		$('#m_district_id').val(current.id);
    });


	$("#m_sub_district_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/sub_district/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/sub_district/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadSubDistrict();
						}
					});
	        	}
 			}
	});

	$("#reset_sub").click(function(){
		$(":input","#formSubdistrict").val("");
	});


	$("#add_sub").click(function(){
			$("#formSubdistrict").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/sub_district/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
							loadSubDistrict();
							$("#formSubdistrict").trigger("reset");
							$("#m_sub_district_id").val(0);

							// $("#m_sub_district_id").val(data.id);
							// alert("Data Berhasil Disimpan");
							// loadSubDistrict();
							// document.getElementById("formSubdistrict").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#add_district").click(function(){
			$("#formDistrict").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/district/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
							loadDistrict();
							$("#formDistrict").trigger("reset");
							$("#m_district_id_1").val(0);

							// $("#m_district_id_1").val(data.id);
							// alert("Data Berhasil Disimpan");
							// loadDistrict();
							// document.getElementById("formDistrict").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#search_district").keyup(function(){
			loadDistrict(1, $("#formSearchDistrict").serializeArray());
	});

/*	$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadDistrict(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchDistrict").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/district/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_district > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_district_id+"'>"+v.name+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.state+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteDistrict\" onclick=\"$('#act_district').val('delete'); $('#m_district_id_1').val("+v.m_district_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->	
										html += "<button class='btn btn-small btn-info' id=\"editDistrict\" onclick=\"$('#act_district').val('edit'); $('#m_district_id_1').val("+v.m_district_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_district").append(html);				   							
									});
							   }
						   }
						 });										
		}

 	$('#m_city_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/city/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
	
	$('#m_city_name').change(function() {
		var current = $('#m_city_name').typeahead("getActive");
		$('#m_city_id').val(current.id);
    });


	$("#m_district_id_1").change(function(){
			var act = $("#act_district").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/district/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="name")
										{
											$("#m_district_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/district/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadDistrict();
						}
					});
	        	}
 			}
	});

	$("#reset_district").click(function(){
		document.getElementById("formDistrict").reset();
	});


	$("#add_district").click(function(){
			$("#formDistrict").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/district/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
							loadDistrict();
							$("#formDistrict").trigger("reset");
							$("#m_district_id_1").val(0);

							// $("#m_district_id_1").val(data.id);
							// alert("Data Berhasil Disimpan");
							// loadDistrict();
							// document.getElementById("formDistrict").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

		
	$("#search_city").keyup(function(){
			loadCity(1, $("#formSearchCity").serializeArray());
	});

	/*$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadCity(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchCity").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/city/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_city > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_city_id+"'>"+v.name+"</td>";
											html += "<td>"+v.m_state_id+"</td>";
											html += "<td><div class='btn-group'>";
<!--											html += "<button class='btn btn-small btn-danger' id=\"deleteCity\" onclick=\"$('#act_city').val('delete'); $('#m_city_id_1').val("+v.m_city_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->
											html += "<button class='btn btn-small btn-info' id=\"editCity\" onclick=\"$('#act_city').val('edit'); $('#m_city_id_1').val("+v.m_city_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_city").append(html);				   							
									});
							   }
						   }
						 });										
		}
 	$('#m_state_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/state/getState/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 3,
		delay : 3,
		autoSelect : true
    });
	
	$('#m_state_name').change(function() {
		var current = $('#m_state_name').typeahead("getActive");
		$('#m_state_id').val(current.id);
    });


	$("#m_city_id_1").change(function(){
			var act = $("#act_city").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/city/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="name")
										{
											$("#m_city_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/city/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadCity();
						}
					});
	        	}
 			}
	});

	$("#reset_city").click(function(){
		document.getElementById("formCity").reset();
	});

	$("#add_city").click(function(){
			$("#formCity").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/city/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
							loadCity();
							$("#formCity").trigger("reset");
							$("#m_city_id_1").val(0);

							// $("#m_city_id_1").val(data.id);
							// alert("Data Berhasil Disimpan");
							// loadCity();
							// document.getElementById("formCity").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#search_state").keyup(function(){
			loadState(1, $("#formSearchState").serializeArray());
	});

	/*$("#btnCancel").click(function(){
		$("#formSearch").trigger("reset");
		loadSubDistrict(1);
	});
*/
		function loadState(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearchState").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/state/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_state > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.m_state_id+"'>"+v.name+"</td>";
											html += "<td><div class='btn-group'>";
<!--										html += "<button class='btn btn-small btn-danger' id=\"deleteState\" onclick=\"$('#act_state').val('delete'); $('#m_state_id_1').val("+v.m_state_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";-->
											html += "<button class='btn btn-small btn-info' id=\"editState\" onclick=\"$('#act_state').val('edit'); $('#m_state_id_1').val("+v.m_state_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_state").append(html);				   							
									});
							   }
						   }
						 });										
		}

	//loadState();	

	$("#m_state_id_1").change(function(){
			var act = $("#act_state").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/state/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
										if(k=="name")
										{
											$("#m_state_name_1").val(v);
										}
										else
										{
											$("#"+k.toLowerCase()).val(v);
										}	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('data akan dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/state/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadState();
						}
					});
	        	}
 			}
	});

	$("#reset_state").click(function(){
		$(":input","#formState").val("");
	});


	$("#add_state").click(function(){
			$("#formState").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/state/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
							loadState();
							$("#formStat").trigger("reset");
							$("#m_state_id_1").val(0);

							// $("#m_state_id_1").val(data.id);
							// alert("Data Berhasil Disimpan");
							// loadState();
							// document.getElementById("formState").reset();							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	
	loadTab();
	
	function loadTab(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
		}
		if(activeTab=="#sub_district")
		{
			loadSubDistrict(page);	
		}
		else
		if(activeTab=="#district")
		{
			loadDistrict(page);	
		}
		else
		if(activeTab=="#city")
		{
			loadCity(page);	
		}
		else
		if(activeTab=="#state")
		{
			loadState(page);				
		}
	}

	$("a").click(function(e){
			e.preventDefault();
	});	


});
</script>