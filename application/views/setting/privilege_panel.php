<div id="main" class="container bootcards-container">
<!-- left list column -->
<div class="row">
<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
    <div class="panel panel-default" style="margin-top:30px;">
        <div class="panel-heading clearfix">
            <h3 class="panel-title pull-left"><b>User Privileges</b></h3>
        </div>
        <div class="panel-body" style="padding:0px;">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#user">User</a></li>
              <li><a data-toggle="tab" href="#role">Role</a></li>
              <li><a data-toggle="tab" href="#function">Function</a></li>
            </ul>
            
            <div class="tab-content">
              <div id="user" class="tab-pane fade in active">
                    <div class="panel-heading" style="background-color:#FFF">
                        <span style="font-size:14px; font-weight:bold">User</span>
                    </div>
					<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formUser" name="formUser">
            <div class="form-group">
                <label for="m_branch_id" class="control-label col-xs-3 ">Kantor Cabang</label>
                <div class="col-xs-2">
                         <input class="form-control" type="text" id="m_branch_code" name="m_branch_code" placeholder="kode cabang" data-provide="typeahead" disabled="disabled"/>
                </div>
                <div class="col-xs-4">
                          <input class="form-control" type="text" id="m_branch_name" name="m_branch_name" placeholder="cari nama cabang" data-provide="typeahead" />
                </div>
            </div>
            <div class="form-group">
              <label for="gaji" class="control-label col-xs-8 col-md-3">Pilih Nama Karyawan</label>
                <div class="col-xs-2">
                      <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                </div>
                <div class="col-xs-4">
                      <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="cari nama karyawan" data-provide="typeahead" />
                      <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="id" value="0"/>                                                    
                </div>                
            </div>
            <div class="form-group">
							<label for="name" class="control-label col-xs-8 col-md-3">Username</label>
							<div class="col-xs-4">
	                    		<input type='text' class="form-control" name="name" id="name" placeholder="Username"/>
	                    		<input type='hidden' class="form-control" name="m_user_id" id="m_user_id" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div>
            <div class="form-group" style="display:none" id="div_plama">
							<label for="district" class="control-label col-xs-8 col-md-3">Password Lama</label>
							<div class="col-xs-4">
	                    		<input type='password' class="form-control" name="password_lama" id="password_lama" placeholder="Password"/>
	                    		<input type='hidden' class="form-control" name="password_hidden" id="password_hidden" placeholder="Password"/>
							</div>

						</div>
            <div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-3">Password</label>
							<div class="col-xs-4">
	                    		<input type='password' class="form-control" name="password" id="password" placeholder="Password"/>
							</div>

						</div>
                		<div class="form-group">
							<label for="district" class="control-label col-xs-8 col-md-3">Confirm Password</label>
							<div class="col-xs-4">
	                    		<input type='password' class="form-control" name="confirm_password" id="confirm_password" placeholder="Password"/>
							</div>

						</div>
                		<div class="form-group">
							<div class="col-xs-6 col-md-2">
							</div>
							<div class="col-xs-8 col-md-5">
                <div class="btn-group pull-right">
									<button class="btn btn-primary" id="saveUser">Save</button>
									<!-- <button class="btn btn-danger" id="resetUser">Reset</button> -->
								</div>
							</div>
                        </div>
					</form>	
                    <hr  />	

                        <div class="table-responsive">
                            <div class="panel-heading" style="background-color:#FFF">
                                <div class="search-form">
                                    <div class="row">
                                        <form id="formSearchUser" role="form">
                                            <div class="col-xs-4 pull-right">
                                                <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    <input type="text" name="name" id="search_state" class="form-control" placeholder="Cari Username" />
                                                </div>
                                            </div>
                                        </form>	
                                    </div>
                                </div>		
                            </div>
                            <table class="table table-bordered" id="tbl_user" style="font-size:12px">
                                <thead>
                                    <tr class="info"><th>Username</th><th>Nama Karyawan</th><th>Cabang</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                    <!-- data user -->
                                </tbody>
                            </table>
                        </div>
					</div>
              </div>
              <div id="role" class="tab-pane fade">
                    <div class="panel-heading" style="background-color:#FFF">
                        <span style="font-size:14px; font-weight:bold">Role</span>
                    </div>
					<div class="panel-body" style="padding:10px;">

					<form class="form-horizontal" role="form" id="formRole" name="formRole">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Role</label>
							<div class="col-xs-6 col-md-5">
	                    		<input type='text' class="form-control" name="name" id="name" placeholder="Role"/>
	                    		<input type='hidden' class="form-control" name="m_role_id" id="m_role_id" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div>
                		<div class="form-group">
							<div class="col-xs-6 col-md-2">
							</div>
							<div class="col-xs-8 col-md-5">
                <div class="btn-group pull-right">
									<button class="btn btn-primary" id="saveRole">Save</button>
									<!-- <button class="btn btn-danger" id="resetRole">Reset</button> -->
								</div>
							</div>
                        </div>
					</form>	
                    <hr  />	

                        <div class="table-responsive">
                            <div class="panel-heading" style="background-color:#FFF">
                                <div class="search-form">
                                    <div class="row">
                                        <form id="formSearchRole" role="form">
                                            <div class="col-xs-4 pull-right">
                                                <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    <input type="text" name="name" id="search_role" class="form-control" placeholder="Cari Role" />
                                                </div>
                                            </div>
                                        </form>	
                                    </div>
                                </div>		
                            </div>
                            <table class="table table-bordered" id="tbl_role">
                                <thead>
                                    <tr class="info"><th>Role Name</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                    <!-- data user -->
                                </tbody>
                            </table>
                        </div>
					</div>
              </div>
			  
              <div id="function" class="tab-pane fade in">
                    <div class="panel-heading" style="background-color:#FFF">
                        <span style="font-size:14px; font-weight:bold">Function</span>
                    </div>
					<div class="panel-body" style="padding:10px;">

					<form class="form-horizontal" role="form" id="formFunction" name="formFunction">
                		<div class="form-group">
							<label for="name" class="control-label col-xs-6 col-md-2">Function</label>
							<div class="col-xs-6 col-md-5">
	                    		<input type='text' class="form-control" name="name" id="name" placeholder="Name"/>
	                    		<input type='hidden' class="form-control" name="m_function_id" id="m_function_id" placeholder="id" value="0"/>
	                    		<input type='hidden' class="form-control" name="act" id="act" placeholder="act"/>
							</div>
						</div>
                		<div class="form-group">
							<div class="col-xs-6 col-md-2">
							</div>
							<div class="col-xs-8 col-md-5">
                <div class="btn-group pull-right">
									<button class="btn btn-primary" id="saveFunction">Save</button>
									<!-- <button class="btn btn-danger" id="resetFunction">Reset</button> -->
								</div>
							</div>
                        </div>
					</form>	
                    <hr  />	

                        <div class="table-responsive">
                            <div class="panel-heading" style="background-color:#FFF">
                                <div class="search-form">
                                    <div class="row">
                                        <form id="formSearchFunction" role="form">
                                            <div class="col-xs-4 pull-right">
                                                <div class="input-group">
                                                  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    <input type="text" name="name" id="search_function" class="form-control" placeholder="Cari Function" />
                                                </div>
                                            </div>
                                        </form>	
                                    </div>
                                </div>		
                            </div>
                            <table class="table table-bordered" id="tbl_function">
                                <thead>
                                    <tr class="info"><th>Function</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                    <!-- data user -->
                                </tbody>
                            </table>
                        </div>
					</div>
              </div>
            </div>
        </div>
		<!--  end body -->
        
		<!--  footer -->
        <div class="panel-footer center-block">
            <div class="btn-group" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="#" id="first">
                        <i class="fa fa-fast-backward fa-fw"></i> 
                        <span>First</span>
                    </a>
                    <a class="btn btn-default" href="#" id="prev">
                        <i class="fa fa-backward fa-fw"></i> 
                        <span>Prev</span>
                    </a>
                    <a class="btn btn-default" href="#" id="next">
                        <i class="fa fa-forward fa-fw"></i> 
                        <span>Next</span>
                    </a>
                    <a class="btn btn-default" href="#" id="last">
                        <i class="fa fa-fast-forward fa-fw"></i> 
                        <span>Last</span>
                    </a>
                    <a class="btn btn-default" href="#" >
                        <span id="textpage"></span>
                    </a>
                </div>
            </div>
        </div>		                                    
		<!--  end footer -->
	</div>
</div>
</div>


<!-- modal akses user -->

<div class="modal fade" role="dialog" aria-hidden="true" id="addAksesUser">
  <div class="modal-dialog">
	<div class="modal-content">
    <form class="form-horizontal" id="formAksesUser">
    <div class="modal-header">
      <div class="btn-group pull-right">
        <button id="resetAksesUser" class="btn btn-danger" data-dismiss="modal">
          Cancel
        </button>
        <button id="saveAksesUser" class="btn btn-success save" data-dismiss="modal">
          Save
        </button>
      </div>
      <h3 class="modal-title pull-left">Akses User</h3>

    </div>

    <div class="modal-body"> 
      <div class="form-group">
        <label class="col-xs-3 control-label">Username</label>
        <div class="col-xs-9">
          <input type="hidden" name="m_user_id" id="m_user_id" class="form-control" placeholder="ID">
          <input type="text" name="username" id="username" class="form-control" placeholder="Username">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 col-md-3 control-label">Hak Akses</label>
        <div class="col-xs-12 col-md-4">
            <select name="select_m_role_id" style="height:40%;" id="opsi_role" multiple class="form-control">
            </select>        
         </div>
         <div class="col-xs-12 col-md-1 no-padding">
             <div class="btn-group-vertical">
                  <button type="button" id="sel" class="btn btn-small btn-primary"><i class="fa fa-arrow-right"></i></button>
                  <button type="button" id="desel" class="btn btn-small btn-primary"><i class="fa fa-arrow-left"></i></button>
                  <button type="button" id="sel_all" class="btn btn-small btn-primary"><i class="fa fa-fast-forward"></i></button>
                  <button type="button" id="desel_all" class="btn btn-small btn-primary"><i class="fa fa-fast-backward"></i></button>
            </div>
         </div>
        <div class="col-xs-12 col-md-4">
            <select name="m_role_id[]" style="height:40%;" id="selected_role" multiple class="form-control">
            </select>        
         </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<!-- end modal akses user -->


<!-- modal akses function -->

<div class="modal fade" role="dialog" aria-hidden="true" id="addAksesFunction">
  <div class="modal-dialog">
	<div class="modal-content">
    <form class="form-horizontal" id="formAksesFunction">
    <div class="modal-header">
      <div class="btn-group pull-right">
        <button id="resetAksesFunction" class="btn btn-danger save" data-dismiss="modal">
          Cancel
        </button>
        <button id="saveAksesFunction" class="btn btn-success save" data-dismiss="modal">
          Save
        </button>
      </div>
      <h3 class="modal-title pull-left">Akses User</h3>

    </div>

    <div class="modal-body"> 
      <div class="form-group">
        <label class="col-xs-3 control-label">Role</label>
        <div class="col-xs-9">
          <input type="hidden" name="m_role_id" id="m_role_id" class="form-control" placeholder="ID">
          <input type="text" name="name" id="name" class="form-control" placeholder="Role">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 col-md-3 control-label">Hak Akses Function</label>
        <div class="col-xs-12 col-md-4">
            <select name="select_m_function_id" style="height:40%;" id="opsi_function" multiple class="form-control">
            </select>        
         </div>
         <div class="col-xs-12 col-md-1 no-padding">
             <div class="btn-group-vertical">
                  <button type="button" id="sel" class="btn btn-small btn-primary"><i class="fa fa-arrow-right"></i></button>
                  <button type="button" id="desel" class="btn btn-small btn-primary"><i class="fa fa-arrow-left"></i></button>
                  <button type="button" id="sel_all" class="btn btn-small btn-primary"><i class="fa fa-fast-forward"></i></button>
                  <button type="button" id="desel_all" class="btn btn-small btn-primary"><i class="fa fa-fast-backward"></i></button>
            </div>
         </div>
        <div class="col-xs-12 col-md-4">
            <select name="m_function_id[]" style="height:40%;" id="selected_function" multiple class="form-control">
            </select>        
         </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<!-- end modal akses user -->



<!-- modal akses role -->

<div class="modal fade" role="dialog" aria-hidden="true" id="addAksesRole">
  <div class="modal-dialog">
	<div class="modal-content">
    <form class="form-horizontal" id="formAksesRole">
    <div class="modal-header">
      <div class="btn-group pull-right">
        <button id="resetAksesRole" class="btn btn-danger" data-dismiss="modal">
          Cancel
        </button>
        <button id="saveAksesRole" class="btn btn-success save" data-dismiss="modal">
          Save
        </button>
      </div>
      <h3 class="modal-title pull-left">Akses Role</h3>

    </div>

    <div class="modal-body"> 
      <div class="form-group">
        <label class="col-xs-3 control-label">Username</label>
        <div class="col-xs-9">
          <input type="hidden" name="m_role_id" id="m_role_id" class="form-control" placeholder="ID">
          <input type="text" name="name" id="name" class="form-control" placeholder="Role">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 col-md-3 control-label">Hak Akses</label>
	  </div>
      <div class="table-responsive">
            <table class="table table-bordered" id="tbl_akses">
                <thead>
                    <tr class="info"><th>Menu</th><th>View</th><th>Edit</th><th>Delete</th></tr>
                </thead>
                <tbody>
                    <!-- data user -->
                </tbody>
            </table>
      </div>
	  
    </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>


<!-- end modal akses role -->

<script type="text/javascript">
$(function(){

	var curpage = 1;
	var totpage = 1;

	var activeTab = $('.nav-tabs .active > a').attr('href');

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	//show selected tab / active
	  activeTab = $(e.target).attr('href');
	  loadTab();
	});	

// button geser select

$("#formAksesUser [id=sel]").click(function(){
	var foo = []; 
	$('#opsi_role :selected').each(function(i, selected){ 
		$('#selected_role').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesUser [id=desel]").click(function(){
	var foo = []; 
	$('#selected_role :selected').each(function(i, selected){ 
		$('#opsi_role').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesUser [id=sel_all]").click(function(){
	var foo = []; 
	$('#opsi_role option').each(function(i, selected){ 
		$('#selected_role').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesUser [id=desel_all]").click(function(){
	var foo = []; 
	$('#selected_role option').each(function(i, selected){ 
		$('#opsi_role').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

// end button geser


// button geser select akses function

$("#formAksesFunction [id=sel]").click(function(){
	var foo = []; 
	$('#opsi_function :selected').each(function(i, selected){ 
		$('#selected_function').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesFunction [id=desel]").click(function(){
	var foo = []; 
	$('#selected_function :selected').each(function(i, selected){ 
		$('#opsi_function').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesFunction [id=sel_all]").click(function(){
	var foo = []; 
	$('#opsi_function option').each(function(i, selected){ 
		$('#selected_function').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

$("#formAksesFunction [id=desel_all]").click(function(){
	var foo = []; 
	$('#selected_function option').each(function(i, selected){ 
		$('#opsi_function').append($('<option>', {
			value: $(selected).val(),
			text: $(selected).text()
		}));
		$(selected).remove();
	});			
});

// end button geser

$("#formAksesUser").ajaxForm({
		type: 'POST',
		url: "<?php echo base_url("index.php/user/saveAkses"); ?>",
		data: $(this).serialize(),
		success: function(data)
		{
		   data = jQuery.parseJSON(data);
		   if(data.success) 
		   {
				alert("Data Berhasil Disimpan");
				resetFormAkses();
		   }
			else
			{
				alert(data.message);	
			}
		}
});


$("#formAksesFunction").ajaxForm({
		type: 'POST',
		url: "<?php echo base_url("index.php/role/saveAksesFunction"); ?>",
		data: $(this).serialize(),
		success: function(data)
		{
		   data = jQuery.parseJSON(data);
		   if(data.success) 
		   {
				alert("Data Berhasil Disimpan");
				resetFormAkses();
		   }
			else
			{
				alert(data.message);	
			}
		}
});


	$("#formFunction [id=m_function_id]").change(function(){
			var act = $("#formFunction [id=act]").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/functions/get/'); ?>/"+id,
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
									$("#formFunction [id="+k.toLowerCase()+"]").val(v);		
								});
						   }
					   }
				 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('Data akan Dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/functions/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadFunction();
						}
					});
	        	}
 			}
	});



$("#formAksesRole").ajaxForm({
		type: 'POST',
		url: "<?php echo base_url("index.php/role/saveAksesMenu"); ?>",
		data: $(this).serialize(),
		success: function(data)
		{
		   data = jQuery.parseJSON(data);
		   if(data.success) 
		   {
				alert("Data Berhasil Disimpan");
				resetFormAksesRole();
		   }
			else
			{
				alert(data.message);	
			}
		}
});

$("#saveAksesUser").click(function(){
	$('#selected_role option').each(function(i, selected){ 
		$(selected).attr("selected","selected");
	});	

	$('#opsi_role option').each(function(i, selected){ 
		$(selected).attr("selected","selected");
	});	
			
	$("#formAksesUser").submit();
});

$("#resetAksesUser").click(function(){
	resetFormAkses();
});

$("#saveAksesRole").click(function(){
	$("#formAksesRole").submit();
});

$("#resetAksesRole").click(function(){
	resetFormAksesRole();
});


$("#saveAksesFunction").click(function(){
	$('#selected_function option').each(function(i, selected){ 
		$(selected).attr("selected","selected");
	});	

	$('#opsi_function option').each(function(i, selected){ 
		$(selected).attr("selected","selected");
	});	
			
	$("#formAksesFunction").submit();
});

$("#resetAksesFunction").click(function(){
	resetFormAksesFunction();
});

function resetFormAkses()
{
	$("#formAksesUser").trigger("reset");
	$("#formAksesUser input[type=hidden]").val(0);	

	$('#opsi_role option').each(function(i, selected){ 
		$(selected).remove();
	});			

	$('#selected_role option').each(function(i, selected){ 
		$(selected).remove();
	});			
}

function resetFormAksesFunction()
{
	$("#formAksesFunction").trigger("reset");
	$("#formAksesFunction input[type=hidden]").val(0);	

	$('#opsi_function option').each(function(i, selected){ 
		$(selected).remove();
	});			

	$('#selected_function option').each(function(i, selected){ 
		$(selected).remove();
	});			
}

function resetFormAksesRole()
{
	$("#formAksesRole").trigger("reset");
	$("#formAksesRole input[type=hidden]").val(0);	
}

// akses user
/*	$('#select_role').each(function(){    
		var select = $(this);
		var values = {};    
		$('option',select).each(function(i, option){
			values[option.value] = option.selected;        
		}).click(function(event){        
			values[this.value] = !values[this.value];
			$('option',select).each(function(i, option){            
				option.selected = values[option.value];        
			});
		});
	});
*/
// end akses user

// user code
	loadTab();
	
	function loadTab(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
		}
		if(activeTab=="#user")
		{
			loadUser(page);	
		}
		else
		if(activeTab=="#role")
		{
			loadRole(page);	
		}
		else
		{
			loadFunction(page);				
		}
	}
	
	$("#saveUser").click(function(){
			
			if($("#password").val()!=$("#confirm_password").val())
			{
				alert("Konfirmasi Password Tidak Sama");
				return false;
			}

			if($("#password_lama").val()!=$("#password_hidden").val() && $("#formUser [id=act]").val()=="edit")
			{
				alert("Password Lama Tidak Sesuai");
				return false;
			}

			if($("#password_lama").val()=="" && $("#act").val()=="adit")
			{
				alert("Password Lama Harus Diisi");
				return false;
			}

			if($("#confirm_password").val()=="")
			{
				alert("Confirm Password Harus Diisi");
				return false;
			}

			if($("#password").val()=="")
			{
				alert("Password Harus Diisi");
				return false;
			}

			if($("#name").val()=="")
			{
				alert("Username Harus Diisi");
				return false;
			}

			$("#formUser").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/user/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_user_id").val(data.id);
							alert("Data Berhasil Disimpan");
							loadUser();
							$("#resetUser").trigger("click");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#resetUser").click(function(e){
		e.preventDefault();
		$("#formUser").trigger("reset");
		$("#formUser input[type=hidden]").val(0);
		$("#div_plama").hide();
	});

	function loadUser(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
			curpage = 1;
			totpage = 1;			
		}
		
		var par = new Array();
		par["page"] = page;

		q = $("#formSearchUser").serializeArray();

		$.each(q, function(k,v){
			par[v.name] = v.value;
		});

		par = $.extend({}, par);

		$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/user/getList/'); ?>",
					   data: par, // serializes the form's elements.
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {
								$("#tbl_user > tbody").empty();
								$('#textpage').html("Page "+page+" of "+data.totpage);
								curpage = page;
								totpage = data.totpage;
								$.each(data.data, function(k,v){
									var html = "<tr><td>"+v.name+"</td>";
										html += "<td>"+v.personal_name+"</td>";
										html += "<td>"+v.branch+"</td>";
										html += "<td><div class='btn-group'>";
										html += "<button class='btn btn-small btn-danger' id=\"deleteUser\" onclick=\"$('#formUser [id=act]').val('delete'); $('#m_user_id').val("+v.M_USER_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
										html += "<button class='btn btn-small btn-info' id=\"editUser\" onclick=\"$('#formUser [id=act]').val('edit'); $('#formUser [id=m_user_id]').val("+v.M_USER_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
										html += "<button class='btn btn-small btn-primary' id=\"aksesUser\" href=\"#\" data-toggle=\"modal\" data-target=\"#addAksesUser\" onclick=\"$('#formAksesUser [id=m_user_id]').val("+v.M_USER_ID+").trigger('change'); $('#opsi_role option').remove(); $('#selected_role option').remove();\"><i class='fa fa-key'></i></button>";
										html += "</div>";
										html += "</td></tr>";
									$("#tbl_user").append(html);				   							
								});
						   }
					   }
					 });										
	}



	$("#saveFunction").click(function(){
			
			if($("#formFunction [name=name]").val()=="")
			{
				alert("Function Harus Diisi");
				return false;
			}

			$("#formFunction").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/functions/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_function_id").val(data.id);
							alert("Data Berhasil Disimpan");
							loadFunction();
							$("#resetFunction").trigger("click");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#resetFunction").click(function(e){
		e.preventDefault();
		$("#formFunction").trigger("reset");
		$("#formFunction input[type=hidden]").val(0);
	});

	function loadFunction(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
			curpage = 1;
			totpage = 1;			
		}
		
		var par = new Array();
		par["page"] = page;

		q = $("#formSearchFunction").serializeArray();

		$.each(q, function(k,v){
			par[v.name] = v.value;
		});

		par = $.extend({}, par);

		$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/functions/getList/'); ?>",
					   data: par, // serializes the form's elements.
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {
								$("#tbl_function > tbody").empty();
								$('#textpage').html("Page "+page+" of "+data.totpage);
								curpage = page;
								totpage = data.totpage;
								$.each(data.data, function(k,v){
									var html = "<tr><td>"+v.name+"</td>";
										html += "<td><div class='btn-group'>";
										html += "<button class='btn btn-small btn-danger' id=\"deleteFunction\" onclick=\"$('#formFunction [id=act]').val('delete'); $('#m_function_id').val("+v.M_FUNCTION_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
										html += "<button class='btn btn-small btn-info' id=\"editFunction\" onclick=\"$('#formFunction [id=act]').val('edit'); $('#formFunction [id=m_function_id]').val("+v.M_FUNCTION_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
										html += "</div>";
										html += "</td></tr>";
									$("#tbl_function").append(html);				   							
								});
						   }
					   }
					 });										
	}
	
	$("#search_state").keyup(function(){
			loadUser();
	});


	$("#formAksesUser [id=m_user_id]").change(function(){
		var id = $(this).val();
		var aksesuser = new Array();
		if(id!=0)
		{
			$.ajax({
				   type: "GET",
				   async: false,
				   url: "<?php echo base_url('index.php/role/get/'); ?>",
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {	
							$.each(data.data, function(k,v){
								aksesuser.push({"value" : v.m_role_id,
												"name" : v.name});
							});
					   }
				   }
			});	
	
			$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/user/getAkses/'); ?>/"+id,
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {	
							$.each(data.data, function(k,v){
								$("#formAksesUser [name="+k.toLowerCase()+"]").val(v);		
							});
							
							if(data.data.role.length==0)
							{
								$.each(aksesuser, function(k,v){
									$('#opsi_role').append($('<option>', {
										value: v.value,
										text: v.name
									}));
								});
							}
							else
							{
								var currole = new Array();
								
								$.each(data.data.role, function(k2,v2){
									currole.push(v2.m_role_id);
								});
								$.each(aksesuser, function(k,v){
									if(currole.indexOf(v.value)==-1)
									{
										$('#opsi_role').append($('<option>', {
											value: v.value,
											text: v.name
										}));
									}
									else
									{
										$('#selected_role').append($('<option>', {
											value: v.value,
											text: v.name
										}));									
									}
								});
							}
					   }
				   }
			 });	
		}
	});


	$("#formAksesFunction [id=m_role_id]").change(function(){
		var id = $(this).val();
		var aksesuser = new Array();
		if(id!=0)
		{
			$.ajax({
				   type: "GET",
				   async: false,
				   url: "<?php echo base_url('index.php/role/getFunction/'); ?>",
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {	
							$.each(data.data, function(k,v){
								aksesuser.push({"value" : v.m_function_id,
												"name" : v.name});
							});
					   }
				   }
			});	
	
			$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/role/getAksesFunction/'); ?>/"+id,
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {	
							$.each(data.data, function(k,v){
								$("#formAksesFunction [name="+k.toLowerCase()+"]").val(v);		
							});
							
							if(data.data.role.length==0)
							{
								$.each(aksesuser, function(k,v){
									$('#opsi_function').append($('<option>', {
										value: v.value,
										text: v.name
									}));
								});
							}
							else
							{
								var currole = new Array();
								
								$.each(data.data.role, function(k2,v2){
									currole.push(v2.m_function_id);
								});
								$.each(aksesuser, function(k,v){
									if(currole.indexOf(v.value)==-1)
									{
										$('#opsi_function').append($('<option>', {
											value: v.value,
											text: v.name
										}));
									}
									else
									{
										$('#selected_function').append($('<option>', {
											value: v.value,
											text: v.name
										}));									
									}
								});
							}
					   }
				   }
			 });	
		}
	});


	$("#formAksesRole [id=m_role_id]").change(function(){
		var id = $(this).val();
		var aksesuser = new Array();
		if(id!=0)
		{
			$.ajax({
				   type: "GET",
				   async: false,
				   url: "<?php echo base_url('index.php/role/getAksesMenu/'); ?>/"+id,
				   success: function(data)
				   {
					   
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {	
							$("#formAksesRole [id=m_role_id]").val(data.id);
							$("#formAksesRole [id=name]").val(data.name);
							$("#tbl_akses > tbody").empty();
							$.each(data.menu, function(k,v){
								html = "<tr><td>"+v.name+"</td>";
								html += "<td><input type='checkbox' name='view_"+v.m_menu_id+"' id='view_"+v.m_menu_id+"'  /></td>";
								html += "<td><input type='checkbox' name='edit_"+v.m_menu_id+"' id='edit_"+v.m_menu_id+"'  /></td>";
								html += "<td><input type='checkbox' name='delete_"+v.m_menu_id+"' id='delete_"+v.m_menu_id+"'  /></td></tr>";
								$("#tbl_akses > tbody").append(html);

								if(v.view_ == "Y")
								{
									$("#view_"+v.m_menu_id).attr("checked","checked");									
								}
								if(v.edit_ == "Y")
								{
									$("#edit_"+v.m_menu_id).attr("checked","checked");									
								}
								if(v.delete == "Y")
								{
									$("#delete_"+v.m_menu_id).attr("checked","checked");									
								}
							});
					   }
				   }
			});	
		}
	});


	$("#formUser [id=m_user_id]").change(function(){
			var act = $("#formUser [id=act]").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
				$("#div_plama").show();
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/user/get/'); ?>/"+id,
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
										if(k!="password") 
											$("#formUser [id="+k.toLowerCase()+"]").val(v);		
//											$("#"+k.toLowerCase()).val(v);		
										else
											$("#password_hidden").val(v);		
								});
						   }
					   }
				 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('Data akan Dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/user/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadUser();
						}
					});
	        	}
 			}
	});



// end user code

// role code
	$("#saveRole").click(function(){
			
			if($("#formRole [name=name]").val()=="")
			{
				alert("Role Name Harus Diisi");
				return false;
			}

			$("#formRole").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/role/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#m_role_id").val(data.id);
							alert("Data Berhasil Disimpan");
							loadRole();
							$("#resetRole").trigger("click");							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});

	$("#resetRole").click(function(e){
		e.preventDefault();
		$("#formRole").trigger("reset");
		$("#formRole input[type=hidden]").val(0);
	});

	function loadRole(page)
	{
		if(typeof page === 'undefined')
		{
			page = 1;
			curpage = 1;
			totpage = 1;			
		}
		var par = new Array();
		par["page"] = page;

		q = $("#formSearchRole").serializeArray();

		$.each(q, function(k,v){
			par[v.name] = v.value;
		});

		par = $.extend({}, par);

		$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/role/getList/'); ?>",
					   data: par, // serializes the form's elements.
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {
								$("#tbl_role > tbody").empty();
								$('#textpage').html("Page "+page+" of "+data.totpage);
								curpage = page;
								totpage = data.totpage;
								$.each(data.data, function(k,v){
									var html = "<tr><td>"+v.name+"</td>";
										html += "<td><div class='btn-group'>";
										html += "<button class='btn btn-small btn-danger' id=\"deleteRole\" onclick=\"$('#formRole [name=act]').val('delete'); $('#m_role_id').val("+v.M_ROLE_ID+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
										html += "<button class='btn btn-small btn-info' id=\"editRole\" onclick=\"$('#formRole [id=act]').val('edit'); $('#m_role_id').val("+v.M_ROLE_ID+").trigger('change');\"><i class='fa fa-edit'></i></button>";
										html += "<button class='btn btn-small btn-primary' id=\"aksesRole\" href=\"#\" data-toggle=\"modal\" data-target=\"#addAksesRole\" onclick=\"$('#formAksesRole [id=m_role_id]').val("+v.M_ROLE_ID+").trigger('change');\"><i class='fa fa-key'></i></button>";
										html += "<button class='btn btn-small btn-warning' id=\"aksesFunction\" href=\"#\" data-toggle=\"modal\" data-target=\"#addAksesFunction\" onclick=\"$('#formAksesFunction [id=m_role_id]').val("+v.M_ROLE_ID+").trigger('change'); $('#opsi_function option').remove(); $('#selected_function option').remove();\"><i class='fa fa-user-secret'></i></button>";
										html += "</div>";
										html += "</td></tr>";
									$("#tbl_role").append(html);				   							
								});
						   }
					   }
					 });										
	}
	
	$("#search_role").keyup(function(){
			loadRole();
	});

	$("#formRole [id=m_role_id]").change(function(){
			var act = $("#formRole [id=act]").val();
			var id = $(this).val();

			if(act=="edit")
			{
				$.ajax({
					   type: "GET",
					   url: "<?php echo base_url('index.php/role/get/'); ?>/"+id,
					   success: function(data)
					   {
						   
						   data = jQuery.parseJSON(data);
						   if(data.success) 
						   {	
								$.each(data.data, function(k,v){
										$("#formRole [name="+k.toLowerCase()+"]").val(v);
								});
						   }
					   }
				 });	
			}
			else
			if(act=="delete")
			{
				var result = window.confirm('Data akan Dihapus?');
    	        if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/role/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadRole();
						}
					});
	        	}
 			}
	});

// end role code
	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{
			if(activeTab=="#user") loadUser(next);			
			else if(activeTab=="#role") loadRole(next);			
			else loadFunction(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			if(activeTab=="#user") loadUser(prev);			
			else if(activeTab=="#role") loadRole(prev);			
			else loadFunction(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
		if(activeTab=="#user") loadUser(next);			
		else if(activeTab=="#role") loadRole(next);			
		else loadFunction(next);
	});

	$("#last").click(function(){
		var next = totpage;
		if(activeTab=="#user") loadUser(next);			
		else if(activeTab=="#role") loadRole(next);			
		else loadFunction(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	


  
});
});
</script>