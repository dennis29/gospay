<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: 
* Development Group: GOS Payment (HRIS)
* Description: Tampilan daftar Menu
* 
********************************************************************/  -->
<div id="main" class="container bootcards-container">
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
		<div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
		<div class="panel panel-default" style="margin-top:30px;">
			<div class="panel-heading clearfix">
				<h3 class="panel-title pull-left"><b>List Menu</b></h3>
				<div class="btn-group pull-right">
					<a class="btn btn-primary" href="#" data-toggle="modal" data-target="#addMenu" id="btnAdd">
					<i class="fa fa-plus"></i> 
					<span>Add</span>
					</a>
				</div>
			</div>
			<div class="table-responsive">
				<table class="table table-hover" id="menu_list">
				  <thead>
					<tr class="info"><th>Name</th><th>Parent</th><th>Order Role</th><th>URL</th><th>Action</th></tr>
				  </thead>
				  <tbody>
			
				  </tbody>
				</table>
			</div>
			<div class="panel-footer center-block">
				<div class="btn-group" role="group">
				  <a class="btn btn-default" href="#" id="first" >
					<i class="fa fa-fast-backward fa-fw"></i> 
					<span>First</span>
				  </a>
				  <a class="btn btn-default" href="#" id="prev" >
					<i class="fa fa-backward fa-fw"></i> 
					<span>Prev</span>
				  </a>
				  <a class="btn btn-default" href="#" id="next" >
					<i class="fa fa-forward fa-fw"></i> 
					<span>Next</span>
				  </a>
				  <a class="btn btn-default" href="#" id="last" >
					<i class="fa fa-fast-forward fa-fw"></i> 
					<span>Last</span>
				  </a>
				  <a class="btn btn-default" href="#" >
					<span id="textpage"></span>
				  </a>
				</div>
			</div>
		</div>
	</div>
</div>
</div>




<div class="modal fade" role="dialog" aria-hidden="true" id="addMenu">
  <div class="modal-dialog">
	<div class="modal-content">
    <form class="form-horizontal" id="formMenu">
    <div class="modal-header">
      <div class="btn-group pull-right">
        <button class="btn btn-danger" data-dismiss="modal" id="btnCancel">
          Cancel
        </button>
        <button class="btn btn-success save" data-dismiss="modal">
          Save
        </button>
      </div>
      <h3 class="modal-title pull-left" id="modal_title">Menu</h3>

    </div>

    <div class="modal-body"> 
      <div class="form-group">
        <label class="col-xs-3 control-label">Name</label>
        <div class="col-xs-9">
          <input type="hidden" name="m_menu_id" id="m_menu_id" class="form-control" placeholder="id" value="0">
          <input type="text" name="name_menu" id="name_menu" class="form-control" placeholder="Name">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Parent</label>
        <div class="col-xs-9">
          <?php echo $comboParent; ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Order Role</label>
        <div class="col-xs-9">
          <input type="text" name="order_role" id="order_role" class="form-control" placeholder="Order Role">
        </div>
      </div>
      <div class="form-group">
        <label class="col-xs-3 control-label">Url</label>
        <div class="col-xs-9">
          <input type="text" name="url" id="url" class="form-control" placeholder="URL">
        </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
<!--    <small>Built with Bootcards - Form Card</small>
-->  </div>
          
    </div>
  </div>
</div>

<script>
$(function()
{
		var curpage = 1;
		var totpage = 1;

		function loadMenu(page)
		{
			if(typeof page === 'undefined')
				page = 1;
			
			var par = new Array();
			par["page"] = page;

			//q = $("#formSearch").serializeArray();

			//$.each(q, function(k,v){
			//	par[v.name] = v.value;
			//});

			par = $.extend({}, par);

			$.ajax({
				   type: "GET",
				   url: "<?php echo base_url('index.php/menu/get/'); ?>",
				   data: par, // serializes the form's elements.
				   success: function(data)
				   {
					   //console.log(data);
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$('#textpage').html("Page "+page+" of "+data.totpage);
							curpage = page;
							totpage = data.totpage;

						    $("#menu_list > tbody").empty();
							$.each(data.data, function(k,v){
								var html = "<tr><td>"+v.name_menu+"</td>";
									html += "<td>"+v.parent_id+"</td>";
									html += "<td>"+v.order_role+"</td>";
									html += "<td>"+v.url+"</td>";
									html += "<td><div class='btn-group'>";
									html += "<button class='btn btn-small btn-danger'><i class='fa fa-trash-o'></i></button>";
									html += "<button class='btn btn-small btn-info'";
									html += " onclick=\"$('#btnAdd').trigger('click');"; 
									html += "$('#m_menu_id').val('"+v.m_menu_id+"');";
									html += "$('#name_menu').val('"+v.name_menu+"');";
									html += "$('#parent_id').val('"+v.parent_id+"'); ";
									html += "$('#order_role').val('"+v.order_role+"'); ";
									html += "$('#url').val('"+v.url+"');\">";									
									html += "<i class='fa fa-edit'></i></button>";
									html += "</div>";
									html += "</td></tr>";
								$("#menu_list").append(html);				   							
							});
					   }
				   }
				 });			
		}

	loadMenu();

	$("#alert").hide();

	$(".save").click(function(){
			$("#formMenu").submit();		
	});

	
	$("#formMenu").submit(function() {
		$.ajax({
			   type: "POST",
			   url: "<?php echo base_url('index.php/menu/save'); ?>",
			   data: $("#formMenu").serialize(), // serializes the form's elements.
			   success: function(data)
			   {
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {
						loadMenu();
				   }
			   }
			 });
	
		return false; // avoid to execute the actual submit of the form.
	});		

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) loadMenu(next);			
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1) loadMenu(prev);			
	});
	
	$("#first").click(function(){
		var next = 1;
		loadMenu(next);			
	});

	$("#last").click(function(){
		var next = totpage;
		loadMenu(next);			
	});
	
	$("#btnCancel").click(function(){
		$("#m_menu_id").val(0);
		$("#formMenu").trigger("reset");
					
	});
	

});
</script>