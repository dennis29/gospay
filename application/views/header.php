<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />      <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->
	<meta http-equiv="Page-Enter" content="progid:DXImageTransform.Microsoft.Fade(Duration=2)">
	<meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.Fade(Duration=2)">

    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <title>GOS Payment</title>
    <!-- Fav icon -->
    <link rel="shortcut icon" href="http://localhost/gospay/asset/img/favicon.png">
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url('asset/css/bootcards-desktop.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/fileinput.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/bootstrap-datetimepicker.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/font-awesome.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/select2.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/jquery-ui.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('asset/css/sb-admin-2.css'); ?>"/>
    <!-- JS -->
	<script type="text/javascript" src="<?php echo base_url('asset/js/jquery.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootcards.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/fileinput.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/moment.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/form.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.tokeninput.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.plugin.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-tokenfield.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap-tokenfield.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/numeric.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/autoNumeric.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('asset/js/function.js'); ?>"></script>

    <script type="text/javascript" src="<?php echo base_url('asset/js/select2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootstrap3-typeahead.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/bootbox.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery-ui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('asset/js/jquery.mask.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('asset/js/accounting.js'); ?>"></script>
	


    	 
	<!--  DATAGRID plugin -->
	<!-- <link rel="stylesheet" href="<?php echo base_url('asset/bs_grid/jquery.bs_grid.css'); ?>"/> -->
	<!-- <script type="text/javascript" src="<?php echo base_url('asset/bs_grid/jquery.bs_grid.min.js'); ?>"></script> -->
	<!-- <script type="text/javascript" src="<?php echo base_url('asset/bs_grid/localization/en.min.js'); ?>"></script>  -->
	    
	<style>
	
	.loading {
	  background: url('<?php echo base_url('/asset/img/loading.gif'); ?>') no-repeat;
	}
	.required:after{
		color: #d00 !important;
		content: "*" !important;
		position: absolute !important;
		margin-left: 5px !important;
		top:7px !important;
	}

	.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
		border-top: 1px solid #ddd;
		line-height: 1.42857;
		padding: 5px;
		vertical-align: top;
	}

	.list-group-item-text{
		line-height:normal !important;	
	}

	.table tbody tr:hover td, .table-hover tbody tr:hover th {
		background: #eeeeee; /* Old browsers */
		background: -moz-linear-gradient(top,  #eeeeee 0%, #eeeeee 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eeeeee), color-stop(100%,#eeeeee)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #eeeeee 0%,#eeeeee 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #eeeeee 0%,#eeeeee 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #eeeeee 0%,#eeeeee 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #eeeeee 0%,#eeeeee 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#eeeeee',GradientType=0 ); /* IE6-9 */
	}	
	
   .static-panel {
		  min-height: 10;
		  max-height: 100%;
		  overflow-y: scroll;
		  overflow-x: hidden;
	}
	
	.no-padding {
		padding-left: 0 !important;	
		padding-right: 0 !important;	
	}

	.addon-fix {
		min-width:130px;
		text-align:left;
	}
	input[type=number]::-webkit-inner-spin-button, 
	input[type=number]::-webkit-outer-spin-button { 
  		-webkit-appearance: none; 
  		margin: 0; 
	}
	input[type=number] {
    	-moz-appearance:textfield;
	}
	.scrollable-pane{
		height:300px;
		overflow-x:scroll;
	}
	.table-striped-column > tbody > tr:nth-of-type(even) {
    	background-color: #FFFFFF;
	}
	.table-striped-column > tbody > tr:nth-of-type(odd) {
    	background-color: #CCCCCC;
	}
    .js {
		display: none;
	}
	.load {
		display:    none;
		position:   static;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 204, 204, 204, .8 ) 
					url('<?php echo base_url('asset/img/loading.gif'); ?>')
					50% 50% 
					no-repeat;
	}	

	body {
         padding-top:50px;
        }

        #masthead { 
         min-height:250px;
        }

        #masthead h1 {
         font-size: 30px;
         line-height: 1;
         padding-top:20px;
        }

        #masthead .well {
         margin-top:8%;
        }

        @media screen and (min-width: 768px) {
          #masthead h1 {
            font-size: 50px;
          }
        }

        .navbar-bright {
         background-color:#111155;
         color:#fff;
        }

        .affix-top,.affix{
         position: fixed;
        }

        @media (min-width: 979px) {
        .sideBar.boxLeft.affix-top {
            position: fixed;
            margin-top:30px;
            width:228px;
          }
          
         .sideBar.boxLeft.affix {
            position: fixed;
            top:70px;
            width:228px;
          }
        }

        .sideBar {
		    position: s;
		    z-index: 1000;
		}
		.sideBar.boxLeft {
		    width: 300px;
		    float:left;
		    padding: 0px 0px 0px 0px;
		    left:50px;		    
		    top:15%;
		    /*background-color:#eee;*/
		}

		.sideBar.boxLeft ul {
			list-style-type: none;

		}
		
		.sideBar.boxLeft a {
			position: static;
		    display: block;
		    padding: 10px 15px;
		    text-decoration: none;
		}

		.sideBar.boxLeft li:hover {
			border:0 #eee solid;
          	border-right-width:5px;
          	background-color:#eee;

		}

		.sideBar.boxLeft ul li .active {
          border:0 #eee solid;
          border-right-width:5px;
          background-color:#FFFFFF;
        }

		.mainPage 
		{
		    padding: 0 0 0 250px;
		}

		.mainPage p
		{
		   text-align:left;
		    padding:0 0 0 2px;
		}

	

</style>
<script type="text/javascript">
$(function()
{
	/*$(document).ajaxStart(function(){
		$(".load").show();
	});
	$(document).ajaxStop(function(){
		$(".load").hide();
	});*/
});
</script>
</head>