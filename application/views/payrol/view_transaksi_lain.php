<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Transaksi Pendapatan Lain-lain
* 
********************************************************************/  -->

<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
</style>

<div class="row">
    <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
        <div id="alert" class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Sukses!</strong> Data berhasil disimpan.
        </div>
        <div class="panel panel-default" style="margin-top:0px;">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"><b>Transaksi Pendapatan Lain-lain</b></h3>                
            </div>          
                <div class="panel-body" style="padding:10px;">
                    <form class="form-horizontal" role="form" id="formTransaksiLain" name="formTransaksiLain">                       
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Tanggal Transaksi</label>
                                    <div class="col-xs-2">
                                       <?php echo $month_id;?> 
                                    </div>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" id="" name="" placeholder="Periode Gaji" data-provide="typeahead"/>                                                          
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3"></label>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen1" value="1" class=""/> Jumlah Jam                                    
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen0" value="0" class=""/> Real
                                </div>                           
                            </div>
                            <hr  />                            
                            <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-8 col-md-3">Pilih Transaksi</label>
                                <div class="col-xs-2">
                                    <input class="form-control" type="text" id="m_p_description_transaction_code" name="m_p_description_transaction_code" placeholder="kode transaksi" data-provide="typeahead" disabled="disabled"/>
                                    <input type='hidden' class="form-control" name="m_p_description_transaction_id" id="m_p_description_transaction_id" placeholder="id" value="0"/>
                                </div>
                                <div class="col-xs-4">
                                    <input class="form-control" type="text" id="m_p_description_transaction_name" name="m_p_description_transaction_name" placeholder="Cari Transaksi" data-provide="typeahead" />                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-8 col-md-3">Pilih Karyawan</label>
                                <div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
                                <div class="col-xs-4">
                                     <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Cari nama karyawan" data-provide="typeahead" />                  
                                </div>                              
                            </div>
                            <div class="form-group">
                                <label for="tarif_client" class="control-label col-xs-8 col-md-3">Nilai</label>
                                <div class="col-xs-3">
                                    <input class="form-control" type="text" id="" name="" placeholder="" data-provide="typeahead" />                   
                                </div>                              
                            </div>
                            
                            <hr  />
                           <div class="form-group">                                            
                                <label for="file" class="control-label col-xs-3 ">Default Directory</label>
                                <div class="col-xs-8">
                                    <input type="file" class="filestyle" data-placeholder="No file">
                                </div>
                           </div>
                            <hr  />
                           <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3"></label>
                                <div class="col-xs-8 col-md-8">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" id="">Proses Data</button>
                                        
                                    </div>
                                </div>
                            </div>                            
                    </form> 
                    <hr  />                 

                    <div class="table-responsive">
                        
                    </br>
                        <div class="panel-heading" style="background-color:#FFF">                           
                        </div>
                        <table class="table table-bordered" id="tbl_transaksi_lain">
                                <thead>
                                <tr class="info"><th>Tanggal Transaksi</th><th>Periode</th><th>Hari</th><th>Type</th><th>Mulai</th><th>Selesai</th><th>Break</th><th>Jumlah Jam</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <!-- data overtime -->
                                </tbody>
                        </table>                        
                    </div>
                </div>  
                <div class="panel-footer center-block">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default" href="#" id="first">
                                        <i class="fa fa-fast-backward fa-fw"></i> 
                                        <span>First</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="prev" >
                                        <i class="fa fa-backward fa-fw"></i> 
                                        <span>Prev</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="next" >
                                        <i class="fa fa-forward fa-fw"></i> 
                                        <span>Next</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="last" >
                                        <i class="fa fa-fast-forward fa-fw"></i> 
                                        <span>Last</span>
                                    </a>
                                    <a class="btn btn-default" href="#" >
                                        <span id="textpage"></span>
                                    </a>
                                </div>
                            </div>
                            
                    </div>      
        </div>
    </div>
</div>


<script>
$(function()
{
    var timeout;

    var curpage = 1;
    var totpage = 1;

    function loadTransaksiLain(page)
        {
            if(typeof page === 'undefined')
            {
                page = 1;
                curpage = 1;
                totpage = 1;
            }           
            
            var par = new Array();
            par["page"] = page;

            q = $("#formSearch").serializeArray();

            $.each(q, function(k,v){
                par[v.name] = v.value;
            });

            par = $.extend({}, par);

            $.ajax({
                           type: "GET",
                           url: "<?php echo base_url('index.php/overtime_detail/getList/'); ?>",
                           data: par, // serializes the form's elements.
                           success: function(data)
                           {
                               
                               data = jQuery.parseJSON(data);
                               if(data.success) 
                               {
                                    $("#tbl_transaksi_lain > tbody").empty();
                                    $('#textpage').html("Page "+page+" of "+data.totpage);
                                        if(data.totpage == 0)
                                        {
                                            $("#tbl_transaksi_lain > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");   
                                        }
                                    curpage = page;
                                    totpage = data.totpage;
                                    $.each(data.data, function(k,v){
                                        var html = "<tr><td><input type=hidden value='"+v.gp_p_overtime_detail_id+"'>"+v.family_name+"</td>";
                                            html += "<td>"+v.ref_relation_id+"</td>";
                                            html += "<td>"+v.m_birth_place_id+","+v.birth_date+"</td>";
                                            html += "<td>"+v.ref_religion_id+"</td>";
                                            html += "<td><div class='btn-group'>";
                                            html += "<button class='btn btn-small btn-danger' id=\"deleteovertime_detail\" onclick=\"$('#act').val('delete'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                                            html += "<button class='btn btn-small btn-info' id=\"editovertime_detail\" onclick=\"$('#act').val('edit'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                                            html += "</div>";
                                            html += "</td></tr>";
                                        $("#tbl_transaksi_lain").append(html);                                            
                                    });
                               }
                           }
                         });                                        
        }

    loadTransaksiLain(); 

    $("#next").click(function(){
        var next = curpage+1;
        if(next <= totpage) 
        {       
            loadTransaksiLain(next);
        }
    });

    $("#prev").click(function(){
        var prev = curpage-1;
        if(prev >= 1)
        {
            loadTransaksiLain(prev);
        }
    });
    
    $("#first").click(function(){
        var next = 1;
            loadTransaksiLain(next);
    });

    $("#last").click(function(){
        var next = totpage;
            loadTransaksiLain(next);
    });
    
    $("a").click(function(e){
            e.preventDefault();
    }); 

   
    $("#alert").hide();

    $("#save").click(function(){
            $("#formTransaksiLain").ajaxForm({
                    type: 'POST',
                    url: "<?php echo base_url("index.php/overtime_detail/save"); ?>",
                    data: $(this).serialize(),
                    success: function(data)
                    {
                       data = jQuery.parseJSON(data);
                       if(data.success) 
                       {
                            $("#alert").show();
                                loadTransaksiLain();
                                $("#formTransaksiLain").trigger("reset");
                                $("#gp_p_overtime_detail_id").val(0);                          
                       }
                        else
                        {
                            alert(data.message);    
                        }
                    }
            });
    });


    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tat_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });


    $('#overtime_componen').change(function() {

        if ( $(this).find("option:selected").text() == 1)
        {
         // $('#gp_personal_name').show();
         $("#overtime").attr("style", "display:block");
        }
          else
        {
              //hide
          //$('#gp_personal_name').hide();
          $("#overtime").attr("style", "display:none");
         
        }
            
    });

    $(":file").filestyle({placeholder: "No file"}); 


});
</script>

