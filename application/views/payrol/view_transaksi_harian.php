<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Transaksi Harian
* 
********************************************************************/  -->

<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
</style>

<div class="row">
    <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
        <div id="alert" class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Sukses!</strong> Data berhasil disimpan.
        </div>
        <div class="panel panel-default" style="margin-top:0px;">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"><b>Transaksi Harian</b></h3>                
            </div>          
                <div class="panel-body" style="padding:10px;">
                    <form class="form-horizontal" role="form" id="formTransaksiHarian" name="formTransaksiHarian">                       
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Tanggal Transaksi</label>
                                    <div class="col-xs-2">
                                        <?php echo $month_id;?>
                                    </div>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" id="" name="" placeholder="Periode Gaji" data-provide="typeahead"/>                                                          
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3"></label>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen1" value="1" class=""/> Jumlah Jam                                    
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen0" value="0" class=""/> Real
                                </div>                           
                            </div>
                            <div class="form-group">
                            <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
                                <div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Cari Nama Karyawan" data-provide="typeahead" />
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="id" value="0"/>                                                   
                                </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Client</label>                                    
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Kode - Nama Client" data-provide="typeahead" disabled="disabled"/>                                                            
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Jabatan</label>
                                <div class="col-xs-8">
                                    <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="Grade - Type - Jabatan" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                <div class="col-xs-8">                                                
                                    <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="Lokasi Kerja" disabled="disabled"/>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Periode</label>
                                <div class="col-xs-3">
                                    <div class='input-group date ' id='tmt_date'>
                                        <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label for="overtime_detail" class="control-label col-xs-1 ">s/d</label>
                                <div class="col-xs-3">
                                    <div class='input-group date ' id='tat_date'>
                                        <input type='text' class="form-control" id="tat_date" name="tat_date"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="btn-group ">
                                        <button class="btn btn-primary" id="">List Data</button>                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3"></label>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen1" value="1" class=""/> Singel                                 
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen0" value="0" class=""/> Multi
                                </div>                           
                            </div>
                            <div class="col-xs-4" >                                    
                                    <div class="col-xs-14" id="overtime" style="display: none;">
                                        <input class="form-control" type="text" id="gp_personal_name" name="gp_personal_name" placeholder="cari nama nrk / karyawan" data-provide="typeahead" />  
                                        <input class="form-control" type="hidden" id="nrk"  placeholder="cari nama nrk / karyawan" data-provide="typeahead" />
                                    </div>  
                            </div>
                            <div class="form-group" id="overtime" style="display: none;">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Tipe</label>
                                    <div class="col-xs-6 ">
                                        <?php echo $overtime_type; ?>
                                    </div>                                    
                            </div>                            
                            <div class="form-group" id="overtime" style="display: none;">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Jam Mulai</label>
                                <div class="col-xs-3">                                                
                                    <input class="form-control" type="text" id="start_hour" name="start_hour" placeholder="Jam Mulai"/>
                                    
                                </div>
                            </div>
                            <div class="form-group" id="overtime" style="display: none;">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Jam Selesai</label>
                                <div class="col-xs-3">                                                
                                    <input class="form-control" type="text" id="end_hour" name="end_hour" placeholder="Jam Selesai"/>
                                    
                                </div>
                            </div>
                            <div class="form-group" id="overtime" style="display: none;">
                              <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Break</label>                  
                               <div class="col-xs-3">
                                     <div class="input-group">
                                        <input class="form-control" type="text" id="height" name="height" placeholder="">
                                        <span class="input-group-addon" id="basic-addon1">menit</span>
                                     </div>
                                </div>
                            </div>                            
                            <div class="form-group" id="overtime" style="display: none;">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Jumlah Lembur</label>
                                <div class="col-xs-3">                                                
                                    <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder=""/>
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3"></label>
                                <div class="col-xs-8 col-md-11">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" id="">Proses Data</button>
                                        
                                    </div>
                                </div>
                            </div>                            
                    </form> 
                    <hr  />                 

                    <div class="table-responsive">
                        
                    </br>
                        <div class="panel-heading" style="background-color:#FFF">                           
                        </div>
                        <table class="table table-bordered" id="tbl_transaksi_harian">
                                <thead>
                                <tr class="info"><th>Tanggal Transaksi</th><th>Periode</th><th>Hari</th><th>Type</th><th>Mulai</th><th>Selesai</th><th>Break</th><th>Jumlah Jam</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <!-- data overtime -->
                                </tbody>
                        </table>                        
                    </div>
                </div>  
                <div class="panel-footer center-block">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default" href="#" id="first">
                                        <i class="fa fa-fast-backward fa-fw"></i> 
                                        <span>First</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="prev" >
                                        <i class="fa fa-backward fa-fw"></i> 
                                        <span>Prev</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="next" >
                                        <i class="fa fa-forward fa-fw"></i> 
                                        <span>Next</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="last" >
                                        <i class="fa fa-fast-forward fa-fw"></i> 
                                        <span>Last</span>
                                    </a>
                                    <a class="btn btn-default" href="#" >
                                        <span id="textpage"></span>
                                    </a>
                                </div>
                            </div>
                            
                    </div>      
        </div>
    </div>
</div>


<script>
$(function()
{
    var timeout;

    var curpage = 1;
    var totpage = 1;

    function loadTransaksiHarian(page)
        {
            if(typeof page === 'undefined')
            {
                page = 1;
                curpage = 1;
                totpage = 1;
            }           
            
            var par = new Array();
            par["page"] = page;

            q = $("#formSearch").serializeArray();

            $.each(q, function(k,v){
                par[v.name] = v.value;
            });

            par = $.extend({}, par);

            $.ajax({
                           type: "GET",
                           url: "<?php echo base_url('index.php/transaksi_harian/getList/'); ?>",
                           data: par, // serializes the form's elements.
                           success: function(data)
                           {
                               
                               data = jQuery.parseJSON(data);
                               if(data.success) 
                               {
                                    $("#tbl_transaksi_harian > tbody").empty();
                                    $('#textpage').html("Page "+page+" of "+data.totpage);
                                        if(data.totpage == 0)
                                        {
                                            $("#tbl_transaksi_harian > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");   
                                        }
                                    curpage = page;
                                    totpage = data.totpage;
                                    $.each(data.data, function(k,v){
                                        var html = "<tr><td><input type=hidden value='"+v.gp_p_overtime_detail_id+"'>"+v.family_name+"</td>";
                                            html += "<td>"+v.ref_relation_id+"</td>";
                                            html += "<td>"+v.m_birth_place_id+","+v.birth_date+"</td>";
                                            html += "<td>"+v.ref_religion_id+"</td>";
                                            html += "<td><div class='btn-group'>";
                                            html += "<button class='btn btn-small btn-danger' id=\"deleteovertime_detail\" onclick=\"$('#act').val('delete'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                                            html += "<button class='btn btn-small btn-info' id=\"editovertime_detail\" onclick=\"$('#act').val('edit'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                                            html += "</div>";
                                            html += "</td></tr>";
                                        $("#tbl_transaksi_harian").append(html);                                            
                                    });
                               }
                           }
                         });                                        
        }

    loadTransaksiHarian(); 

    $("#next").click(function(){
        var next = curpage+1;
        if(next <= totpage) 
        {       
            loadTransaksiHarian(next);
        }
    });

    $("#prev").click(function(){
        var prev = curpage-1;
        if(prev >= 1)
        {
            loadTransaksiHarian(prev);
        }
    });
    
    $("#first").click(function(){
        var next = 1;
            loadTransaksiHarian(next);
    });

    $("#last").click(function(){
        var next = totpage;
            loadTransaksiHarian(next);
    });
    
    $("a").click(function(e){
            e.preventDefault();
    }); 

   
    $("#alert").hide();

    $("#save").click(function(){
            $("#formTransaksiHarian").ajaxForm({
                    type: 'POST',
                    url: "<?php echo base_url("index.php/transaksi_harian/save"); ?>",
                    data: $(this).serialize(),
                    success: function(data)
                    {
                       data = jQuery.parseJSON(data);
                       if(data.success) 
                       {
                            $("#alert").show();
                                loadTransaksiHarian();
                                $("#formTransaksiHarian").trigger("reset");
                                $("#gp_p_overtime_detail_id").val(0);                          
                       }
                        else
                        {
                            alert(data.message);    
                        }
                    }
            });
    });


    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tat_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });


    $('#overtime_componen').change(function() {

        if ( $(this).find("option:selected").text() == 1)
        {
         // $('#gp_personal_name').show();
         $("#overtime").attr("style", "display:block");
        }
          else
        {
              //hide
          //$('#gp_personal_name').hide();
          $("#overtime").attr("style", "display:none");
         
        }
            
    });

    


});
</script>

