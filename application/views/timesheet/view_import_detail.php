<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Import Overtime Detail
* 
********************************************************************/  -->
<div class="row">
	<div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
	    <div id="alert" class="alert alert-success alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Sukses!</strong> Data berhasil disimpan.
		</div>
	    <div class="panel panel-default" style="margin-top:0px;">
			<div class="panel-heading clearfix">
	            <h3 class="panel-title pull-left"><b>Import Overtime Detail</b></h3>	            
			</div>			
				<div class="panel-body" style="padding:10px;">
					<form class="form-horizontal" role="form" id="formImportDetail" name="formImportDetail">			    		
			    		<div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Tanggal Transaksi</label>
                                    <div class="col-xs-2">
                                       <?php echo $month_id;?>
                                    </div>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Periode Gaji" data-provide="typeahead"/>                                                          
                                    </div>                              
                        </div>
						<div class="form-group">                                            
		                    <label for="file" class="control-label col-xs-3 ">Default Directory</label>
							<div class="col-xs-8">
	                            <input type="file" class="filestyle" data-placeholder="No file">
			            	</div>
			            </div>
						<div class="form-group">
							<label for="" class="control-label col-xs-8 col-md-3"></label>
							<div class="col-xs-8 col-md-8">
								<div class="btn-group pull-right">
									<button class="btn btn-primary" id="">Proses Data</button>
									<!-- <a class="btn btn-danger" id="reset">Reset</a> -->
								</div>
							</div>
						</div>
					</form>	
					<hr  />	
					<div class="table-responsive">
						
						<br />
						<div class="panel-heading" style="background-color:#FFF">							
						</div>
						<table class="table table-bordered" id="tbl_import_detail">
								<thead>
								<tr class="info"><th>Tanggal</th><th>NRK</th><th>Nama Karyawan</th><th>Tanggal Proses</th><th>Type</th><th>Mulai</th><th>Selesai</th><th>Break</th><th>Action</th></tr>
								</thead>
								<tbody>
								<!-- data Daftar import_detail Formal -->
						    </tbody>
						</table>						
						</div>
				</div>	
				<div class="panel-footer center-block">
							<div class="btn-group" role="group" aria-label="...">
								<div class="btn-group" role="group">
									<a class="btn btn-default" href="#" id="first">
										<i class="fa fa-fast-backward fa-fw"></i> 
										<span>First</span>
									</a>
									<a class="btn btn-default" href="#" id="prev" >
										<i class="fa fa-backward fa-fw"></i> 
										<span>Prev</span>
									</a>
									<a class="btn btn-default" href="#" id="next" >
										<i class="fa fa-forward fa-fw"></i> 
										<span>Next</span>
									</a>
									<a class="btn btn-default" href="#" id="last" >
										<i class="fa fa-fast-forward fa-fw"></i> 
										<span>Last</span>
									</a>
									<a class="btn btn-default" href="#" >
										<span id="textpage"></span>
									</a>
								</div>
							</div>
							<div class="btn-group pull-right" id="btnkaryawan">				
				                <button type="button" class="btn btn-primary" id="listkaryawanbutton" onClick="$('#content').load('<?php echo base_url('index.php/personal/getPanel/'); ?>');">
				                  List Karyawan
				                </button>
			            	</div>
					</div>		
		</div>
	</div>
</div>


<script>
$(function()
{
	var timeout;

	var curpage = 1;
	var totpage = 1;

		function loadImportDetail(page)
		{
			if(typeof page === 'undefined')
			{
				page = 1;
				curpage = 1;
				totpage = 1;
			}			
			
			var par = new Array();
			par["page"] = page;

			q = $("#formSearch").serializeArray();

			$.each(q, function(k,v){
				par[v.name] = v.value;
			});

			par = $.extend({}, par);

			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/import_detail/getList/'); ?>",
						   data: par, // serializes the form's elements.
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {
									$("#tbl_import_detail > tbody").empty();
									$('#textpage').html("Page "+page+" of "+data.totpage);
										if(data.totpage == 0)
										{
											$("#tbl_import_detail > tbody").html("<tr><td colspan = '9' style='color:red'><center> Data tidak ditemukan </center></td></tr>");	
										}
									curpage = page;
									totpage = data.totpage;
									$.each(data.data, function(k,v){
										var html = "<tr><td><input type=hidden value='"+v.gp_education_id+"'>"+v.ref_institution_type+"</td>";
											html += "<td>"+v.institution_name+"</td>";
											html += "<td>"+v.major+"</td>";
											html += "<td>"+v.m_city_id+"</td>";
											html += "<td>"+v.graduated+"</td>";
											html += "<td>"+v.gpa+"</td>";
											html += "<td><div class='btn-group'>";
											html += "<button class='btn btn-small btn-danger' id=\"deleteimport_detail\" onclick=\"$('#act').val('delete'); $('#gp_education_id').val("+v.gp_education_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
											html += "<button class='btn btn-small btn-info' id=\"editimport_detail\" onclick=\"$('#act').val('edit'); $('#gp_education_id').val("+v.gp_education_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
											html += "</div>";
											html += "</td></tr>";
										$("#tbl_import_detail").append(html);				   							
									});
							   }
						   }
						 });										
		}

	loadImportDetail();	

	$("#next").click(function(){
		var next = curpage+1;
		if(next <= totpage) 
		{		
			loadImportDetail(next);
		}
	});

	$("#prev").click(function(){
		var prev = curpage-1;
		if(prev >= 1)
		{
			loadImportDetail(prev);
		}
	});
	
	$("#first").click(function(){
		var next = 1;
			loadImportDetail(next);
	});

	$("#last").click(function(){
		var next = totpage;
			loadImportDetail(next);
	});
	
	$("a").click(function(e){
			e.preventDefault();
	});	

    

    $('#m_city_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/import_detail/getCity/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_city_name').change(function() {
		var current = $('#m_city_name').typeahead("getActive");
		try{
			$('#m_city_id').val(current.id);
		}catch(e){
			alert("Pilih kota dari List yang Ada");
		}
    });
	
	
	$('#m_city_name').blur(function(){
			$('#m_city_name').trigger("change");
	});	

	
	$('#m_country_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function() {
				return $.get('<?php echo base_url('index.php/import_detail/getCountry/'); ?>/'+query, function (data) {
				   data = jQuery.parseJSON(data);
				   return process(data.s);
				});
            }, 500);
        },
		autoSelect : true,
		minLength : 1,
		delay : 3
    });

	$('#m_country_name').change(function() {
		var current = $('#m_country_name').typeahead("getActive");
		try{
			$('#m_country_id').val(current.id);
		}catch(e){
			alert("Pilih negara dari List yang Ada");
		}
    });
	
	
	$('#m_country_name').blur(function(){
			$('#m_country_name').trigger("change");
	});	

	

	$("#gp_education_id").change(function(){
			var act = $("#act").val();
			var id = $(this).val();
			
			if(act=="edit")
			{
			$.ajax({
						   type: "GET",
						   url: "<?php echo base_url('index.php/import_detail/get/'); ?>/"+id,
						   success: function(data)
						   {
							   
							   data = jQuery.parseJSON(data);
							   if(data.success) 
							   {	
									$.each(data.data, function(k,v){
											$("#"+k.toLowerCase()).val(v);	
									});
							   }
						   }
						 });	
			}
			else
			if(act=="delete")
			{
				bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

					if (result == true) {
					$.when(deleteRow("<?php echo base_url('index.php/import_detail/delete/'); ?>", id)).done(function(rs){
						if(rs)
						{
							//alert("Data Dihapus");
							loadImportDetail();
						}
					});
	        	}
				}); 
				// var result = window.confirm('data akan dihapus?');
    // 	        if (result == true) {
				// 	$.when(deleteRow("<?php echo base_url('index.php/cabang/delete/'); ?>", id)).done(function(rs){
				// 		if(rs)
				// 		{
				// 			//alert("Data Dihapus");
				// 			loadCabang();
				// 		}
				// 	});
	   //      	}
 			}
	});

	$("#reset").click(function(){
		$("#formImportDetail").trigger("reset");
		$("#gp_education_id").val(0);
	});

	$("#alert").hide();

	$("#save").click(function(){
			$("#formImportDetail").ajaxForm({
					type: 'POST',
					url: "<?php echo base_url("index.php/import_detail/save"); ?>",
					data: $(this).serialize(),
					success: function(data)
					{
					   data = jQuery.parseJSON(data);
					   if(data.success) 
					   {
							$("#alert").show();
								loadImportDetail();
								$("#formImportDetail").trigger("reset");
								$("#gp_education_id").val(0);

							
					   }
						else
						{
							alert(data.message);	
						}
					}
			});
	});


	

	$(":file").filestyle({placeholder: "No file"}); 
    


});
</script>