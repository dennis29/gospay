<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 02, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Overtime Manual Periode Karyawan
* 
********************************************************************/  -->

<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
</style>

<div class="row">
    <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
        <div id="alert" class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Sukses!</strong> Data berhasil disimpan.
        </div>
        <div class="panel panel-default" style="margin-top:0px;">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"><b>Overtime Manual Periode</b></h3>                
            </div>          
                <div class="panel-body" style="padding:10px;">
                    <form class="form-horizontal" role="form" id="formOvertimePeriode" name="formOvertimePeriode">                       
                            <div class="form-group">
                                <label for="overtime_periode" class="control-label col-xs-8 col-md-3">Tanggal Transaksi</label>
                                    <div class="col-xs-2">
                                        <?php echo $month_id;?>
                                    </div>
                                    <div class="col-xs-3">
                                        <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Periode Gaji" data-provide="typeahead"/>                                                          
                                    </div>                              
                            </div>                            
                            <div class="form-group">
                            <label for="Mutasi" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
                                <div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
                                <div class="col-xs-6">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Cari Nama Karyawan" data-provide="typeahead" />
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="id" value="0"/>                                                   
                                </div>                              
                            </div>
                            <div class="form-group">
                                <label for="Mutasi" class="control-label col-xs-8 col-md-3">Client</label>                                    
                                    <div class="col-xs-8">
                                        <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Kode - Nama Client" data-provide="typeahead" disabled="disabled"/>                                                            
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="jabatan" class="control-label col-xs-3 ">Jabatan</label>
                                <div class="col-xs-8">
                                    <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="Grade - Type - Jabatan" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="m_location_id" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                <div class="col-xs-8">                                                
                                    <input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder="Lokasi Kerja" disabled="disabled"/>
                                    
                                </div>
                            </div>                            
                                <label for="overtime_periode" class="control-label col-xs-8 col-md-3"></label>
                                <div class="col-xs-8 col-md-8">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" id="">Proses Data</button>
                                        
                                    </div>
                                </div>
                            </div>                            
                    </form> 
                    <hr  />                 

                    <div class="table-responsive">
                        
                    </br>
                        <div class="panel-heading" style="background-color:#FFF">                           
                        </div>
                        <table class="table table-bordered" id="tbl_overtime_periode">
                                <thead>
                                <tr class="info"><th>Tanggal Proses</th><th>Type</th><th>Mulai</th><th>Selesai</th><th>Break</th><th>Jumlah Jam</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <!-- data overtime -->
                                </tbody>
                        </table>                        
                    </div>
                </div>  
                <div class="panel-footer center-block">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default" href="#" id="first">
                                        <i class="fa fa-fast-backward fa-fw"></i> 
                                        <span>First</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="prev" >
                                        <i class="fa fa-backward fa-fw"></i> 
                                        <span>Prev</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="next" >
                                        <i class="fa fa-forward fa-fw"></i> 
                                        <span>Next</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="last" >
                                        <i class="fa fa-fast-forward fa-fw"></i> 
                                        <span>Last</span>
                                    </a>
                                    <a class="btn btn-default" href="#" >
                                        <span id="textpage"></span>
                                    </a>
                                </div>
                            </div>
                            
                    </div>      
        </div>
    </div>
</div>


<script>
$(function()
{
    var timeout;

    var curpage = 1;
    var totpage = 1;

    function loadOvertimePeriode(page)
        {
            if(typeof page === 'undefined')
            {
                page = 1;
                curpage = 1;
                totpage = 1;
            }           
            
            var par = new Array();
            par["page"] = page;

            q = $("#formSearch").serializeArray();

            $.each(q, function(k,v){
                par[v.name] = v.value;
            });

            par = $.extend({}, par);

            $.ajax({
                           type: "GET",
                           url: "<?php echo base_url('index.php/overtime_periode/getList/'); ?>",
                           data: par, // serializes the form's elements.
                           success: function(data)
                           {
                               
                               data = jQuery.parseJSON(data);
                               if(data.success) 
                               {
                                    $("#tbl_overtime_periode > tbody").empty();
                                    $('#textpage').html("Page "+page+" of "+data.totpage);
                                        if(data.totpage == 0)
                                        {
                                            $("#tbl_overtime_periode > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");   
                                        }
                                    curpage = page;
                                    totpage = data.totpage;
                                    $.each(data.data, function(k,v){
                                        var html = "<tr><td><input type=hidden value='"+v.m_p_overtime_periode_id+"'>"+v.family_name+"</td>";
                                            html += "<td>"+v.ref_relation_id+"</td>";
                                            html += "<td>"+v.m_birth_place_id+","+v.birth_date+"</td>";
                                            html += "<td>"+v.ref_religion_id+"</td>";
                                            html += "<td><div class='btn-group'>";
                                            html += "<button class='btn btn-small btn-danger' id=\"deleteovertime_periode\" onclick=\"$('#act').val('delete'); $('#m_p_overtime_periode_id').val("+v.m_p_overtime_periode_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                                            html += "<button class='btn btn-small btn-info' id=\"editovertime_periode\" onclick=\"$('#act').val('edit'); $('#m_p_overtime_periode_id').val("+v.m_p_overtime_periode_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                                            html += "</div>";
                                            html += "</td></tr>";
                                        $("#tbl_overtime_periode").append(html);                                            
                                    });
                               }
                           }
                         });                                        
        }

    loadOvertimePeriode(); 

    $("#next").click(function(){
        var next = curpage+1;
        if(next <= totpage) 
        {       
            loadOvertimePeriode(next);
        }
    });

    $("#prev").click(function(){
        var prev = curpage-1;
        if(prev >= 1)
        {
            loadOvertimePeriode(prev);
        }
    });
    
    $("#first").click(function(){
        var next = 1;
            loadOvertimePeriode(next);
    });

    $("#last").click(function(){
        var next = totpage;
            loadOvertimePeriode(next);
    });
    
    $("a").click(function(e){
            e.preventDefault();
    }); 

    

    
    
    $("#m_p_overtime_periode_id").change(function(){
            var act = $("#act").val();
            var id = $(this).val();
            
            if(act=="edit")
            {
            $.ajax({
                           type: "GET",
                           url: "<?php echo base_url('index.php/overtime_periode/get/'); ?>/"+id,
                           success: function(data)
                           {
                               
                               data = jQuery.parseJSON(data);
                               if(data.success) 
                               {    
                                    $.each(data.data, function(k,v){
                                            $("#"+k.toLowerCase()).val(v);  
                                    });
                               }
                           }
                         });    
            }
            else
            if(act=="delete")
            {
                bootbox.confirm("Anda yakin data akan dihapus?", function(result) {

                    if (result == true) {
                    $.when(deleteRow("<?php echo base_url('index.php/overtime_periode/delete/'); ?>", id)).done(function(rs){
                        if(rs)
                        {
                            //alert("Data Dihapus");
                            loadOvertimePeriode();
                        }
                    });
                }
                }); 
                
            }
    });

    $("#reset").click(function(){
        $("#formOvertimePeriode").trigger("reset");
        $("#m_p_overtime_periode_id").val(0);
    });

    $("#alert").hide();

    $("#save").click(function(){
            $("#formOvertimePeriode").ajaxForm({
                    type: 'POST',
                    url: "<?php echo base_url("index.php/overtime_periode/save"); ?>",
                    data: $(this).serialize(),
                    success: function(data)
                    {
                       data = jQuery.parseJSON(data);
                       if(data.success) 
                       {
                            $("#alert").show();
                                loadOvertimePeriode();
                                $("#formOvertimePeriode").trigger("reset");
                                $("#m_p_overtime_periode_id").val(0);                          
                       }
                        else
                        {
                            alert(data.message);    
                        }
                    }
            });
    });


    $('#gp_personal_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            //alert($('#m_client_id').val());
            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/overtime_periode/getPersonal/'); ?>/'+query+"/"+$('#m_client_id').val(), function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
    $('#gp_personal_name').change(function() {
        current = $('#gp_personal_name').typeahead("getActive");
        $('#gp_personal_id').val(current.id);
        //$('#m_client_code').val(current.code);
    });

    $('#gp_personal_name').blur(function() {
        if($('#gp_personal_id').val()==0)
        {
            // alert("Pilih ");
            $('#gp_personal_id').val(0);
        }
    }); 


});
</script>

