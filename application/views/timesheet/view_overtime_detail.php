<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 2, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Overtime Manual Detail Karyawan
* 
********************************************************************/  -->

<style>
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;

}
</style>

    <div class="col-sm-12 col-md-offset" id="form-detail" data-title="Contacts">
        <div id="alert" class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Sukses!</strong> Data berhasil disimpan.
        </div>
        <div class="panel panel-default" style="margin-top:0px;">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"><b>Overtime Manual Detail</b></h3>                
            </div>          
                <div class="panel-body" style="padding:10px;">
                    <form class="form-horizontal" role="form" id="formOvertimeDetail" name="formOvertimeDetail">  
					<div class="row">
					  <div class="col-xs-5 col-sm-9">
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-7 col-md-3">Tanggal Transaksi</label>
                                    <div class="col-xs-3">
                                        <?php echo $p_period_months_salary;?>
                                    </div>
                                    <div class="col-xs-4">
                                        <input class="form-control" type="text" id="periode_gaji" name="" placeholder="Periode Gaji" data-provide="typeahead"/>                                                          
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3"></label>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen1" value="1" class=""/> Jumlah Jam                                    
                                </div>
                                <div class="col-xs-2">
                                    <input type="radio" name="overtime_componen" id="overtime_componen0" value="0" class=""/> Real
                                </div>                           
                            </div>
                            <div class="form-group">
                            <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Nama Karyawan</label>
                                <div class="col-xs-2">
                                    <input class="form-control" type="text" id="nrk" name="nrk" placeholder="NRK" data-provide="typeahead" disabled="disabled"/>
                                </div>
                                <div class="col-xs-5">
                                    <input class="form-control" type="text" id="personal_name" name="personal_name" placeholder="Cari Nama Karyawan" data-provide="typeahead" />
                                    <input type='hidden' class="form-control" name="gp_personal_id" id="gp_personal_id" placeholder="id" value="0"/>                                                   
                                </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-8 col-md-3">Client</label>                                    
                                    <div class="col-xs-7">
                                        <input class="form-control" type="text" id="m_client_name" name="m_client_name" placeholder="Kode - Nama Client" data-provide="typeahead" disabled="disabled"/>                                                            
                                    </div>                              
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Jabatan</label>
                                <div class="col-xs-7">
                                    <input class="form-control" type="text" id="m_structure_position_name" name="m_structure_position_name" placeholder="Grade - Type - Jabatan" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Lokasi Kerja</label>
                                <div class="col-xs-7">                                                
                                    <input class="form-control" type="text" id="m_location_name" name="" placeholder="Lokasi Kerja" disabled="disabled"/>
                                    
                                </div>
                            </div>
							<div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-3 ">Periode</label>
                                <div class="col-xs-3">
                                    <div class='input-group date '>
                                        <input type='text' class="form-control" id="tmt_date" name="tmt_date"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <label for="overtime_detail" class="control-label col-xs-1 ">s/d</label>
                                <div class="col-xs-3">
                                    <div class='input-group date '>
                                        <input type='text' class="form-control" id="tat_date" name="tat_date"/>
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="btn-group ">
									<label for="overtime_detail" class="control-label col-xs-6 col-md-3"></label>
									<div class="col-xs-7 col-md-3">
                                        <button class="btn btn-primary" id="list_data">List Data</button>   
								    </div>
                                </div>
                            </div>
						</div>

						<div class="col-xs-1 col-sm-3">
						   <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-1"></label>
                                <div class="col-xs-3">
                                    <input type="radio" name="overtime_type" id="overtime_type1" value="1" class="rb"/> Singel                                 
                                </div>
                                <div class="col-xs-3">
                                    <input type="radio" name="overtime_type" id="overtime_type0" value="0" class="rb"/> Multi
                                </div>                           
                            </div>
                           <div class="form-group" id="overtime" >
								<div class="form-group" >
									<label for="overtime_detail" class="control-label col-xs-3 col-md-6">Tipe</label>
										<div class="col-xs-4 " >
											<?php echo $overtime_type; ?>
										</div>                                    
								
									<label for="overtime_detail" class="control-label col-xs-6">Jam Mulai</label>
									<div class="col-xs-3">                                                
										<input class="form-control" type="text" id="start_hour" name="start_hour" placeholder="Mulai"/>
										
									</div>
								
									<label for="overtime_detail" class="control-label col-xs-6 ">Jam Selesai</label>
									<div class="col-xs-3">                                                
										<input class="form-control" type="text" id="end_hour" name="end_hour" placeholder="Selesai"/>
										
									</div>
								
								  <label for="overtime_detail" class="control-label col-xs-8 col-md-6">Break</label>                  
								   <div class="col-xs-4">
										 <div class="input-group">
											<input class="form-control" type="text" id="height" name="height" placeholder="">
											<span class="input-group-addon" id="basic-addon1">menit</span>
										 </div>
									</div>
								
									<label for="overtime_detail" class="control-label col-xs-6 ">Jumlah Lembur</label>
									<div class="col-xs-3">                                                
										<input class="form-control" type="text" id="m_location_name" name="m_location_name" placeholder=""/>
										
									</div>
								</div>
							</div>  
						</div>
                            
                         <div class="form-group">
                                <label for="overtime_detail" class="control-label col-xs-6 col-md-3"></label>
                                <div class="col-xs-7 col-md-9">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" id="">Proses Data</button>
                                        
                                    </div>
                                </div>
                         </div>                            
                    </form> 
                    <hr  />                 

                    <div class="table-responsive">                      
                        <table class="table table-bordered" id="tbl_overtime_detail" style='font-size:13px'>
                                <thead>
                                <tr class="info"><th>Tanggal Transaksi</th><th>Periode</th><th>Hari</th><th>Type</th><th>Mulai</th><th>Selesai</th><th>Break</th><th>Jumlah Jam</th><th>Action</th></tr>
                                </thead>
                                <tbody>
                                <!-- data overtime -->
                                </tbody>
                        </table>                        
                    </div>

                </div>  
                <!--div class="panel-footer center-block">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-default" href="#" id="first">
                                        <i class="fa fa-fast-backward fa-fw"></i> 
                                        <span>First</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="prev" >
                                        <i class="fa fa-backward fa-fw"></i> 
                                        <span>Prev</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="next" >
                                        <i class="fa fa-forward fa-fw"></i> 
                                        <span>Next</span>
                                    </a>
                                    <a class="btn btn-default" href="#" id="last" >
                                        <i class="fa fa-fast-forward fa-fw"></i> 
                                        <span>Last</span>
                                    </a>
                                    <a class="btn btn-default" href="#" >
                                        <span id="textpage"></span>
                                    </a>
                                </div>
                            </div>                       
                </div-->      
        </div>
    </div>



<script>
$(function()
{
    var timeout;

    var curpage = 1;
    var totpage = 1;

    function loadOvertimeDetail(page)
        {
            if(typeof page === 'undefined')
            {
                page = 1;
                curpage = 1;
                totpage = 1;
            }           
            
            var par = new Array();
            par["page"] = page;

            q = $("#formSearch").serializeArray();

            $.each(q, function(k,v){
                par[v.name] = v.value;
            });

            par = $.extend({}, par);

            $.ajax({
                           type: "GET",
                           url: "<?php echo base_url('index.php/overtime_detail/getList/'); ?>",
                           data: par, // serializes the form's elements.
                           success: function(data)
                           {
                               
                               data = jQuery.parseJSON(data);
                               if(data.success) 
                               {
                                    $("#tbl_overtime_detail > tbody").empty();
                                    $('#textpage').html("Page "+page+" of "+data.totpage);
                                        if(data.totpage == 0)
                                        {
                                            $("#tbl_overtime_detail > tbody").html("<tr><td colspan = '7' style='color:red'><center> Data tidak ditemukan </center></td></tr>");   
                                        }
                                    curpage = page;
                                    totpage = data.totpage;
                                    $.each(data.data, function(k,v){
                                        var html = "<tr><td><input type=hidden value='"+v.gp_p_overtime_detail_id+"'>"+v.gp_p_overtime_detail_id+"</td>";
                                            html += "<td>"+v.gp_p_overtime_detail_id+"</td>";
                                            html += "<td>"+v.gp_p_overtime_detail_id+","+v.gp_p_overtime_detail_id+"</td>";
                                            html += "<td>"+v.gp_p_overtime_detail_id+"</td>";
                                            html += "<td><div class='btn-group'>";
                                            html += "<button class='btn btn-small btn-danger' id=\"deleteovertime_detail\" onclick=\"$('#act').val('delete'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                                            html += "<button class='btn btn-small btn-info' id=\"editovertime_detail\" onclick=\"$('#act').val('edit'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                                            html += "</div>";
                                            html += "</td></tr>";
                                        $("#tbl_overtime_detail").append(html);                                            
                                    });
                               }
                           }
                         });                                        
        }

    loadOvertimeDetail(); 

    $("#next").click(function(){
        var next = curpage+1;
        if(next <= totpage) 
        {       
            loadOvertimeDetail(next);
        }
    });

    $("#prev").click(function(){
        var prev = curpage-1;
        if(prev >= 1)
        {
            loadOvertimeDetail(prev);
        }
    });
    
    $("#first").click(function(){
        var next = 1;
            loadOvertimeDetail(next);
    });

    $("#last").click(function(){
        var next = totpage;
            loadOvertimeDetail(next);
    });
    
    $("a").click(function(e){
            e.preventDefault();
    }); 

   
    $("#alert").hide();

    $("#save").click(function(){
            $("#formOvertimeDetail").ajaxForm({
                    type: 'POST',
                    url: "<?php echo base_url("index.php/overtime_detail/save"); ?>",
                    data: $(this).serialize(),
                    success: function(data)
                    {
                       data = jQuery.parseJSON(data);
                       if(data.success) 
                       {
                            $("#alert").show();
                                loadOvertimeDetail();
                                $("#formOvertimeDetail").trigger("reset");
                                $("#gp_p_overtime_detail_id").val(0);                          
                       }
                        else
                        {
                            alert(data.message);    
                        }
                    }
            });
    });

	
	$("#list_data").click(function(){
		 var tmt = $("#tmt_date").val().split('-');
		 var tat = $("#tat_date").val().split('-');
		 var startDate = new Date(tmt[1]+'/'+tmt[0]+'/'+tmt[2]);
		 var endDate  = new Date(tat[1]+'/'+tat[0]+'/'+tat[2]);
	//	 var hasil = new array();
		 
		 hasil = getDates( startDate, endDate );
   $("#tbl_overtime_detail > tbody").empty();
		$.each(hasil, function(k,v){
		
		     bagi = v.split('/');	 
			 var hari = " ";
			 var tgl = bagi[0].split('-');
			 var tanggal = tgl[2]+'-'+tgl[1]+'-'+tgl[0];
			
			// cari hari libur di m_p_day_off
			var xData = $.xResponse("<?php echo base_url('index.php/overtime_detail/getDayOff/'); ?>/"+tanggal, {issession: 1,selector: true});

			if(xData != '') hari = xData;
			else hari = " Hari " + bagi[1];

              
             var html = "<tr><td><input type=hidden value='"+v+"'>"+$("#p_period_months_salary_id").find("option:selected").text()+"</td>";
                 html += "<td>"+bagi[0]+"</td>";
				 html += "<td>"+hari+"</td>";
                 html += "<td><div class='btn-group'>";
                 html += "<button class='btn btn-small btn-danger' id=\"deleteovertime_detail\" onclick=\"$('#act').val('delete'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-trash-o'></i></button>";
                 html += "<button class='btn btn-small btn-info' id=\"editovertime_detail\" onclick=\"$('#act').val('edit'); $('#gp_p_overtime_detail_id').val("+v.gp_p_overtime_detail_id+").trigger('change');\"><i class='fa fa-edit'></i></button>";
                 html += "</div>";
                 html += "</td></tr>";
              $("#tbl_overtime_detail").append(html);                                            
        });
			
      return false;
			
    });


$.extend({
    xResponse: function(url, data) {

        // local var
        var theResponse = null;

        // jQuery ajax

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            dataType: "html",
            async: false,
            success: function(respText) {
                theResponse = respText;
            }
        });
        return theResponse;
    }

});

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tmt_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });

    $(".a-prevent").click(function(e) { e.preventDefault(); });
    $('#tat_date').datetimepicker({
                format : "DD-MM-YYYY",
                defaultDate : new Date()
    });


    $('#overtime_type').change(function() {

        if ( $(this).find("option:selected").text() == 1)
        {
         // $('#gp_personal_name').show();
         $("#overtime").attr("style", "display:block");
        }
          else
        {
              //hide
          //$('#gp_personal_name').hide();
          $("#overtime").attr("style", "display:none");
         
        }
            
    });

    $('#personal_name').typeahead({
        source: function (query, process) {
            if (timeout) {
                clearTimeout(timeout);
            }
            //alert($('#m_client_id').val());
            timeout = setTimeout(function() {
                return $.get('<?php echo base_url('index.php/overtime_detail/getPersonal/'); ?>/'+query, function (data) {
                   data = jQuery.parseJSON(data);
                   return process(data.s);
                });
            }, 500);
        },
        autoSelect : false,
        minLength : 1,
        delay : 3
    });
    
    var current = 0;
	$('#personal_name').change(function() {
		current = $('#personal_name').typeahead("getActive");
		$('#gp_personal_id').val(current.id);
		$('#nrk').val(current.nrk);
		$('#m_unit_name').val(current.m_unit_name);
		$('#m_client_name').val(current.m_client_name);
		$('#m_structure_position_name').val(current.m_structure_position_name);
		$('#m_location_name').val(current.m_location_name);
		date=(current.join_date).split('-');
		$('#join_date').val(date[2]+"-"+date[1]+"-"+date[0]); 
		
    });

	$('#personal_name').blur(function() {
		if($('#gp_personal_id').val()==0)
		{
			// alert("Pilih ");
			$('#gp_personal_id').val(0);
		}
    });	

	$("#p_period_months_salary_id").change(function(){

        var id = $("#p_period_months_salary_id").val();

		$.ajax({
            type: "GET",
            url: "<?php echo base_url('index.php/overtime_detail/getMonthSalary/'); ?>/"+id,
            success: function(data)
			 {			   
				   data = jQuery.parseJSON(data);
				   if(data.success) 
				   {	
						$.each(data.data, function(k,v){
							$("#periode_gaji").val(v.description);
																
						});
				   }
			 }
		});

	});


	$('#overtime_type0').click(function() {
		$("#overtime").attr("style", "display:block");
	});
	$('#overtime_type1').click(function() {
		 $("#overtime").attr("style", "display:none");
	});


	function getDates( d1, d2 ){
		  var oneDay = 24*3600*1000;
		  var weekday=new Array(7);
			  weekday[0]="Minggu";
			  weekday[1]="Senin";
			  weekday[2]="Selasa";
			  weekday[3]="Rabu";
			  weekday[4]="Kamis";
			  weekday[5]="Jumat";
			  weekday[6]="Sabtu";
		  for (var d=[],ms=d1*1,last=d2*1;ms<=last;ms+=oneDay){
			  var tanggal = new Date(ms).toLocaleFormat("%d-%m-%Y");
		      var hari = weekday[new Date(ms).getDay()];

			  d.push(tanggal+"/"+hari); 

		  }
		  return d;
	}

});



</script>

