<!-- /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 24, 2015
* Development Group: GOS Payment (HRIS)
* Description: Tampilan Sidebar
* 
********************************************************************/  -->
    
    <!-- CSS untuk side bar -->

    <style type="text/css">
       
        #leftCol {
        position: fixed;
        width: 250px;        
        overflow-y: scroll;
        top: 30;
        bottom:10;
        }

        /*
        A Bootstrap 3.1 affix sidebar template
        from http://bootply.com

        This CSS code should follow the 'bootstrap.css'
        in your HTML file.

        license: MIT
        author: bootply.com
        */

        
    </style>
    <!-- Bootstrap Core CSS -->


    <div id="leftCol" class="navbar-default sidebar" role="navigation" style="position:fixed">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">                        
                        <li>
                            <a href="#"><!-- <i class="fa fa-wrench fa-fw"></i> --> Master Admin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="" id='tab-perusahaan' onclick="$('#content-master').load('<?php echo base_url('index.php/perusahaan/getPanel');?> ')">Perusahaan</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/cabang/getPanel');?> ')">Cabang</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/client/getPanel');?> ')">Client</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/unit_kerja/getPanel');?> ')">Unit Kerja</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/posisi/getPanel');?> ')">Tipe Jabatan</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/struktur_jabatan/getPanel');?> ')">Struktur Jabatan</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/lokasi/getPanel');?> ')">Lokasi Kerja</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/area/getPanel');?> ')">Wilayah dan Area</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/kriteria/getPanel');?> ')">Kriteria Kelompok</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/bank/getPanel');?> ')">Bank</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/cuti_bersama/getPanel');?> ')">Cuti Bersama</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/parameter_cuti/getPanel');?> ')">Parameter Cuti</a></li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><!-- <i class="fa fa-wrench fa-fw"></i> --> Master Payrol<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                                
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/deskripsi/getPanel');?> ')">Deskripsi Penggajian</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/tarif/getPanel');?> ')">Tarif</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/tarif_client/getPanel');?> ')">Tarif Client</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/pajak_reguler/getPanel');?> ')">Struktur Perhitungan Pajak</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/libur/getPanel');?> ')">Hari Libur</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/payroll_period/getPanel');?> ')">Periode Payroll Client</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/kelompok_transaksi/getPanel');?> ')">Kelompok Transaksi THP</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/overtime_parameter/getPanel');?> ')">Parameter Overtime</a></li>
                               <!--  <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/otorisasi/getPanel');?> ')">Otorisasi</a></li>
                                <li><a href="" onclick="$('#content-master').load('<?php echo base_url('index.php/proses_gaji/getPanel');?> ')">Proses Gaji</a></li> -->
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
     </div>
    <div class="col-md-12">
    <div class="mainPage" id="content-master" style="padding-top:30px">
    
    </div>
     </div>
    <script>

    $("a").click(function(e){
        e.preventDefault();
    });

    $("#tab-perusahaan").trigger('click');

    /* activate sidebar */
    $('#sidebar').affix({
      offset: {
        top: 235
      }
    });

    /* activate scrollspy menu */
    var $body   = $(document.body);
    var navHeight = $('.navbar').outerHeight(true) + 10;

    $body.scrollspy({
        target: '#leftCol',
        offset: navHeight
    });

    /* smooth scrolling sections */
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 50
            }, 1000);
            return false;
          }
        }
    });


    </script>
    
    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="<?php echo base_url('asset/bower_components/metisMenu/dist/metisMenu.min.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script>
        $(function() {

            $('#side-menu').metisMenu();

        });

        //Loads the correct sidebar on window load,
        //collapses the sidebar on window resize.
        // Sets the min-height of #page-wrapper to window size
        $(function() {
            $(window).bind("load resize", function() {
                topOffset = 50;
                width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
                if (width < 768) {
                    $('div.navbar-collapse').addClass('collapse');
                    topOffset = 100; // 2-row-menu
                } else {
                    $('div.navbar-collapse').removeClass('collapse');
                }

                height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
                height = height - topOffset;
                if (height < 1) height = 1;
                if (height > topOffset) {
                    $("#page-wrapper").css("min-height", (height) + "px");
                }
            });

            var url = window.location;
            // var element = $('ul.nav a').filter(function() {
            //     return this.href == url || url.href.indexOf(this.href) == 0;
            // }).addClass('active').parent().parent().addClass('in').parent();
            // if (element.is('li')) {
            //     element.addClass('active');
            // }
        });

    </script>