<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: October 12, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Timesheet
* 
********************************************************************/ 

class Payrol extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("view_sidebar_payrol");	
	}
	
}