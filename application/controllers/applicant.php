<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applicant extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-applicant",$data["akses"])) $data["addapplicant"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addapplicant"] = 0;
*/
		$this->load->model("Fj_personal", "pers");
		$this->load->view("applicantPanel",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Fj_personal", "pers");
		$this->pers->order_by("FJ_PERSONAL_ID", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("M_BRANCH_ID")!=1) // kalau bukan login dg akses All
		{
			$arrct["M_BRANCH_ID"] = $this->session->userdata("M_BRANCH_ID");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();
		if(is_array($data["list"]))
		{
			foreach($data["list"] as $k=>$r)
			{
				$this->load->model("M_sub_district", "ms");
				$rw = $this->ms->get($r->M_SUB_DISTRICT_ID);
				if($rw) $data["list"][$k]->M_SUB_DISTRICT_ID = $rw->NAME;	
	
				$this->load->model("M_city", "mc");
				$rw1 = $this->mc->get($r->M_CITY_ID);
				if($rw1) $data["list"][$k]->M_CITY_ID = $rw1->NAME;	
	
				$this->load->model("M_reference_list", "ml");
				$rw2 = $this->ml->get($r->GENDER_ID);
				if($rw2) $data["list"][$k]->GENDER_ID = $rw2->NAME;	
	
				$this->load->model("M_state", "mst");
				$rw3 = $this->mst->get($r->M_STATE_ID);
				if($rw3) $data["list"][$k]->M_STATE_ID = $rw3->NAME;	
	
				$this->load->model("Fj_education", "edu");
				$rw4 = $this->edu->get_by("FJ_PERSONAL_ID",($r->FJ_PERSONAL_ID));
				$education = $this->edu->count_by("FJ_PERSONAL_ID",($r->FJ_PERSONAL_ID));
				if($education>0)
				{

					$this->load->model("Fj_education", "edu");
					$rw4 = $this->edu->get_by("FJ_PERSONAL_ID",($r->FJ_PERSONAL_ID));
					if($rw4)
					{
						$data["list"][$k]->HIGHSCHOOL_INSTITUTION = $rw4->HIGHSCHOOL_INSTITUTION;	
						$data["list"][$k]->DIPLOMA_INSTITUTION = $rw4->DIPLOMA_INSTITUTION;	
						$data["list"][$k]->S1_INSTITUTION = $rw4->S1_INSTITUTION;	
						$data["list"][$k]->S1_GPA = $rw4->S1_GPA;	
						$data["list"][$k]->DIPLOMA_GPA = $rw4->DIPLOMA_GPA;	
					}
				}
				else
				{
					$data["list"][$k]->HIGHSCHOOL_INSTITUTION = 0;	
					$data["list"][$k]->DIPLOMA_INSTITUTION = 0;	
					$data["list"][$k]->S1_INSTITUTION = 0;	
					$data["list"][$k]->S1_GPA = 0;	
					$data["list"][$k]->DIPLOMA_GPA = 0;	
				}	
			}
			
		}
		else
		{
			
		}
		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->pers->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	
	function getApplicant($id)
	{
		$page = $this->input->get("page");
		$this->load->model("FJ_personal", "pers");
		$offset = ($page==1) ? 0 : $page*25;
		$this->pers->limit(25,$offset);
		$data["list"] = $this->pers->get($id);

		$this->load->model("M_sub_district", "ms");
		$data["list"]->M_SUB_DISTRICT_NAME = $this->ms->get($data["list"]->M_SUB_DISTRICT_ID)->NAME;
		$data["list"]->IDCARD_SUB_DISTRICT_NAME = $this->ms->get($data["list"]->IDCARD_SUB_DISTRICT_ID)->NAME;

		$this->load->model("M_district", "md");
		$data["list"]->M_DISTRICT_NAME = $this->md->get($data["list"]->M_DISTRICT_ID)->NAME;
		$data["list"]->IDCARD_DISTRICT_NAME = $this->md->get($data["list"]->IDCARD_DISTRICT_ID)->NAME;
		
		$this->load->model("M_city", "mc");
		$data["list"]->M_CITY_NAME = $this->mc->get($data["list"]->M_CITY_ID)->NAME;
		$data["list"]->IDCARD_CITY_NAME = $this->mc->get($data["list"]->IDCARD_CITY_ID)->NAME;
		$data["list"]->BIRTH_PLACE_NAME = $this->mc->get($data["list"]->BIRTH_PLACE)->NAME;

		$this->load->model("M_state", "mst");
		$data["list"]->M_STATE_NAME = $this->mst->get($data["list"]->M_STATE_ID)->NAME;
		$data["list"]->IDCARD_STATE_NAME = $this->mst->get($data["list"]->IDCARD_STATE_ID)->NAME;

		$this->load->model("Fj_history", "hist");
		$hired = $this->hist->get_by(array("FJ_JOB_ORDER_ID" => $data["list"]->FJ_JOB_ORDER_ID, "FJ_PERSONAL_ID" => $data["list"]->FJ_PERSONAL_ID));
//		print_r($data["list"]->FJ_JOB_ORDER_ID);
//		echo $this->db->last_query();
		if($hired) $data["list"]->STATUS_TRACKING_ID =$hired->STATUS_TRACKING_ID;
		else $data["list"]->STATUS_TRACKING_ID = 0;		
		

		$this->load->model("Fj_job_order", "jo");
		$nomor_jo = $this->jo->get($data["list"]->FJ_JOB_ORDER_ID);
		if($nomor_jo) $data["list"]->JO_NO =$nomor_jo->JO_NO;
		else $data["list"]->JO_NO = "";		

		if($nomor_jo) $data["list"]->FJ_JOB_ORDER_ID =$nomor_jo;
		else $data["list"]->FJ_JOB_ORDER_ID = 0;		

	
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}


	function getJobFunction()
	{
		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_reference_list", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->likes("NAME",$q)->dropdown("NAME");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $k;
			$arr[$i]["name"] = $v;
			$i++;
			
		}
	//	echo $this->db->last_query();
		echo json_encode($arr);
	}

	function getOversea($auto="")
	{
		$q = $this->input->get("term");
		$page = $this->input->get("page");
		$this->load->model("M_reference_list", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->likes_oversea("NAME",$q)->dropdown("NAME");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $k;
			if($auto=="") $arr[$i]["name"] = $v;
			else $arr[$i]["value"] = $v;
			$i++;
			
		}
	//	echo $this->db->last_query();
		echo json_encode($arr);
	}

	
	function save()
	{
		$arr = array();
		$vapplicant = $this->config->item("applicant");
		$cek = 0;
		$msg = "";
		foreach($this->input->post() as $k=>$v)
		{
			if(!in_array($k,array("file_photo_check","sameaddress","fj_personal_id","m_sub_district_name",
								  "m_district_name","m_city_name","m_state_name","idcard_sub_district_name",
								  "idcard_district_name","idcard_city_name","idcard_state_name","birth_place_name","file_photo")))
				$arr[strtoupper($k)] = $v;
			
			if((isset($vapplicant[$k]["required"])) && $vapplicant[$k]["required"]=="required")
			{
				if(in_array($v,array("0","","null","NULL")))
				{
					$cek = 1;
					$msg .= $vapplicant[$k]["name"]." Wajib Diisi\n";
				}
			}
		}

		if($cek==0)
		{
			$arr["M_BRANCH_ID"] = $this->session->userdata("M_BRANCH_ID");
			if($this->input->post("sameaddress")=="on")
			{
				$arr["M_DISTRICT_ID"] = $arr["IDCARD_DISTRICT_ID"];
				$arr["M_SUB_DISTRICT_ID"] = $arr["IDCARD_SUB_DISTRICT_ID"];
				$arr["M_CITY_ID"] = $arr["IDCARD_CITY_ID"];
				$arr["M_STATE_ID"] = $arr["IDCARD_STATE_ID"];
			}
			$this->load->model("Fj_personal", "pers");
			$filename = $arr["ID_CARD_NO"]."-".$arr["NAME"];
			$upload = array();
			$message = "";
//			print_r($_FILES["file_photo"]);				
			if(isset($_FILES["file_photo"]) && !empty($_FILES["file_photo"]) )
			{
				$upload = $this->do_upload("file_photo", $filename);
				if($upload["success"])
				{
					$arr["FILE_PHOTO"] = $upload["message"]["file_name"];			
					$message = "Upload Berhasil";	
				}
				else
				{
					$message = "Upload Gagal";	
					//$arr["FILE_PHOTO"] = $this->input->post("file_photo_check");			
				}
			}
			if($this->input->post("fj_personal_id")==0)
			{
				$rs = $this->pers->insert($arr);		
			}
			else
			{
				$rs = $this->pers->update($this->input->post("fj_personal_id"),$arr);
				$rs = $this->input->post("fj_personal_id");					
			}
			
			//$rs = $this->pers->insert($arr);		
			
			if($rs) echo json_encode(array("success" => true, "id" => $rs, "message" => $message."\nData Berhasil DIsimpan"));
			else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
		}
		else
		{
			echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan\n".$msg));			
		}
	}
	
	function do_upload($filename, $file_new)
	{
		$config['upload_path'] = './asset/uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['image_width']  = '1024';
		$config['image_height']  = '768';
		$config['file_name']  = $file_new;
		$config['overwrite']  = true;
		
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload($filename))
		{
			$error = array('error' => $this->upload->display_errors());
			return array("success" => false, "message" => $error);
		}
		else
		{
			$data = $this->upload->data();

			return array("success" => true, "message" => $data);
		}
	}
	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vapplicant"] = $this->config->item('applicant');
		$data["vfamily"] = $this->config->item('family');
		$data["form"] = $form;
		$data["fj_personal_id"] = $id;
		$data["gender_id"] = $this->getReference(array("id" => "gender_id",
													   "name" => "gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender
		$data["religion_id"] = $this->getReference(array("id" => "religion_id",
													   "name" => "religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReference(array("id" => "marital_status_id",
													   "name" => "marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 7));

		$data["blood_type_id"] = $this->getReference(array("id" => "blood_type_id",
													   "name" => "blood_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 8));

		$data["skin_color_id"] = $this->getReference(array("id" => "skin_color_id",
													   "name" => "skin_color_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 9));

		$data["available_id"] = $this->getReference(array("id" => "available_id",
													   "name" => "available_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "selected" => 23,
													   "refid" => 10));
		$data["father_education"] = $this->getReference(array("id" => "father_education",
													   "name" => "father_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["mother_education"] = $this->getReference(array("id" => "mother_education",
													   "name" => "mother_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_1_education"] = $this->getReference(array("id" => "child_1_education",
													   "name" => "child_1_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_2_education"] = $this->getReference(array("id" => "child_2_education",
													   "name" => "child_2_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_3_education"] = $this->getReference(array("id" => "child_3_education",
													   "name" => "child_3_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["spouse_education"] = $this->getReference(array("id" => "spouse_education",
													   "name" => "spouse_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["language_id"] = $this->getReference(array("id" => "language_id",
													   "name" => "language_id",
													   "class" => "form-control",
													   "refid" => 11)); // 11 = language
		$data["language_level_id"] = $this->getReference(array("id" => "language_level_id",
													   "name" => "language_level_id",
													   "class" => "form-control",
													   "refid" => 13)); // 13 = language level
		$data["skill_level_id"] = $this->getReference(array("id" => "skill_level_id",
													   "name" => "skill_level_id",
													   "class" => "form-control",
													   "refid" => 13)); // 13 = skill level
		$data["skill_id"] = $this->getReference(array("id" => "skill_id",
													   "name" => "skill_id",
													   "class" => "form-control",
													   "refid" => 17)); // 17 = skill
		$data["recso_id"] = $this->getReference(array("id" => "recso_id",
													   "name" => "recso_id",
													   "class" => "form-control",
													   "order_by" => "NAME",
													   "refid" => 15)); // 15 = recso
		$data["fj_source_media_id"] = $this->getReference(array("id" => "fj_source_media_id",
													   "name" => "fj_source_media_id",
													   "class" => "form-control",
													   "refid" => 16)); // 16 = source media
		$data["job_function_id"] = $this->getReference(array("id" => "job_function_id",
													   "name" => "job_function_id",
													   "class" => "form-control",
													   "refid" => 18)); // 16 = fungsi kerja
		$data["course_from_month"] = $this->getReference(array("id" => "course_from_month",
													   "name" => "course_from_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["course_to_month"] = $this->getReference(array("id" => "course_to_month",
													   "name" => "course_to_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["organization_from_month"] = $this->getReference(array("id" => "organization_from_month",
													   "name" => "organization_from_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["organization_to_month"] = $this->getReference(array("id" => "organization_to_month",
													   "name" => "organization_to_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["branch_id"] = $this->getCombo(array("name" => "branch_id",
													 "model" => "m_branch",
													 "id" => "branch_id",
													 //"selected" => $this->session->userdata("M_BRANCH_ID"),
													// "disabled" => "disabled",
													 "column" => "NAME",
													 "class" => "form-control"));
		$data["m_branch_id"] = $this->getCombo(array("name" => "m_branch_id",
													 "model" => "m_branch",
													 "id" => "m_branch_id",
													 "selected" => $this->session->userdata("M_BRANCH_ID"),
													 "disabled" => "disabled",
													 "column" => "NAME",
													 "class" => "form-control"));

/*		$this->load->model("M_reference_list", "ml");
		$ov = $this->ml->get_many_by("M_REFERENCE_ID",5);
		$i = 0;
		foreach($ov as $k=>$v)
		{
			$data["oversea_id"][$i]["id"] = $v->M_REFERENCE_LIST_ID;
			$data["oversea_id"][$i]["name"] = $v->NAME;
			$i++;		
		}
*/

		$this->load->model("M_reference_list", "ml");
		$pas = $this->ml->get_many_by("M_REFERENCE_ID",18);
		$i = 0;
		foreach($pas as $k=>$v)
		{
			$data["passion"][$i]["id"] = $v->M_REFERENCE_LIST_ID;
			$data["passion"][$i]["name"] = $v->NAME;
			$i++;		
		}
	//	echo $this->db->last_query();

		$this->load->view("detailAppPanel",$data);			
	}

	function getPersonal($id_card_no)
	{
		$education = "";
		$this->load->model("Fj_personal", "pers");
		$this->pers->limit(1);
		$data = $this->pers->get_by('ID_CARD_NO', $id_card_no);
		foreach($data as $k=>$r)
		{
			$this->load->model("Fj_education", "edu");
			$rw = $this->edu->get_by("FJ_PERSONAL_ID",($data->FJ_PERSONAL_ID));			
			if($rw)
			{
				$data->S1 = $rw->S1_INSTITUTION;	
				$data->DIPLOMA = $rw->DIPLOMA_INSTITUTION;	
				$data->SMA = $rw->HIGHSCHOOL_INSTITUTION;	
			}
			else
			{
				$data->S1 = "";	
				$data->DIPLOMA = "";	
				$data->SMA = "";
				$data->FJ_PERSONAL_ID=0;				
			}
		}
		if($data->S1 != "")
		{
			$education = "S1";
		}
		else		
		if($data->S1 == "" && $data->DIPLOMA != "" )
		{
			$education = "Diploma";
		}		
		else		
		if($data->S1 == "" && $data->DIPLOMA == "" && $data->SMA != "" )
		{
			$education = "SMA";
		}		
		else
		{
			$education = "";
		}
		
		echo json_encode(array("success" => true, "name" => $data->NAME,"fj_personal_id" => $data->FJ_PERSONAL_ID, "education" => $education));
	}

	function getForm()
	{
		//cek user session
		$m_user_id = $this->session->userdata("M_USER_ID");
		if(empty($m_user_id))
		{
			$this->load->view("login_applicant");	
		}
		else
		{
			$this->load->view("startForm");			
		}
//		$this->load->view("startForm");			
//		$this->load->view("header");			
//		$this->getDetail(0,"form");			
	}

	function getForm1($id=0,$form='')
	{
		$this->load->view("header");			
		$data["vapplicant"] = $this->config->item('applicant');
		$data["vfamily"] = $this->config->item('family');
		$data["form"] = $form;
		$data["fj_personal_id"] = $id;
		$data["gender_id"] = $this->getReference(array("id" => "gender_id",
													   "name" => "gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender
		$data["religion_id"] = $this->getReference(array("id" => "religion_id",
													   "name" => "religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReference(array("id" => "marital_status_id",
													   "name" => "marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 7));

		$data["blood_type_id"] = $this->getReference(array("id" => "blood_type_id",
													   "name" => "blood_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 8));

		$data["skin_color_id"] = $this->getReference(array("id" => "skin_color_id",
													   "name" => "skin_color_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 9));

		$data["available_id"] = $this->getReference(array("id" => "available_id",
													   "name" => "available_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "selected" => 23,
													   "refid" => 10));
		$data["father_education"] = $this->getReference(array("id" => "father_education",
													   "name" => "father_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["mother_education"] = $this->getReference(array("id" => "mother_education",
													   "name" => "mother_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_1_education"] = $this->getReference(array("id" => "child_1_education",
													   "name" => "child_1_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_2_education"] = $this->getReference(array("id" => "child_2_education",
													   "name" => "child_2_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["child_3_education"] = $this->getReference(array("id" => "child_3_education",
													   "name" => "child_3_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["spouse_education"] = $this->getReference(array("id" => "spouse_education",
													   "name" => "spouse_education",
													   "class" => "form-control",
													   "refid" => 30)); // 1 = gender
		$data["language_id"] = $this->getReference(array("id" => "language_id",
													   "name" => "language_id",
													   "class" => "form-control",
													   "refid" => 11)); // 11 = language
		$data["language_level_id"] = $this->getReference(array("id" => "language_level_id",
													   "name" => "language_level_id",
													   "class" => "form-control",
													   "refid" => 13)); // 13 = language level
		$data["skill_level_id"] = $this->getReference(array("id" => "skill_level_id",
													   "name" => "skill_level_id",
													   "class" => "form-control",
													   "refid" => 13)); // 13 = skill level
		$data["skill_id"] = $this->getReference(array("id" => "skill_id",
													   "name" => "skill_id",
													   "class" => "form-control",
													   "refid" => 17)); // 17 = skill
		$data["recso_id"] = $this->getReference(array("id" => "recso_id",
													   "name" => "recso_id",
													   "class" => "form-control",
													   "refid" => 15)); // 15 = recso
		$data["fj_source_media_id"] = $this->getReference(array("id" => "fj_source_media_id",
													   "name" => "fj_source_media_id",
													   "class" => "form-control",
													   "refid" => 16)); // 16 = source media
		$data["job_function_id"] = $this->getReference(array("id" => "job_function_id",
													   "name" => "job_function_id",
													   "class" => "form-control",
													   "refid" => 18)); // 16 = fungsi kerja
		$data["course_from_month"] = $this->getReference(array("id" => "course_from_month",
													   "name" => "course_from_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["course_to_month"] = $this->getReference(array("id" => "course_to_month",
													   "name" => "course_to_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["organization_from_month"] = $this->getReference(array("id" => "organization_from_month",
													   "name" => "organization_from_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["organization_to_month"] = $this->getReference(array("id" => "organization_to_month",
													   "name" => "organization_to_month",
													   "class" => "form-control",
													   "refid" => 23)); 
		$data["m_branch_id"] = $this->getCombo(array("name" => "m_branch_id",
													 "model" => "m_branch",
													 "id" => "m_branch_id",
													 "selected" => $this->session->userdata("M_BRANCH_ID"),
													 "disabled" => "disabled",
													 "column" => "NAME",
													 "class" => "form-control"));

		$this->load->model("M_reference_list", "ml");
		$pas = $this->ml->get_many_by("M_REFERENCE_ID",18);
		$i = 0;
		foreach($pas as $k=>$v)
		{
			$data["passion"][$i]["id"] = $v->M_REFERENCE_LIST_ID;
			$data["passion"][$i]["name"] = $v->NAME;
			$i++;		
		}
		$this->load->view("ApplicantForm",$data);			
	}
	
	function getPdf($id) //PP for AppServ
	{
		$html = '';
		$url = base_url();
		$this->load->library("Pdf");
		$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('Personal Particular');
  		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    	//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    	//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '', '', '');
		$pdf->setPrintHeader(false);
        $pdf->AddPage();
		$pdf->SetFont('helvetica', 'BI', 18);
		///$pdf->Cell(0, 0, 'PERSONAL PARTICULAR', 0, 1, '0');
		$pdf->SetFont('helvetica', '', 9, '', false);
		
		//menampilkan data pribadi kandidat
		$this->load->model("FJ_personal", "pers");
		$data = $this->pers->get($id);
		foreach($data as $k=>$v)
		{
			$this->load->model("M_reference_list", "ml");
			$sex = $this->ml->get($data->GENDER_ID);
			if($sex) $sex =$sex->NAME;
			else $sex = "-";		

			$marital = $this->ml->get($data->MARITAL_STATUS_ID);
			if($marital) $marital =$marital->NAME;
			else $marital = "-";		

			$religion = $this->ml->get($data->RELIGION_ID);
			if($religion) $religion =$religion->NAME;
			else $religion = "-";		

			$recso = $this->ml->get($data->RECSO_ID);
			if($recso) $recso =$recso->NAME;
			else $recso = "-";		

			$this->load->model("M_sub_district", "ms");
			$subdistrict = $this->ms->get($data->M_SUB_DISTRICT_ID);
			if($subdistrict) $subdistrict =$subdistrict->NAME;
			else $subdistrict = "-";		

			$this->load->model("M_district", "md");
			$district = $this->md->get($data->M_DISTRICT_ID);
			if($district) $district =$district->NAME;
			else $district = "-";		

			$this->load->model("M_city", "mc");
			$city = $this->mc->get($data->M_CITY_ID);
			if($city) $city =$city->NAME;
			else $city = "-";		

			$birth_place = $this->mc->get($data->BIRTH_PLACE);
			if($birth_place) $birth_place =$birth_place->NAME;
			else $birth_place = "-";		

			$this->load->model("M_state", "mst");
			$province = $this->mst->get($data->M_STATE_ID);
			if($province) $province =$province->NAME;
			else $province = "-";		
		}			
		
		$file_photo =$data->FILE_PHOTO;
		if($file_photo!="" || $file_photo!=null)
		{
			$photo = '<img src="'.$url.'/asset/uploads/'.$file_photo.'"  width="100" height="120" border="1" />';			
		}
		else
		{
			$photo = '<img src="'.$url.'/asset/uploads/nophoto.png"  width="100" height="120" border="1" />';			
		}
		$idcard =$data->ID_CARD_NO;
		$name =$data->NAME;
		$birth_date = $this->view_date($data->BIRTH_DATE,1);
		$height =$data->HEIGHT;
		$address =$data->ADDRESS;
		
		//echo $this->db->last_query();

		//menampilkan data pendidikan formal
		$this->load->model("FJ_education", "edu");
		$data2 = $this->edu->get_by("FJ_PERSONAL_ID",$id);	
		$row = $this->edu->count_by("FJ_PERSONAL_ID", $id);
		if($row>0)
		{
			$sma =$data2->HIGHSCHOOL_INSTITUTION;
			$sma_major =$data2->HIGHSCHOOL_MAJOR;
			$sma_graduated =$data2->HIGHSCHOOL_GRADUATED;
			
			$diploma =$data2->DIPLOMA_INSTITUTION;
			$diploma_major =$data2->DIPLOMA_MAJOR;
			$diploma_graduated =$data2->DIPLOMA_GRADUATED;
			$diploma_gpa =$data2->DIPLOMA_GPA;
			
			$s1 =$data2->S1_INSTITUTION;
			$s1_major =$data2->S1_MAJOR;
			$s1_graduated =$data2->S1_GRADUATED;
			$s1_gpa =$data2->S1_GPA;
		}
		else
		{
			$sma ="";
			$sma_major ="";
			$sma_graduated ="";
			
			$diploma ="";
			$diploma_major ="";
			$diploma_graduated ="";
			$diploma_gpa ="";
			
			$s1 ="";
			$s1_major ="";
			$s1_graduated ="";
			$s1_gpa ="";
		}	
		$education = "";		

		if($sma != "" && $diploma != "" && $s1 != "") //jika semua pendidikan diisi
		{
			$education .= "<tr><td><strong>SMA</strong></td><td>".$sma."</td><td>".$sma_major."</td><td>&nbsp;</td><td>".$sma_graduated."</td><td>&nbsp;</td><td>-</td></tr>
						<tr><td><strong>Diploma (1/2/3)</strong></td><td>".$diploma."</td><td>".$diploma_major."</td><td>&nbsp;</td><td>".$diploma_graduated."</td><td>&nbsp;</td><td>".$diploma_gpa."</td></tr>
						<tr><td><strong>Sarjana (S1)</strong></td><td>".$s1."</td><td>".$s1_major."</td><td>&nbsp;</td><td>".$s1_graduated."</td><td>&nbsp;</td><td>".$s1_gpa."</td></tr>";
		}
		else
		if($sma != "" && $diploma != "" && $s1 == "") //jika yang diisi hanya SMA dan diploma
		{
			$education .= "<tr><td><strong>SMA</strong></td><td>".$sma."</td><td>".$sma_major."</td><td>&nbsp;</td><td>".$sma_graduated."</td><td>&nbsp;</td><td>-</td></tr>
						<tr><td><strong>Diploma (1/2/3)</strong></td><td>".$diploma."</td><td>".$diploma_major."</td><td>&nbsp;</td><td>".$diploma_graduated."</td><td>&nbsp;</td><td>".$diploma_gpa."</td></tr>";
		}		
		else
		if($sma != "" && $diploma == "" && $s1 != "") //jika yang diisi hanya SMA dan S1
		{
			$education .= "<tr><td><strong>SMA</strong></td><td>".$sma."</td><td>".$sma_major."</td><td>&nbsp;</td><td>".$sma_graduated."</td><td>&nbsp;</td><td>-</td></tr>
						<tr><td><strong>Sarjana (S1)</strong></td><td>".$s1."</td><td>".$s1_major."</td><td>&nbsp;</td><td>".$s1_graduated."</td><td>&nbsp;</td><td>".$s1_gpa."</td></tr>";
		}		
		else
		if($sma == "" && $diploma == "" && $s1 != "") //jika yang diisi hanya S1
		{
			$education .= "<tr><td><strong>Sarjana (S1)</strong></td><td>".$s1."</td><td>".$s1_major."</td><td>&nbsp;</td><td>".$s1_graduated."</td><td>&nbsp;</td><td>".$s1_gpa."</td></tr>";
		}
		else
		if($sma != "" && $diploma == "" && $s1 == "") //jika yang diisi hanya SMA
		{
			$education .= "<tr><td><strong>SMA</strong></td><td>".$sma."</td><td>".$sma_major."</td><td>&nbsp;</td><td>".$sma_graduated."</td><td>&nbsp;</td><td>-</td></tr>";
		}
		else
		if($sma == "" && $diploma != "" && $s1 == "") //jika yang diisi hanya diploma
		{
			$education .= "<tr><td><strong>Diploma (1/2/3)</strong></td><td>".$diploma."</td><td>".$diploma_major."</td><td>&nbsp;</td><td>".$diploma_graduated."</td><td>&nbsp;</td><td>".$diploma_gpa."</td></tr>";
		}
		else //jika tidak ada data
		{
			$education="<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
		}

		//menampilkan data kursus dan pelatihan
		$kursus = "";
		$this->load->model("Fj_course", "kurs");
		$data3["list"] = $this->kurs->get_many_by("FJ_PERSONAL_ID",$id);
		$row = $this->kurs->count_by("FJ_PERSONAL_ID", $id);
		if($row>0)
		{
			foreach($data3["list"] as $k=>$v)
			{
				$this->load->model("M_reference_list", "ml");
				if(($v!="" || $v!=0))
				$course_from_month = $this->ml->get($data3["list"][$k]->COURSE_FROM_MONTH);
				if($course_from_month) $course_from_month  =$course_from_month ->NAME;
				else $course_from_month  = "";

				$course_to_month = $this->ml->get($data3["list"][$k]->COURSE_TO_MONTH);
				if($course_to_month) $course_to_month  =$course_to_month ->NAME;
				else $course_to_month  = "";

				$course_from_year = $data3["list"][$k]->COURSE_FROM_YEAR;
				if($course_from_year !="" ) $course_from_year  =$course_from_year;
				else $course_from_year  = "";

				$course_to_year = $data3["list"][$k]->COURSE_TO_YEAR;
				if($course_to_year !="" ) $course_to_year  =$course_to_year;
				else $course_to_year  = "";
						
				$kursus .= "<tr><td>".$data3["list"][$k]->COURSE."</td>
								<td>".$data3["list"][$k]->INSTITUTION."</td>
								<td>".$course_from_month." ".$course_from_year."</td>
								<td>".$course_to_month." ".$course_to_year."</td>
								<td>&nbsp;</td>
							</tr>";
			}
		}
		else
		{
				$kursus .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";			
		}

		//menampilkan data pengalaman kerja	
		$experience = "";
		$this->load->model("Fj_experience", "ex");
		$data4["list"] = $this->ex->get_many_by("FJ_PERSONAL_ID",$id);
		$row = $this->ex->count_by("FJ_PERSONAL_ID", $id);
		$exp_from = "";
		$exp_to = "";
		
		if($row>0)
		{
			foreach($data4["list"] as $k=>$v)
			{
				if($data4["list"][$k]->EXPERIENCE_FROM_DATE != 0) $exp_from = $this->view_date($data4["list"][$k]->EXPERIENCE_FROM_DATE,1);
				else $exp_from = "-";
				if($data4["list"][$k]->EXPERIENCE_TO_DATE != 0) $exp_to = $this->view_date($data4["list"][$k]->EXPERIENCE_TO_DATE,1);
				else $exp_to = "-";
				$experience .= "<tr><td>".$data4["list"][$k]->COMPANY_NAME."</td>
									<td>".$data4["list"][$k]->EXPERIENCE_POSITION."</td>
									<td>".$exp_from."</td>
									<td>".$exp_to."</td>
								</tr>";
			}
		}
		else
		{
				$experience .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
		}
		
		

		//menampilkan keahlian		
		$this->load->model("FJ_skill", "skill");
		$data6 = $this->skill->get_by("FJ_PERSONAL_ID",$id);
		$row = $this->skill->count_by("FJ_PERSONAL_ID", $id);
		if($row>0)
		{
			$skill1 = $data6->SKILL_1;
			$skill2 = $data6->SKILL_2;
			$skill3 = $data6->SKILL_3;
			$skill4 = $data6->SKILL_4;		
		}
		else
		{
			$skill1 = "-";
			$skill2 = "-";
			$skill3 = "-";
			$skill4 = "-";		
		}
		//menampilkan hasil interview
		$this->load->model("Fj_interview", "ok");
		$data8 = $this->ok->get_by("FJ_PERSONAL_ID",$id);
		$row = $this->ok->count_by("FJ_PERSONAL_ID", $id);
		if($row>0)
		{
			$result = $data8->INTERVIEW_RESULT;
		}
		else
		{
			$result = "";
		}
		
		$logo_1 = '<img src="'.$url.'asset/img/logo.png" width="90" height="40">';			
		$logo_2 = '<img src="'.$url.'asset/img/gos.JPG" width="40" height="40">';	
		$judul = '<p><font size="20"><b>PERSONAL PARTICULAR</b></font></p>';		
			
		$html = <<<EOD
				<table border="0">
					<tr>
						<td width="30%">$logo_1  $logo_2</td>
						<td align="left">&nbsp;
						</td>
						<td align="right" rowspan="2">$photo</td>
					</tr>
					<tr>
						<td colspan="2" align="center" valign="top"><div align="center">$judul</div></td>
					</tr>
				</table>
				<p>
				<table border="1" cellpadding="2">
					<tr>
						<td colspan="2" bgcolor="#CCC"><strong>Personal Data</strong></td>
					</tr>
					<tr>
						<td>
							<table cellpadding="2">
							<tr>
								<td width="90">Applicant ID</td><td width="100%">: $idcard</td>
							</tr>
							<tr>
								<td>Applicant Name</td><td>: $name</td>
							</tr>
							<tr>
								<td>Place / Date of Birth</td><td>: $birth_place , $birth_date</td>
							</tr>
							<tr>
								<td>Sex</td><td>: $sex</td>
							</tr>
							<tr>
								<td>Height</td><td>: $height cm</td>
							</tr>
							<tr>
								<td>Marital Status</td><td>: $marital</td>
							</tr>
							<tr>
								<td>Religion</td><td>: $religion</td>
							</tr>		
							</table>						
						</td>
						<td>
							<table cellpadding="2" >
							<tr>
								<td width="60">Address</td><td width="10">:</td><td width="70%">$address</td>
							</tr>
							<tr>
								<td>Sub District</td><td width="10">:</td><td>$subdistrict</td>
							</tr>
							<tr>
								<td>District</td><td width="10">:</td><td>$district</td>
							</tr>
							<tr>
								<td>City</td><td width="10">:</td><td>$city</td>
							</tr>
							<tr>
								<td>State</td><td width="10">:</td><td>$province</td>
							</tr>
							<tr>
								<td>Country</td><td width="10">:</td><td></td>
							</tr>		
							</table>	
						</td>
					</tr>
				</table>
				<p>
				<table border = "1" cellpadding="2" width="100%">
					<tr>
						<td colspan="7"  bgcolor="#CCC"><strong>Formal Education</strong></td>
					</tr>
					<tr>
						<td width="15%"><div align="center"><strong>Level</strong></div></td>
						<td width="30%"><div align="center"><strong>Name of School</strong></div></td>
						<td width="20%"><div align="center"><strong>Faculty/Major</strong></div></td>
					  	<td width="10%"><div align="center"><strong>From</strong></div></td>
						<td width="10%"><div align="center"><strong>To</strong></div></td>
						<td width="10%"><div align="center"><strong>Degree</strong></div></td>
						<td width="5%"><div align="center"><strong>IPK</strong></div></td>
					</tr>
					$education
				</table>			
				<p>
				<table border = "1" cellpadding="2">
					<tr>
						<td colspan="5"  bgcolor="#CCC"><strong>Course</strong></td>
					</tr>
					<tr>
						<td width="30%"><div align="center"><strong>Course</strong></div></td>
						<td width="30%"><div align="center"><strong>Institution</strong></div></td>
						<td width="15%"><div align="center"><strong>From</strong></div></td>
						<td width="15%"><div align="center"><strong>To</strong></div></td>
						<td width="10%"><div align="center"><strong>Certificate</strong></div></td>
					</tr>
					$kursus
				</table>			
				<p>
				<table border = "1" cellpadding="2" width="100%">
					<tr>
						<td colspan="7"  bgcolor="#CCC"><strong>Job Experience</strong></td>
					</tr>
					<tr>
						<td width="35%"><div align="center"><strong>Company Name</strong></div></td>
						<td width="35%"><div align="center"><strong>Position</strong></div></td>
						<td width="15%"><div align="center"><strong>From</strong></div></td>
						<td width="15%"><div align="center"><strong>To</strong></div></td>
					</tr>
					$experience
				</table>
				<h4></h4>
				<table>
					<tr>
						<td>
							<table width="400" height="112" border="1" cellpadding="2">
								<tr>
								  <td width="333"  bgcolor="#CCC"><div align="left"><strong>Profile Summary</strong></div></td>
								</tr>
								<tr>
									<td height="60">$result</td>
								</tr>
						  </table>
						</td>
						<td>&nbsp;</td>
						<td>
							<table width="175" border="1" cellpadding="2">
								<tr>
									<td  bgcolor="#CCC"><div align="center"><strong>Skill</strong></div></td>
									<td  bgcolor="#CCC"><div align="center"><strong>Rate</strong></div></td>
								</tr>
								<tr>
									<td>Pajak</td><td>$skill1</td>
								</tr>
								<tr>
									<td>Excel</td><td>$skill2</td>
								</tr>
								<tr>
									<td>Akuntansi</td><td>$skill3</td>
								</tr>
								<tr>
									<td>Photoshop</td><td>$skill4</td>
								</tr>
						  </table>
						</td>
					</tr>
				</table>
						
		
EOD;

		$pdf->writeHTML($html, true, false, false, false, '');
		$pdf->SetXY(10, 260);
		$pdf->Image(''.$url.'/asset/img/ppe.JPG', '', '', 30, 10, '', '', 'T', false, 100, '', false, false, 0, false, false, false);
		$pdf->SetXY(170, 260);
		$pdf->Image(''.$url.'/asset/img/oi.JPG', '', '', 20, 15, '', '', 'T', false, 100, '', false, false, 0, false, false, false);
		$pdf->SetXY(10, 265);
		$pdf->Cell(0, 0, 'PT GOS INDORAYA', 0, 1, 'C', 0, '', 0);
		$pdf->Cell(0, 0, 'Gedung Graha Enka Deli Lt.3', 0, 1, 'C', 0, '', 0);
		$pdf->Cell(0, 0, 'Jl. Buncit Raya no. 12, Jakarta Selatan, 12780', 0, 1, 'C', 0, '', 0);
		
		$pdf->SetAutoPageBreak(false);
        //$pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output(''.$name.'.pdf', 'I');	
	}
		
	function getPosisi($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_reference_list", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->likesbyID("NAME",$q, 24)->dropdown("NAME");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i] = $v;
			$i++;
			
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	
	function getCity($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_city", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->get_many_by("NAME ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->M_CITY_ID;
			$arr[$i]["name"] = $v->NAME;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getSubDistrict($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_sub_district", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(50,$offset);
		$arr = array();
		$q = urldecode($q);
		$q = str_replace("-"," ",$q);
		$data = $this->ml->getTypeahead($q);
		$query = $this->db->last_query();
/*		$query = $this->db->query("SELECT ms.* from ");
		$data = $this->ml->get_many_by("NAME ILIKE '%{$q}%'");
*/	
		$i = 0;
//		echo urldecode($q);
		if($data)
		{
			foreach($data as $k=>$v)
			{
				$arr[$i]["id"] = $v->M_SUB_DISTRICT_ID;
				$arr[$i]["m_district_id"] = $v->M_DISTRICT_ID;
	
				$arr[$i]["name"] = strtoupper($v->sname."-".$v->dname);
				$i++;
			}
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr, "q" => $query));
	}

	function getAddress($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_district", "md");
		$md = $this->md->get($q);
		$arr = array();		
		
		if($this->md->count_by("M_DISTRICT_ID", $q)>0)
		{
			$arr["m_district_name"] = strtoupper($md->NAME);
			$arr["m_district_id"] = $md->M_DISTRICT_ID;
			$this->load->model("M_city", "mc");
			$mc = $this->mc->get($md->M_CITY_ID);

			if($this->mc->count_by("M_CITY_ID", $md->M_CITY_ID)>0)
			{
				$arr["m_city_name"] = strtoupper($mc->NAME);
				$arr["m_city_id"] = $mc->M_CITY_ID;
		
				$this->load->model("M_state", "mst");
				$mst = $this->mst->get($mc->M_STATE_ID);

				if($this->mst->count_by("M_STATE_ID", $mc->M_STATE_ID)>0)
				{
					$arr["m_state_name"] = strtoupper($mst->NAME);
					$arr["m_state_id"] = $mst->M_STATE_ID;
				}
			}
		}	
		
	//	echo $this->db->last_query();
		if(empty($arr))
			echo json_encode(array(0));
		else
			echo json_encode($arr);
	}
}
