<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		//cek user session
		$m_user_id = $this->session->userdata('m_user_id');
		if(empty($m_user_id))
		{
			$this->load->view('view_login');
		}
		else
		{
			$this->main_page();
		}
		
	}

	public function main_page()
	{
		$this->load->model('M_menu','menu');
		$this->menu->order_by('order_role','asc');
		$data['menu'] = $this->menu->get_all();
		$this->load->view('header');
		$this->load->view('view_menu',$data);
		$this->load->view('main_page');
		$this->load->view('footer');
	}

	function cekAkses()
	{
		$m_user_id = $this->session->userdata("M_USER_ID");	
		$this->load->model("M_user_role", "mr");
		
		$dmr = $this->mr->get_many_by("M_USER_ID", $m_user_id);
		
		$this->load->model("M_role_function", "mf");
		$arrf = array();
		$arrid = array();
		foreach($dmr as $k=>$v)
		{
			$dmf = $this->mf->get_many_by("M_ROLE_ID", $v->M_ROLE_ID);
			foreach($dmf as $k2=>$v2)
			{
				$this->load->model("M_function", "mfn");
				$dfn = $this->mfn->get($v2->M_FUNCTION_ID);
				$arrid[] = $dfn->M_FUNCTION_ID;
			}
		}

		$dfnn = $this->mfn->get_all();
		foreach($dfnn as $k=>$v)
		{
			if(!in_array($v->M_FUNCTION_ID, $arrid))
			{
				$arrf[] = $v->NAME;	
			}
		}
		echo json_encode(array("success" => true, "role" => $arrf));		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */