<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 1, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Karyawan
* 
********************************************************************/ 

class Karyawan extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	} 
	
	function getPanel($form='',$id='')
	{
		$address='';
		//$family='';
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vkaryawan"] = $this->config->item('karyawan');
		$data["vfamily"] = $this->config->item('family');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["gender_id"] = $this->getReferensi(array("id" => "gender_id",
													   "name" => "gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender

		$data["religion_id"] = $this->getReferensi(array("id" => "religion_id",
													   "name" => "religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReferensi(array("id" => "ref_marital_status_id",
													   "name" => "ref_marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 3));

		$data["blood_type_id"] = $this->getReferensi(array("id" => "blood_type_id",
													   "name" => "ref_blood_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 4));

		$data["ref_bpjskes_status_id"] = $this->getReferensi(array("id" => "ref_bpjskes_status_id",
													   "name" => "ref_bpjskes_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 5));

		$data["ref_status_kerja_id"] = $this->getReferensi(array("id" => "ref_status_kerja_id",
													   "name" => "ref_status_kerja_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 6));
		
		$data["ref_team_work_id"] = $this->getReferensi(array("id" => "ref_team_work_id",
													   "name" => "ref_team_work_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 23));

		$data["ref_overtime_status_id"] = $this->getReferensi(array("id" => "ref_overtime_status_id",
													   "name" => "ref_overtime_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 35));

		$data["ref_institution_type"] = $this->getReferensi(array("id" => "ref_institution_type",
													   "name" => "ref_institution_type",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 19));

		$data["ref_stat_ptkp"] = $this->getReferensi(array("id" => "ref_stat_ptkp",
													   "name" => "ref_stat_ptkp",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 20));

		$data["ref_status_pkwt_id"] = $this->getReferensi(array("id" => "ref_status_pkwt_id",
													   "name" => "ref_status_pkwt_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 21));

		$data["company_id"] = $this->getCombo(array("name" => "m_company_id",
															 "model" => "m_perusahaan",
															 "id" => "m_company_id",
															 "column" => "m_company_name",
															 "class" => "form-control"));

		$data["country_id"] = $this->getCombo(array("name" => "m_country_id",
															 "model" => "m_country",
															 "id" => "m_country_id",
															 "column" => "name",
															 "class" => "form-control"));

		$data["bank_id"] = $this->getCombo(array("name" => "m_bank_id",
															 "model" => "m_bank",
															 "id" => "m_bank_id",
															 "column" => "m_bank_name",
															 "class" => "form-control"));

		//Keluarga
		$data["vfamily"] = $this->config->item('family');
		$data["gender_id_keluarga"] = $this->getReferensi(array("id" => "gender_id-keluarga",
													   "name" => "ref_gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender

		$data["religion_id_keluarga"] = $this->getReferensi(array("id" => "religion_id-keluarga",
													   "name" => "ref_religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReferensi(array("id" => "marital_status_id",
													   "name" => "marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 3));

	
		$data["ref_bpjskes_status_id_keluarga"] = $this->getReferensi(array("id" => "ref_bpjskes_status_id-keluarga",
													   "name" => "ref_bpjskes_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 5));

		$data["ref_relation_id"] = $this->getReferensi(array("id" => "ref_relation_id-keluarga",
													   "name" => "ref_relation_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 7));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		////
		///Kontrak
		$data["vkontrak"] = $this->config->item('kontrak');
		$data["ref_status_pkwt_id"] = $this->getReferensi(array("id" => "ref_status_pkwt_id",
													   "name" => "ref_status_pkwt_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 21));

		//
		// User
		$data["vkontrak"] = $this->config->item('kontrak');
		$data["ref_criteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 1 = gender
		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		$this->load->model("M_referensi_list", "ml");

		//
		//Gaji
		$data["status"] = $this->getReferensi_status(array("id" => "status",
													   "name" => "status",
													   "class" => "form-control",
													   "refid" => 41)); 	

		//
		
		// Tunjangan
		$data["vtunjangan"] = $this->config->item('tunjangan');
		$data["ref_criteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 1 = gender
		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		//
		// Mutasi Jabatan
		$data["vtunjangan"] = $this->config->item('tunjangan');
		$data["ref_criteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 1 = gender
		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		//
		
		// Mutasi
		$data["vmutasi"] = $this->config->item('mutasi');
		$data["ref_mutation_type_id"] = $this->getReferensi(array("id" => "ref_mutation_type_id",
													   "name" => "ref_mutation_type_id",
													   "class" => "form-control",
													   "refid" => 36)); 

		//
		
		//Kelengkapan Dokumen
		$data["ref_doc_type_id"] = $this->getReferensi(array("id" => "ref_doc_type_id",
													   "name" => "ref_doc_type_id",
													   "class" => "form-control",
													   "refid" => 39));

		$data["ref_doc_status_id"] = $this->getReferensi(array("id" => "ref_doc_status_id",
													   "name" => "ref_doc_status_id",
													   "class" => "form-control",
													   "refid" => 40));

		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		///////
		
		//Sanksi Dan Kedisplinan
		$data["vmutasi"] = $this->config->item('mutasi');
		$data["ref_punishment_type_id"] = $this->getReferensi(array("id" => "ref_punishment_type_id",
													   "name" => "ref_punishment_type_id",
													   "class" => "form-control",
													   "refid" => 9)); // 

		$data["ref_group_punishment"] = $this->getReferensi(array("id" => "ref_group_punishment",
													   "name" => "ref_group_punishment",
													   "class" => "form-control",
													   "refid" => 10)); 

		$data["ref_reference_id"] = $this->getReferensi(array("id" => "ref_reference_id",
													   "name" => "ref_reference_id",
													   "class" => "form-control",
													   "refid" => 37)); // 1 = gender

		
		/////
		
		// Cuti
		
		
		$data["ref_leave_id"] = $this->getReferensi(array("id" => "ref_leave_id",
													   "name" => "ref_leave_id",
													   "class" => "form-control",
													   "refid" => 38)); // 

		
		///
		
		//Alasan Keluar
		$data["ref_nextjob_id"] = $this->getReferensi(array("id" => "ref_nextjob_id",
													   "name" => "ref_nextjob_id",
													   "class" => "form-control",
													   "refid" => 18));

		//
	
			


			// LOADNYA SAMPE SINI.....


		$this->load->view("view_sidebar_karyawan",$data);	
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_personal", "karyawan");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->karyawan->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->karyawan->order_by('gp_personal_id','ASC');
		$data["list"] = $this->karyawan->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->karyawan->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}
	function getAll($id='')
	{	
			$data=array();
			$this->db->select('gp_personal.gp_personal_id as gp_personal, *');
			$this->load->model('Gp_personal','personal');
			$this->db->join("gp_address","gp_address.gp_personal_id=gp_personal.gp_personal_id and gp_address.active='Y'");
			$this->db->join("gp_family","gp_family.gp_personal_id=gp_personal.gp_personal_id");
			$rs=$this->db->where("gp_personal.gp_personal_id",$id)->get('gp_personal')->result_array();
			//$rs = $this->personal->get_many_by("gp_personal.gp_personal_id",$id);
			//echo $this->db->last_query();
			foreach($rs as $row)
			{	
				/*Panggil NRK untuk id  nrk,nrk_alamat,nrk-keluarga,nrk-kontrak,nrk-user,
				nrk-gaji,nrk-tunjangan,nrk-mutasi,nrk-sanksi,nrk-cuti,nrk-atensi,nrk-alasan,nrk-dokumen
				*/
				$data["nrk"] = $row['nrk'];
				$data["nrk-alamat"] = $row['nrk'];
				$data["nrk-keluarga"] = $row['nrk'];
				$data["nrk-kontrak"] = $row['nrk'];
				$data["nrk-user"] = $row['nrk'];
				$data["nrk-gaji"] = $row['nrk'];
				$data["nrk-tunjangan"] = $row['nrk'];
				$data["nrk-mutasi"] = $row['nrk'];
				$data["nrk-sanksi"] = $row['nrk'];
				$data["nrk-cuti"] = $row['nrk'];
				$data["nrk-atensi"] = $row['nrk'];
				$data["nrk-alasan"] = $row['nrk'];
				$data["nrk-dokumen"] = $row['nrk'];
				////////////////
				/*Panggil Nama Karyawan untuk id  personal_name,personal_name-alamat,personal_name-keluarga,personal_name-kontrak
				,personal_name-user,personal_name-gaji,personal_name-tunjangan,personal_name-mutasi,personal_name-sanksi
				,personal_name-cuti,personal_name-alasan,personal_name-dokumen
				*/
				$data["personal_name"] = $row['personal_name'];
				$data["personal_name-alamat"] = $row['personal_name'];
				$data["personal_name-keluarga"] = $row['personal_name'];
				$data["personal_name-kontrak"] = $row['personal_name'];
				$data["personal_name-user"] = $row['personal_name'];
				$data["personal_name-gaji"] = $row['personal_name'];
				$data["personal_name-tunjangan"] = $row['personal_name'];
				$data["personal_name-mutasi"] = $row['personal_name'];
				$data["personal_name-sanksi"] = $row['personal_name'];
				$data["personal_name-cuti"] = $row['personal_name'];
				$data["personal_name-atensi"] = $row['personal_name'];
				$data["personal_name-alasan"] = $row['personal_name'];
				$data["personal_name-dokumen"] = $row['personal_name'];
				///////////////////////////////
				$data["id_card_no"] = $row['id_card_no'];
				$data["m_birth_place_id"] = $row['m_birth_place_id'];
				$data["birth_date"] = $row['birth_date'];
				$data["gender_id"] = $row['ref_gender_id'];
				$data["marital_status_id"] = $row['ref_marital_status_id'];
				$data["religion_id"] = $row['ref_religion_id'];
				$data["m_country_id"] = $row['m_country_id'];
				$data["blood_type_id"] = $row['ref_blood_type_id'];
				/////////Panggil Address Untuk Id  address
				$data["address"] = $row['address'];
				/////////////////
				/////////Panggil Rt Untuk Id  rt
				$data["rt"] = $row['rt'];
				//////////////
				/////////Panggil Rw Untuk Id  rw
				$data["rw"] = $row['rw'];
				////////////////
				//////// Panggil Kelurahan Untuk Id m_sub_district_id
				$data["m_sub_district_id"] = $row['m_sub_district_id'];
				///////////////////
				/////// Panggil Kecamatan Untuk Id m_district_id
				$data["m_district_id"] = $row['m_district_id'];
				//////////////////////
				//////Panggil Untuk Kota Untuk Id m_city_id
				$data["m_city_id"] = $row['m_city_id'];
				///////////////
				////////Panggil Provinsi Untuk Id m_state_id
				$data["m_state_id"] = $row['m_state_id'];
				/////////////////////
				///////Panggil Kode Pos Untuk Id zipe_code
				$data["zipe_code"] = $row['zipe_code'];
				////////////////////
				///////Panggil HP Untuk Id handphone
				$data["handphone"] = $row['handphone'];
				//////////////////////
				///////Panggil No. Telepon Untuk Id home_phone
				$data["home_phone"] = $row['home_phone'];
				/////////////////////
				$data["email_address"] = $row['email_address'];
				//////Panggil Fasilitas Kesehatan Untuk Id m_faskes_id
				$data["m_faskes_id"] = $row['m_faskes_id'];
				///////////////////
				/////////Panggil Dokter Gigi untuk id dokgi
				$data["dokgi"] = $row['dokgi'];
				//////////////
				////Panggil Perusahaan Untuk Id m_company_id
				$data["m_company_id"] = $row['m_company_id'];
				//////////////////
				///Panggil Tanggal Dijabat Untuk Id join_date, join_date-gaji
				$data["join_date"] = $row['join_date'];
				$data["join_date-gaji"] = $row['join_date'];
				////////////////////
				$data["start_permanen_date"] = $row['start_permanen'];
				$data["stop_date"] = $row['stop_date'];
				$data["sk_join"] = $row['sk_join'];
				$data["sk_permanen"] = $row['sk_permanen'];
				$data["ref_status_kerja_id"] = $row['ref_status_kerja_id'];
				$data["m_branch_id"] = $row['m_branch_id'];
				///////Panggil Client Untuk Id m_client_id,m_client_id-kontrak
				$data["m_client_id"] = $row['m_client_id'];
				$data["m_client_id-kontrak"] = $row['m_client_id'];
				/////////////
				$data["m_area_id"] = $row['m_area_id'];
				$data["m_structure_position_id"] = $row['m_structure_position_id'];
				$data["ref_team_work_id"] = $row['ref_team_work_id'];
				$data["ref_status_kerja_id"] = $row['ref_status_kerja_id'];
				$data["team_work_date"] = $row['team_work_date'];
				//Panggil SIPO untuk id  sipo_id
				$data["sipo_id"] = $row['sipo_id'];
				//////////////
				//Panggil Nama Ibu Kandung untuk id  mother_name
				$data["mother_name"] = $row['mother_name'];
				//////////////
				//Panggil No. Kartu Keluarga (KK) untuk id  family_card_no,family_card_no-keluarga
				$data["family_card_no"] = $row['family_card_no'];
				$data["family_card_no-keluarga"] = $row['family_card_no'];
				//////////////
				//Panggil Berhak Lembur untuk id  ref_overtime_status_id
				$data["ref_overtime_status_id"] = $row['ref_overtime_status_id'];
				//////////////
				//Panggil Berhak BPJS Kesehatan untuk id  ref_bpjskes_status_id,ref_bpjskes_status_id_keluarga
				$data["ref_bpjskes_status_id"] = $row['ref_bpjskes_status_id'];
				//////////////
				//Panggil No. Jamsostek untuk id  jamsostek_no
				$data["jamsostek_no"] = $row['jamsostek_no'];
				//////////////
				//Panggil Informasi Pajak BPJS Kesehatan untuk id  bpjskes_no
				$data["bpjskes_no"] = $row['bpjskes_no'];
				//////////////
				//Panggil Status PTKP untuk id  ref_stat_ptkp
				$data["ref_stat_ptkp"] = $row['ref_stat_ptkp'];
				//////////////
				//Panggil Jumlah Tanggungan untuk id  number_dependents
				$data["number_dependents"] = $row['number_dependents'];
				//////////////
				//Panggil No. NPWP untuk id  npwp_no
				$data["npwp_no"] = $row['npwp_no'];
				//////////////
				//Panggil Nama Bank untuk id  m_bank_id
				$data["m_bank_id"] = $row['m_bank_id'];
				//////////////
				//Panggil No. Rekening untuk id  acc_id
				$data["acc_id"] = $row['acc_id'];
				//////////////
				//Panggil Pemilik Rekening untuk id  acc_id
				$data["acc_name"] = $row['acc_name'];
				//////////////
				$this->load->model("M_city","city");
				$city=$this->city->get($data['m_birth_place_id']);
				if($city)
				{
				$data["birth_place_name"] = $city->name;
				}
				$this->load->model("M_sub_district", "sub_district");
				$sub_district = $this->sub_district->get($row['m_sub_district_id']);
				if($sub_district)
				{
				////Panggil Kelurahan Untuk Id m_sub_district_name
				$data["m_sub_district_name"] = $sub_district->name;
				////////////
				}		
				$this->load->model("M_district", "md");
				$md = $this->md->get($row['m_district_id']);	
				////Panggil Kecamatan Untuk Id m_district_name
				$data["m_district_name"] = strtoupper($md->name);
				////////////
				$this->load->model("M_city", "mc");
				$mc = $this->mc->get($md->m_city_id);

				if($mc)
				{
					////Panggil Kota Untuk Id m_city_name, m_city_id
					$data["m_city_id"] = $mc->m_city_id;
					$data["m_city_name"] = strtoupper($mc->name);
					/////////////
					$this->load->model("M_state", "mst");
					$mst = $this->mst->get($mc->m_state_id);

					if($mst)
					{
					////Panggil Provinsi Untuk Id m_state_name, m_state_id	
						$data["m_state_id"] = $mst->m_state_id;
						$data["m_state_name"] = strtoupper($mst->name);
					//////////////////	
					}
				}
				$this->load->model("M_faskes", "faskes");
				////Panggil Fasilitas Kesehatan Untuk Id m_faskes_name
				$faskes = $this->faskes->get($row['m_faskes_id']);
				if($faskes)
				{
					$data["m_faskes_name"] = $faskes->m_faskes_name;
				}
				//////////////////
				////Panggil Perusahaan Untuk Id m_company_name
				$this->load->model("M_perusahaan", "company");
				$company = $this->company->get($row['m_company_id']);
				if($company)
				{
					$data["m_company_name"] = $company->m_company_name;
				}
				//////////////////
				$this->load->model("M_cabang", "branch");
				$branch = $this->branch->get($row['m_branch_id']);
				if($branch)
				{
					$data["m_branch_name"] = $branch->m_branch_name;
				}
				$this->load->model("M_client", "client");
				$this->load->model("M_client", "client");
				$client = $this->client->get($row['m_client_id']);
				if($client)
				{	
				/*Panggil Client Untuk m_client_name, m_client_code, m_client_name-kontrak, 
				m_client_code-kontrak , m_client_name-user, m_client_code-user,m_client_code-mutasi,m_client_name-mutasi
				,m_client_code-cuti,m_client_name-cuti
				*/
					$data["m_client_name"] = $client->m_client_name;
					$data["m_client_code"] = $client->m_client_code;
					$data["m_client_name-kontrak"] = $client->m_client_name;
					$data["m_client_code-kontrak"] = $client->m_client_code;
					$data["m_client_name-user"] = $client->m_client_name;
					$data["m_client_code-user"] = $client->m_client_code;
					$data["m_client_name-mutasi"] = $client->m_client_name;
					$data["m_client_code-mutasi"] = $client->m_client_code;
					$data["m_client_name-cuti"] = $client->m_client_name;
					$data["m_client_code-cuti"] = $client->m_client_code;
				////////////////////	
				}
				$this->load->model("M_area", "area");
				$area = $this->area->get($row['m_area_id']);
				if($area)
				{
					$data["area_name"] = $area->area_name;
					$data["wil_name"] = $area->wil_name;
					$this->load->model("M_lokasi", "lokasi");
					$lokasi = $this->lokasi->get($area->m_location_id);
					if($lokasi)
					{
					$data["m_location_name"] = $lokasi->m_location_name;
					$data["m_location_code"] = $lokasi->m_location_code;
					}
				}
				$this->load->model("M_unit_kerja", "unit");
				/////Panggil Unit Untuk m_unit_id
				$unit = $this->unit->get($row['m_unit_id']);
				/////////////
				if($unit)
				{
				/////Panggil Unit Untuk m_unit_name,m_unit_code,  m_unit_name-gaji
					$data["m_unit_name"] = $unit->m_unit_name;
					$data["m_unit_name-gaji"] = $unit->m_unit_name;
					$data["m_unit_code"] = $unit->m_unit_code;
				}
				$this->load->model("M_struktur_jabatan", "struktur_jabatan");
				$struktur_jabatan = $this->struktur_jabatan->get($row['m_structure_position_id']);
				if($unit)
				{	
				/*Panggil Jabatan Untuk Id m_structure_position_name, m_structure_position_id,
				m_structure_position_name-user, m_structure_position_id-user,m_structure_position_name-gaji
				*/
					$data["m_structure_position_name"] = $struktur_jabatan->m_structure_position_name;
					$data["m_structure_position_id-user"] = $struktur_jabatan->m_structure_position_id.'-'.
					$struktur_jabatan->m_structure_position_name;
					$data["m_structure_position_name-gaji"] = $struktur_jabatan->m_structure_position_name;
					$struktur_jabatan->m_structure_position_name;
				//////////////////////////////////	
					$data["ref_grade_id"] = $struktur_jabatan->ref_grade_id;
					$data["m_structure_position_code"] = $struktur_jabatan->m_structure_position_id;
					$data["m_position_type_id"] = $struktur_jabatan->m_position_type_id;
				}
				
				$this->load->model("Gp_pkwt", "pkwt");
				$pkwt = $this->pkwt->get_many_by('gp_personal_id',$id);
				if($pkwt)
				{
					foreach($pkwt as $k=>$v)
						{
							$data["pkwt_no"] = $v->pkwt_no;
							$data["pkwt_date"] = $v->pkwt_date;
							$data["start_contract_date"] = $v->start_contract_date;
							$data["stop_contract_date"] = $v->stop_contract_date;
							$data["ref_status_pkwt_id"] = $v->ref_status_pkwt_id;
							$data["job_no"] = $v->job_no;
							$data["job_date"] = $v->job_date;
							$data["letter_job_date"] = $v->letter_job_date;
							$data["pks_no"] = $v->pks_no;
							$data["stop_pks_date"] = $v->stop_pks_date;
						}
				}
				////////////////////Panggil User Untuk Id m_criteria_name, m_criteria_id, m_criteria_code/////////////////////////
				$this->load->model("Gp_user_client", "user_client");
				$user_client = $this->user_client->get_many_by('gp_personal_id',$id);
				if($user_client)
				{
					foreach($user_client as $k=>$v)
						{
							$this->load->model("M_kriteria", "criteria");
							$criteria = $this->criteria->get_many_by("m_criteria_id = ".$v->m_criteria_id." and  ref_criteria_id = '67' and active = 'Y' ");
							if($criteria)
							{
								foreach($criteria as $k_criteria=>$v_criteria)
								{
									$data["m_criteria_id"] = $v_criteria->m_criteria_id;
									$data["m_criteria_code"] = $v_criteria->ref_criteria_id;
									$data["m_criteria_name"] = $v_criteria->m_criteria_name;
								}
							}
						}
				}
				/////////////////////////////////////////////////////////////////
				
				////////////////////Panggil HRBP Untuk Id m_criteria_name2, m_criteria_id2, m_criteria_code2/////////////////////////
				$this->load->model("Gp_user_client", "user_client");
				$user_client = $this->user_client->get_many_by('gp_personal_id',$id);
				if($user_client)
				{
					foreach($user_client as $k=>$v)
						{
							$this->load->model("M_criteria", "criteria");
							$criteria = $this->criteria->get_many_by("m_criteria_id = ".$v->m_criteria_id." and  ref_criteria_id = '68' and active = 'Y' ");
							if($criteria)
							{
								foreach($criteria as $k_criteria=>$v_criteria)
								{
									$data["m_criteria_id2"] = $v_criteria->m_criteria_id;
									$data["m_criteria_code2"] = $v_criteria->ref_criteria_id;
									$data["m_criteria_name2"] = $v_criteria->m_criteria_name;
								}
							}
						}
				}
				/////////////////////////////////////////////////////////////////
				
				////////////////////Panggil Cost Center Untuk Id m_criteria_name3, m_criteria_id3, m_criteria_code3/////////////////////////
				$this->load->model("Gp_user_client", "user_client");
				$user_client = $this->user_client->get_many_by('gp_personal_id',$id);
				if($user_client)
				{
					foreach($user_client as $k=>$v)
						{
							$this->load->model("M_criteria", "criteria");
							$criteria = $this->criteria->get_many_by("m_criteria_id = ".$v->m_criteria_id." and  ref_criteria_id = '69' and active = 'Y' ");
							if($criteria)
							{
								foreach($criteria as $k_criteria=>$v_criteria)
								{
									$data["m_criteria_id3"] = $v_criteria->m_criteria_id;
									$data["m_criteria_code3"] = $v_criteria->ref_criteria_id;
									$data["m_criteria_name3"] = $v_criteria->m_criteria_name;
								}
							}
						}
				}
				/////////////////////////////////////////////////////////////////
				
				
				////////////////////Panggil Asuransi Untuk Id m_criteria_name4, m_criteria_id4, m_criteria_code4/////////////////////////
				$this->load->model("Gp_user_client", "user_client");
				$user_client = $this->user_client->get_many_by('gp_personal_id',$id);
				if($user_client)
				{
					foreach($user_client as $k=>$v)
						{
							$this->load->model("M_criteria", "criteria");
							$criteria = $this->criteria->get_many_by("m_criteria_id = ".$v->m_criteria_id." and  ref_criteria_id = '70' and active = 'Y' ");
							if($criteria)
							{
								foreach($criteria as $k_criteria=>$v_criteria)
								{
									$data["m_criteria_id4"] = $v_criteria->m_criteria_id;
									$data["m_criteria_code4"] = $v_criteria->ref_criteria_id;
									$data["m_criteria_name4"] = $v_criteria->m_criteria_name;
								}
							}
						}
				}
				/////////////////////////////////////////////////////////////////
				
				////////////////////Panggil Bucket Untuk Id m_criteria_name5, m_criteria_id5, m_criteria_code5/////////////////////////
				$this->load->model("Gp_user_client", "user_client");
				$user_client = $this->user_client->get_many_by('gp_personal_id',$id);
				if($user_client)
				{
					foreach($user_client as $k=>$v)
						{
							$this->load->model("M_criteria", "criteria");
							$criteria = $this->criteria->get_many_by("m_criteria_id = ".$v->m_criteria_id." and  ref_criteria_id = '71' and active = 'Y' ");
							if($criteria)
							{
								foreach($criteria as $k_criteria=>$v_criteria)
								{
									$data["m_criteria_id5"] = $v_criteria->m_criteria_id;
									$data["m_criteria_code5"] = $v_criteria->ref_criteria_id;
									$data["m_criteria_name5"] = $v_criteria->m_criteria_name;
								}
							}
						}
				}
				/////////////////////////////////////////////////////////////////
				
				////////////////////Panggil Informasi Pendidikan Terakhir Untuk Id ref_institution_type/////////////////////////
				$this->load->model("Gp_education", "education");
				$education = $this->education->get_many_by('gp_personal_id',$id);
				if($education)
				{
					foreach($education as $k_education=>$v_education)
						{
							$data["ref_institution_type"] = $v_education->ref_institution_type;
				////////////////////Panggil Nama Sekolah / Universitas Untuk Id institution_name/////////////////////////			
							$data["institution_name"] = $v_education->institution_name;
				////////////////////Panggil Jurusan Untuk Id major/////////////////////////			
							$data["major"] = $v_education->major;
				////////////////////Panggil IPK Untuk Id gpa/////////////////////////			
							$data["gpa"] = $v_education->gpa;
				////////////////////Panggil Tahun Lulus Untuk Id graduated/////////////////////////			
							$data["graduated"] = $v_education->graduated;				
						}
				}
				/////////////////////////////////////////////////////////////////
				
				
				
				}

		echo json_encode(array("success" => true, "data" => $data));
	// });
	}
	
}
