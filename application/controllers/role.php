<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}  
 
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_role", "pers");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pers->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pers->order_by('name','ASC');
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0, $echo = 0)
	{
		$this->load->model("M_role", "ms");
		try{
			$page = $this->input->get("page");
			$limit = $this->config->item("limit");
			$offset = ($page==1) ? 0 : ($page-1)*$limit;
			$this->ms->limit($limit,$offset);
		}
		catch(Exception $e)
		{
			
		}
		$this->ms->order_by("name","ASC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
		}
		else
		{
			$data["list"] = $this->ms->get($id);
		}		
		if($echo==0) echo json_encode(array("success" => true, "data" => $data["list"]));		
		else return json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_role_id","act"))))
				$arr[($k)] = $v;
		}
		$m_role_id = $this->input->post("m_role_id");
		$this->load->model("M_role", "st");
		if($m_role_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_role_id"),$arr);					
			$rs = $this->input->post("m_role_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_role", "st");
//		$rs = $this->kurs->delete($id);		
		$rs = $this->st->delete($id);					
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
	
	function getAksesMenu($id)
	{
		$this->load->model("M_role", "st");
		$role = $this->st->get($id);
		$id = $role->m_role_id;
		$name = $role->name;
		
		$this->load->model("M_menu","mn");
		
		$this->mn->order_by("m_menu_id", "ASC");
		$menu = $this->mn->get_all();
		
		foreach($menu as $k=>$v)
		{
			$this->load->model("M_role_menu","mr");
			$mrole = $this->mr->get_many_by(array("m_menu_id"=>$v->m_menu_id, "m_role_id"=>$id));			
			foreach($mrole as $k2=>$v2)
			{
				$menu[$k]->view = $v2->view;				
				$menu[$k]->edit = $v2->edit;				
				$menu[$k]->delete = $v2->delete;				
			}
		}
		
		echo json_encode(array("success" => true,"id" => $id, "name" => $name, "menu" => $menu));
	}
	
	function saveAksesMenu()
	{
		$arr = array();
		$m_role_id = $this->input->post("m_role_id");
		foreach($this->input->post() as $k=>$v)	
		{
			if(!in_array($k,array("m_role_id","name")))	
			{
				$k = explode("_",$k);
				$arr[$k[1]]["m_role_id"] = $this->input->post("m_role_id");				
				$arr[$k[1]]["m_menu_id"] = $k[1];
				$arr[$k[1]][strtoupper($k[0])] = "Y";
			}
		}
		
		$arrt = array();
		$this->load->model("M_role_menu","mr");
		
		if(!empty($arr))
		{
			$this->mr->delete_by("m_role_id", $m_role_id);
			$i = 0;
			foreach($arr as $kk=>$vv)
			{
				$rs = $this->mr->insert($vv);
				if(!$rs) $i=1;
			}
			if($i==1)
				echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));	
			else
				echo json_encode(array("success" => true, "message" => "Data Berhasil Disimpan"));	
		}
		else
		{
			echo json_encode(array("success" => true, "message" => "Data Tidak Disimpan"));	
		}
	}
	
	function getFunction($id=0)
	{
		$this->load->model("M_function", "ms");
		try{
			$page = $this->input->get("page");
			$limit = $this->config->item("limit");
			$offset = ($page==1) ? 0 : ($page-1)*$limit;
//			$this->ms->limit($limit,$offset);
		}
		catch(Exception $e)
		{
			
		}
		$this->ms->order_by("name","ASC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
		}
		else
		{
			$data["list"] = $this->ms->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}


	function getAksesFunction($id)
	{
		$this->load->model("M_role_function","mr");
		$data["list"]["role"] = $this->mr->get_many_by("m_role_id", $id);
		$dataid = json_decode($this->get($id,1));
		if($dataid->success)
		{
			$data["list"]["name"] = $dataid->data->name;
		}
		
		if($data["list"]) echo json_encode(array("success" => true, "data" => $data["list"]));				
		else echo json_encode(array("success" => false, "message" => "Record Not Found"));				
	}

	function saveAksesFunction()
	{
		$arr = array();
		$arr["m_role_id"] = $this->input->post("m_role_id");
		$m_role_id = $this->input->post("m_role_id");
		
		$m_function_id = $this->input->post("m_function_id");
		$opsi_function = $this->input->post("opsi_function");

		$this->load->model("M_role_function", "st");
		
		if(is_array($m_function_id) && !empty($m_function_id))
		{
			$this->st->delete_by(array("m_role_id" => $m_role_id));
			foreach($m_function_id as $k=>$v)
			{
				$cek = $this->st->get_by(array("m_role_id" => $m_role_id, "m_function_id" => $v));
				$arr["m_function_id"] = $v;
				if(empty($cek))
				{
					$rs = $this->st->insert($arr);
				}
				else
				{
					$rs = $this->st->update($cek->m_role_function_id, $arr);
				}
			}
		}
		else
		{
			$rs = $this->st->delete_by(array("m_role_id" => $m_role_id));
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
}