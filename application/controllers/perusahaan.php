<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Perusahaan
* 
********************************************************************/ 

class Perusahaan extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_perusahaan");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_perusahaan", "perusahaan");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->perusahaan->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[$k] = $v;
					$arrct[] = $k." ILIKE '%{$v}%'";
				}
			}
		}
		
		$this->perusahaan->order_by('m_company_id','DESC');
		$data["list"] = $this->perusahaan->search($arr)->get_all();
		//echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->perusahaan->count_by($arrct);
		$ct = ceil($ct/$limit);
		//echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_perusahaan", "perusahaan");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->perusahaan->limit($limit,$offset);
		$this->perusahaan->order_by("m_company_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->perusahaan->get_all();
		}
		else
		{
			$data["list"] = $this->perusahaan->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_company_id","act"))))
			$arr[($k)] = strtoupper($v);			
		}
		$m_company_id = $this->input->post("m_company_id");
		$this->load->model("M_perusahaan", "st");
		if($m_company_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_company_id"),$arr);					
			$rs = $this->input->post("m_company_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_perusahaan", "st");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}