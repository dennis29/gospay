<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}
	
	function getPanel()
	{
		$this->load->model("M_menu", "mn");
		$data["comboParent"] = $this->getCombo(array("name" => "parent_id",
													 "model" => "m_menu",
													 "id" => "parent_id",
													 "column" => "name_menu",
													 "class" => "form-control"));
		$data["list"] = $this->mn->get_all();
		$this->load->view("setting/view_menu_list",$data);	
	}
	
	function get()
	{


		$page = $this->input->get("page");
		$this->load->model("M_menu", "mn");

		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->mn->limit($limit,$offset);
		$this->db->order_by("m_menu_id","ASC");
		$data["list"] = $this->mn->get_all();

		$ct = $this->mn->count_all();
		$ct = ceil($ct/$limit);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if(!in_array($k,array("m_menu_id")))
				$arr[($k)] = $v;
		}
		$this->load->model("M_menu", "mn");
//		$rs = $this->mn->insert($arr);		
		if($this->input->post("m_menu_id")==0)
		{
			$rs = $this->mn->insert($arr);		
		}
		else
		{
			$rs = $this->mn->update($this->input->post("m_menu_id"),$arr);					
		}
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}

	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_menu", "mn");
		$rs = $this->exp->delete($id);		
/*		$arr["ACTIVE"]="N";	
		$rs = $this->mn->update($id,$arr);		
*/		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}