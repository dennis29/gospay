<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Overtime Manual Periode Karyawan
* 
********************************************************************/ 

class Overtime_periode extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function get()
	{
		 
	}		
		
	
	function save()
	{
		
		
	}
	
	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");
		$data["form"] = $form;
		$data["m_p_overtime_parameter_id"] = $id;
		$data["month_id"] = $this->getReferensi(array("id" => "month_id",
													   "name" => "month_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 49));


		$this->load->view("timesheet/view_overtime_periode",$data);			
	}

	
	

	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	

	function getJabatan($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_struktur_jabatan", "structure_position");
		$offset = ($page==1) ? 0 : $page*25;
		$this->structure_position->limit(25,$offset);
		$arr = array();
		$data = $this->structure_position->get_many_by("m_structure_position_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_structure_position_id;
			$arr[$i]["code"] = $v->m_structure_position_code;
			$arr[$i]["name"] = $v->m_structure_position_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}



}
