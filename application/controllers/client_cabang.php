<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 27, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Client Cabang
* 
********************************************************************/ 

class Client_cabang extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_client");	
	}


function getListClientCabang()
	{
		$page = $this->input->get("page");
		$m_client_id = $this->input->get("m_client_id");

		$this->load->model("M_client_branch", "ccabang");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->ccabang->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." = '{$v}'";
				}
			}
		}
	//	$this->ccabang->order_by('m_client_id','DESC');
		$data["list"] = $this->ccabang->carisama($m_client_id)->get_all();
	//echo $this->db->last_query();
		foreach ($data["list"] as $k=>$r) 
			{			
				$this->load->model("M_perusahaan","perusahaan");
				$perusahaan = $this->perusahaan->get($r->m_company_id);
				if($perusahaan) $data["list"][$k]->m_company_name = $perusahaan->m_company_name;	
				else $data["list"][$k]->m_company_name = "-";

			}
		$arrct["active"] = "Y";
		$ct = $this->ccabang->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_client_branch", "ccabang");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		if ($_GET)
		$this->ccabang->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->ccabang->order_by('m_client_branch_id','DESC');
		$data["list"] = $this->ccabang->search($arr)->get_all();
	//	echo $this->db->last_query();
		foreach ($data["list"] as $k=>$r) 
			{			
				$this->load->model("M_client_branch","ccabang");
				$ccabang = $this->ccabang->get($r->m_client_branch_id);
				if($ccabang) $data["list"][$k]->m_client_name = $ccabang->m_client_name;	
				else $data["list"][$k]->m_client_name = "-";

			}
		$arrct["active"] = "Y";
		$ct = $this->ccabang->count_by($arrct);
		$ct = ceil($ct/$limit);
	//	echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function getListClientCabang_a()
	{
		$page = $this->input->get("page");
		$m_client_id= $this->input->get('m_client_id');
		$this->load->model("M_client_branch", "ccabang");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		if ($_GET)
		$this->ccabang->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		$this->ccabang->order_by('m_client_branch_id','DESC');
		$data["list"] = $this->ccabang->carisama($arr)->get_all();
		$arrct["active"] = "Y";
		$ct = $this->ccabang->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function findListClientCabang()
	{
		$page = $this->input->get("page");
		$m_client_id= $this->input->get('m_client_id');
		$m_cari= $this->input->get('m_cari');
		$this->load->model("M_client_branch", "ccabang");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		if ($_GET)
		$this->ccabang->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		$this->ccabang->order_by('m_branch_name','DESC');
		$data["list"] = $this->ccabang->carimirip($m_client_id,$m_cari)->get_all();
		$arrct["active"] = "Y";
		$ct = $this->ccabang->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_client_branch", "ccabang");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->ccabang->limit($limit,$offset);
		$this->ccabang->order_by("m_client_branch_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->ccabang->get_all();
		}
		else
		{
			$data["list"] = $this->ccabang->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	} 
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_client_branch_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_client_branch_id = $this->input->post("m_client_branch_id");
		$this->load->model("M_client_branch", "st");
		if($m_client_branch_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_client_branch_id"),$arr);					
			$rs = $this->input->post("m_client_branch_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
    function save1()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_client_branch_id","act"))))
			$arr[($k)] = $v;
		}
		$m_client_branch_id = $this->input->post("m_client_branch_id");
		$this->load->model("M_client_branch", "st");
		if($m_client_branch_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_client_branch_id"),$arr);					
			$rs = $this->input->post("m_client_branch_id");					
		}
	
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}

	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_client_branch", "st");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$arr[$i]["company"] = $v->m_company_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getCabang($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_cabang", "cabang");
		$offset = ($page==1) ? 0 : $page*25;
		$this->cabang->limit(25,$offset);
		$arr = array();
		$data = $this->cabang->get_many_by("m_branch_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_branch_id;
			$arr[$i]["code"] = $v->m_branch_code;
			$arr[$i]["name"] = $v->m_branch_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


}