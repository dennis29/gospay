<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Personal Karyawan
* 
********************************************************************/ 

class Personal extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/view_list_karyawan",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Gp_personal", "pers");
		$this->pers->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();
		if(is_array($data["list"]))
		{
			foreach($data["list"] as $k=>$r)
			{
				$this->load->model("M_sub_district", "ms");
				$rw = $this->ms->get($r->m_sub_district_id);
				if($rw) $data["list"][$k]->m_sub_district_id = $rw->name;	
	
				$this->load->model("M_city", "mc");
				$rw1 = $this->mc->get($r->m_city_id);
				if($rw1) $data["list"][$k]->m_city_id = $rw1->name;	
	
				$this->load->model("M_referensi_list", "ml");
				$rw2 = $this->ml->get($r->gender_id);
				if($rw2) $data["list"][$k]->gender_id = $rw2->name;	
	
				$this->load->model("M_state", "mst");
				$rw3 = $this->mst->get($r->m_state_id);
				if($rw3) $data["list"][$k]->m_state_id = $rw3->name;

				$this->load->model("M_faskes","faskes");
				$faskes = $this->faskes->get($r->m_faskes_id);
				if($faskes) $data["list"][$k]->m_faskes_name = $faskes->m_faskes_name;	
				else $data["list"][$k]->m_faskes_name = "-";

				$this->load->model("M_cabang","cabang");
				$cabang = $this->cabang->get($r->m_branch_id);
				if($cabang) $data["list"][$k]->m_branch_name = $cabang->m_branch_name;	
				else $data["list"][$k]->m_branch_name = "-";

				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;	
				else $data["list"][$k]->m_client_name = "-";

				$this->load->model("M_lokasi","location");
				$location = $this->location->get($r->m_location_id);
				if($location) $data["list"][$k]->m_location_name = $location->m_location_name;	
				else $data["list"][$k]->m_location_name = "-";

				$this->load->model("M_unit_kerja","unit");
				$unit = $this->unit->get($r->m_unit_id);
				if($unit) $data["list"][$k]->m_unit_name = $unit->m_unit_name;	
				else $data["list"][$k]->m_unit_name = "-";

				$this->load->model("M_kriteria","kriteria");
				$kriteria = $this->kriteria->get($r->m_criteria_id);
				if($kriteria) $data["list"][$k]->m_criteria_name = $kriteria->m_criteria_name;	
				else $data["list"][$k]->m_criteria_name = "-";

				$this->load->model("M_struktur_jabatan","jabatan");
				$jabatan = $this->jabatan->get($r->m_structure_position_id);
				if($jabatan) $data["list"][$k]->m_structure_position_name = $jabatan->m_structure_position_name;	
				else $data["list"][$k]->m_structure_position_name = "-";	
				
			}
			
		}
		else
		{
			
		}
		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->pers->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_personal", "karyawan");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->karyawan->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->karyawan->order_by('gp_personal_id','ASC');
		$data["list"] = $this->karyawan->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
		{	
			$this->load->model("M_cabang","cabang");
			$cabang = $this->cabang->get($r->m_branch_id);
			if($cabang) $data["list"][$k]->m_branch_name = $cabang->m_branch_name;	
			else $data["list"][$k]->m_branch_name = "-";

			$this->load->model("M_client","client");
			$client = $this->client->get($r->m_client_id);
			if($client) $data["list"][$k]->m_client_name = $client->m_client_name;	
			else $data["list"][$k]->m_client_name = "-";

			$this->load->model("M_area","area");
			$area = $this->area->get($r->m_area_id);
	//	echo $this->db->last_query();
			if($area) 
			{
				$this->load->model("M_lokasi","location");
				$location = $this->location->get($area->m_location_id);
	//	echo $this->db->last_query();
				if($location) $data["list"][$k]->m_location_name = $location->m_location_name;	
				else $data["list"][$k]->m_location_name = "-";
			}

		}
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->karyawan->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 	
	function  cari_nrk($id=''){
	$data='';
	$cek='';
	$max='';
	$nrk='';
	$this->load->model('Gp_personal','personal');
	$this->db->select("max(nrk)");
	$data=$this->personal->get_many_by("m_client_id  = '".$id."'");
	foreach($data as $k=>$v)
	{
	  $max = $v->max;
	}
	
	echo json_encode(array("success" => true, 's' => $max));
	}
	function save()
	{
		$start_permanen='';
		$arr = array();
		$arr2 = array();
		$arr3 = array();
		$arr4 = array();
		$arr5 = array();
		$arr6 = array();
		$arr7 = array();
		$vkaryawan = $this->config->item("karyawan");
		$cek = 0;
		$msg = "";
		$rs = "";
		$rs2 = "";
		$rs3 = "";
		$rs4 = "";
		$rs5 = "";
		$rs6 = "";
		$rs7 = "";
		$bisaSave=1;
		//echo 'nrk = '.$this->input->get('nrk');
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k,array("gp_personal_id","m_sub_district_name",'gpa','start_permanen_date',
								  "m_district_name","m_city_name",'birth_place_name','m_birth_place_id',"m_state_name","address",
								  'rt','rw','m_district_id','m_sub_district_id',
								  'm_city_id','m_state_id','zipe_code','handphone','home_phone'
								  ,'m_faskes_id','dokgi','m_branch_name','m_client_name',
								  'm_location_name','m_location_id','wil_name','area_name','m_unit_name',
								  'm_structure_position_name','pkwt_no','pkwt_date','start_contract_date',
								  'stop_contract_date','ref_status_pkwt_id','job_no','job_date','birth_place',
								  'birth_date','gender_id','m_criteria_name','m_criteria_name2','m_criteria_name3',
								  'm_criteria_name4','m_criteria_name5','m_criteria_id','m_criteria_id2','m_criteria_id3',
								  'm_criteria_id4','m_criteria_id5','m_criteria_code','m_criteria_code2','m_criteria_code3',
								  'm_criteria_code4','m_criteria_code5','ref_institution_type','institution_name','major','ipk',
								  'graduated',
								  'm_faskes_name',"religion_id","idcard_sub_district_name")))
				{
				
				$arr[$k] = strtoupper($v);
				
				}
			
			if((isset($vkaryawan[$k]["required"])) && $vkaryawan[$k]["required"]=="required")
			{
				if(in_array($v,array("0","","false","null","NULL")))
				{
					$cek = 1;
					$msg .= $vkaryawan[$k]["name"]." Wajib Diisi\n";
					$bisaSave=0;
				}
			}
		}
		if ($bisaSave==1)
		{
		$arr['nrk'] = $this->input->get('nrk');
		$arr['m_client_id'] = $this->input->get('m_client_id');
		$pecah=explode('-',$this->input->get('start_permanen_date'));
		$start_permanen=$pecah[2]."-".$pecah[1]."-".$pecah[0];
		$arr['start_permanen'] = $start_permanen;
				//echo 'NRK = '.$k;
		//echo 'NRK =='.$arr['nrk'];
		$this->load->model("Gp_personal", "karyawan");
		$rs = $this->karyawan->insert($arr);
		//Batasi, Load Dan Save Ke Tabel GP_EDUCATION
		foreach($this->input->get() as $k=>$v)
		{
			if(in_array($k,array('ref_institution_type','institution_name','major','graduated','gpa')))
				{
				$arr2[$k] = strtoupper($v);
				}
				
			
		}
		$arr2['gp_personal_id'] = $rs;
		$this->load->model("Gp_education", "pendidikan");
		$rs2 = $this->pendidikan->insert($arr2);
		//Batasi, Load Dan Save Ke Tabel GP_FAMILY
		foreach($this->input->get() as $k=>$v)
		{
			if(in_array($k,array('m_birth_place_id','family_name','birth_date','gender_id','bpjskes_no','religion_id','ref_bpjskes_status_id')))
				{
				if ($k=='gender_id') $k='ref_gender_id';
				if ($k=='religion_id') $k='ref_religion_id';
				if ($k=='family_name') $k='personal_name';
				$arr3[$k] = strtoupper($v);
				}
				
			
		}
		$arr3['ref_relation_id'] = '34';
		$arr3['gp_personal_id'] = $rs;
		$this->load->model("Gp_family", "keluarga");
		$rs3 = $this->keluarga->insert($arr3);
		//Batasi, Load Dan Save Ke Tabel GP_ADDRESS
		foreach($this->input->get() as $k=>$v)
		{
			if(in_array($k,array("address",'rt','rw','m_sub_district_id','m_district_id','m_city_id','m_state_id','zipe_code',
			'home_phone','handphone','m_faskes_id','dokgi')))
				{
				$arr4[$k] = strtoupper($v);
				}
				
			
		}
		$arr4['gp_personal_id'] = $rs;
		$this->load->model("gp_address", "alamat");
		$rs4 = $this->alamat->insert($arr4);
		//Batasi, Load Dan Save Ke Tabel GP_ATENSI
		foreach($this->input->get() as $k=>$v)
		{
			if(in_array($k,array("add_date",'title')))
				{
				$arr5[$k] = strtoupper($v);
				}
				
			
		}
		$arr5['gp_personal_id'] = $rs;
		$this->load->model("gp_atensi", "atensi");
		$rs5 = $this->atensi->insert($arr5);
		//Batasi, Load Dan Save Ke Tabel GP_PKWT
		foreach($this->input->get() as $k=>$v)
		{
			if(in_array($k,array("start_contract_date",'stop_contract_date','ref_status_pkwt_id','pkwt_no','pkwt_date'
			,'job_no','job_date'
			)))
				{
				$arr6[$k] = strtoupper($v);
				}
				
			
		}
		$arr6['gp_personal_id'] = $rs;
		$this->load->model("gp_pkwt", "pkwt");
		$rs6 = $this->pkwt->insert($arr6);
		$this->load->model("Gp_user_client", "klien");
		$this->load->model("m_criteria", "kriteria");
		// Cek Apakah Data Input User Baru?, Jika Ya Maka Simpan 
		if ($this->input->get('m_criteria_code') == 0) 
		{
		$arr7['ref_criteria_id'] = 67;
		$arr7['m_criteria_name'] = $this->input->get('m_criteria_name');;
		$arr7['active'] = "Y";
		$rs7 = $this->kriteria->insert($arr7);
		//Simpan Ke GP_USER_CLIENT
		unset($arr7);
		$arr7 = array();
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $rs7;
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		///
		}
		else
		{
		$arr7 = array();
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $this->input->get('m_criteria_id');
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		}
		unset($arr7);
		$arr7 = array();
		////////////////
		// Cek Apakah Data Input HRBP Baru?, Jika Ya Maka Simpan 
		if ($this->input->get('m_criteria_code2') == 0) 
		{
		$arr7['ref_criteria_id'] = 68;
		$arr7['m_criteria_name'] = $this->input->get('m_criteria_name2');;
		$arr7['active'] = "Y";
		$rs7 = $this->kriteria->insert($arr7);
		//Simpan Ke GP_USER_CLIENT
		unset($arr7);
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $rs7;
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		///
		}
		else
		{
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $this->input->get('m_criteria_id2');
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		}
		unset($arr7);
		$arr7 = array();
		////////////////
		// Cek Apakah Data Input Cost Center Baru?, Jika Ya Maka Simpan 
		if ($this->input->get('m_criteria_code3') == 0) 
		{
		$arr7['ref_criteria_id'] = 69;
		$arr7['m_criteria_name'] = $this->input->get('m_criteria_name3');;
		$arr7['active'] = "Y";
		$rs7 = $this->kriteria->insert($arr7);
		//Simpan Ke GP_USER_CLIENT
		unset($arr7);
		$arr7 = array();
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $rs7;
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		///
		}
		else
		{
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $this->input->get('m_criteria_id3');
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		}
		unset($arr7);
		$arr7 = array();
		////////////////
		////////////////
		// Cek Apakah Data Input Asuransi Baru?, Jika Ya Maka Simpan 
		if ($this->input->get('m_criteria_code4') == 0) 
		{
		$arr7['ref_criteria_id'] = 70;
		$arr7['m_criteria_name'] = $this->input->get('m_criteria_name4');;
		$arr7['active'] = "Y";
		$rs7 = $this->kriteria->insert($arr7);
		//Simpan Ke GP_USER_CLIENT
		unset($arr7);
		$arr7 = array();
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $rs7;
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		///
		}
		else
		{
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $this->input->get('m_criteria_id4');
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		}
		unset($arr7);
		$arr7 = array();
		////////////////
		////////////////
		// Cek Apakah Data Input Bucket Baru?, Jika Ya Maka Simpan 
		if ($this->input->get('m_criteria_code5') == 0) 
		{
		$arr7['ref_criteria_id'] = 71;
		$arr7['m_criteria_name'] = $this->input->get('m_criteria_name5');;
		$arr7['active'] = "Y";
		$rs7 = $this->kriteria->insert($arr7);
		//Simpan Ke GP_USER_CLIENT
		unset($arr7);
		$arr7 = array();
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $rs7;
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		///
		}
		else
		{
		$arr7['gp_personal_id'] = $rs;
		$arr7['m_criteria_id'] = $this->input->get('m_criteria_id5');
		$arr7['tmt_date'] = date("d-m-Y H:i:s");
		$arr7['active'] = "Y";
		$this->klien->insert($arr7);
		}
		////////////////
		/*
		$this->db->all_query();
		
		var_dump( $this->db );
		
		*/
		}
		
		echo json_encode(array("success" => true, "id" => $rs, "msg" => $msg));
	}
	
	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vkaryawan"] = $this->config->item('karyawan');
		$data["vfamily"] = $this->config->item('family');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["gender_id"] = $this->getReferensi(array("id" => "gender_id",
													   "name" => "gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender

		$data["religion_id"] = $this->getReferensi(array("id" => "religion_id",
													   "name" => "religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReferensi(array("id" => "ref_marital_status_id",
													   "name" => "ref_marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 3));

		$data["blood_type_id"] = $this->getReferensi(array("id" => "blood_type_id",
													   "name" => "ref_blood_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 4));

		$data["ref_bpjskes_status_id"] = $this->getReferensi(array("id" => "ref_bpjskes_status_id",
													   "name" => "ref_bpjskes_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 5));

		$data["ref_status_kerja_id"] = $this->getReferensi(array("id" => "ref_status_kerja_id",
													   "name" => "ref_status_kerja_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 6));
		
		$data["ref_team_work_id"] = $this->getReferensi(array("id" => "ref_team_work_id",
													   "name" => "ref_team_work_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 23));

		$data["ref_overtime_status_id"] = $this->getReferensi(array("id" => "ref_overtime_status_id",
													   "name" => "ref_overtime_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 35));

		$data["ref_institution_type"] = $this->getReferensi(array("id" => "ref_institution_type",
													   "name" => "ref_institution_type",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 19));

		$data["ref_stat_ptkp"] = $this->getReferensi(array("id" => "ref_stat_ptkp",
													   "name" => "ref_stat_ptkp",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 20));

		$data["ref_status_pkwt_id"] = $this->getReferensi(array("id" => "ref_status_pkwt_id",
													   "name" => "ref_status_pkwt_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 21));

		$data["company_id"] = $this->getCombo(array("name" => "m_company_id",
															 "model" => "m_perusahaan",
															 "id" => "m_company_id",
															 "column" => "m_company_name",
															 "class" => "form-control"));

		$data["country_id"] = $this->getCombo(array("name" => "m_country_id",
															 "model" => "m_country",
															 "id" => "m_country_id",
															 "column" => "name",
															 "class" => "form-control"));

		$data["bank_id"] = $this->getCombo(array("name" => "m_bank_id",
															 "model" => "m_bank",
															 "id" => "m_bank_id",
															 "column" => "m_bank_name",
															 "class" => "form-control"));



		$this->load->view("karyawan/view_personal",$data);			
	}

	
	
	function getCity($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_city", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->get_many_by("name  ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_city_id;
			$arr[$i]["name"] = $v->name ;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getSubDistrict($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_sub_district", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(50,$offset);
		$arr = array();
		$q = urldecode($q);
		$q = str_replace("-"," ",$q);
		$data = $this->ml->getTypeahead($q);
		$query = $this->db->last_query();
/*		$query = $this->db->query("SELECT ms.* from ");
		$data = $this->ml->get_many_by("NAME ILIKE '%{$q}%'");
*/	
		$i = 0;
//		echo urldecode($q);
		if($data)
		{
			foreach($data as $k=>$v)
			{
				$arr[$i]["id"] = $v->m_sub_district_id;
				$arr[$i]["m_district_id"] = $v->m_district_id;
	
				$arr[$i]["name"] = strtoupper($v->sname."-".$v->dname);
				$i++;
			}
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr, "q" => $query));
	}

	function getAddress($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_district", "md");
		$md = $this->md->get($q);
		$arr = array();		
		
		if($this->md->count_by("m_district_id", $q)>0)
		{
			$arr["m_district_name"] = strtoupper($md->name);
			$arr["m_district_id"] = $md->m_district_id;
			$this->load->model("M_city", "mc");
			$mc = $this->mc->get($md->m_city_id);

			if($this->mc->count_by("m_city_id", $md->m_city_id)>0)
			{
				$arr["m_city_name"] = strtoupper($mc->name);
				$arr["m_city_id"] = $mc->m_city_id;
		
				$this->load->model("M_state", "mst");
				$mst = $this->mst->get($mc->m_state_id);

				if($this->mst->count_by("m_state_id", $mc->m_state_id)>0)
				{
					$arr["m_state_name"] = strtoupper($mst->name);
					$arr["m_state_id"] = $mst->m_state_id;
				}
			}
		}	
		
	//	echo $this->db->last_query();
		if(empty($arr))
			echo json_encode(array(0));
		else
			echo json_encode($arr);
	}

	function getFaskes($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_faskes", "faskes");
		$offset = ($page==1) ? 0 : $page*25;
		$this->faskes->limit(25,$offset);
		$arr = array();
		$data = $this->faskes->get_many_by("m_faskes_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_faskes_id;
			$arr[$i]["code"] = $v->m_faskes_code;
			$arr[$i]["name"] = $v->m_faskes_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getCabang($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_cabang", "cabang");
		$offset = ($page==1) ? 0 : $page*25;
		$this->cabang->limit(25,$offset);
		$arr = array();
		$data = $this->cabang->get_many_by("m_branch_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_branch_id;
			$arr[$i]["code"] = $v->m_branch_code;
			$arr[$i]["name"] = $v->m_branch_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'" );
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["m_client_start_pks_date"] = $v->m_client_start_pks_date;
			$arr[$i]["m_client_end_pks_date"] = $v->m_client_end_pks_date;
			$arr[$i]["name"] = " (  ".$v->m_client_code."  )  ".$v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getLokasi($q='',$id_client='')
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_area", "area");
		$offset = ($page==1) ? 0 : $page*25;
		//$this->area->limit(25,$offset);
		$arr = array();
		$this->db->select('m_location.m_location_name,m_location.m_location_id')->group_by('m_location.m_location_name,m_location.m_location_id');
		$data = $this->area->get_many_by("m_area.m_client_id = '".$id_client."' and m_location_name ILIKE '%{$q}%'" );
		
		$i = 0;
		foreach($data as $k=>$v)
		{	
			//$this->load->model("M_lokasi", "lokasi");
			//$this->db->where("m_location_name ILIKE '%{$q}%'" );
			$arr[$i]['id'] = $v->m_location_id;
			$arr[$i]['name'] = $v->m_location_name;
			$i++;
		}
		
		//echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	
	function getLokasiTerpisah($q='')
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_area", "area");
		$offset = ($page==1) ? 0 : $page*25;
		//$this->area->limit(25,$offset);
		$arr = array();
		$this->db->select('m_location.m_location_name,m_location.m_location_id')->group_by('m_location.m_location_name,m_location.m_location_id');
		$data = $this->area->get_many_by("m_location_name ILIKE '%{$q}%'" );
		
		$i = 0;
		foreach($data as $k=>$v)
		{	
			//$this->load->model("M_lokasi", "lokasi");
			//$this->db->where("m_location_name ILIKE '%{$q}%'" );
			$arr[$i]['id'] = $v->m_location_id;
			$arr[$i]['name'] = $v->m_location_name;
			$i++;
		}
		
		//echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getWilayah($q='',$id_client='',$id_area='')
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_area", "area");
		$offset = ($page==1) ? 0 : $page*25;
		//$this->area->limit(25,$offset);
		$arr = array();
		//$this->db->select('m_location.m_location_name,m_location.m_location_id')->group_by('m_location.m_location_name,m_location.m_location_id');
		$data = $this->area->get_many_by("m_area.m_client_id = '".$id_client."'and m_area.m_location_id = '".$id_area."' and wil_name ILIKE '%{$q}%'" );
		//echo $this->db->last_query();
		$i = 0;
		foreach($data as $k=>$v)
		{	
			//$this->load->model("M_lokasi", "lokasi");
			//$this->db->where("m_location_name ILIKE '%{$q}%'" );
			$arr[$i]['id'] = $v->m_area_id;
			$arr[$i]['name'] = $v->wil_name." ( ".$v->area_name." ) ";
			$arr[$i]['area_name'] = $v->area_name;
			$i++;
		}
		
		//echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	

	function getUnit($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_unit_kerja", "unit");
		$offset = ($page==1) ? 0 : $page*25;
		$this->unit->limit(25,$offset);
		$arr = array();
		$data = $this->unit->get_many_by("m_unit_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_unit_id;
			$arr[$i]["code"] = $v->m_unit_code;
			$arr[$i]["name"] = $v->m_unit_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getJabatan($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_struktur_jabatan", "structure_position");
		$offset = ($page==1) ? 0 : $page*25;
		$this->structure_position->limit(25,$offset);
		$arr = array();
		$data = $this->structure_position->get_many_by("m_structure_position_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_structure_position_id;
			$arr[$i]["code"] = $v->m_structure_position_code;
			$arr[$i]["name"] = $v->m_structure_position_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getKriteriaUser($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%' and ref_criteria_id = '67'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getKriteria($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$ref_criteria_id=$this->input->get('ref_criteria_id');
		if ($ref_criteria_id) $this->db->where('ref_criteria_id',$ref_criteria_id);
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getKriteriaHRBP($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%' and ref_criteria_id = '68'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getKriteriaCostCenter($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%' and ref_criteria_id = '69'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getKriteriaAsuransi($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%' and ref_criteria_id = '70'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
	
	function getKriteriaBucket($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_criteria", "kriteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->kriteria->limit(25,$offset);
		$arr = array();
		$data = $this->kriteria->get_many_by("m_criteria_name ILIKE '%{$q}%' and ref_criteria_id = '71'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["code"] = $v->ref_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}





}
