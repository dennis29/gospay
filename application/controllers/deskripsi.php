<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 21, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Deskripsi Transaksi
* 
********************************************************************/ 

class Deskripsi extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$data["tax_type_id"] = $this->getReferensi(array("id" => "ref_tax_type_id",
													   "name" => "ref_tax_type_id",
													   "class" => "form-control",
													   "refid" => 25)); // 1 = tax_type

		$data["income_type_id"] = $this->getReferensi(array("id" => "ref_income_type_id",
													   "name" => "ref_income_type_id",
													   "class" => "form-control",
													   "refid" => 26)); // 1 = income_type

		$data["transaction_type_id"] = $this->getReferensi(array("id" => "ref_transaction_type_id",
													   "name" => "ref_transaction_type_id",
													   "class" => "form-control",
													   "refid" => 27)); // 1 = transaction_type_id

		$this->load->view("master/view_deskripsi", $data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_description_transaction", "transaksi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->transaksi->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->transaksi->order_by('m_p_description_transaction_id','DESC');
		$data["list"] = $this->transaksi->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{			
				$this->load->model("M_referensi_list", "pjk");
				$tax = $this->pjk->get($r->ref_tax_type_id);
				if($tax) $data["list"][$k]->ref_tax_type_id = $tax->name;
				else $data["list"][$k]->ref_tax_type_id = "";
				
				$this->load->model("M_referensi_list", "pd");
				$income = $this->pd->get($r->ref_income_type_id);
				if($income) $data["list"][$k]->ref_income_type_id = $income->name;
				else $data["list"][$k]->ref_income_type_id = "";

				$this->load->model("M_referensi_list", "tr");
				$trans = $this->tr->get($r->ref_transaction_type_id);
				if($trans) $data["list"][$k]->ref_transaction_type_id = $trans->name;
				else $data["list"][$k]->ref_transaction_type_id = "";				
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->transaksi->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_description_transaction", "transaksi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->transaksi->limit($limit,$offset);
		$this->transaksi->order_by("m_p_description_transaction_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->transaksi->get_all();
		}
		else
		{
			$data["list"] = $this->transaksi->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{

        
		$arr = array();
		$m_p_description_transaction_id = $this->input->post("m_p_description_transaction_id");
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_description_transaction_id","act",'tipe_transaksi','jenis_pajak','transaction_name_hidden'))))
			{
				if ( $k != 'transaction_name_hidden') 
				{
					if ( $k == 'm_p_description_transaction_name' ) 
					{
						$v = $this->input->post("m_p_descriotion_name"). " ( " . $this->input->post("jenis_pajak"). " - " . $this->input->post("tipe_transaksi"). " )";
						$arr[($k)] = $v;
					}
					else $arr[($k)] = strtoupper($v);

				}
			}
		}
		
		$this->load->model("M_p_description_transaction", "transaksi");
		if($m_p_description_transaction_id==0)
		{

			$rs = $this->transaksi->insert($arr);		
		}
		else
		{
			$rs = $this->transaksi->update($this->input->post("m_p_description_transaction_id"),$arr);					
			$rs = $this->input->post("m_p_description_transaction_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_description_transaction", "transaksi");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->transaksi->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function type_name($id="")
	{
		$id = $this->input->get("id");
		$this->load->model("M_referensi_list", "referensi");
		
		$data["list"] = $this->referensi->get($id);
		
	//	echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"]));
	}



}