<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Overtime
* 
********************************************************************/ 

class Overtime extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		
		$data["ref_type_tarif_id"] = $this->getReferensi(array("id" => "ref_type_tarif_id",
													   "name" => "ref_type_tarif_id",
													   "class" => "form-control",
													   "refid" => 28)); // 1		

		$data["tax_type_id"] = $this->getReferensi(array("id" => "ref_tax_type_id",
													   "name" => "ref_tax_type_id",
													   "class" => "form-control",
													   "refid" => 25)); // 

		$data["ref_entitled_overtime_id"] = $this->getReferensi(array("id" => "ref_entitled_overtime_id",
													   "name" => "ref_entitled_overtime_id",
													   "class" => "form-control",
													   "refid" => 47)); // 

		$data["ref_overtime_type_id"] = $this->getReferensi(array("id" => "ref_overtime_type_id",
													   "name" => "ref_overtime_type_id",
													   "class" => "form-control",
													   "refid" => 48)); // 

		$this->load->view("master/view_overtime",$data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_overtime_parameter", "tc");
		$this->db->where("\"m_p_tarif_transaction\".\"active\" = 'Y'");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tc->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}

					//$arr[($k)] = $v;
					//$arrct[] = ($k)." ILIKE '%{$v}%'";
					//if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','M_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
				}
				
			}
		}
		$this->tc->order_by('M_p_overtime_parameter.M_p_overtime_parameter_id','DESC');
		$data["list"] = $this->tc->search($arr)->get_all();
//echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{	
			/*
				$this->load->model("M_p_overtime_parameter","tc");
				$tc = $this->tc->get($r->m_client_id);
				if($tc) $data["list"][$k]->m_client_name = $tc->m_client_name;
			*/
			    $this->load->model("M_p_tarif","tarif");
				$tarif = $this->tarif->get($r->m_p_tarif_id);
				if($tarif) $data["list"][$k]->m_p_tarif_name = $tarif->m_p_tarif_name;

				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;

				$this->load->model("Gp_personal","personal");
				$personal = $this->personal->get($r->gp_personal_id);
				if($personal) 
				{
					$data["list"][$k]->personal_name = $personal->personal_name;
					$data["list"][$k]->nrk = $personal->nrk;
				}

				$this->load->model("M_p_description_transaction","transaction");
				$transaction = $this->transaction->get($r->m_p_description_transaction_id);
				if($transaction) 
				{
					$data["list"][$k]->m_p_description_transaction_name = $transaction->m_p_description_transaction_name;
					$data["list"][$k]->m_p_description_transaction_code = $transaction->m_p_description_transaction_code;
				}

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_category_id);
				if($referensi) $data["list"][$k]->kategori = $referensi->name;
				
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_tax_type_client_id);
				if($referensi) $data["list"][$k]->jenis_pajak = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_magnitude_id);
				if($referensi) $data["list"][$k]->besaran = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_multiplier_id);
				if($referensi) $data["list"][$k]->pengali = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_type_transaction_id);
				if($referensi) $data["list"][$k]->jenis_transaksi = $referensi->name;

				
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->tc->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 



	function getList1()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_overtime_parameter", "tc");
		$this->db->where("\"m_p_tarif_transaction\".\"active\" = 'Y'");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tc->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}

					//$arr[($k)] = $v;
					//$arrct[] = ($k)." ILIKE '%{$v}%'";
					if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','M_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
				}
				
			}
		}
		$this->tc->order_by('M_p_overtime_parameter.M_p_overtime_parameter_id','DESC');
		$data["list"] = $this->tc->search($arr)->get_all();
//echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{	
			/*
				$this->load->model("M_p_overtime_parameter","tc");
				$tc = $this->tc->get($r->m_client_id);
				if($tc) $data["list"][$k]->m_client_name = $tc->m_client_name;
			*/
			    $this->load->model("M_p_tarif","tarif");
				$tarif = $this->tarif->get($r->m_p_tarif_id);
				if($tarif) $data["list"][$k]->m_p_tarif_name = $tarif->m_p_tarif_name;

				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;

				$this->load->model("Gp_personal","personal");
				$personal = $this->personal->get($r->gp_personal_id);
				if($personal) 
				{
					$data["list"][$k]->personal_name = $personal->personal_name;
					$data["list"][$k]->nrk = $personal->nrk;
				}

				$this->load->model("M_p_description_transaction","transaction");
				$transaction = $this->transaction->get($r->m_p_description_transaction_id);
				if($transaction) 
				{
					$data["list"][$k]->m_p_description_transaction_name = $transaction->m_p_description_transaction_name;
					$data["list"][$k]->m_p_description_transaction_code = $transaction->m_p_description_transaction_code;
				}

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_category_id);
				if($referensi) $data["list"][$k]->kategori = $referensi->name;
				
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_tax_type_client_id);
				if($referensi) $data["list"][$k]->jenis_pajak = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_magnitude_id);
				if($referensi) $data["list"][$k]->besaran = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_multiplier_id);
				if($referensi) $data["list"][$k]->pengali = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_type_transaction_id);
				if($referensi) $data["list"][$k]->jenis_transaksi = $referensi->name;

				
			}
//		echo $this->db->last_query();
if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','M_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		$arrct["M_p_overtime_parameter.active"] = "Y";
		$ct = $this->tc->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 


 	function get()
	{
		$id1 = $this->input->get("id1");
		$id = $this->input->get("id");
		$page = $this->input->get("page");
		$this->load->model("M_p_overtime_parameter", "tc");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->tc->limit($limit,$offset);
		$this->tc->db->join('m_p_tarif','M_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		$this->tc->db->join('m_client','M_p_overtime_parameter.m_client_id=m_client.m_client_id');
		$this->tc->db->join('m_p_description_transaction','m_p_description_transaction.m_p_description_transaction_id=m_p_tarif_transaction.m_p_description_transaction_id');
	//	$this->tc->db->join('gp_personal','gp_personal.gp_personal_id=M_p_overtime_parameter.gp_personal_id','left');
		$this->tc->order_by("M_p_overtime_parameter.M_p_overtime_parameter_id","DESC");
        
		if($id==0)
		{
			$data["list"] = $this->tc->get_all();
		}
		else
		{
			$this->tc->db->where('"m_p_tarif_transaction"."m_p_tarif_transaction_id" = ' ."'$id1'");
			$data["list"] = $this->tc->get($id);
		}	
//	echo $this->db->last_query();
		
		foreach ($data["list"] as $k => $r) 
		{
		
			$this->load->model("Gp_personal","personal");
			$personal = $this->personal->get($data["list"]->gp_personal_id);
			if($personal) 
			{
				$data["list"][$k]->personal_name = $personal->personal_name;
				$data["list"][$k]->nrk = $personal->nrk;
			}
			
		}
	
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		$awhere='';
		$arr1 = array();
		$faktor1 = $this->input->post("faktor1");
		$faktor2 = $this->input->post("faktor2");
		$value = $faktor1 . '/' . $faktor2;
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("M_p_overtime_parameter_id","act","m_p_tarif_name","m_client_name","m_p_description_transaction_name","gp_personal_name","faktor1","faktor2","m_p_tarif_transaction_id"))))
			{
				if ( $k == 'm_p_tarif_id' || $k == 'm_client_id' || $k == 'ref_category_id' || $k == 'gp_personal_id' || $k == 'ref_tax_type_client_id' )
				{
					$arr[($k)] = strtoupper($v);
				    $awhere .=  strtoupper($v).',';
				}
				else
				     $arr1[($k)] = strtoupper($v);
				
			}
		}

		if ( $faktor1 !="" || $faktor1 != 0 ) $arr1[("factor_value")] = $value;

		$M_p_overtime_parameter_id = $this->input->post("M_p_overtime_parameter_id");
		$this->load->model("M_p_overtime_parameter", "tarifclient");
		//echo $M_p_overtime_parameter_id;
		if($M_p_overtime_parameter_id==0)
		{  
			$awhere= substr($awhere,0,-1);

            $rs = $this->tarifclient->getdata($awhere); 

			if ( $rs == 0 )
				$rs = $this->tarifclient->insert($arr);	

		    $arr1[("M_p_overtime_parameter_id")] = $rs;
			$m_p_tarif_transaction_id = $this->input->post("m_p_tarif_transaction_id");
			$this->load->model("M_p_tarif_transaction", "tariftransaksi");
			if($m_p_tarif_transaction_id==0)
			{
				$rs1 = $this->tariftransaksi->insert($arr1);		
			}
			else
			{
				$rs1 = $this->tariftransaksi->update($this->input->post("m_p_tarif_transaction_id"),$arr);					
				$rs1= $this->input->post("m_p_tarif_transaction_id");					
			}
			    
			
		}
		else
		{
			$rs = $this->tarifclient->update($this->input->post("M_p_overtime_parameter_id"),$arr);					
			$rs = $this->input->post("M_p_overtime_parameter_id");
			$this->load->model("M_p_tarif_transaction", "tariftransaksi");
			$rs = $this->tariftransaksi->update($this->input->post("m_p_tarif_transaction_id"),$arr1);					
			$rs = $this->input->post("m_p_tarif_transaction_id");
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id1 = $this->input->get("id1");
		$id = $this->input->get("id");
		$this->load->model("M_p_overtime_parameter", "tarifclient");
		$this->load->model("M_p_tarif_transaction", "tariftransaction");

		$rs = $this->tarifclient->gettarifclient($id);
//echo $this->db->last_query();
//echo 'rows = '.$rs;
		
		$arr["active"]="N";
		if ( $rs == 1 )
			$rs = $this->tarifclient->update($id,$arr);

		$rs = $this->tariftransaction->update($id1,$arr);

		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getTarif($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_p_tarif", "tarif");
		$offset = ($page==1) ? 0 : $page*25;
		$this->tarif->limit(25,$offset);
		$arr = array();
		$data = $this->tarif->get_many_by("m_p_tarif_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_p_tarif_id;
			$arr[$i]["code"] = $v->m_p_tarif_id;
			$arr[$i]["name"] = $v->m_p_tarif_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}



	function getPersonal($q='',$r='')
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("Gp_personal", "personal");
		$offset = ($page==1) ? 0 : $page*25;
		$this->personal->limit(25,$offset);
		$arr = array();
		$data = $this->personal->get_many_personal("(nrk ILIKE '%{$q}%' OR personal_name ILIKE '%{$q}%') and m_client_id = '{$r}'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->gp_personal_id;
			$arr[$i]["name"] = '( '.$v->nrk . ' )   ' . $v->personal_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


}