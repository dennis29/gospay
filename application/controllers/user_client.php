<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller User Klien Karyawan
* 
********************************************************************/ 

class User_client extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
function get($id)
	{
		$this->load->model("Gp_user_client", "user_client");
		$this->db->order_by('gp_user_client_id','DESC');
		$data = $this->user_client->get_many_personal('gp_user_client_id',$id);
		foreach($data as $k_user_client => $v_user_client)
		{
			$this->load->model('M_kriteria','kriteria');
			$kriteria=$this->kriteria->get($v_user_client->m_criteria_id);
			if ($kriteria)
			{
			$arr['ref_criteria_id'] = $kriteria->ref_criteria_id;
			$arr['m_criteria_name-user'] = $kriteria->m_criteria_name;
			}
			$arr['m_criteria_id-user'] = $v_user_client->m_criteria_id;
			$arr['note-user'] = $v_user_client->note;
			
		////
			
			
	
		}
		echo json_encode(array("success" => true, "data" => $arr));	
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_education", "education");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->education->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->education->order_by('gp_education_id','ASC');
		$data["list"] = $this->education->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->education->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 
	function getListById($id='')
	{
		$page = $this->input->get("page");
		$ref_criteria=$this->input->get('ref_criteria_id');
		if ($ref_criteria) $this->db->join('m_criteria','gp_user_client.m_criteria_id=m_criteria.m_criteria_id')->where('ref_criteria_id',$ref_criteria);
		$this->load->model("Gp_user_client", "user_client");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$data = array();
		$arrct = array();
		$arr = array();
		$this->user_client->limit($limit,$offset);
		if ($id != '')
		{
		$this->user_client->order_by('gp_user_client_id','ASC');
		$data = $this->user_client->get_many_personal('gp_personal_id',$id);
		foreach($data as $k_user_client => $v_user_client)
		{
			// Panggil Id Serial
			$arr[$i]["gp_user_client_id"] = $v_user_client->gp_user_client_id;
			//////////
			// Panggil Kriteria
			$this->load->model("M_Kriteria", "criteria");
			$criteria = $this->criteria->get($v_user_client->m_criteria_id);
			if ($criteria) {
				$arr[$i]["m_criteria_name"] = $criteria->m_criteria_name;
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($criteria->ref_criteria_id);
				if($referensi) $arr[$i]["m_criteria_id"] = $referensi->name;
			}
			//////////
			// Panggil TMT Date
			$arr[$i]["tmt_date"] = $v_user_client->tmt_date;
			//////////
			// Panggil Keterangan
			if ($v_user_client->note == '')  $arr[$i]["note"] = '-';
			else $arr[$i]["note"] = $v_user_client->note;
			//////////
			$i++;
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
		}
		if ($ref_criteria) $this->db->join('m_criteria','gp_user_client.m_criteria_id=m_criteria.m_criteria_id')->where('ref_criteria_id',$ref_criteria);
		//$this->db->where('ref_criteria_id',$ref_criteria);
		$ct = $this->user_client->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}

	function save($gp_personal_id='')
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_user_client_id","act-user_client",'m_structure_position_id','nrk','personal_name'
			,'ref_criteria_id','m_criteria_name','m_criteria_code'
			))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_user_client_id = $this->input->post("gp_user_client_id");
		$ref_criteria_id = $this->input->post('ref_criteria_id');
		$m_criteria_id = $this->input->post("m_criteria_id");
		$this->load->model("Gp_user_client", "user_client");
		$this->load->model("M_kriteria", "kriteria");
		if ($gp_personal_id)
		{
		if($gp_user_client_id  == 0)
		{
			if ($m_criteria_id==0)
			{
				$data['ref_criteria_id']=$ref_criteria_id;
				$data['m_criteria_name']=$this->input->post("m_criteria_name");
				$arr['gp_personal_id']=$gp_personal_id;
				$idKriteria_baru=$this->kriteria->insert($data);
				$arr['m_criteria_id']=$idKriteria_baru;
				$arr['active']='Y';
			}
			//$this->db->where('ref_criteria_id',$ref_criteria_id)->where;
			$user_client = $this->user_client->get_many_by('gp_personal_id',$gp_personal_id);
			foreach ($user_client as $k_user_client => $v_user_client)
			{
							$this->load->model('M_Kriteria','kriteria');
							$kriteria=$this->kriteria->get($v_user_client->m_criteria_id);
							if ($kriteria)
							{
								if ($kriteria->ref_criteria_id == $ref_criteria_id)
								{	
									$data2['active']='N';
									$this->user_client->update($v_user_client->gp_user_client_id,$data2);	
								}
							}
							
			}
			
			$rs = $this->user_client->insert($arr);		
		}
		else
		{
			$rs = $this->education->update($this->input->post("gp_education_id"),$arr);					
			$rs = $this->input->post("gp_education_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
		}
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vkontrak"] = $this->config->item('kontrak');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["ref_criteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 1 = gender
		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_user_client",$data);			
	}

	function getCriteria($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_kriteria", "criteria");
		$offset = ($page==1) ? 0 : $page*25;
		$this->criteria->limit(25,$offset);
		$arr = array();
		$data = $this->criteria->get_many_by("m_criteria_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_criteria_id;
			$arr[$i]["name"] = $v->m_criteria_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}	
}
