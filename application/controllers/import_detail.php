<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Import Detail
* 
********************************************************************/ 

class Import_detail extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function get()
	{
		$page = $this->input->get("page");		
		$this->load->model("Gp_personal", "pers");
		$this->pers->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		
	}
		
	
	function save()
	{
		
		
	}
	
	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");
		$data["form"] = $form;
		$data["m_p_overtime_parameter_id"] = $id;
		$data["month_id"] = $this->getReferensi(array("id" => "month_id",
													   "name" => "month_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 49));


		$this->load->view("timesheet/view_import_detail",$data);			
	}

	


}
