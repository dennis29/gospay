<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Transaksi Harian
* 
********************************************************************/ 

class Transaksi_harian extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Gp_personal", "pers");
	}
		
	
	function save()
	{
		
		
	}
	
	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");
		$data["form"] = $form;
		$data["m_p_overtime_parameter_id"] = $id;
		$data["month_id"] = $this->getReferensi(array("id" => "month_id",
													   "name" => "month_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 49));
		

		$this->load->view("payrol/view_transaksi_harian",$data);			
	}


}
