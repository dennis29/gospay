<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 28, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Posisi
* 
********************************************************************/ 

class Posisi extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_posisi");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_posisi", "posisi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->posisi->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->posisi->order_by('m_position_type_id','DESC');
		$data["list"] = $this->posisi->search($arr)->get_all();
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->posisi->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_posisi", "posisi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->posisi->limit($limit,$offset);
		$this->posisi->order_by("m_position_type_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->posisi->get_all();
		}
		else
		{
			$data["list"] = $this->posisi->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_position_type_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_position_type_id = $this->input->post("m_position_type_id");
		$this->load->model("M_posisi", "posisi");
		if($m_position_type_id==0)
		{
			$rs = $this->posisi->insert($arr);		
		}
		else
		{
			$rs = $this->posisi->update($this->input->post("m_position_type_id"),$arr);					
			$rs = $this->input->post("m_position_type_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_posisi", "posisi");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->posisi->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}