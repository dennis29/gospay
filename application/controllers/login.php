<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 24, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses Login
* 
********************************************************************/ 

class Login extends CI_Controller {

	/**
	 * Fungsinya buat login, logout, cek aktivitas, dll.
	 */
	public function index()
	{
		//cek user session
	}
	
	public function cekLogin()
	{ 
		$username = $this->security->xss_clean($this->input->post("username"));	
		$password = ($this->security->xss_clean($this->input->post("password")));	
			
		$this->load->model("M_user", "user");
//		$this->user->where();
		if($this->user->count_by(array("name" => $username, "password" => $password))==1)
		{
			$usr = $this->user->get_by(array("name" => $username, "password" => $password));
			$this->session->set_userdata("m_user_id", $usr->m_user_id);
			//$this->session->set_userdata("M_BRANCH_ID", $usr->M_BRANCH_ID);
			//akses
			/*	$m_user_id = $usr->M_USER_ID;	
				$this->load->model("M_user_role", "mr");
				
				$dmr = $this->mr->get_many_by("M_USER_ID", $m_user_id);
				
				$this->load->model("M_role_function", "mf");
				$arrf = array();
				$arrid = array();
				foreach($dmr as $k=>$v)
				{
					$dmf = $this->mf->get_many_by("M_ROLE_ID", $v->M_ROLE_ID);
					foreach($dmf as $k2=>$v2)
					{
						$this->load->model("M_function", "mfn");
						$dfn = $this->mfn->get($v2->M_FUNCTION_ID);
						if ($dfn) $arrid[] = $dfn->M_FUNCTION_ID;
					}
				}		
				$this->load->model("M_function", "mfn");
				$dfnn = $this->mfn->get_all();
				if($dfnn)
				{
					foreach($dfnn as $k=>$v)
					{
						if(!in_array($v->M_FUNCTION_ID, $arrid))
						{
							$arrf[] = $v->NAME;	
						}
					}			
				}
				else
				{
					$arrf[] = "";	
				}
			//get akses function		
			$this->session->set_userdata("AKSES", $arrf);*/
			
			echo json_encode(array("success" => true, "message" => "Login Success"));
		}
		else
		{
			echo json_encode(array("success" => false, "message" => "Login Gagal Silakan Ulangi!"));
		}
	}		
	
	function out()
	{
/*		$this->load->model("M_user", "mu");
		$isLogin = $this->mu->update($this->session->userdata("M_USER_ID"),array("IS_LOGIN" => 'N'));
*/		$this->session->sess_destroy();	
		$this->load->view("view_login");			
	}

}