<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Keluarga Karyawan
* 
********************************************************************/ 

class Keluarga extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	
		
	function getList($id='')
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_family", "family");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->family->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->family->order_by('gp_family_id','ASC');
		$data["list"] = $this->family->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->family->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function getListById($id='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_family", "family");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$this->family->limit($limit,$offset);
		$data = array();
		$arrct = array();
		$arr = array();
		if ($id != '')
		{
		/*
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{	
					if ($id == '') 
					{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
					else
					{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%' and gp_personal_id =".$id;
					}
				}
			}
		}
		*/
		$this->family->order_by('gp_family_id','ASC');
		$data = $this->family->get_many_by('gp_personal_id',$id);
		foreach($data as $k_family=>$v_family)
		{
			// Panggil Id Serial
			$arr[$i]["gp_family_id"] = $v_family->gp_family_id;
			//////////
			// Panggil Nama
			$arr[$i]["family_name"] = $v_family->family_name;
			//////////
			// Panggil Hubungan 
			$this->load->model("M_referensi_list", "referensi");
			$referensi_relasi = $this->referensi->get($v_family->ref_relation_id);
			if ($referensi_relasi) $arr[$i]["family_relation"] = $referensi_relasi->name;
			////////
			////Panggil Tempat Lahir
			$this->load->model("M_city", "city");
			$city = $this->city->get($v_family->m_birth_place_id);
			if ($city) $arr[$i]["m_birth_place"] = $city->name;
			////////
			////Panggil Tanggal Lahir
			$arr[$i]["birth_date"] = $v_family->birth_date;
			/////////
			// Panggil Agama 
			$referensi_agama = $this->referensi->get($v_family->ref_religion_id);
			if ($referensi_agama) $arr[$i]["ref_religion_name"] = $referensi_agama->name;
			////////
			$i++;
		}
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		}
		$ct = $this->family->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}	
	function get($id='')
	{
		
		$this->load->model("Gp_family", "family");	
		$data = $this->family->get_many_by('gp_family_id',$id);
		foreach($data as $k_family => $v_family)
		{
				$this->load->model("Gp_personal", "pers");	
				$pers = $this->pers->get($v_family->gp_personal_id);
				//Panggil Kartu Keluarga Untuk Id family_card_no-keluarga
				if ($pers) $data["family_card_no-keluarga"] = $pers->family_card_no;
				////////////
				//Panggil Nama Anggota Untuk Id family_name
				$data["family_name"] =$v_family->family_name;
				///////////
				////Panggil Hubungan Untuk Id ref_relation_id-keluarga
				$data["ref_relation_id-keluarga"] = $v_family->ref_relation_id;
				//////
				/// Panggil Tempat Lahir Untuk Id m_birth_place_id
				$data["m_birth_place_id-keluarga"] = $v_family->m_birth_place_id;
				////////
				////Panggil Tempat Lahir Untuk id m_birth_place_name
				$this->load->model("M_city", "city");	
				$city = $this->city->get($v_family->m_birth_place_id);
				if ($city) $data["m_birth_place_name"] = $city->name;
				///////////
				/// Panggil Tanggal Lahir untuk id birth_date-keluarga
				$data["birth_date-keluarga"] = $v_family->birth_date;
				////////
				//// Panggil Jenis Kelamin untuk id gender_id-keluarga
				$data["gender_id-keluarga"] = $v_family->ref_gender_id;
				//////////////Panggil Agama untuk id religion_id-keluarga
				$data["religion_id-keluarga"] = $v_family->ref_religion_id;
				///////////////////
				///Panggil BPJS Untuk Id ref_bpjskes_status_id-keluarga
				$data["ref_bpjskes_status_id-keluarga"] = $v_family->ref_bpjskes_status_id;
				/////
				////Panggil No BPJS Untuk Id bpjskes_no-keluarga
				$data["bpjskes_no-keluarga"] = $v_family->bpjskes_no;
				/////////
				
		}
		echo json_encode(array("success" => true, "data" => $data));		
	}
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_family_id","act-keluarga",'family_card_no'))))
			{
			$arr[($k)] = strtoupper($v);
			}
		}
		$gp_family_id = $this->input->post("gp_family_id");
		$this->load->model("Gp_family", "family");
		if($gp_family_id==0)
		{
			//Simpan Data Ke Gp_family
			$rs = $this->family->insert($arr);
			/////
			//Update Data family_card_no Ke Gp_personal
			unset($arr);
			$arr = array();
			$this->load->model("Gp_personal", "personal");
			foreach($this->input->post() as $k=>$v)
			{
				if( ($v!="" || $v!=0) && (in_array($k, array('family_card_no'))))
				{
				$arr[($k)] = strtoupper($v);
				}
			}
			$arr[($k)] = 
			$rs = $this->family->update($this->input->post("gp_family_id"),$arr);	
		}
		else
		{
			$rs = $this->family->update($this->input->post("gp_family_id"),$arr);					
			$rs = $this->input->post("gp_family_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");
		$data["vfamily"] = $this->config->item('family');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["gender_id"] = $this->getReferensi(array("id" => "gender_id",
													   "name" => "gender_id",
													   "class" => "form-control",
													   "refid" => 1)); // 1 = gender

		$data["religion_id"] = $this->getReferensi(array("id" => "religion_id",
													   "name" => "religion_id",
													   "class" => "form-control",
													   "refid" => 2)); // 2 = religion

		$data["marital_status_id"] = $this->getReferensi(array("id" => "marital_status_id",
													   "name" => "marital_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Status ==",
													   "refid" => 3));

		$data["blood_type_id"] = $this->getReferensi(array("id" => "blood_type_id",
													   "name" => "blood_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 4));

		$data["ref_bpjskes_status_id"] = $this->getReferensi(array("id" => "ref_bpjskes_status_id",
													   "name" => "ref_bpjskes_status_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 5));

		$data["ref_relation_id"] = $this->getReferensi(array("id" => "ref_relation_id",
													   "name" => "ref_relation_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 7));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_keluarga",$data);			
	}

	function getCity($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_city", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->get_many_by("name  ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_city_id;
			$arr[$i]["name"] = $v->name ;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

}
