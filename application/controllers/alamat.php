<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 05, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Alamat Karyawan
* 
********************************************************************/ 

class Alamat extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_address", "address");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->address->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->address->order_by('gp_address_id','ASC');
		$data["list"] = $this->address->search($arr)->get_all();
		foreach ($data["list"] as $k=>$r) 
			{			
				$this->load->model("M_faskes","faskes");
				$faskes = $this->faskes->get($r->m_faskes_id);
				if($faskes) $data["list"][$k]->m_faskes_name = $faskes->m_faskes_name;	
				else $data["list"][$k]->m_faskes_name = "-";

				$this->load->model("M_sub_district", "ms");
				$rw = $this->ms->get($r->m_sub_district_id);
				if($rw) $data["list"][$k]->m_sub_district_id = $rw->name;
			}


//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->address->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}
	
	function getListById($id='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_address", "address");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$this->address->limit($limit,$offset);
		$data = array();
		$arrct = array();
		$arr = array();
		if ($id != '')
		{
		$this->address->order_by('gp_address_id','DESC');
		$data = $this->address->get_many_personal('gp_personal_id',$id);
//echo $this->db->last_query();
		foreach($data as $k_address => $v_address)
		{
			// Panggil Id Serial
			$arr[$i]["gp_address_id"] = $v_address->gp_address_id;
			//////////
			// Panggil Nama
			$arr[$i]["address"] = $v_address->address;
			//////////
			// Panggil RT
			$arr[$i]["rt"] = $v_address->rt;
			//////////
			// Panggil RW
			$arr[$i]["rw"] = $v_address->rw;
			//////////
			// Panggil Kode Pos
			$arr[$i]["zipe_code"] = $v_address->zipe_code;
			//////////
			// Panggil Kelurahan
			$this->load->model("M_sub_district", "sub_district");
			$sub_district = $this->sub_district->get($v_address->m_sub_district_id);
			if ($sub_district) $arr[$i]["sub_district_name"] = $sub_district->name;
			//////////
			// Panggil Kecamatan
			$this->load->model("M_district", "district");
			$district = $this->district->get($v_address->m_district_id);
			if ($district) $arr[$i]["district_name"] = $district->name;
			//////////
			// Panggil Kota
			$this->load->model("M_city", "city");
			$city = $this->district->get($v_address->m_city_id);
			if ($city) $arr[$i]["city_name"] = $city->name;
			//////////
			// Panggil Handphone
			$arr[$i]["handphone"] = $v_address->handphone;
			//////////
			// Panggil No Telepon Rumah
			$arr[$i]["home_phone"] = $v_address->home_phone;
			//////////
			// Panggil Note
			if ($v_address->note =='') $arr["note"][$i] = '-';
			else $arr[$i]["note"] = $v_address->note;
			//////////
			$i++;
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
		}
		$ct = $this->address->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}
	function get($id='')
	{
		$this->load->model("Gp_address", "address");
		$arr = array();
		$data = $this->address->get_many_personal('gp_address_id',$id);
//echo $this->db->last_query();
		foreach($data as $k_address => $v_address)
		{
			// Panggil Id Serial
			$arr["gp_address_id"] = $v_address->gp_address_id;
			//////////
			// Panggil Nama
			$arr["address-alamat"] = $v_address->address;
			//////////
			// Panggil RT
			$arr["rt-alamat"] = $v_address->rt;
			//////////
			// Panggil RW
			$arr["rw-alamat"] = $v_address->rw;
			//////////
			// Panggil Kode Pos
			$arr["zipe_code-alamat"] = $v_address->zipe_code;
			//////////
			// Panggil Kelurahan Untuk Id m_sub_district_id-alamat, m_sub_district_name-alamat
			$arr["m_sub_district_id-alamat"] = $v_address->m_sub_district_id;
			$this->load->model("M_sub_district", "sub_district");
			$sub_district = $this->sub_district->get($v_address->m_sub_district_id);
			if ($sub_district) $arr["m_sub_district_name-alamat"] = $sub_district->name;
			//////////
			// Panggil Kecamatan Untuk Id m_district_id-alamat, m_district_name-alamat
			$arr["m_district_id-alamat"] = $v_address->m_district_id;
			$this->load->model("M_district", "district");
			$district = $this->district->get($v_address->m_district_id);
			if ($district) $arr["m_district_name-alamat"] = $district->name;
			//////////
			// Panggil Kota Untuk Id m_city_id-alamat, m_city_name-alamat
			$arr["m_city_id-alamat"] = $v_address->m_city_id;
			$this->load->model("M_city", "city");
			$city = $this->district->get($v_address->m_city_id);
			if ($city) $arr["m_city_name-alamat"] = $city->name;
			//////////
			// Panggil Provinsi Untuk Id m_state_id-alamat, m_state_name-alamat
			$arr["m_state_id-alamat"] = $v_address->m_state_id;
			$this->load->model("M_state", "state");
			$state = $this->state->get($v_address->m_state_id);
			if ($state) $arr["m_state_name-alamat"] = $state->name;
			// Panggil Handphone
			$arr["handphone-alamat"] = $v_address->handphone;
			//////////
			// Panggil No Telepon Rumah
			$arr["home_phone-alamat"] = $v_address->home_phone;
			//////////
			// Panggil Fasilitas Kesehatan Untuk Id m_faskes_id-alamat
			$arr["m_faskes_id-alamat"] = $v_address->m_faskes_id;
			//////////
			// Panggil Fasilitas Kesehatan Untuk Id m_faskes_name-alamat
			$this->load->model("M_faskes", "faskes");
			$faskes = $this->faskes->get($v_address->m_faskes_id);
			if ($faskes) $arr["m_faskes_name-alamat"] = $faskes->m_faskes_name;
			//////////
			// Panggil Dokter Gigi
			$arr["dokgi-alamat"] = $v_address->dokgi;
			//////////
			
			// Panggil Keterangan
			$arr["note"] = $v_address->note;
			//////////
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr));		
	}
	
	function save($id='')
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_address_id",'m_sub_district_name',
			'm_district_name','m_city_name','m_state_name','m_faskes_name'
			))))
			$arr[($k)] = strtoupper($v);
		}
		$arr['gp_personal_id'] = $id;
		$gp_address_id = $this->input->post("gp_address_id");
		$this->load->model("Gp_address", "address");
		if($gp_address_id==0)
		{	
			$data['active']='N';
			$this->db->where('gp_personal_id',$id);
			$rg = $this->address->update_all($data);
			$rs = $this->address->insert($arr);		
		}
		else
		{
			$rs = $this->address->update($this->input->post("gp_address_id"),$arr);					
			$rs = $this->input->post("gp_address_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["form"] = $form;
		$data["gp_personal_id"] = $id;

		
		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_alamat",$data);			
	}


	function getSubDistrict($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_sub_district", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(50,$offset);
		$arr = array();
		$q = urldecode($q);
		$q = str_replace("-"," ",$q);
		$data = $this->ml->getTypeahead($q);
		$query = $this->db->last_query();
/*		$query = $this->db->query("SELECT ms.* from ");
		$data = $this->ml->get_many_by("NAME ILIKE '%{$q}%'");
*/	
		$i = 0;
//		echo urldecode($q);
		if($data)
		{
			foreach($data as $k=>$v)
			{
				$arr[$i]["id"] = $v->m_sub_district_id;
				$arr[$i]["m_district_id"] = $v->m_district_id;
	
				$arr[$i]["name"] = strtoupper($v->sname."-".$v->dname);
				$i++;
			}
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr, "q" => $query));
	}

	function getAddress($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_district", "md");
		$md = $this->md->get($q);
		$arr = array();		
		
		if($this->md->count_by("m_district_id", $q)>0)
		{
			$arr["m_district_name"] = strtoupper($md->name);
			$arr["m_district_id"] = $md->m_district_id;
			$this->load->model("M_city", "mc");
			$mc = $this->mc->get($md->m_city_id);

			if($this->mc->count_by("m_city_id", $md->m_city_id)>0)
			{
				$arr["m_city_name"] = strtoupper($mc->name);
				$arr["m_city_id"] = $mc->m_city_id;
		
				$this->load->model("M_state", "mst");
				$mst = $this->mst->get($mc->m_state_id);

				if($this->mst->count_by("m_state_id", $mc->m_state_id)>0)
				{
					$arr["m_state_name"] = strtoupper($mst->name);
					$arr["m_state_id"] = $mst->m_state_id;
				}
			}
		}	

	}

	function getFaskes($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_faskes", "faskes");
		$offset = ($page==1) ? 0 : $page*25;
		$this->faskes->limit(25,$offset);
		$arr = array();
		$data = $this->faskes->get_many_by("m_faskes_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_faskes_id;
			$arr[$i]["code"] = $v->m_faskes_code;
			$arr[$i]["name"] = $v->m_faskes_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}	
	function delete()
	{
		$id = $_GET["id"];
		
		$this->load->model("Gp_address", "address");
		$rs = $this->address->delete($id);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
	
}