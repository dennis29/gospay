<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Overtime Detail Karyawan
* 
********************************************************************/ 

class Overtime_detail extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function get()
	{
		 
	}

	function getList()
	{
		 
	}
		
	
	function save()
	{
		 
	}

	
	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";
		
		$data["akses"] = $this->session->userdata("AKSES");
		$data["form"] = $form;
		$data["m_p_overtime_parameter_id"] = $id;

		$data["overtime_type"] = $this->getReferensi_Type(array("id" => "status",
													   "name" => "status",
													   "class" => "form-control",
													   "refid" => 50));
		
		$data["p_period_months_salary"] = $this->getComboSalary(array("name" => "p_period_months_salary_id",
													 "model" => "p_period_months_salary",
													 "id" => "p_period_months_salary_id",
													 "column" => "days_salary",
													 "class" => "form-control"));


		$this->load->view("timesheet/view_overtime_detail",$data);		
		
	}

	
	
	



	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	

	function getJabatan($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_struktur_jabatan", "structure_position");
		$offset = ($page==1) ? 0 : $page*25;
		$this->structure_position->limit(25,$offset);
		$arr = array();
		$data = $this->structure_position->get_many_by("m_structure_position_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_structure_position_id;
			$arr[$i]["code"] = $v->m_structure_position_code;
			$arr[$i]["name"] = $v->m_structure_position_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


	function getMonthSalary($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("P_period_months_salary", "p_period_months_salary");
		$offset = ($page==1) ? 0 : $page*25;
		$this->p_period_months_salary->limit(25,$offset);
		$arr = array();
		$data = $this->p_period_months_salary->get_many_by("status = false");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["description"] = $v->description;
			
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("success" => true,"data" => $arr));
	}

	function getPersonal($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("Gp_personal", "karyawan");
		$offset = ($page==1) ? 0 : $page*25;
		$this->karyawan->limit(25,$offset);
		$arr = array();
		$data = $this->karyawan->get_many_by("personal_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->gp_personal_id;
			$arr[$i]["nrk"] = $v->nrk;
			$arr[$i]["name"] = '( '.$v->nrk . ' )   ' . $v->personal_name;
			$arr[$i]["join_date"] = $v->join_date;
			
			$this->load->model("M_unit_kerja", "unit");   
			$unit = $this->unit->get($v->m_unit_id);
			if($unit)
			{
				$arr[$i]["m_unit_name"] = $unit->m_unit_name;
			}
			
			$this->load->model("M_struktur_jabatan", "struktur_jabatan");
			$struktur_jabatan = $this->struktur_jabatan->get($v->m_structure_position_id);
			if($unit)
			{	
				$arr[$i]["m_structure_position_name"] = $struktur_jabatan->m_structure_position_name;
			}

			$this->load->model("M_client", "client");   
			$unit = $this->client->get($v->m_client_id);
			if($unit)
			{
				$arr[$i]["m_client_name"] = $unit->m_client_name;
			}

			$this->load->model("M_area", "area");
			$area = $this->area->get($v->m_area_id);
			if($area)
			{	
				$this->load->model("M_lokasi", "location");   
				$location = $this->location->get($area->m_location_id);
				if($location)
				{
					$arr[$i]["m_location_name"] = $location->m_location_name;
				}
			}
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


	function getDayOff($q)
	{

		$this->load->model("M_p_day_off", "day_off");
		$arr = array();
		$data = $this->day_off->get_by("day_off = '{$q}'");
/*
		foreach($data as $k=>$v)
		{
			$arr["note"] = $v->note;
			
		}
	
	*/
//	echo $this->db->last_query();
	//	if($data) echo json_encode(array("success" => true,"data" => $data->note));
	if($data) echo $data->note;
		
	}



}
