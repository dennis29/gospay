<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Mutasi Karyawan
* 
********************************************************************/ 

class Mutasi extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Gp_personal", "pers");
		$this->pers->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->pers->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_mutation", "mutation");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->mutation->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->mutation->order_by('gp_mutation_id','ASC');
		$data["list"] = $this->mutation->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{			
				$this->load->model("M_cabang","cabang");
				$cabang = $this->cabang->get($r->m_branch_id);
				if($cabang) $data["list"][$k]->m_branch_name = $cabang->m_branch_name;	
				else $data["list"][$k]->m_branch_name = "-";

				$this->load->model("M_lokasi","location");
				$location = $this->location->get($r->m_location_id);
				if($location) $data["list"][$k]->m_location_name = $location->m_location_name;	
				else $data["list"][$k]->m_location_name = "-";

				$this->load->model("M_unit_kerja","unit");
				$unit = $this->unit->get($r->m_unit_id);
				if($unit) $data["list"][$k]->m_unit_name = $unit->m_unit_name;	
				else $data["list"][$k]->m_unit_name = "-";

				$this->load->model("M_struktur_jabatan","jabatan");
				$jabatan = $this->jabatan->get($r->m_structure_position_id);
				if($jabatan) $data["list"][$k]->m_structure_position_name = $jabatan->m_structure_position_name;	
				else $data["list"][$k]->m_structure_position_name = "-";
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->mutation->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_mutation_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_mutation_id = $this->input->post("gp_mutation_id");
		$this->load->model("Gp_mutation", "mutation");
		if($gp_mutation_id==0)
		{
			$rs = $this->mutation->insert($arr);		
		}
		else
		{
			$rs = $this->mutation->update($this->input->post("gp_mutation_id"),$arr);					
			$rs = $this->input->post("gp_mutation_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vmutasi"] = $this->config->item('mutasi');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["ref_mutation_type_id"] = $this->getReferensi(array("id" => "ref_mutation_type_id",
													   "name" => "ref_mutation_type_id",
													   "class" => "form-control",
													   "refid" => 36)); 

		$this->load->view("karyawan/view_mutasi",$data);			
	}

		function getCabang($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_cabang", "cabang");
		$offset = ($page==1) ? 0 : $page*25;
		$this->cabang->limit(25,$offset);
		$arr = array();
		$data = $this->cabang->get_many_by("m_branch_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_branch_id;
			$arr[$i]["code"] = $v->m_branch_code;
			$arr[$i]["name"] = $v->m_branch_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getLokasi($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_lokasi", "location");
		$offset = ($page==1) ? 0 : $page*25;
		$this->location->limit(25,$offset);
		$arr = array();
		$data = $this->location->get_many_by("m_location_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_location_id;
			$arr[$i]["code"] = $v->m_location_code;
			$arr[$i]["name"] = $v->m_location_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getUnit($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_unit_kerja", "unit");
		$offset = ($page==1) ? 0 : $page*25;
		$this->unit->limit(25,$offset);
		$arr = array();
		$data = $this->unit->get_many_by("m_unit_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_unit_id;
			$arr[$i]["code"] = $v->m_unit_code;
			$arr[$i]["name"] = $v->m_unit_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getJabatan($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_struktur_jabatan", "structure_position");
		$offset = ($page==1) ? 0 : $page*25;
		$this->structure_position->limit(25,$offset);
		$arr = array();
		$data = $this->structure_position->get_many_by("m_structure_position_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_structure_position_id;
			$arr[$i]["code"] = $v->m_structure_position_code;
			$arr[$i]["name"] = $v->m_structure_position_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
}

