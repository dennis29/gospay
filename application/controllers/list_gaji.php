<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Info Gaji Karyawan
* 
********************************************************************/ 

class List_gaji extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		//Gaji
		$data["status"] = $this->getReferensi_status(array("id" => "status",
													   "name" => "status",
													   "class" => "form-control",
													   "refid" => 41)); 

		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/view_list_gaji",$data);	
	} 
	

	function get($id=0)    
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_salary", "gaji");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->gaji->limit($limit,$offset);
		$this->gaji->order_by("gp_salary_id","DESC");		
		$this->gaji->db->join('gp_personal','gp_personal.gp_personal_id=gp_salary.gp_personal_id');
		$this->gaji->db->join('m_client','m_client.m_client_id=gp_personal.m_client_id');
		$this->gaji->db->join('m_unit','gp_personal.m_unit_id=gp_personal.m_unit_id');
		$this->gaji->db->join('m_structure_position','m_structure_position.m_structure_position_id=gp_personal.m_structure_position_id');
		if($id==0)
		{
			$data["list"] = $this->gaji->get_all();
		}
		else
		{
			$data["list"] = $this->gaji->get($id);
		}	
		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}


	
		
	function getList()
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_salary", "gaji");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->gaji->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->gaji->order_by('gp_salary_id','ASC');
		$data["list"] = $this->gaji->search($arr)->get_all();
//	echo $this->db->last_query();
		foreach ($data["list"] as $k=>$r) 
			{		
			
				$this->load->model("Gp_personal","personal");
				$personal = $this->personal->get($r->gp_personal_id);
				if($personal) 
				{
					$data["list"][$k]->personal_name = "( ".$personal->nrk." ) ".$personal->personal_name;
					$data["list"][$k]->nrk = $personal->nrk;

					$this->load->model("M_client","client");
					$client = $this->client->get($personal->m_client_id);
					if($client) $data["list"][$k]->m_client_name = $client->m_client_name;	
					else $data["list"][$k]->m_client_name = "-";
				}

				$this->load->model("M_user","user");
				$user = $this->user->get($r->created_by);
				if($client) $data["list"][$k]->user_name = $user->name;	
				else $data["list"][$k]->user_name = "-";
				

			}
		$arrct["active"] = "Y";
		$ct = $this->gaji->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_salary_id","act","m_client_id"))))
			{
				if( $k=='status' && $v=='Aktif' ) $v='Y';
				if( $k=='status' && $v=='Tidak Aktif' ) $v='N';

				$arr[($k)] = strtoupper($v);
			}
		}
		$gp_salary_id = $this->input->post("gp_salary_id");
		$this->load->model("Gp_salary", "gaji");
		if($gp_salary_id==0)
		{
			$rs = $this->gaji->insert($arr);		
		}
		else
		{
			$rs = $this->gaji->update($this->input->post("gp_salary_id"),$arr);					
			$rs = $this->input->post("gp_salary_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");	
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["status"] = $this->getReferensi(array("id" => "status",
													   "name" => "status",
													   "class" => "form-control",
													   "refid" => 41)); 	
		

		$this->load->view("karyawan/view_list_gaji",$data);			
	}

	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getPersonal($q,$c)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("Gp_personal", "karyawan");
		$offset = ($page==1) ? 0 : $page*25;
		$this->karyawan->limit(25,$offset);
		$arr = array();
		$data = $this->karyawan->get_many_by("personal_name ILIKE '%{$q}%' and \"m_client_id\"='{$c}'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->gp_personal_id;
			$arr[$i]["nrk"] = $v->nrk;
			$arr[$i]["name"] = '( '.$v->nrk . ' )   ' . $v->personal_name;
			$arr[$i]["join_date"] = $v->join_date;
			
			$this->load->model("M_unit_kerja", "unit");   
			$unit = $this->unit->get($v->m_unit_id);
			if($unit)
			{
				$arr[$i]["m_unit_name"] = $unit->m_unit_name;
			}
			
			$this->load->model("M_struktur_jabatan", "struktur_jabatan");
			$struktur_jabatan = $this->struktur_jabatan->get($v->m_structure_position_id);
			if($unit)
			{	
				$arr[$i]["m_structure_position_name"] = $struktur_jabatan->m_structure_position_name;
			}
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("Gp_salary", "gaji");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->gaji->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

}
