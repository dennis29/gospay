<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Tunjangan Karyawan
* 
********************************************************************/ 

class Tunjangan extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_fixed_transaction", "tunjangan");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tunjangan->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->tunjangan->order_by('gp_fixed_transaction_id','ASC');
		$data["list"] = $this->tunjangan->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{			
				$this->load->model("M_p_description_transaction","transaksi");
				$transaksi = $this->transaksi->get($r->m_client_id);
				if($transaksi) $data["list"][$k]->m_p_description_transaction_name = $transaksi->m_client_name;		
				
			}
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->tunjangan->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save($gp_personal_id_ref='')
	{
		if ($gp_personal_id_ref)
		{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_fixed_transaction_id"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_fixed_transaction_id = $this->input->post("gp_fixed_transaction_id");
		$this->load->model("Gp_fixed_transaction", "tunjangan");
		if($gp_fixed_transaction_id==0)
		{
			$data['active']='N';
			$arr['gp_personal_id']=$gp_personal_id_ref;
			$this->db->where('gp_personal_id',$gp_personal_id_ref);
			$this->tunjangan->update_all($data);
			$rs = $this->tunjangan->insert($arr);		
		}
		else
		{
			$rs = $this->tunjangan->update($this->input->post("gp_fixed_transaction_id"),$arr);					
			$rs = $this->input->post("gp_fixed_transaction_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
		}
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vtunjangan"] = $this->config->item('tunjangan');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["ref_criteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 1 = gender
		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_tunjangan",$data);			
	}
	
	function getListById($gp_personal_id_ref='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_fixed_transaction", "fixed_transaction");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$data = array();
		$arrct = array();
		$arr = array();
		if ($gp_personal_id_ref != '')
		{
		$this->db->order_by('gp_fixed_transaction_id','DESC');
		$data = $this->fixed_transaction->get_many_personal('gp_personal_id',$gp_personal_id_ref);
		foreach($data as $k_gp_fixed_transaction_id => $v_gp_fixed_transaction_id)
		{
			// Panggil Id Serial
			$arr[$i]["gp_fixed_transaction_id"] = $v_gp_fixed_transaction_id->gp_fixed_transaction_id;
			///////
			//Panggil Kode Transaksi
			$arr[$i]["m_p_description_transaction_id"] = $v_gp_fixed_transaction_id->m_p_description_transaction_id;
			//////
			//////
			$this->load->model('M_p_description_transaction','description_transaction');
			$description_transaction=$this->description_transaction->get($v_gp_fixed_transaction_id->m_p_description_transaction_id);
			if ($description_transaction)
			{
			//Panggil Nama Transaksi
			$arr[$i]["m_p_description_transaction_name"] = $description_transaction->m_p_description_transaction_name; 
			////
			}
			//////
			//Panggil Nilai
			$arr[$i]["value"] = number_format($v_gp_fixed_transaction_id->value,0,'.',',');
			//////////
			//Panggil TMT
			$arr[$i]['transaction_start_date']=date('d-M-Y',strtotime($v_gp_fixed_transaction_id->transaction_start_date));
			////
			//Panggil TAT
			$arr[$i]['transaction_end_date']=date('d-M-Y',strtotime($v_gp_fixed_transaction_id->transaction_end_date));
			////
			$i++;
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
		$this->db->where('gp_personal_id',$gp_personal_id_ref);
		}
		$ct = $this->fixed_transaction->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}
	function get($gp_fixed_transaction_id='')
	{
		$this->load->model("Gp_fixed_transaction", "fixed_transaction");
		$data = $this->fixed_transaction->get_many_personal('gp_fixed_transaction_id',$gp_fixed_transaction_id);
		foreach($data as $k_gp_fixed_transaction_id => $v_gp_fixed_transaction_id)
		{
			//Panggil Kode Transaksi
			$arr["m_p_description_transaction_id"] = $v_gp_fixed_transaction_id->m_p_description_transaction_id;
			//////
			$this->load->model('M_p_description_transaction','description_transaction');
			$description_transaction=$this->description_transaction->get($v_gp_fixed_transaction_id->m_p_description_transaction_id);
			if ($description_transaction)
			{
			//Panggil Nama Transaksi
			$arr["m_p_description_transaction_name"] = $description_transaction->m_p_description_transaction_name; 
			////
			//Panggil Kode Transaksi
			$arr["m_p_description_transaction_code"] = $description_transaction->m_p_description_transaction_code; 
			////
			}
			//////
			//Panggil Nilai
			$arr["value-tunjangan"] =$v_gp_fixed_transaction_id->value;
			//////////
			//Panggil TMT
			$arr['transaction_start_date-tunjangan']=date('d-m-Y',strtotime($v_gp_fixed_transaction_id->transaction_start_date));
			////
			//Panggil TAT
			$arr['transaction_end_date-tunjangan']=date('d-m-Y',strtotime($v_gp_fixed_transaction_id->transaction_end_date));
			////
			//Panggil Note
			$arr['note-tunjangan']=$v_gp_fixed_transaction_id->note;
			////
		}

		echo json_encode(array("success" => true, "data" => $arr));		
	}


	function getTransaksi($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_p_description_transaction", "p_description_transaction");
		$offset = ($page==1) ? 0 : $page*25;
		$this->p_description_transaction->limit(25,$offset);
		$arr = array();
		$data = $this->p_description_transaction->get_many_by("m_p_description_transaction_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_p_description_transaction_id;
			$arr[$i]["code"] = $v->m_p_description_transaction_code;
			$arr[$i]["name"] = $v->m_p_description_transaction_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
}

