<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Functions extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}  
 
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_function", "pers");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pers->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pers->order_by('name','ASC');
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0, $echo=0)
	{
		$this->load->model("M_function", "ms");
		try{
			$page = $this->input->get("page");
			$limit = $this->config->item("limit");
			$offset = ($page==1) ? 0 : ($page-1)*$limit;
			$this->ms->limit($limit,$offset);
		}
		catch(Exception $e)
		{
			
		}

		$this->ms->order_by("name","ASC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
		}
		else
		{
			$data["list"] = $this->ms->get($id);
		}		
		if($echo==0)
		echo json_encode(array("success" => true, "data" => $data["list"]));		
		else return json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_function_id","act","password_lama","confirm_password","password_hidden"))))
				$arr[strtoupper($k)] = $v;
		}
		$m_function_id = $this->input->post("m_function_id");
		$this->load->model("M_function", "st");
		if($m_function_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_function_id"),$arr);					
			$rs = $this->input->post("m_function_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function saveAkses()
	{
		$arr = array();
		$arr["m_user_id"] = $this->input->post("m_user_id");
		$m_user_id = $this->input->post("m_user_id");
		
		$m_role_id = $this->input->post("m_role_id");
		$opsi_role = $this->input->post("opsi_role");

		$this->load->model("M_user_role", "st");
		
		if(is_array($m_role_id) && !empty($m_role_id))
		{
			$this->st->delete_by(array("m_user_id" => $m_user_id));
			foreach($m_role_id as $k=>$v)
			{
				$cek = $this->st->get_by(array("m_user_id" => $m_user_id, "m_role_id" => $v));
				$arr["m_role_id"] = $v;
				if(empty($cek))
				{
					$rs = $this->st->insert($arr);
				}
				else
				{
					$rs = $this->st->update($cek->m_user_role_id, $arr);
				}
			}
		}
		else
		{
			$rs = $this->st->delete_by(array("m_user_id" => $m_user_id));
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_function", "st");
//		$rs = $this->kurs->delete($id);		
		$rs = $this->st->delete($id);					
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}


	function getAkses($id)
	{
		$this->load->model("M_user_role","mr");
		$data["list"]["role"] = $this->mr->get_many_by("m_user_id", $id);
		$dataid = json_decode($this->get($id,1));
		if($dataid->success)
		{
			$data["list"]["username"] = $dataid->data->name;
		}
		
		if($data["list"]) echo json_encode(array("success" => true, "data" => $data["list"]));				
		else echo json_encode(array("success" => false, "message" => "Record Not Found"));				
	}
	
	
	function getChangePassword()
	{
		$this->load->model("M_user","mu");
		$rs = $this->mu->get($this->session->userdata("m_user_id"));
		$this->load->model("M_cabang","mb");
		
		$br = $this->mb->get($rs->m_branch_id);
		$rs->branch = $br->name;
		
		$this->load->view("change_password", (array)$rs);	
	}

	
}