<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Kelompok Transaksi
* 
********************************************************************/ 

class Kelompok_transaksi extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_kelompok_transaksi");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_thp_column", "column");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->column->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->column->order_by('m_p_thp_column_id','DESC');
		$data["list"] = $this->column->search($arr)->get_all();
	// echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->column->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function getListGroup()
	{
		$page = $this->input->get("page1");

		$this->load->model("M_p_thp_group", "group");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$cari=0;
		$this->group->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page1")))
			{
				if($v!="") 
				{
					//$arr[($k)] = $v;
					//$arrct[] = ($k)." ILIKE '%{$v}%'";
					$arr[($k)] = $v;
			
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
					if ( $k == 'column_name')	
					{
							$this->group->db->join('m_p_thp_column','m_p_thp_column.m_p_thp_column_id=m_p_thp_group.m_p_thp_column_id');
							$cari=1;
					}
				}
			}
		}
		$this->group->order_by('m_client_id','ASC');
		$this->group->order_by('m_p_thp_group.m_p_thp_column_id','ASC');
		$data["list"] = $this->group->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{
				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;

				$this->load->model("M_p_thp_column", "column");
				$client = $this->column->get($r->m_p_thp_column_id);
				if($client) 
				{
					$data["list"][$k]->column_name = $client->column_name;
					$data["list"][$k]->ref_label_type_id = $client->ref_label_type_id;
				}

				$this->load->model("M_p_description_transaction","transaction");
				$transaction = $this->transaction->get($r->m_p_description_transaction_id);
				if($transaction) 
				{
					$data["list"][$k]->m_p_description_transaction_name = $transaction->m_p_description_transaction_name;
					$data["list"][$k]->m_p_description_transaction_code = $transaction->m_p_description_transaction_code;
				}

			}

	    if ( $cari=1)	
			$this->group->db->join('m_p_thp_column','m_p_thp_column.m_p_thp_column_id=m_p_thp_group.m_p_thp_column_id');
		$arrct["m_p_thp_group.active"] = "Y";
		$ct = $this->group->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage1" => $ct));		
	} 


 	function getthpcolumn($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_thp_column", "column");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->column->limit($limit,$offset);
		$this->column->order_by("m_p_thp_column_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->column->get_all();
		}
		else
		{
			$data["list"] = $this->column->get($id);
		}	
		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}

	function getthpgroup($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_thp_group", "group");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->group->limit($limit,$offset);
		$this->group->db->join('m_p_thp_column','m_p_thp_column.m_p_thp_column_id=m_p_thp_group.m_p_thp_column_id');
		$this->group->db->join('m_client','m_p_thp_group.m_client_id=m_client.m_client_id');
		$this->group->db->join('m_p_description_transaction','m_p_description_transaction.m_p_description_transaction_id=m_p_thp_group.m_p_description_transaction_id');
		$this->group->order_by("m_p_thp_group_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->group->get_all();
		}
		else
		{
			$data["list"] = $this->group->get($id);
		}	
		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_thp_column_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_p_thp_column_id = $this->input->post("m_p_thp_column_id");
		$this->load->model("M_p_thp_column", "column");
		if($m_p_thp_column_id==0)
		{
			$rs = $this->column->insert($arr);		
		}
		else
		{
			$rs = $this->column->update($this->input->post("m_p_thp_column_id"),$arr);					
			$rs = $this->input->post("m_p_thp_column_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}

	function save1()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_thp_group_id","act1","m_client_name","m_client_code","m_p_thp_column_name","m_p_description_transaction_code","m_p_description_transaction_name"))))
			{
				if ( $k == 'm_p_thp_column_id1' ) $k = 'm_p_thp_column_id';
				$arr[($k)] = strtoupper($v);
			}
		}
		$m_p_thp_group_id = $this->input->post("m_p_thp_group_id");
		$this->load->model("M_p_thp_group", "group");
		if($m_p_thp_group_id==0)
		{
			$rs = $this->group->insert($arr);		
		}
		else
		{
			$rs = $this->group->update($this->input->post("m_p_thp_group_id"),$arr);					
			$rs = $this->input->post("m_p_thp_group_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_thp_column", "column");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->column->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function delete1()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_thp_group", "group");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->group->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getColumn($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_p_thp_column", "column");
		$offset = ($page==1) ? 0 : $page*25;
		$this->column->limit(25,$offset);
		$arr = array();
		$data = $this->column->get_many_by("column_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_p_thp_column_id;
			$arr[$i]["name"] = $v->column_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

}