<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Tarif
* 
********************************************************************/ 

class Tarif extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_tarif");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_tarif", "tarif");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tarif->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->tarif->order_by('m_p_tarif_id','DESC');
		$data["list"] = $this->tarif->search($arr)->get_all();
	// echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->tarif->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 


 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_tarif", "tarif");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->tarif->limit($limit,$offset);
		$this->tarif->order_by("m_p_tarif_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->tarif->get_all();
		}
		else
		{
			$data["list"] = $this->tarif->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_tarif_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_p_tarif_id = $this->input->post("m_p_tarif_id");
		$this->load->model("M_p_tarif", "tarif");
		if($m_p_tarif_id==0)
		{
			$rs = $this->tarif->insert($arr);		
		}
		else
		{
			$rs = $this->tarif->update($this->input->post("m_p_tarif_id"),$arr);					
			$rs = $this->input->post("m_p_tarif_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_tarif", "tarif");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->tarif->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

}