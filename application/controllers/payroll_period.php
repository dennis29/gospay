<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Payroll Period
* 
********************************************************************/ 

class Payroll_period extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		
		$data["ref_prorata_id"] = $this->getReferensi(array("id" => "ref_prorata_id",
													   "name" => "ref_prorata_id",
													   "class" => "form-control",
													   "refid" => 42));

		$data["ref_fixed_day_id"] = $this->getReferensi(array("id" => "ref_fixed_day_id",
													   "name" => "ref_fixed_day_id",
													   "class" => "form-control",
													   "refid" => 43)); 

		$data["ref_month_salary_id"] = $this->getReferensi(array("id" => "ref_month_salary_id",
													   "name" => "ref_month_salary_id",
													   "class" => "form-control",
													   "refid" => 44));

		$data["ref_pay_period_id"] = $this->getReferensi(array("id" => "ref_pay_period_id",
													   "name" => "ref_pay_period_id",
													   "class" => "form-control",
													   "refid" => 45));

		$data["ref_ovt_period_id"] = $this->getReferensi(array("id" => "ref_ovt_period_id",
													   "name" => "ref_ovt_period_id",
													   "class" => "form-control",
													   "refid" => 45));

		$data["ref_pay_type_id"] = $this->getReferensi(array("id" => "ref_pay_type_id",
													   "name" => "ref_pay_type_id",
													   "class" => "form-control",
													   "refid" => 46));

		$data["ref_ovt_type_id"] = $this->getReferensi(array("id" => "ref_ovt_type_id",
													   "name" => "ref_ovt_type_id",
													   "class" => "form-control",
													   "refid" => 46));


		$this->load->view("master/view_payroll_period",$data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_payroll_period", "payrol");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->payrol->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
				}
			}
		}
		$this->payrol->order_by('m_p_payroll_period_id','DESC');
		$data["list"] = $this->payrol->search($arr)->get_all();
	//	echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{			
				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;	
				
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_prorata_id);
				if($referensi) $data["list"][$k]->ref_prorata_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_fixed_day_id);
				if($referensi) $data["list"][$k]->ref_fixed_day_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_month_salary_id);
				if($referensi) $data["list"][$k]->ref_month_salary_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_pay_period_id);
				if($referensi) $data["list"][$k]->ref_pay_period_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_pay_type_id);
				if($referensi) $data["list"][$k]->ref_pay_type_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_ovt_period_id);
				if($referensi) $data["list"][$k]->ref_ovt_period_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_ovt_type_id);
				if($referensi) $data["list"][$k]->ref_ovt_type_id = $referensi->name;
				
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->payrol->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 


 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_payroll_period", "payrol");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->payrol->limit($limit,$offset);
		$this->payrol->db->join('m_client','m_p_payroll_period.m_client_id=m_client.m_client_id');
		$this->payrol->order_by("m_p_payroll_period_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->payrol->get_all();
		}
		else
		{
			$data["list"] = $this->payrol->get($id);
		}

		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_payroll_period_id","act","m_client_name"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_p_payroll_period_id = $this->input->post("m_p_payroll_period_id");
		$this->load->model("M_p_payroll_period", "payrol");
		if($m_p_payroll_period_id==0)
		{
			$rs = $this->payrol->insert($arr);		
		}
		else
		{
			$rs = $this->payrol->update($this->input->post("m_p_payroll_period_id"),$arr);					
			$rs = $this->input->post("m_p_payroll_period_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_payroll_period", "payrol");
		$arr["active"]="N";	
		$rs = $this->payrol->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	
	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


}