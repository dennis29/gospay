<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Libur
* 
********************************************************************/ 

class Libur extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_libur");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_day_off", "libur");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->libur->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->libur->order_by('day_off','ASC');
		$data["list"] = $this->libur->search($arr)->get_all();
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->libur->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 


 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_day_off", "libur");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->libur->limit($limit,$offset);
		$this->libur->order_by("m_p_day_off_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->libur->get_all();
		}
		else
		{
			$data["list"] = $this->libur->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$potong='';
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_day_off_id","act"))))
			{
				if ($k=='day_off')
				{	
				$potong = explode('-',$v);
				$v=$potong[2].'-'.$potong[1].'-'.$potong[0];
				}
				$arr[($k)] = strtoupper($v);
			}
			
		}
		$m_p_libur_id = $this->input->post("m_p_day_off_id");
		$this->load->model("M_p_day_off", "libur");
		if($m_p_libur_id==0)
		{  
			$arr['active']='Y';
			$rs = $this->libur->insert($arr);		
		}
		else
		{
			$rs = $this->libur->update($this->input->post("m_p_day_off_id"),$arr);					
			$rs = $this->input->post("m_p_day_off_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_day_off", "libur");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->libur->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

}