<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class State extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}
 		 	 
 	function getPanel() 
	{
//		$this->load->model("Fj_education", "edu");

		$this->load->view("master_data");	
	}

	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_state", "pers");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pers->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pers->order_by('m_state_id','ASC');
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();
		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_state", "ms");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->ms->limit($limit,$offset);
		$this->ms->order_by("m_state_id","ASC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
		}
		else
		{
			$data["list"] = $this->ms->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_state_id_1","m_state_name_1","act_state"))))
			$arr[($k)] = $v;
		}
		$m_state_id = $this->input->post("m_state_id_1");
		$this->load->model("M_state", "st");
		$arr["name"] = $this->input->post("m_state_name_1");
		if($m_state_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_state_id_1"),$arr);					
			$rs = $this->input->post("m_state_id_1");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_state", "st");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getState($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_state", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$q = urldecode($q);
		$q = str_replace("-"," ",$q);
		$data = $this->ml->get_many_by("name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_state_id;
			$arr[$i]["name"] = $v->name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}	
	
}