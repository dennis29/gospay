<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Kontrak Karyawan
* 
********************************************************************/ 

class Kontrak extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Gp_pkwt", "kontrak");
		$this->kontrak->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->kontrak->limit($limit,$offset);
		$data["list"] = $this->kontrak->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->kontrak->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->kontrak->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_pkwt", "pkwt");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pkwt->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pkwt->order_by('gp_pkwt_id','ASC');
		$data["list"] = $this->pkwt->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->pkwt->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_pkwt_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_pkwt_id = $this->input->post("gp_pkwt_id");
		$this->load->model("Gp_pkwt", "pkwt");
		if($gp_pkwt_id==0)
		{
			$rs = $this->pkwt->insert($arr);		
		}
		else
		{
			$rs = $this->pkwt->update($this->input->post("gp_pkwt_id"),$arr);					
			$rs = $this->input->post("gp_pkwt_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	function getListById($id='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_pkwt", "pkwt");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$this->pkwt->limit($limit,$offset);
		$data = array();
		$arrct = array();
		$arr = array();
		if ($id != '')
		{
		$this->pkwt->order_by('gp_pkwt_id','ASC');
		$data = $this->pkwt->get_many_by('gp_personal_id',$id);
		foreach($data as $k_pkwt => $v_pkwt)
		{
			// Panggil Id Serial
			$arr[$i]["gp_pkwt_id"] = $v_pkwt->gp_pkwt_id;
			//////////
			// Panggil Mulai
			$arr[$i]["start_contract_date"] = $v_pkwt->start_contract_date;
			//////////
			// Panggil Sampai
			$arr[$i]["stop_contract_date"] = $v_pkwt->stop_contract_date;
			//////////
			// Panggil Status Kontrak
			$this->load->model("M_referensi_list", "referensi_list");
			$referensi_list = $this->referensi_list->get($v_pkwt->ref_status_pkwt_id);
			if ($referensi_list) $arr[$i]["ref_status_pkwt"] = $referensi_list->name;
			//////////
			// Panggil No PKWT
			$arr[$i]["pkwt_no"] = $v_pkwt->pkwt_no;
			//////////
			// Panggil Catatan
			if ($v_pkwt->pkwt_no == '') $arr[$i]["note"] = '';
			else $arr[$i]["note"] = $v_pkwt->pkwt_no;
			//////////
			$i++;
		}
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		}
		$ct = $this->pkwt->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}

	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vkontrak"] = $this->config->item('kontrak');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["ref_status_pkwt_id"] = $this->getReferensi(array("id" => "ref_status_pkwt_id",
													   "name" => "ref_status_pkwt_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 21));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_kontrak",$data);			
	}
}
