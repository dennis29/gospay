<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Dokumen Karyawan
* 
********************************************************************/ 

class Dokumen extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("Gp_personal", "pers");
		$this->pers->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->pers->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_document", "document");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->document->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->document->order_by('gp_document_id','ASC');
		$data["list"] = $this->document->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->document->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_document_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_document_id = $this->input->post("gp_document_id");
		$this->load->model("Gp_document", "document");
		if($gp_document_id==0)
		{
			$rs = $this->document->insert($arr);		
		}
		else
		{
			$rs = $this->document->update($this->input->post("gp_document_id"),$arr);					
			$rs = $this->input->post("gp_document_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");	
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;

		$data["ref_doc_type_id"] = $this->getReferensi(array("id" => "ref_doc_type_id",
													   "name" => "ref_doc_type_id",
													   "class" => "form-control",
													   "refid" => 39));

		$data["ref_doc_status_id"] = $this->getReferensi(array("id" => "ref_doc_status_id",
													   "name" => "ref_doc_status_id",
													   "class" => "form-control",
													   "refid" => 40));

		
		$data["criteria_id"] = $this->getCombo(array("name" => "m_criteria_id",
															 "model" => "m_kriteria",
															 "id" => "m_criteria_id",
															 "column" => "m_criteria_name",
															 "class" => "form-control"));

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_dokumen2",$data);			
	}
}

