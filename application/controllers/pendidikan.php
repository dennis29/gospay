<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Pendidikan Karyawan
* 
********************************************************************/ 

class Pendidikan extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	

		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get()
	{
		$page = $this->input->get("page");
		
		$this->load->model("gp_personal", "pers");
		$this->pers->order_by("gp_personal_id", "DESC");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[strtoupper($k)] = $v;
					$arrct[] = strtoupper($k)." ILIKE '%{$v}%'";
				}
			}
		}	
		if($this->session->userdata("m_branch_id")!=1) // kalau bukan login dg akses All
		{
			$arrct["m_branch_id"] = $this->session->userdata("m_branch_id");
		}
		
		$this->pers->limit($limit,$offset);
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
		$total = $this->pers->count_by($arrct);
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct,"total" => $total));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_education", "education");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->education->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->education->order_by('gp_education_id','ASC');
		$data["list"] = $this->education->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->education->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_education_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_education_id = $this->input->post("gp_education_id");
		$this->load->model("Gp_education", "education");
		if($gp_education_id==0)
		{
			$rs = $this->education->insert($arr);		
		}
		else
		{
			$rs = $this->education->update($this->input->post("gp_education_id"),$arr);					
			$rs = $this->input->post("gp_education_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan")); 
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["form"] = $form;
		$data["gp_personal_id"] = $id;

		$data["ref_institution_type"] = $this->getReferensi(array("id" => "ref_institution_type",
													   "name" => "ref_institution_type",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 19));	

		$data["ref_cost_type_id"] = $this->getReferensi(array("id" => "ref_cost_type_id",
													   "name" => "ref_cost_type_id",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 33));	

		$data["ref_recognized_companies"] = $this->getReferensi(array("id" => "ref_recognized_companies",
													   "name" => "ref_recognized_companies",
													   "class" => "form-control",
													   "placeholder" => "== Select ==",
													   "refid" => 34));	

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_pendidikan",$data);			
	}

	function getCity($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_city", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->get_many_by("name  ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_city_id;
			$arr[$i]["name"] = $v->name ;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	function getCountry($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_country", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$data = $this->ml->get_many_by("name  ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_country_id;
			$arr[$i]["name"] = $v->name ;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}
}
