<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Kriteria
* 
********************************************************************/ 

class Kriteria extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$data["kriteria_id"] = $this->getReferensi(array("id" => "ref_criteria_id",
													   "name" => "ref_criteria_id",
													   "class" => "form-control",
													   "refid" => 17)); // 17 = kriteria

		$this->load->view("master/view_kriteria",$data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_kriteria", "kriteria");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->kriteria->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'ref_criteria_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
				}
			}
		}
		$this->kriteria->order_by('ref_criteria_id','DESC');
		$this->kriteria->order_by('m_criteria_name','ASC');
		$data["list"] = $this->kriteria->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{			
				// $this->load->model("M_referensi","referensi");
				// $client = $this->referensi->get($r->m_referensi_id);
				// if($referensi) $data["list"][$k]->name = $referensi->name;

				$this->load->model("M_referensi_list", "ml");
				$rw = $this->ml->get($r->ref_criteria_id);
				if($rw) $data["list"][$k]->ref_criteria_id = $rw->name;
				else $data["list"][$k]->ref_criteria_id = "";

				$this->load->model("M_kriteria","kriteria");
				$posisi = $this->kriteria->get($r->m_criteria_id);
				if($posisi) $data["list"][$k]->m_criteria_name = $posisi->m_criteria_name;
				
			}	

		// echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->kriteria->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_kriteria", "kriteria");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->kriteria->limit($limit,$offset);
		$this->kriteria->order_by("m_criteria_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->kriteria->get_all();
		}
		else
		{
			$data["list"] = $this->kriteria->get($id);
		}	
	//	echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}

	function getbycriteria($id=0)
	{
		$page = $this->input->get("page");
		$id = $this->input->get("id");
		$this->load->model("M_kriteria", "kriteria");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->kriteria->limit($limit,$offset);
		$this->kriteria->order_by("m_criteria_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->kriteria->get_all_by_refcriteriaid($id);
		}
		else
		{
			$data["list"] = $this->kriteria->get_by_refcriteriaid($id);
		}	

		$arrct["active"] = "Y";
		$ct = $this->kriteria->count_by($arrct);
		$ct = ceil($ct/$limit);
//	echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		

	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_criteria_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_criteria_id = $this->input->post("m_criteria_id");
		$this->load->model("M_kriteria", "st");
		if($m_criteria_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_criteria_id"),$arr);					
			$rs = $this->input->post("m_criteria_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_kriteria", "st");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}