<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Lokasi
* 
********************************************************************/ 

class Lokasi extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_lokasi");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_lokasi", "lokasi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->lokasi->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->lokasi->order_by('m_location_id','DESC');
		$data["list"] = $this->lokasi->search($arr)->get_all();
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->lokasi->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_lokasi", "lokasi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->lokasi->limit($limit,$offset);
		$this->lokasi->order_by("m_location_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->lokasi->get_all();
		}
		else
		{
			$data["list"] = $this->lokasi->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_location_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_location_id = $this->input->post("m_location_id");
		$this->load->model("M_lokasi", "st");
		if($m_location_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_location_id"),$arr);					
			$rs = $this->input->post("m_location_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_lokasi", "lokasi");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->lokasi->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}