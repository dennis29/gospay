<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sub_district extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}  
 
 	function getPanel() 
	{
//		$this->load->model("Fj_education", "edu");

		$this->load->view("master_data");	
	}

	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_sub_district", "pers");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pers->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pers->order_by('m_sub_district_id','ASC');
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();
		foreach($data["list"] as $k=>$r)
		{
			$this->load->model("M_district", "ml");
			$city = $this->ml->get($r->m_district_id);
			$rw = $this->ml->get($r->m_district_id);
			if($rw) 
			{
				$data["list"][$k]->district = $rw->name;	
			}
			$this->load->model("M_city", "mc");
			$rw1 = $this->mc->get($city->m_city_id);
			if($rw1) 
			{
				$data["list"][$k]->city = $rw1->name;	
			}
			$this->load->model("M_state", "ms");
			$rw2 = $this->ms->get($rw1->m_state_id);
			if($rw2) 
			{
				$data["list"][$k]->state = $rw2->name;	
			}

		}

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_sub_district", "ms");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->ms->limit($limit,$offset);
		$this->ms->order_by("m_sub_district_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
			$this->load->model("M_district", "md");
			$rw1 = $this->md->get($data["list"]->m_district_id);
			if($rw1) $data["list"]->m_district_name = $rw1->name;	
		}
		else
		{
			$data["list"] = $this->ms->get($id);
			$this->load->model("M_district", "md");
			$rw1 = $this->md->get($data["list"]->m_district_id);
			if($rw1) $data["list"]->m_district_name = $rw1->name;	
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_sub_district_id","m_district_name","act"))))
			$arr[($k)] = $v;
		}
		$m_sub_district_id = $this->input->post("m_sub_district_id");
		$this->load->model("M_sub_district", "st");
		if($m_sub_district_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_sub_district_id"),$arr);					
			$rs = $this->input->post("m_sub_district_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_sub_district", "st");
//		$rs = $this->kurs->delete($id);		
		$arr["active"]="N";
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
	
}