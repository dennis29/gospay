<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 27, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Client
* 
********************************************************************/ 

class Client extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$data["m_company_id"] = $this->getCombo(array("name" => "m_company_id",
															 	"model" => "m_perusahaan",
																 "id" => "m_company_id",
																 // "selected" => $this->session->userdata("M_BRANCH_ID"),
																 // "disabled" => "disabled",
																 "column" => "m_company_name",
																 "class" => "form-control"));

		$this->load->view("master/view_client",$data);	
	}

	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_client", "client");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->client->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[$k] = $v;
					$arrct[] = $k." ILIKE '%{$v}%'";
				}
			}
		}
		$this->client->order_by('m_client_code','DESC');
		$data["list"] = $this->client->search($arr)->get_all();
//		echo $this->db->last_query();
		foreach ($data["list"] as $k=>$r) 
			{			
				$this->load->model("M_perusahaan","perusahaan");
				$perusahaan = $this->perusahaan->get($r->m_company_id);
				if($perusahaan) $data["list"][$k]->m_company_name = $perusahaan->m_company_name;	
				else $data["list"][$k]->m_company_name = "-";

			}
		$arrct["active"] = "Y";
		$ct = $this->client->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	}

	function get($id=0)
	{

		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->client->limit($limit,$offset);
		$this->client->order_by("m_client_code","DESC");
		if($id==0)
		{
			$data["list"] = $this->client->get_all();
		}
		else
		{
			$data["list"] = $this->client->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));	
		
		
	} 
	  
	function get_data_nama_company($id='')
	{
		$this->load->model("M_perusahaan", "client");
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}   

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_client_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_client_id = $this->input->post("m_client_id");
		$this->load->model("M_client", "st");
		if($m_client_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_client_id"),$arr);					
			$rs = $this->input->post("m_client_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function save1()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_client_branch_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_client_branch_id = $this->input->post("m_client_branch_id");
		$this->load->model("M_client", "st");
		if($m_client_branch_id==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_client_branch_id"),$arr);					
			$rs = $this->input->post("m_client_branch_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_client", "st");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

}