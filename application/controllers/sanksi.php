<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Sanksi Karyawan
* 
********************************************************************/ 

class Sanksi extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_punishment", "punishment");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->punishment->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->punishment->order_by('gp_punishment_id','ASC');
		$data["list"] = $this->punishment->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->punishment->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 
	function getListById($gp_personal_id_ref='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_punishment", "punishment");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$data = array();
		$arrct = array();
		$arr = array();
		if ($gp_personal_id_ref != '')
		{
		$this->db->order_by('gp_punishment_id','DESC');
		$data = $this->punishment->get_many_personal('gp_personal_id',$gp_personal_id_ref);
		foreach($data as $k_punishment => $v_punishment)
		{
			// Panggil Id Serial
			$arr[$i]["gp_punishment_id"] = $v_punishment->gp_punishment_id;
			///////
			$this->load->model('M_referensi_list','referensi_list');
			$referensi_list=$this->referensi_list->get($v_punishment->ref_punishment_type_id);
			if ($referensi_list)
			//Panggil Nama Hukuman
			$arr[$i]["name"] = $referensi_list->name;
			//////
			//Panggil Mulai
			$arr[$i]['punishment_start_date']=date('d-M-Y',strtotime($v_punishment->punishment_start_date));
			////
			//Panggil Akhir
			$arr[$i]['punishment_end_date']=date('d-M-Y',strtotime($v_punishment->punishment_end_date));
			////
			//Panggil Tanggal Keputusan
			$arr[$i]['decision_date']=date('d-M-Y',strtotime($v_punishment->decision_date));
			////
			//Panggil No Keputusan
			$arr[$i]["decision_no"] = $v_punishment->decision_no;
			//////
			//Panggil Sebab
			$arr[$i]["reason"] = $v_punishment->reason;
			//////
			$i++;
		}
		$this->db->where('gp_personal_id',$gp_personal_id_ref);
		}
		$ct = $this->punishment->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}
	
	function get($id='')
	{
	
		$this->load->model("Gp_punishment", "punishment");
		$arr = array();
		$this->db->order_by('gp_punishment_id','DESC');
		$data = $this->punishment->get_many_personal('gp_punishment_id',$id);
		foreach($data as $k_punishment => $v_punishment)
		{
			// Panggil Jenis Sanksi
			$arr["ref_punishment_type_id"] = $v_punishment->ref_punishment_type_id;
			////
			//Panggil Tanggal Mulai
			$arr['punishment_start_date']=date('d-m-Y',strtotime($v_punishment->punishment_start_date));
			////
			//Panggil Akhir
			$arr['punishment_end_date']=date('d-m-Y',strtotime($v_punishment->punishment_end_date));
			////
			//Panggil No Keputusan
			$arr["decision_no"] = $v_punishment->decision_no;
			//////
			//Panggil Tanggal Keputusan
			$arr['decision_date']=date('d-m-Y',strtotime($v_punishment->decision_date));
			////
			//Panggil Rujukan
			$arr["ref_reference_id"] = $v_punishment->ref_reference_id;
			//////
			//Panggil Pasal
			$arr['article']=$v_punishment->article;
			////
			//Panggil Ayat
			$arr['verse']=$v_punishment->verse;
			////
			//Panggil Huruf
			$arr['letter']=$v_punishment->letter;
			////
			//Panggil Sebab
			$arr["reason"] = $v_punishment->reason;
			//////
			//Panggil  Golongan Hukuman
			$arr["ref_group_punishment"] = $v_punishment->ref_group_punishment;
			/////
			//Panggil Pejabat TTD
			$arr['official']=$v_punishment->official;
			////
			//Panggil Keterangan
			$arr["note-sanksi"] = $v_punishment->note;
			//////
		}
		echo json_encode(array("success" => true, "data" => $arr));		
	}

	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_punishment_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_punishment_id = $this->input->post("gp_punishment_id");
		$this->load->model("Gp_punishment", "punishment");
		if($gp_punishment_id==0)
		{
			$rs = $this->punishment->insert($arr);		
		}
		else
		{
			$rs = $this->punishment->update($this->input->post("gp_punishment_id"),$arr);					
			$rs = $this->input->post("gp_punishment_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["vmutasi"] = $this->config->item('mutasi');
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;

		$data["ref_punishment_type_id"] = $this->getReferensi(array("id" => "ref_punishment_type_id",
													   "name" => "ref_punishment_type_id",
													   "class" => "form-control",
													   "refid" => 9)); // 

		$data["ref_group_punishment"] = $this->getReferensi(array("id" => "ref_group_punishment",
													   "name" => "ref_group_punishment",
													   "class" => "form-control",
													   "refid" => 10)); 

		$data["ref_reference_id"] = $this->getReferensi(array("id" => "ref_reference_id",
													   "name" => "ref_reference_id",
													   "class" => "form-control",
													   "refid" => 37)); // 1 = gender
		
		

		$this->load->model("M_referensi_list", "ml");
		$pas = $this->ml->get_many_by("m_referensi_id",18);
		

		$this->load->view("karyawan/view_sanksi",$data);			
	}
}

