<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Gaji Karyawan
* 
********************************************************************/ 

class Gaji extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  

	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get($id)
	{	
		$this->load->model("Gp_salary", "salary");
		$data = $this->salary->get_many_personal('gp_salary_id',$id);
		foreach($data as $k_salary => $v_salary)
		{
		//Panggil TMT Gaji
		$row_salary['salary_start_date']=date('d-m-Y',strtotime($v_salary->salary_start_date));
		//Panggil Gaji Pokok
		$row_salary['value']=$v_salary->value;
		//Panggil Status
		if ($v_salary->status == 'N' || $v_salary->status == '0') $row_salary['status']='Tidak Aktif';
		else $row_salary['status']='Aktif';
		//Panggil Tanggal SK
		$row_salary['sk_date-gaji']=date('d-m-Y',strtotime($v_salary->sk_date));
		//Panggil No SK
		$row_salary['sk_no-gaji']=$v_salary->sk_no;
		//Panggil Keterangan
		$row_salary['note-gaji']=$v_salary->note;
		}
		echo json_encode(array("success" => true, "data" => $row_salary));		
	}
		
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_salary", "gaji");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->gaji->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->gaji->order_by('gp_salary_id','ASC');
		$data["list"] = $this->gaji->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->gaji->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 
	function getListById($id='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_salary", "salary");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$data = array();
		$arrct = array();
		$arr = array();
		if ($id != '')
		{
		$this->db->order_by('gp_salary_id','DESC');
		$data = $this->salary->get_many_personal('gp_personal_id',$id);
		foreach($data as $k_salary => $v_salary)
		{
			// Panggil Id Serial
			$arr[$i]["gp_salary_id"] = $v_salary->gp_salary_id;
			//Panggil TMT Date
			$arr[$i]["salary_start_date"] = date('d-M-Y', strtotime($v_salary->salary_start_date));
			//Panggil Gaji Pokok
			$arr[$i]["value"] = number_format($v_salary->value,0,'.',',');
			//Panggil Status
			$arr[$i]["status"] = $v_salary->status;
			//Panggil Keterangan
			$arr[$i]["note"] = $v_salary->note;
			//////////
			$i++;
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
		$this->db->where('gp_personal_id',$id);
		}
		$ct = $this->salary->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));		
	}

	function save($gp_personal_id_ref='')
	{	
		if ($gp_personal_id_ref)
		{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_salary_id")))) $arr[($k)] = strtoupper($v);
		}
		$gp_salary_id = $this->input->post("gp_salary_id");
		$this->load->model("Gp_salary", "gaji");
					$active=$this->input->post('status');
		if($gp_salary_id==0)
		{
			$this->db->where('gp_personal_id',$gp_personal_id_ref);
			$data['active']='N';
			$data['status']='N';
			$arr['gp_personal_id']=$gp_personal_id_ref;
			if ($active=='Aktif') $arr['status']='Y';
			else $arr['status']='N';
			$update=$this->gaji->update_all($data);
			$rs = $this->gaji->insert($arr);		
		}
		else
		{
			if ($active=='Aktif') $arr['status']='Y';
			else $arr['status']='N';
			$rs = $this->gaji->update($this->input->post("gp_salary_id"),$arr);					
			$rs = $this->input->post("gp_salary_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
		}
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		$data["status"] = $this->getReferensi(array("id" => "status",
													   "name" => "status",
													   "class" => "form-control",
													   "refid" => 41)); 	
		

		$this->load->view("karyawan/view_gaji",$data);			
	}
}
