<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 07, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Master Alamat
* 
********************************************************************/ 

class Master_alamat extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("setting/view_master_alamat");	
	}
	
}