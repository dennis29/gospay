<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class City extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 * Kalau session ga ada, tendang..
	 */
	public function index()
	{
		
	}
 		 
 	function getPanel() 
	{
//		$this->load->model("Fj_education", "edu");

		$this->load->view("master_data");	
	}

	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_city", "pers");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->pers->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->pers->order_by('m_city_id','ASC');
		$data["list"] = $this->pers->search($arr)->get_all();
		//echo $this->db->last_query();
		foreach($data["list"] as $k=>$r)
		{
			$this->load->model("M_state", "ml");
			$rw = $this->ml->get($r->m_state_id);
			if($rw) 
			{
				$data["list"][$k]->m_state_id = $rw->name;	
			}

		}

		$ct = $this->pers->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_city", "ms");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->ms->limit($limit,$offset);
		$this->ms->order_by("M_CITY_ID","ASC");
		if($id==0)
		{
			$data["list"] = $this->ms->get_all();
			$this->load->model("M_state", "md");
			$rw1 = $this->md->get($data["list"]->m_state_id);
			if($rw1) $data["list"]->m_state_name = $rw1->name;	
		}
		else
		{
			$data["list"] = $this->ms->get($id);
			$this->load->model("M_state", "md");
			$rw1 = $this->md->get($data["list"]->m_state_id);
			if($rw1) $data["list"]->m_state_name = $rw1->name;	
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_city_id_1","m_city_name_1","m_state_name","act_city"))))
			$arr[($k)] = $v;
		}
		$m_city_id_1 = $this->input->post("m_city_id_1");
		$arr["name"] = $this->input->post("m_city_name_1");
		$this->load->model("M_city", "st");
		if($m_city_id_1==0)
		{
			$rs = $this->st->insert($arr);		
		}
		else
		{
			$rs = $this->st->update($this->input->post("m_city_id_1"),$arr);					
			$rs = $this->input->post("m_city_id_1");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_city", "st");
//		$rs = $this->kurs->delete($id);		
		$arr["active"]="N";	
		$rs = $this->st->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
	
	function getCity($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_city", "ml");
		$offset = ($page==1) ? 0 : $page*25;
		$this->ml->limit(25,$offset);
		$arr = array();
		$q = urldecode($q);
		$q = str_replace("-"," ",$q);
		$data = $this->ml->get_many_by("name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_city_id;
			$arr[$i]["name"] = $v->name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}

	
}