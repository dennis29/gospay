<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 13, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Overtime
* 
********************************************************************/ 

class Overtime_parameter extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		
		$data["ref_category_id"] = $this->getReferensi(array("id" => "ref_category_id",
													   "name" => "ref_category_id",
													   "class" => "form-control",
													   "refid" => 28)); // 		

		$data["ref_taxes_id"] = $this->getReferensi(array("id" => "ref_taxes_id",
													   "name" => "ref_taxes_id",
													   "class" => "form-control",
													   "refid" => 25)); // 

		$data["ref_max_ovt_hours_id"] = $this->getReferensi_default(array("id" => "ref_max_ovt_hours_id",
													   "name" => "ref_max_ovt_hours_id",
													   "class" => "form-control",
													   "refid" => 47)); // 

		$data["ref_overtime_type_id"] = $this->getReferensi(array("id" => "ref_overtime_type_id",
													   "name" => "ref_overtime_type_id",
													   "class" => "form-control",
													   "refid" => 48)); // 

		$this->load->view("master/view_overtime_parameter",$data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_overtime_parameter", "tc");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tc->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}

					//$arr[($k)] = $v;
					//$arrct[] = ($k)." ILIKE '%{$v}%'";
					//if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','M_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
				}
				
			}
		}
		$this->tc->order_by('m_p_overtime_parameter.m_p_overtime_parameter_id','DESC');
		$data["list"] = $this->tc->search($arr)->get_all();
//echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{	
			
			    $this->load->model("M_p_tarif","tarif");
				$tarif = $this->tarif->get($r->m_p_tarif_id);
				if($tarif) $data["list"][$k]->m_p_tarif_name = $tarif->m_p_tarif_name;

				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;

				$this->load->model("Gp_personal","personal");
				$personal = $this->personal->get($r->gp_personal_id);
				if($personal) 
				{
					$data["list"][$k]->personal_name = $personal->personal_name;
					$data["list"][$k]->nrk = $personal->nrk;
				}

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_category_id);
				if($referensi) $data["list"][$k]->ref_category_id = $referensi->name;
				
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_taxes_id);
				if($referensi) $data["list"][$k]->ref_taxes_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_max_ovt_hours_id);
				if($referensi) $data["list"][$k]->ref_max_ovt_hours_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_overtime_type_id);
				if($referensi) $data["list"][$k]->ref_overtime_type_id = $referensi->name;

				
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->tc->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 



	function getList1()
	{
		$page = $this->input->get("page");

		$this->load->model("M_p_overtime_parameter", "tc");
//		$this->db->where("\"m_p_tarif_transaction\".\"active\" = 'Y'");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->tc->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}

					//$arr[($k)] = $v;
					//$arrct[] = ($k)." ILIKE '%{$v}%'";
					if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','m_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
				}
				
			}
		}
		$this->tc->order_by('m_p_overtime_parameter.m_p_overtime_parameter_id','DESC');
		$data["list"] = $this->tc->search($arr)->get_all();
//echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{	
			    $this->load->model("M_p_tarif","tarif");
				$tarif = $this->tarif->get($r->m_p_tarif_id);
				if($tarif) $data["list"][$k]->m_p_tarif_name = $tarif->m_p_tarif_name;

				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;

				$this->load->model("Gp_personal","personal");
				$personal = $this->personal->get($r->gp_personal_id);
				if($personal) 
				{
					$data["list"][$k]->personal_name = $personal->personal_name;
					$data["list"][$k]->nrk = $personal->nrk;
				}

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_category_id);
				if($referensi) $data["list"][$k]->ref_category_id = $referensi->name;
				
				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_taxes_id);
				if($referensi) $data["list"][$k]->ref_taxes_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_max_ovt_hours_id);
				if($referensi) $data["list"][$k]->ref_max_ovt_hours_id = $referensi->name;

				$this->load->model("M_referensi_list","referensi");
				$referensi = $this->referensi->get($r->ref_overtime_type_id);
				if($referensi) $data["list"][$k]->ref_overtime_type_id = $referensi->name;

				
			}
//		echo $this->db->last_query();
if ( $k == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','m_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		$arrct["m_p_overtime_parameter.active"] = "Y";
		$ct = $this->tc->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 


 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_p_overtime_parameter", "tc");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->tc->limit($limit,$offset);
		$this->tc->db->join('m_p_tarif','m_p_overtime_parameter.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		$this->tc->db->join('m_client','m_p_overtime_parameter.m_client_id=m_client.m_client_id');
		$this->tc->db->join('gp_personal','gp_personal.gp_personal_id=m_p_overtime_parameter.gp_personal_id','left');
	//	$this->tc->order_by("m_p_overtime_parameter.m_p_overtime_parameter_id","DESC");
        
		if($id==0)
		{
			$data["list"] = $this->tc->get_all();
		}
		else
		{
			$data["list"] = $this->tc->get($id);
		}	
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_p_overtime_parameter_id","act"))))
			{
				if ($k=='overtime_start' || $k=='overtime_end')
				{	
				$potong = explode('-',$v);
				$v=$potong[2].'-'.$potong[1].'-'.$potong[0];
				}
				$arr[($k)] = strtoupper($v);
			}
		}
		$m_p_overtime_parameter_id = $this->input->post("m_p_overtime_parameter_id");
		$this->load->model("M_p_overtime_parameter", "ovt_par");
		if($m_p_overtime_parameter_id==0)
		{
			$rs = $this->ovt_par->insert($arr);		
		}
		else
		{
			$rs = $this->ovt_par->update($this->input->post("m_p_overtime_parameter_id"),$arr);					
			$rs = $this->input->post("m_p_overtime_parameter_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_p_overtime_parameter", "ovt_par");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->ovt_par->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getTarif($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_p_tarif", "tarif");
		$offset = ($page==1) ? 0 : $page*25;
		$this->tarif->limit(25,$offset);
		$arr = array();
		$data = $this->tarif->get_many_by("m_p_tarif_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_p_tarif_id;
			$arr[$i]["code"] = $v->m_p_tarif_id;
			$arr[$i]["name"] = $v->m_p_tarif_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}



	function getPersonal($q='',$r='')
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("Gp_personal", "personal");
		$offset = ($page==1) ? 0 : $page*25;
		$this->personal->limit(25,$offset);
		$arr = array();
		$data = $this->personal->get_many_personal("(nrk ILIKE '%{$q}%' OR personal_name ILIKE '%{$q}%') and m_client_id = '{$r}'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->gp_personal_id;
			$arr[$i]["name"] = '( '.$v->nrk . ' )   ' . $v->personal_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


}