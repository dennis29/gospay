<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Cuti Bersama
* 
********************************************************************/ 

class Cuti_bersama extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_cuti_bersama");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_mass_leave", "cuti");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->cuti->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->cuti->order_by('m_mass_leave_date','DESC');
		$data["list"] = $this->cuti->search($arr)->get_all();		
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->cuti->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_mass_leave", "cuti");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->cuti->limit($limit,$offset);
		$this->cuti->order_by("m_mass_leave_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->cuti->get_all();
		}
		else
		{
			$data["list"] = $this->cuti->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_mass_leave_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_mass_leave_id = $this->input->post("m_mass_leave_id");
		$this->load->model("M_mass_leave", "cuti");
		if($m_mass_leave_id==0)
		{
			$rs = $this->cuti->insert($arr);		
		}
		else
		{
			$rs = $this->cuti->update($this->input->post("m_mass_leave_id"),$arr);					
			$rs = $this->input->post("m_mass_leave_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_mass_leave", "cuti");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->cuti->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}	

}