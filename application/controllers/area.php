<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 312015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Wilayah & Area
* 
********************************************************************/ 

class Area extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$this->load->view("master/view_area");
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_area", "area");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->area->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'm_client_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
				}
			}
		}
		$this->area->order_by('m_area.m_client_id','DESC');
		$this->area->order_by('m_location.m_location_name','ASC');
		$data["list"] = $this->area->search($arr)->get_all();
	//	echo $this->db->last_query();
		foreach ($data["list"] as $k=> $r) 
			{			
				$this->load->model("M_client","client");
				$client = $this->client->get($r->m_client_id);
				if($client) $data["list"][$k]->m_client_name = $client->m_client_name;
				
				$this->load->model("M_lokasi","lokasi");
				$lokasi = $this->lokasi->get($r->m_location_id);
				if($lokasi) $data["list"][$k]->m_location_name = $lokasi->m_location_name;				
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->area->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_area", "area");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->area->limit($limit,$offset);
		$this->area->order_by("m_area_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->area->get_all();
		}
		else
		{
			$data["list"] = $this->area->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_area_id","act","m_client_name","m_location_name"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_area_id = $this->input->post("m_area_id");
		$this->load->model("M_area", "area");
		if($m_area_id==0)
		{
			$rs = $this->area->insert($arr);		
		}
		else
		{
			$rs = $this->area->update($this->input->post("m_area_id"),$arr);					
			$rs = $this->input->post("m_area_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_area", "area");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->area->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}

	function getClient($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_client", "client");
		$offset = ($page==1) ? 0 : $page*25;
		$this->client->limit(25,$offset);
		$arr = array();
		$data = $this->client->get_many_by("m_client_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_client_id;
			$arr[$i]["code"] = $v->m_client_code;
			$arr[$i]["name"] = $v->m_client_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


	function getLokasi($q)
	{
//		$q = $this->input->get("q");
		$page = $this->input->get("page");
		$this->load->model("M_lokasi", "lokasi");
		$offset = ($page==1) ? 0 : $page*25;
		$this->lokasi->limit(25,$offset);
		$arr = array();
		$data = $this->lokasi->get_many_by("m_location_name ILIKE '%{$q}%'");
		$i = 0;
		foreach($data as $k=>$v)
		{
			$arr[$i]["id"] = $v->m_location_id;
			$arr[$i]["name"] = $v->m_location_name;
			$i++;
		}
	//	echo $this->db->last_query();
		echo json_encode(array("s" => $arr));
	}


}