<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 31, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan proses pada Menu Struktur Jabatan
* 
********************************************************************/ 

class Struktur_jabatan extends MY_Controller {

	/**
	 * Fungsinya buat ngecek session, login, dll.
	 */
	public function index()
	{
		
	} 
	
	function getPanel()
	{
		$data["grade_id"] = $this->getReferensi(array("id" => "ref_grade_id",
													   "name" => "ref_grade_id",
													   "class" => "form-control",
													   "refid" => 8)); // 1 = gender

		$data["m_position_type_id"] = $this->getCombo(array("name" => "m_position_type_id",
															 "model" => "m_posisi",
															 "id" => "m_position_type_id",
															 // "selected" => $this->session->userdata("M_BRANCH_ID"),
															 // "disabled" => "disabled",
															 "column" => "m_position_type_name",
															 "class" => "form-control"));

		$this->load->view("master/view_struktur_jabatan",$data);
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("M_struktur_jabatan", "struktur");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->struktur->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					if ( $k == 'ref_grade_id' || $k == 'm_position_type_id')
					{
						if ( $v != 0 )
							$arrct[] = ($k)." = '{$v}'";
					}
					else
					{
						$arrct[] = ($k)." ILIKE '%{$v}%'";
					}
				}
			}
		}
		$this->struktur->order_by('m_structure_position_id','DESC');
		$this->struktur->order_by('m_position_type_id','ASC');
		$data["list"] = $this->struktur->search($arr)->get_all();
		foreach ($data["list"] as $k=> $r) 
			{			
				// $this->load->model("M_referensi","referensi");
				// $client = $this->referensi->get($r->m_referensi_id);
				// if($referensi) $data["list"][$k]->name = $referensi->name;

				$this->load->model("M_referensi_list", "ml");
				$rw = $this->ml->get($r->ref_grade_id);
				if($rw) $data["list"][$k]->ref_grade_id = $rw->name;
				else $data["list"][$k]->ref_grade_id = "";
				
				$this->load->model("M_posisi","posisi");
				$posisi = $this->posisi->get($r->m_position_type_id);
				if($posisi) $data["list"][$k]->m_position_type_name = $posisi->m_position_type_name;
			}
//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->struktur->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

 	function get($id=0)
	{
		$page = $this->input->get("page");
		$this->load->model("M_struktur_jabatan", "struktur");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$this->struktur->limit($limit,$offset);
		$this->struktur->order_by("m_structure_position_id","DESC");
		if($id==0)
		{
			$data["list"] = $this->struktur->get_all();
		}
		else
		{
			$data["list"] = $this->struktur->get($id);
		}		
		echo json_encode(array("success" => true, "data" => $data["list"]));		
	}
	
	function save()
	{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("m_structure_position_id","act"))))
			$arr[($k)] = strtoupper($v);
		}
		$m_structure_position_id = $this->input->post("m_structure_position_id");
		$this->load->model("M_struktur_jabatan", "struktur");
		if($m_structure_position_id==0)
		{
			$rs = $this->struktur->insert($arr);		
		}
		else
		{
			$rs = $this->struktur->update($this->input->post("m_structure_position_id"),$arr);					
			$rs = $this->input->post("m_structure_position_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
	}
	
	function delete()
	{
		$id = $_GET["id"];
		$this->load->model("M_struktur_jabatan", "struktur");
//		$rs = $this->kurs->delete($id);	
		$arr["active"]="N";	
		$rs = $this->struktur->update($id,$arr);		
		if($rs) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false,"id" => $id));	
	}
}