<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: Oktober 01, 2015
* Development Group: GOS Payment (HRIS)
* Description: Controller Atensi Karyawan
* 
********************************************************************/ 

class Atensi extends MY_Controller { 

	/**  
	 * Fungsinya buat ngecek session, login, dll. 
	 * Kalau session ga ada, tendang..
	 */  
	public function index()
	{
		 
	}
	
	function getPanel()  
	{    
		$data["akses"] = $this->session->userdata("AKSES");	
/*		if(in_array("pp-admin",$data["akses"])) $data["pp_admin"] = 1; //jika 1, tombol pp admin tampil
		else $data["pp_admin"] = 0;

		if(in_array("pp-appserv",$data["akses"])) $data["pp_appserv"] = 1; //jika 1, tombol pp appserv tampil
		else $data["pp_appserv"] = 0;

		if(in_array("add-karyawan",$data["akses"])) $data["addkaryawan"] = 1; //jika 1, tombol add kandidat tampil
		else $data["addkaryawan"] = 0;
*/
		$this->load->model("Gp_personal", "pers");
		$this->load->view("karyawan/ListKaryawan",$data);	
	} 
	
	function get($id='')
	{
		
		$this->load->model("Gp_atensi", "atensi");
		$data=$this->atensi->get_many_personal('gp_atensi_id',$id);
		
		$arr = array();
		foreach($data as $k_atensi => $v_atensi)
		{
		// Panggil Tanggal Catatan
		$arr['add_date-atensi']=date('d-m-Y',strtotime($v_atensi->add_date));
		///
		////Panggil Judul
		$arr['title-atensi']=$v_atensi->title;
		///
		////Panggil Uraian
		$arr['note-atensi']=$v_atensi->note;
		////
		}

		echo json_encode(array("success" => true, "data" => $arr));		
	}
	
	function getListById($gp_personal_id_ref='')
	{
		$page = $this->input->get("page");
		$this->load->model("Gp_atensi", "atensi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;
		$i=0;
		$data = array();
		$arrct = array();
		$arr = array();
		if ($gp_personal_id_ref != '')
		{
		$this->db->order_by('gp_atensi_id','DESC');
		$data = $this->atensi->get_many_personal('gp_atensi_id',$gp_personal_id_ref);
		foreach($data as $k_atensi => $v_atensi)
		{
			$arr[$i]["gp_atensi_id"] = $v_atensi->gp_atensi_id;
			// Panggil Id Serial
			////
			//Panggil Judul
			$arr[$i]["title"] = $v_atensi->title;
			/////
			//Panggil Tanggal Catatan
			$arr[$i]["add_date"] = date('d-M-Y', strtotime($v_atensi->add_date));
			/////
			//Panggil Uraian Catatan
			$arr[$i]["note"] = $v_atensi->note;
			/////
			$i++;
		}
		//		echo $this->db->last_query();
		//$arrct["active"] = "Y";
		$this->db->where('gp_personal_id',$gp_personal_id_ref);
		}
		$ct = $this->atensi->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $arr, "totpage" => $ct));				
	}
	
	function getList()
	{
		$page = $this->input->get("page");

		$this->load->model("Gp_atensi", "atensi");
		$limit = $this->config->item("limit");
		$offset = ($page==1) ? 0 : ($page-1)*$limit;

		$this->atensi->limit($limit,$offset);
		$arr = array();
		$arrct = array();
		foreach($this->input->get() as $k=>$v)
		{
			if(!in_array($k, array("page")))
			{
				if($v!="") 
				{
					$arr[($k)] = $v;
					$arrct[] = ($k)." ILIKE '%{$v}%'";
				}
			}
		}
		$this->atensi->order_by('gp_atensi_id','ASC');
		$data["list"] = $this->atensi->search($arr)->get_all();
		//		echo $this->db->last_query();
		$arrct["active"] = "Y";
		$ct = $this->atensi->count_by($arrct);
		$ct = ceil($ct/$limit);
//		echo $this->db->last_query();
		echo json_encode(array("success" => true, "data" => $data["list"], "totpage" => $ct));		
	} 

	function save($gp_personal_id_ref='')
	{
		if ($gp_personal_id_ref != '')
		{
		$arr = array();
		foreach($this->input->post() as $k=>$v)
		{
			if( ($v!="" || $v!=0) && (!in_array($k, array("gp_atensi_id"))))
			$arr[($k)] = strtoupper($v);
		}
		$gp_atensi_id = $this->input->post("gp_atensi_id");
		$this->load->model("Gp_atensi", "atensi");
		if($gp_atensi_id==0)
		{
			$this->db->where('gp_personal_id',$gp_personal_id_ref);
			$data['active']='N';
			$this->atensi->update_all($data);
			$arr['gp_personal_id']=$gp_personal_id_ref;
			$rs = $this->atensi->insert($arr);		
		}
		else
		{
		
			$rs = $this->atensi->update($this->input->post("gp_atensi_id"),$arr);					
			$rs = $this->input->post("gp_atensi_id");					
		}
		if($rs) echo json_encode(array("success" => true, "id" => $rs));
		else echo json_encode(array("success" => false, "message" => "Data Gagal Disimpan"));
		}
	}


	function getDetail($id,$form='')
	{	
		if($id==0) $data["tabs"]="";
		else $data["tabs"]="tabs";

		$data["akses"] = $this->session->userdata("AKSES");				
		$data["form"] = $form;
		$data["gp_personal_id"] = $id;
		

		$this->load->view("karyawan/view_atensi",$data);			
	}
}

