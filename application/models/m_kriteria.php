<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Master Kriteria 
* 
********************************************************************/ 
class M_kriteria extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_criteria_id";
		$this->_table = "m_criteria";
	//	$this->db->join("m_referensi_list","m_referensi_list.m_referensi_list_id=m_criteria.ref_criteria_id and m_referensi_list.m_referensi_id = '17'");
	
	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
				if ($k == 'ref_criteria_id')
				{
					if ( $v != 0 ) $this->db->where(("m_criteria.".$k)." = '{$v}'");
				}
				else
				{
					$this->db->where(($k)." ILIKE '%{$v}%'");	
				}
			}				
		return $this;
	}	
	
	function searchbycriteria($val)
	{
		$this->db->where('ref_criteria_id',$val)->where('m_criteria',  'Y');			
		return $this;
	}		

}