<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Resign Karyawan
* 
********************************************************************/ 
class Gp_resign extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_resign_id";
		$this->_table = "gp_resign";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("gp_resign")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("gp_resign", $this->session->userdata("gp_resign"));		
		}
		return $this;
	}	
}