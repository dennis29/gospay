<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Cuti Karyawan
* 
********************************************************************/ 
class Gp_leave extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_leave_id";
		$this->_table = "gp_leave";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("gp_leave")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("gp_leave_id", $this->session->userdata("gp_leave_id"));		
		}
		return $this;
	}	
}