<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Daftar Isi Tabel Referensi
* 
********************************************************************/ 
class M_referensi_list extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_referensi_list_id";
		$this->_table = "m_referensi_list";
	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}

	function likes($key,$value)
	{
//		$this->db->like("LOWER(".$key.")",strtolower($value));
		$this->db->where($key." ILIKE '%{$value}%' and \"M_REFERENCE_ID\" = '18'");		
		return $this;
	}

	function likes_oversea($key,$value)
	{
//		$this->db->like("LOWER(".$key.")",strtolower($value));
		$this->db->where($key." ILIKE '%{$value}%' and \"M_REFERENCE_ID\" = '5'");		
		return $this;
	}

	function likesbyID($key,$value,$id)
	{
//		$this->db->like("LOWER(".$key.")",strtolower($value));
		$this->db->where($key." ILIKE '%{$value}%' and \"M_REFERENCE_ID\" = '".$id."'");		
		return $this;
	}				

}