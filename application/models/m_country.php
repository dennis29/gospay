<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Kota
* 
********************************************************************/ 
class M_country extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_country_id";
		$this->_table = "m_country";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("m_country")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("m_country_id", $this->session->userdata("m_country_id"));		
		}
		return $this;
	}	
}