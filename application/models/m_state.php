<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 07, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Alamat --> Provinsi
* 
********************************************************************/ 

class M_state extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_state_id";
		$this->_table = "m_state";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}			
	
}