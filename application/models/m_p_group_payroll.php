<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Kelompok Payrol
* 
********************************************************************/ 
class M_p_group_payroll extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_p_group_payroll_id";
		$this->_table = "m_p_group_payroll";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="")
			{
				
				if ($k == 'm_client_id')
				{
					if ( $v != 0 ) $this->db->where(($k)." = '{$v}'");
				}
				else
				{
					$this->db->where(($k)." ILIKE '%{$v}%'");	
					
		//	if ( $k == 'm_p_tarif_name' && $v !="" ) $this->db->join('m_p_tarif','m_p_tarif_client.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
					
				}
			}		
		}				
		return $this;
	}				
	
	function where_or_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->or_where($k2, $v2);
			}
		}
		return $this;				
	}

}