<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Cuti Bersama
* 
********************************************************************/ 
class M_p_day_off extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_p_day_off_id";
		$this->_table = "m_p_day_off";
	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}				

}