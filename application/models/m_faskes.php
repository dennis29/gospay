<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Fasilitas Kesehatan 
* 
********************************************************************/ 
class M_faskes extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_faskes_id";
		$this->_table = "m_faskes";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("m_faskes")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("m_faskes_id", $this->session->userdata("m_faskes_id"));		
		}
		return $this;
	}	
}