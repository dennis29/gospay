<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Gp_report extends MY_Model{

	function __construct()
	{
		parent::__construct();
	}
	
	function getReference($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected","order_by")))
				$html .= $k."=".$v." ";
		}
		$html .= ">";
		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$this->load->model("M_reference_list","ml");
//		$this->ml->where(array("m_referensi_id" => $arr["refid"]));
		if(empty($arr["order_by"])) $this->ml->order_by("m_referensi_list_id", "ASC");
		else $this->ml->order_by($arr["order_by"], "ASC");
		$rs = $this->ml->get_many_by("m_referensi_id", $arr["refid"]);	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->m_referensi_list_id)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->m_referensi_list_id."\">".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

		
}