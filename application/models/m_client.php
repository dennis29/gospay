<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 /**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Client
* 
********************************************************************/ 

class M_client extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_client_id";
		$this->_table = "m_client";
		$this->db->join('m_company','m_client.m_company_id=m_company.m_company_id');
	}
	function search($arr)
	{

		$this->primary_key = "m_client_id";
		$this->_table = "m_client";
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}						

}