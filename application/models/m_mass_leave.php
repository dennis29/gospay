<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Cuti Bersama
* 
********************************************************************/ 
class M_mass_leave extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_mass_leave_id";
		$this->_table = "m_mass_leave";
	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}				

}