<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Personal Information Karyawan
* 
********************************************************************/ 
class Gp_personal extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_personal_id";
		$this->_table = "gp_personal";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
	/*
		if($this->session->userdata("gp_personal")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("gp_personal_id", $this->session->userdata("gp_personal_id"));		
		}
	*/
		return $this;
	}	
}