<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Tarif Client Payrol
* 
********************************************************************/ 
class M_p_tarif_client extends MY_Model{

	function __construct()
	{
		parent::__construct();
		
		$this->_table = "m_p_tarif_client";
		$this->primary_key = $this->_table. ".m_p_tarif_client_id";
	   // $this->db->join('m_p_tarif',$this->_table.'.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		$this->db->join('m_p_tarif_transaction',$this->_table.'.m_p_tarif_client_id=m_p_tarif_transaction.m_p_tarif_client_id');
		
	
	}

     function search($arr)
	{
// $this->db->join('m_p_tarif','m_p_tarif_client.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		foreach($arr as $k=>$v)
		{
			if($v!="")
			{
				
				if ($k == 'm_client_id')
				{
					if ( $v != 0 ) $this->db->where(($k)." = '{$v}'");
				}
				else
				{
					$this->db->where(($k)." ILIKE '%{$v}%'");	
					
		//	if ( $k == 'm_p_tarif_name' && $v !="" ) $this->db->join('m_p_tarif','m_p_tarif_client.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
					
				}
			}	


			//if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");	
		//	if ( $arr == 'm_p_tarif_name' ) $this->tc->db->join('m_p_tarif','m_p_tarif_client.m_p_tarif_id=m_p_tarif.m_p_tarif_id');
		
		}				
		return $this;
	}		


	function where_or_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->or_where($k2, $v2);
			}
		}
		return $this;				
	}

	function gettarifclient($data)
	{
		
      
		   $hasil=$this->db->query("select * from m_p_tarif_client join m_p_tarif_transaction on \"m_p_tarif_transaction\".\"m_p_tarif_client_id\" = \"m_p_tarif_client\".\"m_p_tarif_client_id\"where \"m_p_tarif_client\".\"m_p_tarif_client_id\" = '$data' ") ;
		$rs=0;
		if ($hasil->num_rows() > 0)
			{
			   foreach ($hasil->result() as $row)
			   {
				  $rs++;
				 
			   }
			} 
			else
				$rs=0;

		
		return $rs;
	}

	function getdata($data)
	{
		$awhere = explode(",", $data);
      
		   $hasil=$this->db->query("select m_p_tarif_client_id from m_p_tarif_client where \"m_client_id\" = '$awhere[0]' and \"m_p_tarif_id\" = '$awhere[1]' and  \"ref_category_id\" = '$awhere[2]' and \"gp_personal_id\" = '$awhere[3]' and \"ref_tax_type_client_id\" = '$awhere[4]' ") ;
		
		if ($hasil->num_rows() > 0)
			{
			   foreach ($hasil->result() as $row)
			   {
				  $rs = $row->m_p_tarif_client_id;
				 
			   }
			} 
			else
				$rs=0;

		
		return $rs;
	}

}