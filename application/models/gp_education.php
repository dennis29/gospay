<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Pendidikan Karyawan
* 
********************************************************************/ 
class Gp_education extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_education_id";
		$this->_table = "gp_education";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("gp_education")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("gp_education_id", $this->session->userdata("gp_education_id"));		
		}
		return $this;
	}	
}