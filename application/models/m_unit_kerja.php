<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 07, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Unit Kerja
* 
********************************************************************/ 

class M_unit_kerja extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_unit_id";
		$this->_table = "m_unit";
		$this->db->join("m_client","m_client.m_client_id = ".$this->_table.".m_client_id");
	}


	function search($arr)
	{
		foreach($arr as $k=>$v)
		{

			if($v!="")
			{
				if ($k == 'm_client_id')
				{
					if ( $v != 0 ) $this->db->where(("m_unit.".$k)." = '{$v}'");
				}
				else
				{
					$this->db->where(($k)." ILIKE '%{$v}%'");	
				}
			}	
		}
		
		return $this;
	}				

}