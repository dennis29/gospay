<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Master Struktur Jabatan
* 
********************************************************************/ 
class M_struktur_jabatan extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_structure_position_id";
		$this->_table = "m_structure_position";
	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if ( $k == 'ref_grade_id' || $k == 'm_position_type_id')
				{
					if ( $v != 0 ) $this->db->where(("m_structure_position.".$k)." = '{$v}'");
				}
				else
				{
					$this->db->where(($k)." ILIKE '%{$v}%'");	
				}		
		}				
		return $this;
	}				

}