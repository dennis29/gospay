<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Client Cabang
* 
********************************************************************/ 
class M_client_branch extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_client_branch_id";
		$this->_table = "m_client_branch";
		$this->db->join('m_client','m_client.m_client_id=m_client_branch.m_client_id');
		$this->db->join('m_company','m_client.m_company_id=m_company.m_company_id');
		$this->db->join('m_branch','m_client_branch.m_branch_id=m_branch.m_branch_id');
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where($k." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("m_client_branch")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("m_client_branch_id", $this->session->userdata("m_client_branch_id"));		
		}
		return $this;
	}	


	function carisama($arr)
	{
		
		
		$this->db->where("m_client_branch."."m_client_id = " ."'" .$arr."'");		
			


		return $this;
	}	
	function carimirip($client_id,$cari)
	{
		
		
		$this->db->where("m_branch."."m_branch_name ilike '%".$cari."%'")->where("m_client_branch."."m_client_id = " ."'" .$client_id."'");
			


		return $this;
	}	
}