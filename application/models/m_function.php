<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Function System
* 
********************************************************************/ 
class M_function extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_function_id";
		$this->_table = "m_function";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}	
	
}