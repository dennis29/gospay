<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 07, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Alamat --> Kelurahan
* 
********************************************************************/ 
class M_sub_district extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_sub_district_id";
		$this->_table = "m_sub_district";
	}

	function getTypeahead($q)
	{
		$this->db->select('m_district.m_district_id, m_sub_district_id, m_sub_district.name AS sname ,m_district.name AS dname');
		$this->db->from($this->_table);
		$this->db->join('m_district', 'm_district.m_district_id = '.$this->_table.'.m_district_id');
		$q2 = explode(" ", $q);
		if(is_array($q2))
		{
			$this->db->where($this->_table.".name ILIKE '%$q%'");
			foreach($q2 as $k=>$v)
			{
				if($k>0)
				{
					if(strlen($v)>3)
						$this->db->or_where("(\"m_district\".\"name\" ILIKE '%$v%' AND \"m_sub_district\".\"name\" ILIKE '%$q2[0]%')");
				}
			}	
		}
		
		$rs = $this->db->get();
		if($rs->num_rows()>0)
		{
			return $rs->result();	
		}
		else
		{
			return false;	
		}
	}
	
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}	
}