<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Area
* 
********************************************************************/ 

class M_area extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_area_id";
		$this->_table = "m_area";
		$this->db->join("m_client","m_client.m_client_id = ".$this->_table.".m_client_id");
		$this->db->join("m_location","m_location.m_location_id = ".$this->_table.".m_location_id");

	}
	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="")
			{
				if($k=="m_client_id") 
				{
					//echo 'ssssss';
					if ( $v != 0 ) $this->db->where($this->_table.".m_client_id" , $v);
					
				}
				else
				{
					if ( $k == 'wil_name') 
					{
						$d = "(wil_name ILIKE '%{$v}%' OR area_name ILIKE '%{$v}%')";
;						$this->db->where($d);

					}
					
					else $this->db->where(($k)." ILIKE '%{$v}%'");	
				}
			}
		}				
		return $this;
	}	
	
	function getlocation($data)
	{
		
      
		   $hasil=$this->db->query("select m_area_id, m_location.m_location_name m_location_name from m_area join m_location on \"m_area\".\"m_location_id\" = \"m_location\".\"m_location_id\" where \"m_area\".\"m_area_id\" = '$data' ") ;


			if ($hasil->num_rows() > 0)
			{
			   foreach ($hasil->result() as $row)
			   {
				  $rs = $row->m_area_id;
				 
			   }
			} 
			else
				$rs=0;

		
		return $rs;
	}

}