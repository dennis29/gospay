<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: October 16, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Master Kolom Transaksi
********************************************************************/ 
class M_p_group_column extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_p_group_column_id";
		$this->_table = "m_p_group_column";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}				
	
	function where_or_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->or_where($k2, $v2);
			}
		}
		return $this;				
	}

}