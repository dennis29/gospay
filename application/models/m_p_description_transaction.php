<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Deskripsi Transaksi Payrol
* 
********************************************************************/ 
class M_p_description_transaction extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_p_description_transaction_id";
		$this->_table = "m_p_description_transaction";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}				
	
	function where_or_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->or_where($k2, $v2);
			}
		}
		return $this;				
	}

	function searchid($id)
	{
		$this->db->select('name')->where('"m_referensi_list_id"',"$id")->get('m_referensi_list')->result_array();		
				
		return $this;
	}

}