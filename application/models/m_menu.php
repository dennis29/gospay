<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Sistem
* 
********************************************************************/ 
class M_menu extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_menu_id";
		$this->_table = "m_menu";
	}

	
	function where_or_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->or_where($k2, $v2);
			}
		}
		return $this;				
	}

}