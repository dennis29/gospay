<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada List Daftar Cuti
* 
********************************************************************/ 
class M_list_leave_client extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_personal_id";
		$this->_table = "gp_personal";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}		
}