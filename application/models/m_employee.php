<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: August 26, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Menu Master Karyawan
* 
********************************************************************/ 
class M_employee extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "m_employee_id";
		$this->_table = "m_employee";
	}

	function search($arr)
	{
		foreach($arr as $k=>$v)
		{
			if($v!="") $this->db->where(($k)." ILIKE '%{$v}%'");		
		}				
		return $this;
	}	

	function where_array($arr)
	{
		foreach($arr as $k=>$v)
		{
			foreach($v as $k2=>$v2)
			{
				$this->db->where($k2, $v2);
			}
		}
		return $this;				
	}
	
}