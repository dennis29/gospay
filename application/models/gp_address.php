<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**********************************************************************
* Original Author: Muhammad Arif AH
* File Creation Date: September 18, 2015
* Development Group: GOS Payment (HRIS)
* Description: Pengaturan database pada Alamat Karyawan
* 
********************************************************************/ 
class Gp_address extends MY_Model{

	function __construct()
	{
		parent::__construct();
		$this->primary_key = "gp_address_id";
		$this->_table = "gp_address";
	}

	function search($arr)
	{
		if(is_array($arr))
		{
			foreach($arr as $k=>$v)
			{
				if($v!="") $this->db->where(strtoupper($k)." ILIKE '%{$v}%'");		
			}				
		}
		if($this->session->userdata("gp_address")!=1) // kalau bukan login dg akses All
		{
			$this->db->where("gp_address_id", $this->session->userdata("gp_address_id"));		
		}
		return $this;
	}	
}