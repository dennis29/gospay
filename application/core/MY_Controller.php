<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{

   function __construct() 
   {
		parent::__construct();
		if(!$this->session->userdata("M_user_id"))
		{
			//$this->load->view("v_login");
		}
		$this->output->enable_profiler(FALSE); // I keep this here so I dont have to manualy edit each controller to see profiler or not        
   }

	function getCombo($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k, array("model", "column","placeholder","selected")))
				$html .= $k."=".$v." ";
		}
		$html .= ">";
		$column = $arr["column"];
		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$m = ucwords($arr["model"]);
		$this->load->model($m);
		$key = $this->$m->primary_key;
		$rs = $this->$m->get_all();		
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->$key)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->$key."\">".$r->$column."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

	function getComboSalary($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k, array("model", "column","placeholder","selected")))
				$html .= $k."=".$v." ";
		}
		$html .= ">";
		$column = $arr["column"];
		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$m = ucwords($arr["model"]);
		$this->load->model($m);
		$key = $this->$m->primary_key;
		$rs = $this->$m->get_many_by("status = false");	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->$key)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->$key."\">".date("d-m-Y",strtotime($r->$column))."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}


	function getReferensi($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected","order_by")))
				$html .= $k."=".$v." ";
		}
		
		$html .= " >";
		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$this->load->model("M_referensi_list","ml");
//		$this->ml->where(array("M_REFERENCE_ID" => $arr["refid"]));
		if(empty($arr["order_by"])) $this->ml->order_by("m_referensi_list", "ASC");
		else $this->ml->order_by($arr["order_by"], "ASC");
		$rs = $this->ml->get_many_by("m_referensi_id", $arr["refid"]);	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->m_referensi_list_id)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->m_referensi_list_id."\">".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

	function getReferensi_status($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected","order_by")))
				$html .= $k."=".$v." ";
		}
		
		$html .= " >";
		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$this->load->model("M_referensi_list","ml");
//		$this->ml->where(array("M_REFERENCE_ID" => $arr["refid"]));
		if(empty($arr["order_by"])) $this->ml->order_by("m_referensi_list", "ASC");
		else $this->ml->order_by($arr["order_by"], "ASC");
		$rs = $this->ml->get_many_by("m_referensi_id", $arr["refid"]);	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->m_referensi_list_id)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->name."\">".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

	function getReferensi_Type($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected","order_by")))
				$html .= $k."=".$v." ";
		}
		
		$html .= " >";
		$this->load->model("M_referensi_list","ml");
//		$this->ml->where(array("M_REFERENCE_ID" => $arr["refid"]));
		if(empty($arr["order_by"])) $this->ml->order_by("m_referensi_list", "ASC");
		else $this->ml->order_by($arr["order_by"], "ASC");
		$rs = $this->ml->get_many_by("m_referensi_id", $arr["refid"]);	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->m_referensi_list_id)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->name."\">".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

	function getReferensi_default($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected","order_by")))
				$html .= $k."=".$v." ";
		}
		
		$html .= " >";
//		if(empty($arr["placeholder"])) $html .= "<option value=\"0\">== Select ==</option>";
//		else $html .= "<option value=\"0\">".$arr["placeholder"]."</option>";
		$this->load->model("M_referensi_list","ml");
//		$this->ml->where(array("M_REFERENCE_ID" => $arr["refid"]));
		if(empty($arr["order_by"])) $this->ml->order_by("m_referensi_list", "ASC");
		else $this->ml->order_by($arr["order_by"], "ASC");
		$rs = $this->ml->get_many_by("m_referensi_id", $arr["refid"]);	
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->m_referensi_list_id)
					$selected = "selected";	
			}
			$html .= "<option ".$selected." value=\"".$r->m_referensi_list_id."\">".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}
	

	function view_date($tgl, $format=0) // 0 format view indonesia, 1 format dd-mm-yyyy
	{
		$tgl = explode(" ",$tgl);
		$tgl1 = explode("-",$tgl[0]);
		$timetgl = mktime(0, 0, 0, $tgl1[1]  , $tgl1[2], $tgl1[0]);
		if($format == 0)
		{
			$hari = array("","Senin", "Selasa", "Rabu","Kamis", "Jumat", "Sabtu","Minggu");
			$bulan = array("","Januari","Februari", "Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
			if(!isset($tgl[1]))
			{
				return $hari[date("N",$timetgl)].", ".$tgl1[2]." ".$bulan[date("n",$timetgl)]." ".$tgl1[0];	
			}
			else
			{
				return $hari[date("N",$timetgl)].", ".$tgl1[2]." ".$bulan[date("n",$timetgl)]." ".$tgl1[0]." ".$tgl[1];				
			}
		}
		else
		{
			if($format==1)
			{
				if(!isset($tgl[1]))		
				{
					return $tgl1[2]."-".$tgl1[1]."-".$tgl1[0]; 
				}
				else
				{
					return $tgl1[2]."-".$tgl1[1]."-".$tgl1[0]." ".$tgl[1]; 				
				}
			}
		}

		//return $tgl;
	}

	function getReferensiList($arr)
	{
		$html = "";
		$html .= "<select ";
		foreach($arr as $k=>$v)
		{
			if(!in_array($k,array("refid", "placeholder", "selected")))
				$html .= $k."=".$v." ";
		}
		$html .= ">";
		if(empty($arr["placeholder"])) $html .= "<option value='0'>== Select ==</option>";
		else $html .= "<option value='0'>".$arr["placeholder"]."</option>";
		$this->load->model("M_referensi_list","ml");
		$rs = $this->ml->get_many_by("M_referensi_id", $arr["refid"]);		
		foreach($rs as $r)
		{
			$selected = "";
			if(isset($arr["selected"]))
			{
				if($arr["selected"]==$r->M_referensi_id)
					$selected = 'selected';	
			}
			$html .= "<option ".$selected." value='".$r->M_referensi_list_id."'>".$r->name."</option>";	
		}
		$html .= "</select>";	
		
		return $html;
	}

	function get_report($query,$filename)
	{
    	$this->load->dbutil();
    	$this->load->helper('file');
		$this->load->helper('download');
		/* get the object   */
		//$report = $this->pers->get_all();
		$report = $this->db->query($query);
		//print_r($report);
		/*  pass it to db utility function  */
		$new_report = $this->dbutil->csv_from_result($report);
		/*  Now use it to write file. write_file helper function will do it */
		//$export = write_file('./asset/uploads/test.csv',$new_report);

		/*  Now use it to download file. force_download helper function will do it */
		$export = force_download($filename.'.csv',$new_report);
		
		if($export) echo json_encode(array("success" => true));
		else echo json_encode(array("success" => false));
		/*  Done    */
	}


	
	
}